#ifndef DECLARATIONS_H
#define DECLARATIONS_H

#include <memory>

namespace cpp
{
    namespace io
    {
        class BufferedWriter;
        using JBufferedWriter = std::shared_ptr<BufferedWriter>;

        class Closeable;
        using JCloseable = std::shared_ptr<Closeable>;
        
        class Console;
        using JConsole = std::shared_ptr<Console>;

        class File;
        using JFile = std::shared_ptr<File>;

        class FileDescriptor;
        using JFileDescriptor = std::shared_ptr<FileDescriptor>;

        class FileOutputStream;
        using JFileOutputStream = std::shared_ptr<FileOutputStream>;

        class FilterOutputStream;
        using JFilterOutputStream = std::shared_ptr<FilterOutputStream>;

        class Flushable;
        using JFlushable = std::shared_ptr<Flushable>;
        
        class InputStream;
        using JInputStream = std::shared_ptr<InputStream>;

        class OutputStream;
        using JOutputStream = std::shared_ptr<OutputStream>;

        class OutputStreamWriter;
        using JOutputStreamWriter = std::shared_ptr<OutputStreamWriter>;

        class PrintStream;
        using JPrintStream = std::shared_ptr<PrintStream>;

        class Writer;
        using JWriter = std::shared_ptr<Writer>;
    }

    namespace lang
    {
        class AbstractStringBuilder;
        using JAbstractStringBuilder = std::shared_ptr<AbstractStringBuilder>;

        class Appendable;
        using JAppendable = std::shared_ptr<Appendable>;

        class AutoCloseable;
        using JAutoCloseable = std::shared_ptr<AutoCloseable>;

        class Boolean;
        using JBoolean = std::shared_ptr<Boolean>;

        class Byte;
        using JByte = std::shared_ptr<Byte>;

        class Character;
        using JCharacter = std::shared_ptr<Character>;

        class CharacterData;
        using JCharacterData = std::shared_ptr<CharacterData>;

        class CharacterData00;
        using JCharacterData00 = std::shared_ptr<CharacterData00>;

        class CharacterData01;
        using JCharacterData01 = std::shared_ptr<CharacterData01>;

        class CharacterData02;
        using JCharacterData02 = std::shared_ptr<CharacterData02>;

        class CharacterData0E;
        using JCharacterData0E = std::shared_ptr<CharacterData0E>;

        class CharacterDataLatin1;
        using JCharacterDataLatin1 = std::shared_ptr<CharacterDataLatin1>;

        class CharacterDataPrivateUse;
        using JCharacterDataPrivateUse = std::shared_ptr<CharacterDataPrivateUse>;

        class CharacterDataUndefined;
        using JCharacterDataUndefined = std::shared_ptr<CharacterDataUndefined>;

        class CharSequence;
        using JCharSequence = std::shared_ptr<CharSequence>;

        class Class;
        using JClass = std::shared_ptr<Class>;

        class ClassLoader;
        using JClassLoader = std::shared_ptr<ClassLoader>;

        class Double;
        using JDouble = std::shared_ptr<Double>;

        class Float;
        using JFloat = std::shared_ptr<Float>;

        class Integer;
        using JInteger = std::shared_ptr<Integer>;

        template <typename T>
        class Iterable;
        template <typename T>
        using JIterable = std::shared_ptr<Iterable<T>>;

        template <typename T>
        class jarray;
        template <typename T>
        using JArray = std::shared_ptr<jarray<T>>;

        class Long;
        using JLong = std::shared_ptr<Long>;

        class Object;
        using JObject = std::shared_ptr<Object>;

        class Package;
        using JPackage = std::shared_ptr<Package>;

        class PostConstructible;
        using JPostConstructible = std::shared_ptr<PostConstructible>;

        class Runnable;
        using JRunnable = std::shared_ptr<Runnable>;
        
        class RuntimePermission;
        using JRuntimePermission = std::shared_ptr<RuntimePermission>;

        class SecurityManager;
        using JSecurityManager = std::shared_ptr<SecurityManager>;

        class Short;
        using JShort = std::shared_ptr<Short>;

        class String;
        using JString = std::shared_ptr<String>;

        class StringBuffer;
        using JStringBuffer = std::shared_ptr<StringBuffer>;

        class StringBuilder;
        using JStringBuilder = std::shared_ptr<StringBuilder>;

        class Thread;
        using JThread = std::shared_ptr<Thread>;

        class ThreadGroup;
        using JThreadGroup = std::shared_ptr<ThreadGroup>;

        class Void;
        using JVoid = std::shared_ptr<Void>;

        namespace annotation
        {
            class Annotation;
            using JAnnotation = std::shared_ptr<Annotation>;
        }

        namespace reflect
        {
            class Type;
            using JType = std::shared_ptr<Type>;
            
            class Member;
            using JMember = std::shared_ptr<Member>;
            
            class Method;
            using JMethod = std::shared_ptr<Method>;
            
            class AnnotatedElement;
            using JAnnotatedElement = std::shared_ptr<AnnotatedElement>;

            namespace builder
            {
                class ClassBuilder;
                using JClassBuilder = std::shared_ptr<ClassBuilder>;

                class PackageBuilder;
                using JPackageBuilder = std::shared_ptr<PackageBuilder>;
            }
        }
    }

    namespace nio
    {
        namespace channels
        {
            class FileChannel;
            using JFileChannel = std::shared_ptr<FileChannel>;
        }

        namespace charset
        {
            class Charset;
            using JCharset = std::shared_ptr<Charset>;

            class CharsetEncoder;
            using JCharsetEncoder = std::shared_ptr<CharsetEncoder>;
        }
    }

    namespace security
    {
        class AccessControlContext;
        using JAccessControlContext = std::shared_ptr<AccessControlContext>;
        
        class AllPermission;
        using JAllPermission = std::shared_ptr<AllPermission>;
        
        template <typename T>
        class PrivilegedAction;
        template <typename T>
        using JPrivilegedAction = std::shared_ptr<PrivilegedAction<T>>;
        
        class ProtectionDomain;
        using JProtectionDomain = std::shared_ptr<ProtectionDomain>;
        
        class Permission;
        using JPermission = std::shared_ptr<Permission>;
    }

    namespace util
    {
        template <typename E>
        class AbstractCollection;
        template <typename E>
        using JAbstractCollection = std::shared_ptr<AbstractCollection<E>>;

        template <typename E>
        class AbstractList;
        template <typename E>
        using JAbstractList = std::shared_ptr<AbstractList<E>>;

        template <typename E>
        class ArrayList;
        template <typename E>
        using JArrayList = std::shared_ptr<ArrayList<E>>;

        template <typename E>
        class Collection;
        template <typename E>
        using JCollection = std::shared_ptr<Collection<E>>;

        template <typename T>
        class Comparator;
        template <typename T>
        using JComparator = std::shared_ptr<Comparator<T>>;
        
        template<typename K, typename V>
        class Dictionary;
        template<typename K, typename V>
        using JDictionary = std::shared_ptr<Dictionary<K, V>>;
        
        template<typename E>
        class Enumeration;
        template<typename E>
        using JEnumeration = std::shared_ptr<Enumeration<E>>;

        template <typename K, typename V>
        class HashMap;
        template <typename K, typename V>
        using JHashMap = std::shared_ptr<HashMap<K, V>>;
        
        template <typename K, typename V>
        class Hashtable;
        template <typename K, typename V>
        using JHashtable = std::shared_ptr<Hashtable<K, V>>;

        template <typename E>
        class Iterator;
        template <typename E>
        using JIterator = std::shared_ptr<Iterator<E>>;

        template <typename E>
        class List;
        template <typename E>
        using JList = std::shared_ptr<List<E>>;

        template <typename E>
        class ListIterator;
        template <typename E>
        using JListIterator = std::shared_ptr<ListIterator<E>>;

        class Locale;
        using JLocale = std::shared_ptr<Locale>;

        template <typename K, typename V>
        class Map;
        template <typename K, typename V>
        using JMap = std::shared_ptr<Map<K, V>>;

        class RandomAccess;
        using JRandomAccess = std::shared_ptr<RandomAccess>;

        template <typename E>
        class Set;
        template <typename E>
        using JSet = std::shared_ptr<Set<E>>;
        
        class Properties;
        using JProperties = std::shared_ptr<Properties>;

        namespace regex
        {
            class Matcher;
            using JMatcher = std::shared_ptr<Matcher>;

            class MatchResult;
            using JMatchResult = std::shared_ptr<MatchResult>;

            class Pattern;
            using JPattern = std::shared_ptr<Pattern>;
        }
    }
}

namespace sun
{
    namespace misc
    {
        class FDBigInteger;
        using JFDBigInteger = std::shared_ptr<FDBigInteger>;

        class FloatingDecimal;
        using JFloatingDecimal = std::shared_ptr<FloatingDecimal>;
        
        class JavaIOAccess;
        using JJavaIOAccess = std::shared_ptr<JavaIOAccess>;
        
        class Unsafe;
        using JUnsafe = std::shared_ptr<Unsafe>;
    }

    namespace nio
    {
        namespace ch
        {
            class Interruptible;
            using JInterruptible = std::shared_ptr<Interruptible>;
        }

        namespace cs
        {
            class StreamEncoder;
            using JStreamEncoder = std::shared_ptr<StreamEncoder>;
        }
    }

    namespace reflect
    {
        class Reflection;
        using JReflection = std::shared_ptr<Reflection>;
    }

    namespace security
    {
        namespace util
        {
            class SecurityConstants;
            using JSecurityConstants = std::shared_ptr<SecurityConstants>;
        }
    }
}

#endif // DECLARATIONS_H
