#ifndef PRIMITIVE_DATA_TYPES_H
#define PRIMITIVE_DATA_TYPES_H

typedef char jbyte;
typedef short jshort;
typedef int jint;
typedef long long jlong;
typedef float jfloat;
typedef double jdouble;
typedef char16_t jchar;
typedef bool jboolean;

typedef unsigned char jubyte;
typedef unsigned short jushort;
typedef unsigned int juint;
typedef unsigned long long julong;

#endif
