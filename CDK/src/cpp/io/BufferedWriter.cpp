#include "BufferedWriter.h"
#include "../lang/System.h"
#include "../lang/IllegalArgumentException.h"
#include "../lang/IndexOutOfBoundsException.h"

using namespace cpp::lang;

namespace cpp
{
    namespace io
    {
        BufferedWriter::BufferedWriter(JWriter out) : BufferedWriter(out, defaultCharBufferSize)
        {
            
        }
        
        BufferedWriter::BufferedWriter(JWriter out, jint sz) : Writer(out)
        {
            if (sz <= 0)
                throw IllegalArgumentException(jstr(u"Buffer size <= 0"));
            this->out = out;
            cb = jnew_array<jchar>(sz);
            nChars = sz;
            nextChar = 0;

            //TODO: lineSeparator = java.security.AccessController.doPrivileged(new sun.security.action.GetPropertyAction("line.separator"));
        }
        
        void BufferedWriter::close()
        {
            jsynchronized(lock);
            
            if (out == nullptr) {
                return;
            }
            
            try {
                flushBuffer();
            } catch(...) {}
            
            out->close(); //TODO: validar se realmente es necesario
            
            out = nullptr;
            cb = nullptr;
        }
        
        void BufferedWriter::flush()
        {
            jsynchronized(lock);
            
            flushBuffer();
            out->flush();
        }
        
        void BufferedWriter::newLine()
        {
            write(lineSeparator);
        }
        
        void BufferedWriter::write(jarray<jchar> cbuf, jint off, jint len)
        {
            jsynchronized(lock);
            
            ensureOpen();
            if ((off < 0) || (off > cbuf.length()) || (len < 0) ||
                ((off + len) > cbuf.length()) || ((off + len) < 0)) {
                throw IndexOutOfBoundsException();
            } else if (len == 0) {
                return;
            }

            if (len >= nChars) {
                flushBuffer();
                out->write(cbuf, off, len);
                return;
            }

            jint b = off, t = off + len;
            while (b < t) {
                jint d = min(nChars - nextChar, t - b);
                System::arraycopy(cbuf, b, cb, nextChar, d);
                b += d;
                nextChar += d;
                if (nextChar >= nChars)
                    flushBuffer();
            }
        }
        
        void BufferedWriter::write(jint c)
        {
            ensureOpen();
            if (nextChar >= nChars)
                flushBuffer();
            cb[nextChar++] = (jchar) c;
        }
        
        void BufferedWriter::write(JString s, jint off, jint len)
        {
            jsynchronized(lock);
            
            ensureOpen();

            jint b = off, t = off + len;
            while (b < t) {
                jint d = min(nChars - nextChar, t - b);
                s->getChars(b, b + d, cb, nextChar);
                b += d;
                nextChar += d;
                if (nextChar >= nChars)
                    flushBuffer();
            }
        }
        
        void BufferedWriter::ensureOpen()
        {
            
        }
        
        void BufferedWriter::flushBuffer()
        {
            
        }
        
        jint BufferedWriter::min(jint a, jint b)
        {
            return -1;
        }
        
        jint BufferedWriter::defaultCharBufferSize = 8192;
    }
}
