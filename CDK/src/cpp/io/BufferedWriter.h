#ifndef BUFFEREDWRITER_H
#define BUFFEREDWRITER_H

#include "Writer.h"

namespace cpp
{
    namespace io
    {
        class BufferedWriter : public Writer
        {
        public:
            using Writer::write;
                        
            BufferedWriter(JWriter out);
            BufferedWriter(JWriter out, jint sz);
            virtual void close();
            virtual void flush();
            virtual void newLine();
            virtual void write(jarray<jchar> cbuf, jint off, jint len);
            virtual void write(jint c);
            virtual void write(cpp::lang::JString s, jint off, jint len);
            
        private:
            void ensureOpen();
            void flushBuffer();
            jint min(jint a, jint b);
            
            JWriter out;
            jarray<jchar> cb;
            jint nChars;
            jint nextChar;
            static jint defaultCharBufferSize;
            cpp::lang::JString lineSeparator;
        };
    }
}

#endif // BUFFEREDWRITER_H
