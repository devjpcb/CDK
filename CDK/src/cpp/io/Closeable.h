#ifndef CPP_IO_CLOSEABLE_H
#define CPP_IO_CLOSEABLE_H

#include "../lang/AutoCloseable.h"

namespace cpp
{
    namespace io
    {
        class Closeable : public virtual cpp::lang::AutoCloseable
        {
        public:
            virtual void close() = 0;
        };
    }
}

#endif // CPP_IO_CLOSEABLE_H
