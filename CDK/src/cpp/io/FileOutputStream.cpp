#include "FileOutputStream.h"

using namespace cpp::lang;
using namespace cpp::nio::channels;

namespace cpp
{
    namespace io
    {
        FileOutputStream::FileOutputStream(JFile file)
        {
            
        }
        
        FileOutputStream::FileOutputStream(JFile file, jboolean append)
        {
            
        }
        
        FileOutputStream::FileOutputStream(JFileDescriptor fdObj)
        {
            
        }
        
        FileOutputStream::FileOutputStream(JString name)
        {
            
        }
        
        FileOutputStream::FileOutputStream(JString name, jboolean append)
        {
            
        }
        
        void FileOutputStream::close()
        {
            
        }
        
        void FileOutputStream::finalize()
        {
            
        }
        
        JFileChannel FileOutputStream::getChannel()
        {
            return nullptr;
        }
        
        JFileDescriptor FileOutputStream::getFD()
        {
            return nullptr;
        }
        
        void FileOutputStream::write(jarray<jbyte> b)
        {
            
        }
        
        void FileOutputStream::write(jarray<jbyte> b, jint off, jint len)
        {
            
        }
        
        void FileOutputStream::write(jint b)
        {
            
        }
    }
}
