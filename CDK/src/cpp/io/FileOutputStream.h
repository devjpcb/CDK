#ifndef CPP_IO_FILEOUTPUTSTREAM_H
#define CPP_IO_FILEOUTPUTSTREAM_H

#include "OutputStream.h"

namespace cpp
{
    namespace io
    {
        class FileOutputStream : public OutputStream
        {
        public:
            FileOutputStream(JFile file);
            FileOutputStream(JFile file, jboolean append);
            FileOutputStream(JFileDescriptor fdObj);
            FileOutputStream(cpp::lang::JString name);
            FileOutputStream(cpp::lang::JString name, jboolean append);
            void close();
            cpp::nio::channels::JFileChannel getChannel();
            JFileDescriptor getFD();
            void write(jarray<jbyte> b);
            void write(jarray<jbyte> b, jint off, jint len);
            void write(jint b);
            
        protected:
            void finalize();
            
            template <typename T, typename... Args>
            friend std::shared_ptr<T> cpp::lang::jnew(Args&&... args);
        };
    }
}

#endif // CPP_IO_FILEOUTPUTSTREAM_H
