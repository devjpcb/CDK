#include "FilterOutputStream.h"
#include "../lang/IndexOutOfBoundsException.h"

using namespace cpp::lang;

namespace cpp
{
    namespace io
    {
        FilterOutputStream::FilterOutputStream(JOutputStream out) : out(out)
        {
        }
        
        void FilterOutputStream::close()
        {
            flush();
        }
        
        void FilterOutputStream::flush()
        {
            out->flush();
        }
        
        void FilterOutputStream::write(jarray<jbyte> b)
        {
            write(b, 0, b.length());
        }
        
        void FilterOutputStream::write(jarray<jbyte> b, jint off, jint len)
        {
            if((off | len | (b.length() - (len + off)) | (off + len)) < 0)
                throw IndexOutOfBoundsException();
            
            for(int i = 0 ; i < len ; i++) {
                write(b[off + i]);
            }
        }
        
        void FilterOutputStream::write(jint b)
        {
             out->write(b);
        }
    }
}
