#ifndef FILTEROUTPUTSTREAM_H
#define FILTEROUTPUTSTREAM_H

#include "OutputStream.h"

namespace cpp
{
    namespace io
    {
        class FilterOutputStream : public OutputStream
        {
        public:
            FilterOutputStream(JOutputStream out);
            virtual void close();
            virtual void flush();
            virtual void write(jarray<jbyte> b);
            virtual void write(jarray<jbyte> b, jint off, jint len);
            virtual void write(jint b);
        
        protected:
            JOutputStream out;
        };
    }
}

#endif // FILTEROUTPUTSTREAM_H
