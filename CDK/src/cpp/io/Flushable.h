#ifndef CPP_IO_FLUSHABLE_H
#define CPP_IO_FLUSHABLE_H

#include "../lang/Object.h"

namespace cpp
{
    namespace io
    {
        class Flushable : public virtual cpp::lang::Object
        {
        public:
            virtual void flush() = 0;
        };
    }
}

#endif // CPP_IO_FLUSHABLE_H
