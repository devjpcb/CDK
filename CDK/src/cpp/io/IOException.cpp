#include "IOException.h"

using namespace cpp::lang;

namespace cpp
{
    namespace io
    {
        IOException::IOException() : Exception()
        {
            
        }
        
        IOException::IOException(JString message) : Exception(message)
        {
            
        }
        
        IOException::IOException(JString message, Throwable& cause) : Exception(message, cause)
        {
            
        }
        
        IOException::IOException(Throwable& cause) : Exception(cause)
        {
            
        }
    }
}
