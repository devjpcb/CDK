#ifndef CPP_IO_IOEXCEPTION_H
#define CPP_IO_IOEXCEPTION_H

#include "../lang/Exception.h"
#include "../lang/Throwable.h"

namespace cpp
{
    namespace io
    {
        class IOException : cpp::lang::Exception
        {
        public:
            IOException();
            IOException(cpp::lang::JString message);
            IOException(cpp::lang::JString message, cpp::lang::Throwable& cause);
            IOException(cpp::lang::Throwable& cause);
        };
    }
}

#endif // CPP_IO_IOEXCEPTION_H
