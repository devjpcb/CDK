#include "OutputStream.h"
#include "../lang/NullPointerException.h"
#include "../lang/IndexOutOfBoundsException.h"

using namespace cpp::lang;

namespace cpp
{
    namespace io
    {
        void OutputStream::close()
        {
            
        }
        
        void OutputStream::flush()
        {
            
        }
        
        void OutputStream::write(jarray<jbyte> b)
        {
            write(b, 0, b.length());
        }
        
        void OutputStream::write(jarray<jbyte> b, jint off, jint len)
        {
            if(b == nullptr)
            {
                throw NullPointerException();
            }
            else if((off < 0) || (off > b.length()) || (len < 0) ||
                    ((off + len) > b.length()) || ((off + len) < 0))
            {
                throw IndexOutOfBoundsException();
            }
            else if(len == 0)
            {
                return;
            }
            for(jint i = 0 ; i < len ; i++)
            {
                write(b[off + i]);
            }
        }
    }
}
