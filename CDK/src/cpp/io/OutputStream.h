#ifndef CPP_IO_OUTPUTSTREAM_H
#define CPP_IO_OUTPUTSTREAM_H

#include "Closeable.h"
#include "Flushable.h"
#include "../lang/Array.h"

namespace cpp
{
    namespace io
    {
        class OutputStream : public Closeable, public Flushable
        {
        public:
            virtual void close();
            virtual void flush();
            virtual void write(jarray<jbyte> b);
            virtual void write(jarray<jbyte> b, jint off, jint len);
            virtual void write(jint b) = 0;
        };
    }
}

#endif // CPP_IO_OUTPUTSTREAM_H
