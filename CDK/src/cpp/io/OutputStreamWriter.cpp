#include "OutputStreamWriter.h"
#include "OutputStream.h"
#include "../lang/NullPointerException.h"
#include "../../sun/nio/cs/StreamEncoder.h"

using namespace cpp::nio;
using namespace cpp::nio::charset;
using namespace cpp::lang;
using namespace sun::nio::cs;

namespace cpp
{
    namespace io
    {
        OutputStreamWriter::OutputStreamWriter(JOutputStream out)
        {

        }

        OutputStreamWriter::OutputStreamWriter(JOutputStream out, JCharset cs)
        {

        }

        OutputStreamWriter::OutputStreamWriter(JOutputStream out, JCharsetEncoder enc)
        {

        }

        OutputStreamWriter::OutputStreamWriter(JOutputStream out, JString charsetName) : Writer(out)
        {
            if (charsetName == nullptr)
                throw NullPointerException(jstr(u"charsetName"));
            se = StreamEncoder::forOutputStreamWriter(out, shared_from_this(), charsetName);
        }

        void OutputStreamWriter::close()
        {

        }

        void OutputStreamWriter::flush()
        {

        }

        JString OutputStreamWriter::getEncoding()
        {
            return nullptr;
        }

        void OutputStreamWriter::write(jarray<jchar> cbuf, jint off, jint len)
        {

        }

        void OutputStreamWriter::write(jint c)
        {

        }

        void OutputStreamWriter::write(JString str, jint off, jint len)
        {

        }
    }
}
