#ifndef OUTPUTSTREAMWRITER_H
#define OUTPUTSTREAMWRITER_H

#include "Writer.h"

namespace cpp
{
    namespace io
    {
        class OutputStreamWriter : public Writer
        {
        public:
            OutputStreamWriter(JOutputStream out);
            OutputStreamWriter(JOutputStream out, cpp::nio::charset::JCharset cs);
            OutputStreamWriter(JOutputStream out, cpp::nio::charset::JCharsetEncoder enc);
            OutputStreamWriter(JOutputStream out, cpp::lang::JString charsetName);
            virtual void close();
            virtual void flush();
            virtual cpp::lang::JString getEncoding();
            virtual void write(jarray<jchar> cbuf, jint off, jint len);
            virtual void write(jint c);
            virtual void write(cpp::lang::JString str, jint off, jint len);
            
        private:
            sun::nio::cs::JStreamEncoder se;
        };
    }
}

#endif // OUTPUTSTREAMWRITER_H
