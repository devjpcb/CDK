#include "PrintStream.h"
#include "File.h"
#include "FileOutputStream.h"
#include "OutputStreamWriter.h"
#include "BufferedWriter.h"
#include "IOException.h"

using namespace cpp::lang;
using namespace cpp::nio::charset;
using namespace cpp::util;

namespace cpp
{
    namespace io
    {
        PrintStream::PrintStream(JFile file) : PrintStream(false, jnew<FileOutputStream>(file)) 
        {

        }

        PrintStream::PrintStream(JFile file, JString csn) : PrintStream(false, toCharset(csn), jnew<FileOutputStream>(file))
        {

        }

        PrintStream::PrintStream(JOutputStream out) : PrintStream(out, false)
        {

        }

        PrintStream::PrintStream(JOutputStream out, jboolean autoFlush) : PrintStream(autoFlush, requireNonNull(out, jstr(u"Null output stream")))
        {

        }

        PrintStream::PrintStream(JOutputStream out, jboolean autoFlush, JString encoding) : PrintStream(autoFlush, requireNonNull(out, jstr(u"Null output stream")), toCharset(encoding))
        {

        }

        PrintStream::PrintStream(JString fileName) : PrintStream(false, std::dynamic_pointer_cast<OutputStream>(jnew<FileOutputStream>(fileName)))
        {

        }

        PrintStream::PrintStream(JString fileName, JString csn) : PrintStream(false, toCharset(csn), std::dynamic_pointer_cast<OutputStream>(jnew<FileOutputStream>(fileName)))
        {

        }
        
        PrintStream::PrintStream(jboolean autoFlush, JOutputStream out) : FilterOutputStream(out), autoFlush(autoFlush), charset(nullptr), trouble(false), closing(false)
        {
        }
        
        PrintStream::PrintStream(jboolean autoFlush, JOutputStream out, JCharset charset) : FilterOutputStream(out), autoFlush(autoFlush), charset(charset), trouble(false), closing(false)
        {
        }
        
        PrintStream::PrintStream(jboolean autoFlush, JCharset charset, JOutputStream out) : PrintStream(autoFlush, out, charset)
        {
        }
        
        void PrintStream::postConstruct()
        {
            if (charset != nullptr) {
                charOut = jnew<OutputStreamWriter>(shared_from_this(), charset);
            } else {
                charOut = jnew<OutputStreamWriter>(shared_from_this());
            }
            
            textOut = jnew<BufferedWriter>(charOut);
        }

        JPrintStream PrintStream::append(jchar c)
        {
            print(c);
            return shared_from_this();
        }

        JPrintStream PrintStream::append(JCharSequence csq)
        {
            if (csq == nullptr)
                print(jstr(u"null"));
            else
                print(csq->toString());
            return shared_from_this();
        }

        JPrintStream PrintStream::append(JCharSequence csq, jint start, jint end)
        {
            /* TODO:
            JCharSequence cs = (csq == nullptr ? jstr(u"null") : csq);
            write(cs->subSequence(start, end)->toString());
            return shared_from_this();
            */
            return nullptr;
        }

        jboolean PrintStream::checkError()
        {
            if (out != nullptr)
                flush();
                
            if (jinstanceof<PrintStream>(out)) {
                JPrintStream ps = std::dynamic_pointer_cast<PrintStream>(out);
                return ps->checkError();
            }
            
            return trouble;
        }

        void PrintStream::clearError()
        {
            trouble = false;
        }

        void PrintStream::close()
        {
            jsynchronized(this);
            
            if (!closing) {
                closing = true;
                try {
                    textOut->close();
                    out->close();
                }
                catch (IOException x) {
                    trouble = true;
                }
                textOut = nullptr;
                charOut = nullptr;
                out = nullptr;
            }
        }

        void PrintStream::flush()
        {
            jsynchronized(this);
            
            try {
                ensureOpen();
                out->flush();
            }
            catch (IOException x) {
                trouble = true;
            }
        }

        JPrintStream PrintStream::format(JLocale l, JString format, jarray<JObject> args)
        {
            /* TODO:
            try {
                jsynchronized(this);
                ensureOpen();
                if ((formatter == nullptr)|| (formatter->locale() != l))
                    formatter = jnew<Formatter>(shared_from_this(), l);
                formatter->format(l, format, args);
            } catch (InterruptedIOException x) {
                Thread::currentThread()->interrupt();
            } catch (IOException x) {
                trouble = true;
            }
            return shared_from_this();
            */
           return nullptr;
        }

        JPrintStream PrintStream::format(JString format, jarray<JObject> args)
        {
            return nullptr;
        }

        void PrintStream::print(jboolean b)
        {

        }

        void PrintStream::print(jchar c)
        {

        }

        void PrintStream::print(jarray<jchar> s)
        {

        }

        void PrintStream::print(jdouble d)
        {

        }

        void PrintStream::print(jfloat f)
        {

        }

        void PrintStream::print(jint i)
        {

        }

        void PrintStream::print(jlong l)
        {

        }

        void PrintStream::print(JObject obj)
        {

        }

        void PrintStream::print(JString s)
        {

        }

        JPrintStream PrintStream::printf(JLocale l, JString format, jarray<JObject> args)
        {
            return nullptr;
        }

        JPrintStream PrintStream::printf(JString format, jarray<JObject> args)
        {
            return nullptr;
        }

        void PrintStream::println()
        {

        }

        void PrintStream::println(jboolean x)
        {

        }

        void PrintStream::println(jchar x)
        {

        }

        void PrintStream::println(jarray<jchar> x)
        {

        }

        void PrintStream::println(jdouble x)
        {

        }

        void PrintStream::println(jfloat x)
        {

        }

        void PrintStream::println(jint x)
        {

        }

        void PrintStream::println(jlong x)
        {

        }

        void PrintStream::println(JObject x)
        {

        }

        void PrintStream::println(JString x)
        {

        }

        void PrintStream::setError()
        {
            trouble = true;
        }

        void PrintStream::write(jarray<jbyte> buf, jint off, jint len)
        {

        }

        void PrintStream::write(jint b)
        {

        }
        
        JCharset PrintStream::toCharset(JString csn)
        {
            return nullptr;
        }
        
        void PrintStream::ensureOpen() 
        {
            if (out == nullptr)
                throw IOException(jstr(u"Stream closed"));
        }
    }
}
