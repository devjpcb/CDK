#ifndef CPP_IO_PRINTSTREAM_H
#define CPP_IO_PRINTSTREAM_H

#include "FilterOutputStream.h"
#include "../lang/PostConstructible.h"
#include "../lang/NullPointerException.h"
#include <mutex>

namespace cpp
{
    namespace io
    {
        class PrintStream : public FilterOutputStream, public cpp::lang::PostConstructible, public std::enable_shared_from_this<PrintStream>
        {
        public:
            PrintStream(JFile file);
            PrintStream(JFile file, cpp::lang::JString csn);
            PrintStream(JOutputStream out);
            PrintStream(JOutputStream out, jboolean autoFlush);
            PrintStream(JOutputStream out, jboolean autoFlush, cpp::lang::JString encoding);
            PrintStream(cpp::lang::JString fileName);
            PrintStream(cpp::lang::JString fileName, cpp::lang::JString csn);
            virtual void postConstruct();
            virtual JPrintStream append(jchar c);
            virtual JPrintStream append(cpp::lang::JCharSequence csq);
            virtual JPrintStream append(cpp::lang::JCharSequence csq, jint start, jint end);
            virtual jboolean checkError();
            virtual void close();
            virtual void flush();
            virtual JPrintStream format(cpp::util::JLocale l, cpp::lang::JString format, jarray<cpp::lang::JObject> args);
            virtual JPrintStream format(cpp::lang::JString format, jarray<cpp::lang::JObject> args);
            virtual void print(jboolean b);
            virtual void print(jchar c);
            virtual void print(jarray<jchar> s);
            virtual void print(jdouble d);
            virtual void print(jfloat f);;
            virtual void print(jint i);
            virtual void print(jlong l);
            virtual void print(cpp::lang::JObject obj);
            virtual void print(cpp::lang::JString s);
            virtual JPrintStream printf(cpp::util::JLocale l, cpp::lang::JString format, jarray<cpp::lang::JObject> args);
            virtual JPrintStream printf(cpp::lang::JString format, jarray<cpp::lang::JObject> args);
            virtual void println();
            virtual void println(jboolean x);
            virtual void println(jchar x);
            virtual void println(jarray<jchar> x);
            virtual void println(jdouble x);
            virtual void println(jfloat x);
            virtual void println(jint x);
            virtual void println(jlong x);
            virtual void println(cpp::lang::JObject x);
            virtual void println(cpp::lang::JString x);
            virtual void write(jarray<jbyte> buf, jint off, jint len);
            virtual void write(jint b);
            
        protected:
            virtual void clearError();
            virtual void setError();
            
        private: 
            PrintStream(jboolean autoFlush, JOutputStream out);
            PrintStream(jboolean autoFlush, JOutputStream out, cpp::nio::charset::JCharset charset);
            PrintStream(jboolean autoFlush, cpp::nio::charset::JCharset charset, JOutputStream out);
            void ensureOpen();
            
            template<typename T>
            static T requireNonNull(T obj, cpp::lang::JString message) {
                if (obj == nullptr)
                    throw cpp::lang::NullPointerException(message);
                return obj;
            }
            
            static cpp::nio::charset::JCharset toCharset(cpp::lang::JString csn);
            
            jboolean autoFlush;
            cpp::nio::charset::JCharset charset;
            JBufferedWriter textOut;
            JOutputStreamWriter charOut;
            jboolean trouble;
            jboolean closing;
        };
    }
}

#endif // CPP_IO_PRINTSTREAM_H
