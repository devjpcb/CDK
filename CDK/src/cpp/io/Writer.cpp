#include "Writer.h"
#include "../lang/NullPointerException.h"

using namespace cpp::lang;

namespace cpp
{
    namespace io
    {
        Writer::Writer()
        {
        }

        Writer::Writer(JObject lock)
        {
            if(lock == nullptr) {
                throw NullPointerException();
            }
            
            this->lock = lock;
        }
        
        void Writer::postConstruct()
        {
            if(lock == nullptr) {
                lock = shared_from_this();
            }
        }

        JAppendable Writer::append(jchar c)
        {
            write(c);
            return shared_from_this();
        }

        JAppendable Writer::append(JCharSequence csq)
        {
            if (csq == nullptr)
                write(jstr(u"null"));
            else
                write(csq->toString());
            return shared_from_this();
        }

        JAppendable Writer::append(JCharSequence csq, jint start, jint end)
        {
            JCharSequence cs = (csq == nullptr ? jstr(u"null") : csq);
            write(cs->subSequence(start, end)->toString());
            return shared_from_this();
        }

        void Writer::write(jarray<jchar> cbuf)
        {
            write(cbuf, 0, cbuf.length());
        }

        void Writer::write(jint c)
        {
            jsynchronized(lock);
            if (writeBuffer == nullptr){
                writeBuffer = jnew_array<jchar>(WRITE_BUFFER_SIZE);
            }
            writeBuffer[0] = static_cast<jchar>(c);
            write(writeBuffer, 0, 1);
        }

        void Writer::write(JString str)
        {
            write(str, 0, str->length());
        }

        void Writer::write(JString str, jint off, jint len)
        {
            jsynchronized(lock);
            jarray<jchar> cbuf;
            if (len <= WRITE_BUFFER_SIZE) {
                if (writeBuffer == nullptr) {
                    writeBuffer = jnew_array<jchar>(WRITE_BUFFER_SIZE);
                }
                cbuf = writeBuffer;
            } else {
                cbuf = jnew_array<jchar>(len);
            }
            str->getChars(off, (off + len), cbuf, 0);
            write(cbuf, 0, len);
        }

        const jint Writer::WRITE_BUFFER_SIZE = 1024;
    }
}
