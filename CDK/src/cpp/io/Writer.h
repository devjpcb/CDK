#ifndef WRITER_H
#define WRITER_H

#include "../lang/Appendable.h"
#include "../lang/Array.h"
#include "Closeable.h"
#include "Flushable.h"
#include <mutex>

namespace cpp
{
    namespace io
    {
        class Writer : public cpp::lang::Appendable, public Closeable, public Flushable, public cpp::lang::PostConstructible, public std::enable_shared_from_this<Writer>
        {
        public:
            virtual void postConstruct();
            virtual cpp::lang::JAppendable append(jchar c);
            virtual cpp::lang::JAppendable append(cpp::lang::JCharSequence csq);
            virtual cpp::lang::JAppendable append(cpp::lang::JCharSequence csq, jint start, jint end);
            virtual void write(jarray<jchar> cbuf);
            virtual void write(jarray<jchar> cbuf, jint off, jint len) = 0;
            virtual void write(jint c);
            virtual void write(cpp::lang::JString str);
            virtual void write(cpp::lang::JString str, jint off, jint len);

        protected:
            Writer();
            Writer(cpp::lang::JObject lock);
            
            cpp::lang::JObject lock;

        private:
            jarray<jchar> writeBuffer;
            static const jint WRITE_BUFFER_SIZE;
        };
    }
}

#endif // WRITER_H
