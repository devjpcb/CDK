#include "AbstractStringBuilder.h"
#include "StringBuffer.h"
#include "StringBuilder.h"
#include "Integer.h"
#include "Long.h"
#include "Character.h"
#include "System.h"
#include "String.h"
#include "../util/Arrays.h"
#include "../../sun/misc/FloatingDecimal.h"
#include "OutOfMemoryError.h"
#include "StringIndexOutOfBoundsException.h"
#include "IllegalArgumentException.h"

using namespace cpp::util;
using namespace sun::misc;

namespace cpp
{
    namespace lang
    {
        AbstractStringBuilder::AbstractStringBuilder() : count(0)
        {
        }

        AbstractStringBuilder::AbstractStringBuilder(jint capacity) : value(capacity), count(0)
        {
        }

        AbstractStringBuilder::~AbstractStringBuilder()
        {
        }

        jint AbstractStringBuilder::length()
        {
            return count;
        }

        jint AbstractStringBuilder::capacity()
        {
            return value.length();
        }

        void AbstractStringBuilder::ensureCapacity(jint minimumCapacity)
        {
            if(minimumCapacity > 0)
                ensureCapacityInternal(minimumCapacity);
        }

        void AbstractStringBuilder::trimToSize()
        {
            if(count < value.length()) {
                value = Arrays::copyOf(value, count);
            }
        }

        void AbstractStringBuilder::setLength(jint newLength)
        {
            if(newLength < 0)
                throw StringIndexOutOfBoundsException(newLength);
            ensureCapacityInternal(newLength);

            if(count < newLength) {
                Arrays::fill(value, count, newLength, u'\0');
            }

            count = newLength;
        }

        jchar AbstractStringBuilder::charAt(jint index)
        {
            if((index < 0) || (index >= count))
                throw StringIndexOutOfBoundsException(index);
            return value[index];
        }

        jint AbstractStringBuilder::codePointAt(jint index)
        {
            if((index < 0) || (index >= count)) {
                throw StringIndexOutOfBoundsException(index);
            }
            return Character::codePointAtImpl(value, index, count);
        }

        jint AbstractStringBuilder::codePointBefore(jint index)
        {
            jint i = index - 1;
            if((i < 0) || (i >= count)) {
                throw StringIndexOutOfBoundsException(index);
            }
            return Character::codePointBeforeImpl(value, index, 0);
        }

        jint AbstractStringBuilder::codePointCount(jint beginIndex, jint endIndex)
        {
            if(beginIndex < 0 || endIndex > count || beginIndex > endIndex) {
                throw IndexOutOfBoundsException();
            }
            return Character::codePointCountImpl(value, beginIndex, endIndex - beginIndex);
        }

        jint AbstractStringBuilder::offsetByCodePoints(jint index, jint codePointOffset)
        {
            if(index < 0 || index > count) {
                throw IndexOutOfBoundsException();
            }
            return Character::offsetByCodePointsImpl(value, 0, count, index, codePointOffset);
        }

        void AbstractStringBuilder::getChars(jint srcBegin, jint srcEnd, jarray<jchar> dst, jint dstBegin)
        {
            if(srcBegin < 0)
                throw StringIndexOutOfBoundsException(srcBegin);
            if((srcEnd < 0) || (srcEnd > count))
                throw StringIndexOutOfBoundsException(srcEnd);
            if(srcBegin > srcEnd)
                throw StringIndexOutOfBoundsException(jstr(u"srcBegin > srcEnd"));
            System::arraycopy(value, srcBegin, dst, dstBegin, srcEnd - srcBegin);
        }

        void AbstractStringBuilder::setCharAt(jint index, jchar ch)
        {
            if((index < 0) || (index >= count))
                throw StringIndexOutOfBoundsException(index);
            value[index] = ch;
        }

        JAbstractStringBuilder AbstractStringBuilder::append(JObject obj)
        {
            return append(String::valueOf(obj));
        }

        JAbstractStringBuilder AbstractStringBuilder::append(JString str)
        {
            if(str == nullptr)
                return appendNull();
            jint len = str->length();
            ensureCapacityInternal(count + len);
            str->getChars(0, len, value, count);
            count += len;
            return shared_from_this();
        }

        JAbstractStringBuilder AbstractStringBuilder::append(JStringBuffer sb)
        {
            if(sb == nullptr)
                return appendNull();
            jint len = sb->length();
            ensureCapacityInternal(count + len);
            sb->getChars(0, len, value, count);
            count += len;
            return shared_from_this();
        }
        
        JAbstractStringBuilder AbstractStringBuilder::append(JStringBuilder sb)
        {
           if(sb == nullptr)
                return appendNull();
            jint len = sb->length();
            ensureCapacityInternal(count + len);
            sb->getChars(0, len, value, count);
            count += len;
            return shared_from_this(); 
        }

        JAbstractStringBuilder AbstractStringBuilder::append(JAbstractStringBuilder asb)
        {
            if(asb == nullptr)
                return appendNull();
            int len = asb->length();
            ensureCapacityInternal(count + len);
            asb->getChars(0, len, value, count);
            count += len;
            return shared_from_this();
        }

        JAppendable AbstractStringBuilder::append(JCharSequence s)
        {
            if(s == nullptr)
                return appendNull();

            JString str = std::dynamic_pointer_cast<String>(s);
            if(str != nullptr)
                return this->append(str);

            JAbstractStringBuilder strBuilder = std::dynamic_pointer_cast<AbstractStringBuilder>(s);
            if(strBuilder != nullptr)
                return this->append(strBuilder);

            return this->append(s, 0, s->length());
        }

        JAppendable AbstractStringBuilder::append(JCharSequence s, jint start, jint end)
        {
            if(s == nullptr)
                s = jstr(u"null");
            if((start < 0) || (start > end) || (end > s->length()))
                throw IndexOutOfBoundsException(jstr(u"start ") + boxing(start) + jstr(u", end ") + boxing(end) + jstr(u", s.length() ") + boxing(s->length()));
            jint len = end - start;
            ensureCapacityInternal(count + len);
            for(jint i = start, j = count; i < end; i++, j++)
                value[j] = s->charAt(i);
            count += len;
            return shared_from_this();
        }

        JAbstractStringBuilder AbstractStringBuilder::append(jarray<jchar> str)
        {
            jint len = str.length();
            ensureCapacityInternal(count + len);
            System::arraycopy(str, 0, value, count, len);
            count += len;
            return shared_from_this();
        }

        JAbstractStringBuilder AbstractStringBuilder::append(jarray<jchar> str, jint offset, jint len)
        {
            if(len > 0)                 // let arraycopy report AIOOBE for len < 0
                ensureCapacityInternal(count + len);
            System::arraycopy(str, offset, value, count, len);
            count += len;
            return shared_from_this();
        }

        JAbstractStringBuilder AbstractStringBuilder::append(jboolean b)
        {
            if(b) {
                ensureCapacityInternal(count + 4);
                value[count++] = u't';
                value[count++] = u'r';
                value[count++] = u'u';
                value[count++] = u'e';
            }
            else {
                ensureCapacityInternal(count + 5);
                value[count++] = u'f';
                value[count++] = u'a';
                value[count++] = u'l';
                value[count++] = u's';
                value[count++] = u'e';
            }

            return shared_from_this();
        }

        JAppendable AbstractStringBuilder::append(jchar c)
        {
            ensureCapacityInternal(count + 1);
            value[count++] = c;
            return shared_from_this();
        }

        JAbstractStringBuilder AbstractStringBuilder::append(jint i)
        {
            if(i == Integer::MIN_VALUE) {
                append(u"-2147483648");
                return shared_from_this();
            }
            jint appendedLength = (i < 0) ? Integer::stringSize(-i) + 1 : Integer::stringSize(i);
            jint spaceNeeded = count + appendedLength;
            ensureCapacityInternal(spaceNeeded);
            Integer::getChars(i, spaceNeeded, value);
            count = spaceNeeded;
            return shared_from_this();
        }

        JAbstractStringBuilder AbstractStringBuilder::append(jlong l)
        {
            if(l == Long::MIN_VALUE) {
                append(u"-9223372036854775808");
                return shared_from_this();
            }
            jint appendedLength = (l < 0) ? Long::stringSize(-l) + 1 : Long::stringSize(l);
            jint spaceNeeded = count + appendedLength;
            ensureCapacityInternal(spaceNeeded);
            Long::getChars(l, spaceNeeded, value);
            count = spaceNeeded;
            return shared_from_this();
        }
        
        JAbstractStringBuilder AbstractStringBuilder::append(jfloat f)
        {
            FloatingDecimal::appendTo(f, shared_from_this());
            return shared_from_this();
        }
        
        JAbstractStringBuilder AbstractStringBuilder::append(jdouble d)
        {
            FloatingDecimal::appendTo(d, shared_from_this());
            return shared_from_this();
        }

        JAbstractStringBuilder AbstractStringBuilder::delete_(jint start, jint end)
        {
            if(start < 0)
                throw StringIndexOutOfBoundsException(start);
            if(end > count)
                end = count;
            if(start > end)
                throw StringIndexOutOfBoundsException();
            jint len = end - start;
            if(len > 0) {
                System::arraycopy(value, start + len, value, start, count - end);
                count -= len;
            }
            return shared_from_this();
        }

        JAbstractStringBuilder AbstractStringBuilder::appendCodePoint(jint codePoint)
        {
            const jint count = this->count;

            if(Character::isBmpCodePoint(codePoint)) {
                ensureCapacityInternal(count + 1);
                value[count] = (jchar) codePoint;
                this->count = count + 1;
            }
            else if(Character::isValidCodePoint(codePoint)) {
                ensureCapacityInternal(count + 2);
                Character::toSurrogates(codePoint, value, count);
                this->count = count + 2;
            }
            else {
                throw IllegalArgumentException();
            }

            return shared_from_this();
        }

        JAbstractStringBuilder AbstractStringBuilder::deleteCharAt(jint index)
        {
            if((index < 0) || (index >= count))
                throw StringIndexOutOfBoundsException(index);
            System::arraycopy(value, index + 1, value, index, count - index - 1);
            count--;
            return shared_from_this();
        }

        JAbstractStringBuilder AbstractStringBuilder::replace(jint start, jint end, JString str)
        {
            if(start < 0)
                throw StringIndexOutOfBoundsException(start);
            if(start > count)
                throw StringIndexOutOfBoundsException(jstr(u"start > length()"));
            if(start > end)
                throw StringIndexOutOfBoundsException(jstr(u"start > end"));

            if(end > count)
                end = count;
            jint len = str->length();
            jint newCount = count + len - (end - start);
            ensureCapacityInternal(newCount);

            System::arraycopy(value, end, value, start + len, count - end);
            str->getChars(value, start);
            count = newCount;
            return shared_from_this();
        }

        JString AbstractStringBuilder::substring(jint start)
        {
            return substring(start, count);
        }

        JCharSequence AbstractStringBuilder::subSequence(jint start, jint end)
        {
            return substring(start, count);
        }

        JString AbstractStringBuilder::substring(jint start, jint end)
        {
            if(start < 0)
                throw StringIndexOutOfBoundsException(start);
            if(end > count)
                throw StringIndexOutOfBoundsException(end);
            if(start > end)
                throw StringIndexOutOfBoundsException(end - start);
            return jnew<String>(value, start, end - start);
        }

        JAbstractStringBuilder AbstractStringBuilder::insert(jint index, jarray<jchar> str, jint offset, jint len)
        {
            if((index < 0) || (index > length()))
                throw StringIndexOutOfBoundsException(index);
            if((offset < 0) || (len < 0) || (offset > str.length() - len))
                throw StringIndexOutOfBoundsException(jstr(u"offset ") + boxing(offset) + jstr(u", len ") + boxing(len) + jstr(u", str.length ") + boxing(str.length()));
            ensureCapacityInternal(count + len);
            System::arraycopy(value, index, value, index + len, count - index);
            System::arraycopy(str, offset, value, index, len);
            count += len;
            return shared_from_this();
        }

        JAbstractStringBuilder AbstractStringBuilder::insert(jint offset, JObject obj)
        {
            return insert(offset, String::valueOf(obj));
        }

        JAbstractStringBuilder AbstractStringBuilder::insert(jint offset, JString str)
        {
            if((offset < 0) || (offset > length()))
                throw StringIndexOutOfBoundsException(offset);
            //if(str == nullptr)
            //str = u"null";
            jint len = str->length();
            ensureCapacityInternal(count + len);
            System::arraycopy(value, offset, value, offset + len, count - offset);
            str->getChars(value, offset);
            count += len;
            return shared_from_this();
        }

        JAbstractStringBuilder AbstractStringBuilder::insert(jint offset, jarray<jchar> str)
        {
            if((offset < 0) || (offset > length()))
                throw StringIndexOutOfBoundsException(offset);
            jint len = str.length();
            ensureCapacityInternal(count + len);
            System::arraycopy(value, offset, value, offset + len, count - offset);
            System::arraycopy(str, 0, value, offset, len);
            count += len;
            return shared_from_this();
        }

        JAbstractStringBuilder AbstractStringBuilder::insert(jint dstOffset, JCharSequence s)
        {
            if(s == nullptr)
                s = jstr(u"null");

            JString str = std::dynamic_pointer_cast<String>(s);
            if(str != nullptr)
                return this->insert(dstOffset, str);
            return this->insert(dstOffset, s, 0, s->length());
        }

        JAbstractStringBuilder AbstractStringBuilder::insert(jint dstOffset, JCharSequence s, jint start, jint end)
        {
            if(s == nullptr)
                s = jstr(u"null");
            if((dstOffset < 0) || (dstOffset > this->length()))
                throw IndexOutOfBoundsException(jstr(u"dstOffset ") + boxing(dstOffset));
            if((start < 0) || (end < 0) || (start > end) || (end > s->length()))
                throw IndexOutOfBoundsException(jstr(u"start ") + boxing(start) + jstr(u", end ") + boxing(end) + jstr(u", s.length() ") + boxing(s->length()));
            int len = end - start;
            ensureCapacityInternal(count + len);
            System::arraycopy(value, dstOffset, value, dstOffset + len, count - dstOffset);
            for(jint i = start; i < end; i++)
                value[dstOffset++] = s->charAt(i);
            count += len;
            return shared_from_this();
        }

        JAbstractStringBuilder AbstractStringBuilder::insert(jint offset, jboolean b)
        {
            return insert(offset, String::valueOf(b));
        }

        JAbstractStringBuilder AbstractStringBuilder::insert(jint offset, jchar c)
        {
            ensureCapacityInternal(count + 1);
            System::arraycopy(value, offset, value, offset + 1, count - offset);
            value[offset] = c;
            count += 1;
            return shared_from_this();
        }

        JAbstractStringBuilder AbstractStringBuilder::insert(jint offset, jint i)
        {
            return insert(offset, String::valueOf(i));
        }

        JAbstractStringBuilder AbstractStringBuilder::insert(jint offset, jlong l)
        {
            return insert(offset, String::valueOf(l));
        }
        
        JAbstractStringBuilder AbstractStringBuilder::insert(jint offset, jfloat f)
        {
            return insert(offset, String::valueOf(f));
        }
        
        JAbstractStringBuilder AbstractStringBuilder::insert(jint offset, jdouble d)
        {
            return insert(offset, String::valueOf(d));
        }

        jint AbstractStringBuilder::indexOf(JString str)
        {
            return indexOf(str, 0);
        }

        jint AbstractStringBuilder::indexOf(JString str, jint fromIndex)
        {
            return String::indexOf(value, 0, count, str, fromIndex);
        }

        jint AbstractStringBuilder::lastIndexOf(JString str)
        {
            return lastIndexOf(str, count);
        }

        jint AbstractStringBuilder::lastIndexOf(JString str, jint fromIndex)
        {
            return String::lastIndexOf(value, 0, count, str, fromIndex);
        }

        JAbstractStringBuilder AbstractStringBuilder::reverse()
        {
            jboolean hasSurrogates = false;
            jint n = count - 1;
            for(jint j = (n - 1) >> 1; j >= 0; j--) {
                jint k = n - j;
                jchar cj = value[j];
                jchar ck = value[k];
                value[j] = ck;
                value[k] = cj;
                if(Character::isSurrogate(cj) || Character::isSurrogate(ck)) {
                    hasSurrogates = true;
                }
            }
            if(hasSurrogates) {
                reverseAllValidSurrogatePairs();
            }
            return shared_from_this();
        }

        jarray<jchar> AbstractStringBuilder::getValue()
        {
            return value;
        }

        void AbstractStringBuilder::ensureCapacityInternal(jint minimumCapacity)
        {
            // overflow-conscious code
            if(minimumCapacity - value.length() > 0)
                expandCapacity(minimumCapacity);
        }

        void AbstractStringBuilder::expandCapacity(jint minimumCapacity)
        {
            jint newCapacity = value.length() * 2 + 2;
            if(newCapacity - minimumCapacity < 0)
                newCapacity = minimumCapacity;
            if(newCapacity < 0) {
                if(minimumCapacity < 0)  // overflow
                    throw OutOfMemoryError();
                newCapacity = Integer::MAX_VALUE;
            }

            value = Arrays::copyOf(value, newCapacity);
        }

        JAbstractStringBuilder AbstractStringBuilder::appendNull()
        {
            jint c = count;
            ensureCapacityInternal(c + 4);
            jarray<jchar> value = this->value;
            value[c++] = 'n';
            value[c++] = 'u';
            value[c++] = 'l';
            value[c++] = 'l';
            count = c;
            return shared_from_this();
        }

        void AbstractStringBuilder::reverseAllValidSurrogatePairs()
        {
            for(jint i = 0; i < count - 1; i++) {
                jchar c2 = value[i];
                if(Character::isLowSurrogate(c2)) {
                    jchar c1 = value[i + 1];
                    if(Character::isHighSurrogate(c1)) {
                        value[i++] = c1;
                        value[i] = c2;
                    }
                }
            }
        }
    }
}
