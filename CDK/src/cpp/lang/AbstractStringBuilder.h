#ifndef CPP_LANG_ABSTRACTSTRINGBUILDER_H
#define CPP_LANG_ABSTRACTSTRINGBUILDER_H

#include "Object.h"
#include "Array.h"
#include "CharSequence.h"
#include "Appendable.h"

namespace cpp
{
    namespace lang
    {
        class AbstractStringBuilder : public Appendable, public CharSequence, public std::enable_shared_from_this<AbstractStringBuilder>
        {
        public:
            AbstractStringBuilder();
            AbstractStringBuilder(jint capacity);
            virtual ~AbstractStringBuilder();

            virtual jint length();
            virtual jint capacity();
            virtual void ensureCapacity(jint minimumCapacity);
            virtual void trimToSize();
            virtual void setLength(jint newLength);
            virtual jchar charAt(jint index);
            virtual jint codePointAt(jint index);
            virtual jint codePointBefore(jint index);
            virtual jint codePointCount(jint beginIndex, jint endIndex);
            virtual jint offsetByCodePoints(jint index, jint codePointOffset);
            virtual void getChars(jint srcBegin, jint srcEnd, jarray<jchar> dst, jint dstBegin);
            virtual void setCharAt(jint index, jchar ch);
            virtual JAbstractStringBuilder append(JObject obj);
            virtual JAbstractStringBuilder append(JString str);
            virtual JAbstractStringBuilder append(JStringBuffer sb);
            virtual JAbstractStringBuilder append(JStringBuilder sb);
            virtual JAbstractStringBuilder append(JAbstractStringBuilder asb);
            virtual JAppendable append(JCharSequence s);
            virtual JAppendable append(JCharSequence s, jint start, jint end);
            virtual JAbstractStringBuilder append(jarray<jchar> str);
            virtual JAbstractStringBuilder append(jarray<jchar> str, jint offset, jint len);
            virtual JAbstractStringBuilder append(jboolean b);
            virtual JAppendable append(jchar c);
            virtual JAbstractStringBuilder append(jint i);
            virtual JAbstractStringBuilder append(jlong l);
            virtual JAbstractStringBuilder append(jfloat f);
            virtual JAbstractStringBuilder append(jdouble d);
            virtual JAbstractStringBuilder delete_(jint start, jint end);
            virtual JAbstractStringBuilder appendCodePoint(jint codePoint);
            virtual JAbstractStringBuilder deleteCharAt(jint index);
            virtual JAbstractStringBuilder replace(jint start, jint end, JString str);
            virtual JString substring(jint start);
            virtual JCharSequence subSequence(jint start, jint end);
            virtual JString substring(jint start, jint end);
            virtual JAbstractStringBuilder insert(jint index, jarray<jchar> str, jint offset, jint len);
            virtual JAbstractStringBuilder insert(jint offset, JObject obj);
            virtual JAbstractStringBuilder insert(jint offset, JString str);
            virtual JAbstractStringBuilder insert(jint offset, jarray<jchar> str);
            virtual JAbstractStringBuilder insert(jint dstOffset, JCharSequence s);
            virtual JAbstractStringBuilder insert(jint dstOffset, JCharSequence s, jint start, jint end);
            virtual JAbstractStringBuilder insert(jint offset, jboolean b);
            virtual JAbstractStringBuilder insert(jint offset, jchar c);
            virtual JAbstractStringBuilder insert(jint offset, jint i);
            virtual JAbstractStringBuilder insert(jint offset, jlong l);
            virtual JAbstractStringBuilder insert(jint offset, jfloat f);
            virtual JAbstractStringBuilder insert(jint offset, jdouble d);
            virtual jint indexOf(JString str);
            virtual jint indexOf(JString str, jint fromIndex);
            virtual jint lastIndexOf(JString str);
            virtual jint lastIndexOf(JString str, jint fromIndex);
            virtual JAbstractStringBuilder reverse();
            virtual JString toString() = 0;
            virtual jarray<jchar> getValue();

        protected:
            void ensureCapacityInternal(jint minimumCapacity);
            void expandCapacity(jint minimumCapacity);
            JAbstractStringBuilder appendNull();
            void reverseAllValidSurrogatePairs();
            
            jarray<jchar> value;
            jint count;
        };
    }
}

#endif // CPP_LANG_ABSTRACTSTRINGBUILDER_H
