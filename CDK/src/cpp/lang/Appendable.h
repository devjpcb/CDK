#ifndef CPP_LANG_APPENDABLE_H
#define CPP_LANG_APPENDABLE_H

#include "Object.h"

namespace cpp
{
    namespace lang
    {
        class Appendable : public virtual Object
        {
        public:
            virtual JAppendable append(JCharSequence csq) = 0;
            virtual JAppendable append(JCharSequence csq, jint start, jint end) = 0;
            virtual JAppendable append(jchar c) = 0;
        };
    }
}

#endif // CPP_LANG_APPENDABLE_H
