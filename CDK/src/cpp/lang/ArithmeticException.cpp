#include "ArithmeticException.h"

namespace cpp
{
    namespace lang
    {
        ArithmeticException::ArithmeticException() : RuntimeException()
        {
        }
        
        ArithmeticException::ArithmeticException(JString s) : RuntimeException(s)
        {
            
        }

        ArithmeticException::~ArithmeticException()
        {
        }
    }
}
