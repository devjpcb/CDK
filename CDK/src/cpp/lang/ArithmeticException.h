#ifndef CPP_LANG_ARITHMETICEXCEPTION_H
#define CPP_LANG_ARITHMETICEXCEPTION_H

#include "RuntimeException.h"

namespace cpp
{
    namespace lang
    {
        class ArithmeticException : public RuntimeException
        {
        public:
            ArithmeticException();
            ArithmeticException(JString s);
            ~ArithmeticException();
        };
    }
}

#endif // CPP_LANG_ARITHMETICEXCEPTION_H
