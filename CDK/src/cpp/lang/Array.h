#ifndef CPP_LANG_JARRAY_H
#define CPP_LANG_JARRAY_H

#include "Object.h"
#include "Boxing.h"
#include <initializer_list>
#include <vector>
#include <array>
#include <memory>

namespace cpp
{
    namespace lang
    {
        template <typename T>
        class jarray : public Object
        {
        public:
            jarray() { }

            explicit jarray(const jint& size) {
                data = std::make_shared<internal_data>(size);
            }

            jarray(const jarray<T>& v) {
                data = v.data;
            }

            jarray(std::initializer_list<T> v) : jarray(static_cast<jint>(v.size())) {
                if (data != nullptr) {
                    for(jint index = 0; index < data->length; index++) {
                        data->value[index] = *(v.begin() + index);
                    }
                }
            }

            jarray(const std::vector<T>& v) : jarray(static_cast<jint>(v.size())) {
                if (data != nullptr) {
                    for(jint index = 0; index < data->length; index++) {
                        data->value[index] = v[index];
                    }
                }
            }

            template <std::size_t N> 
            jarray(const std::array<T, N>& v) : jarray(static_cast<jint>(v.size())) {
                if (data != nullptr) {
                    for(jint index = 0; index < data->length; index++) {
                        data->value[index] = v[index];
                    }
                }
            }

            template <std::size_t N>
            jarray(const T(&v)[N]) : jarray(static_cast<jint>(N)) {
                if (data != nullptr) {
                    for(jint index = 0; index < data->length; index++) {
                        data->value[index] = v[index];
                    }
                }
            }

            jarray(std::nullptr_t pnull) : jarray() {}

            const jint length() const {
                if(data == nullptr)
                    return 0;
                
                return data->length;
            }

            JObject clone() {
                if (data == nullptr)
                    return nullptr;

                jarray<T> copy(data->length);

                for(jint index = 0; index < data->length; index++) {
                    copy.data->value[index] = data->value[index];
                }

                return boxing(copy);
            }

            T* begin() {
                return data->value;
            }

            T* end() {
                return data->value + data->length;
            }

            const T* begin() const {
                return data->value;
            }

            const T* end() const {
                return data->value + data->length;
            }

            jarray<T>& operator = (const jarray<T>& v) {
                data = v.data;
                return *this;
            }

            T& operator [] (const jint& index) {
                return data->value[index];
            }

            const T& operator [] (const jint& index) const {
                return data->value[index];
            }

            jboolean operator == (std::nullptr_t pnull) {
                return (data == nullptr);
            }
            
            jboolean operator == (const jarray<T>& v) {
                return (data == v.data);
            }

            jboolean operator != (std::nullptr_t pnull) {
                return (data != nullptr);
            }
            
            jboolean operator != (const jarray<T>& v) {
                return (data != v.data);
            }

        private:    
            class internal_data {
            public:
                internal_data(int size) : length(size) {
                    const int fixed_size = size + 1;
                    value = new T[fixed_size];
                    std::memset(value, 0, fixed_size * sizeof(T));
                }

                ~internal_data() {
                    delete[] value;
                }

                T* value;
                const jint length;
            };

            std::shared_ptr<internal_data> data;
            
            template <typename NT> 
            friend jarray<typename std::conditional<std::is_class<NT>::value, std::shared_ptr<NT>, NT>::type> jnew_array(jint size);
        };

        template <>
        template <std::size_t N>
        jarray<jbyte>::jarray(const jbyte(&v)[N]) : jarray((static_cast<jint>(N) - 1))
        {
            if(data != nullptr) {
                for(jint index = 0; index < data->length; index++) {
                    data->value[index] = v[index];
                }
            }
        }

        template <>
        template <std::size_t N>
        jarray<jchar>::jarray(const jchar(&v)[N]) : jarray((static_cast<jint>(N) - 1))
        {
            if(data != nullptr) {
                for(jint index = 0; index < data->length; index++) {
                    data->value[index] = v[index];
                }
            }
        }

        template <typename T> 
        inline jarray<typename std::conditional<std::is_class<T>::value, std::shared_ptr<T>, T>::type> jnew_array(jint size)
        {
            jarray<typename std::conditional<std::is_class<T>::value, std::shared_ptr<T>, T>::type> ar(size);
            return ar;
        }
        
        template <typename D, typename O>
        inline jarray<std::shared_ptr<D>> jarray_dynamic_pointer_cast(jarray<std::shared_ptr<O>> o) 
        {
            const jint SIZE = o.length();
            jarray<std::shared_ptr<D>> d(SIZE);
            
            for(jint index = 0; index < SIZE; index++) {
                d[index] = std::dynamic_pointer_cast<D>(o[index]);
            }
            
            return d;
        }
    }
}

using cpp::lang::jarray;
using cpp::lang::jnew_array;
using cpp::lang::jarray_dynamic_pointer_cast;

#endif // CPP_LANG_JARRAY_H
