#include "ArrayIndexOutOfBoundsException.h"
#include "String.h"

namespace cpp
{
    namespace lang
    {
        ArrayIndexOutOfBoundsException::ArrayIndexOutOfBoundsException() : IndexOutOfBoundsException()
        {
        }

        ArrayIndexOutOfBoundsException::ArrayIndexOutOfBoundsException(jint index) : IndexOutOfBoundsException(jstr(u"Array index out of range: ") + boxing(index))
        {
        }
        
        ArrayIndexOutOfBoundsException::ArrayIndexOutOfBoundsException(JString s) : IndexOutOfBoundsException(s)
        {
        }

        ArrayIndexOutOfBoundsException::~ArrayIndexOutOfBoundsException()
        {
        }
    }
}
