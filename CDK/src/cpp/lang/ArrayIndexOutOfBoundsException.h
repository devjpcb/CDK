#ifndef CPP_LANG_ARRAYINDEXOUTOFBOUNDSEXCEPTION_H
#define CPP_LANG_ARRAYINDEXOUTOFBOUNDSEXCEPTION_H

#include "IndexOutOfBoundsException.h"

namespace cpp
{
    namespace lang
    {
        class ArrayIndexOutOfBoundsException : public IndexOutOfBoundsException
        {
        public:
            ArrayIndexOutOfBoundsException();
            ArrayIndexOutOfBoundsException(jint index);
            ArrayIndexOutOfBoundsException(JString s);
            ~ArrayIndexOutOfBoundsException();
        };
    }
}

#endif // CPP_LANG_ARRAYINDEXOUTOFBOUNDSEXCEPTION_H
