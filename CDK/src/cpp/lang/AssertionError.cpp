#include "AssertionError.h"
#include "String.h"

namespace cpp
{
    namespace lang
    {
        //TODO: Terminar de implementar cuando la clase String tenga todo los metodos de valueOf
        AssertionError::AssertionError() : Error()
        {
        }

        AssertionError::AssertionError(JString detailMessage) : Error(detailMessage)
        {
        }

        AssertionError::AssertionError(JObject detailMessage) : Error(String::valueOf(detailMessage))
        {
            // TODO:
            //if (detailMessage instanceof Throwable)
            //    initCause((Throwable) detailMessage);
        }

        AssertionError::AssertionError(jboolean detailMessage) : Error(String::valueOf(detailMessage))
        {
        }

        AssertionError::AssertionError(jchar detailMessage) : AssertionError(String::valueOf(detailMessage))
        {
        }

        AssertionError::AssertionError(jint detailMessage) : AssertionError(String::valueOf(detailMessage))
        {
        }

        AssertionError::AssertionError(jlong detailMessage) : AssertionError(String::valueOf(detailMessage))
        {
        }

        AssertionError::AssertionError(jfloat detailMessage) : AssertionError(String::valueOf(detailMessage))
        {
        }

        AssertionError::AssertionError(jdouble detailMessage) : AssertionError(String::valueOf(detailMessage))
        {
        }

        AssertionError::AssertionError(JString message, Throwable& cause) : Error(message, cause)
        {
        }
        
        AssertionError::~AssertionError()
        {
        }
    }
}
