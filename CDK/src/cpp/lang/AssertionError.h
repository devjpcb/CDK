#ifndef CPP_LANG_ASSERTIONERROR_H
#define CPP_LANG_ASSERTIONERROR_H

#include "Error.h"

namespace cpp
{
    namespace lang
    {
        class AssertionError : public Error
        {
        public:
            AssertionError();
            AssertionError(JObject detailMessage);
            AssertionError(jboolean detailMessage);
            AssertionError(jchar detailMessage);
            AssertionError(jint detailMessage);
            AssertionError(jlong detailMessage);
            AssertionError(jfloat detailMessage);
            AssertionError(jdouble detailMessage);
            AssertionError(JString message, Throwable& cause);
            ~AssertionError();
            
        private:
            AssertionError(JString detailMessage);
            
            friend class sun::misc::FloatingDecimal;
        };
    }
}

#endif // CPP_LANG_ASSERTIONERROR_H
