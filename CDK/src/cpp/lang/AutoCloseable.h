#ifndef AUTOCLOSEABLE_H
#define AUTOCLOSEABLE_H

#include "Object.h"

namespace cpp
{
    namespace lang
    {
        class AutoCloseable : public virtual Object
        {
        public:
            virtual void close() = 0;
        };
    }
}

#endif // AUTOCLOSEABLE_H
