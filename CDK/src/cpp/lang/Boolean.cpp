#include "Boolean.h"
#include "String.h"
#include "reflect/builder/ClassBuilder.h"
#include "reflect/builder/PackageBuilder.h"

using namespace cpp::lang::reflect::builder;

namespace cpp
{
    namespace lang
    {
        Boolean::Boolean() : value(false)
        {
        }

        Boolean::Boolean(jboolean pvalue) : value(pvalue)
        {
        }

        Boolean::Boolean(JString s) : Boolean(parseBoolean(s))
        {
        }

        Boolean::~Boolean()
        {
        }

        jboolean Boolean::booleanValue()
        {
            return value;
        }

        jint Boolean::compare(jboolean x, jboolean y)
        {
            return (x == y) ? 0 : (x ? 1 : -1);
        }

        jint Boolean::compareTo(JBoolean o)
        {
            return compare(value, o->value);
        }

        jboolean Boolean::equals(JObject obj)
        {
            JBoolean bobj = std::dynamic_pointer_cast<Boolean>(obj);
            if(bobj != nullptr) {
                return value == bobj->value;
            }

            return false;
        }

        jint Boolean::hashCode()
        {
            return hashCode(value);
        }

        jint Boolean::hashCode(jboolean pvalue)
        {
            return pvalue ? 1231 : 1237;
        }

        jboolean Boolean::logicalAnd(jboolean a, jboolean b)
        {
            return a && b;
        }

        jboolean Boolean::logicalOr(jboolean a, jboolean b)
        {
            return a || b;
        }

        jboolean Boolean::logicalXor(jboolean a, jboolean b)
        {
            return a ^ b;
        }

        jboolean Boolean::parseBoolean(JString s)
        {
            return ((s != nullptr) && s->equalsIgnoreCase(jstr(u"true")));
        }

        JString Boolean::toString()
        {
            return value ? jstr(u"true") : jstr(u"false");
        }

        JString Boolean::toString(jboolean b)
        {
            return b ? jstr(u"true") : jstr(u"false");
        }

        JBoolean Boolean::valueOf(jboolean b)
        {
            return b ? TRUE : FALSE;
        }

        JBoolean Boolean::valueOf(JString s)
        {
            return parseBoolean(s) ? TRUE : FALSE;
        }

        JClass Boolean::getClass()
        {
            return clazz;
        }

        const JBoolean Boolean::TRUE = jnew<Boolean>(true);
        const JBoolean Boolean::FALSE = jnew<Boolean>(false);
        
        JClass Boolean::clazz = ClassBuilder::create()
            ->setSimpleName(jstr(u"Boolean"))
            ->setPackage(
                PackageBuilder::create()
                    ->setName(jstr(u"cpp::lang"))
                    ->getPackage()
            )
            ->setModifiers(17)
            ->setFuncNewInstance([] { return jnew<Boolean>(); })
            ->getClass();
    }
}
