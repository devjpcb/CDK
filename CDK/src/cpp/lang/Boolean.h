  #ifndef CPP_LANG_BOOLEAN_H
#define CPP_LANG_BOOLEAN_H

#include "Object.h"
#include "Comparable.h"

namespace cpp
{
    namespace lang
    {
        class Boolean : public Comparable<JBoolean>
        {
        public:
            Boolean();
            Boolean(jboolean value);
            Boolean(JString s);
            ~Boolean();

            jboolean booleanValue();
            static jint compare(jboolean x, jboolean y);
            jint compareTo(JBoolean o);
            jboolean equals(JObject obj);
            jint hashCode();
            static jint hashCode(jboolean value);
            static jboolean logicalAnd(jboolean a, jboolean b);
            static jboolean logicalOr(jboolean a, jboolean b);
            static jboolean logicalXor(jboolean a, jboolean b);
            static jboolean parseBoolean(JString s);
            JString toString();
            static JString toString(jboolean b);
            static JBoolean valueOf(jboolean b);
            static JBoolean valueOf(JString s);
            virtual JClass getClass();

            static const JBoolean TRUE;
            static const JBoolean FALSE;
            
            static JClass clazz;
        private:
            jboolean value;
        };
    }
}

#endif // CPP_LANG_BOOLEAN_H
