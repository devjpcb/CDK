
#include "Byte.h"
#include "Short.h"
#include "Integer.h"
#include "Long.h"
#include "Float.h"
#include "Double.h"
#include "Character.h"
#include "Boolean.h"

namespace cpp
{
    namespace lang
    {
        JByte boxing(jbyte b)
        {
            return Byte::valueOf(b);
        }

        jbyte unboxing(JByte b)
        {
            return b->byteValue();
        }

        JShort boxing(jshort i)
        {
            return Short::valueOf(i);
        }

        jshort unboxing(JShort i)
        {
            return i->shortValue();
        }

        JInteger boxing(jint i)
        {
            return Integer::valueOf(i);
        }

        jint unboxing(JInteger i)
        {
            return i->intValue();
        }

        JLong boxing(jlong l)
        {
            return Long::valueOf(l);
        }

        jlong unboxing(JLong l)
        {
            return l->longValue();
        }
        
        JFloat boxing(jfloat f)
        {
            return Float::valueOf(f);
        }
        
        jfloat unboxing(JFloat f)
        {
            return f->floatValue();
        }
        
        JDouble boxing(jdouble d)
        {
            return Double::valueOf(d);
        }
        
        jdouble unboxing(JDouble d)
        {
            return d->doubleValue();
        }

        JCharacter boxing(jchar c)
        {
            return Character::valueOf(c);
        }

        jchar unboxing(JCharacter c)
        {
            return c->charValue();
        }

        JBoolean boxing(jboolean b)
        {
            return Boolean::valueOf(b);
        }

        jboolean unboxing(JBoolean b)
        {
            return b->booleanValue();
        }
    }
}
