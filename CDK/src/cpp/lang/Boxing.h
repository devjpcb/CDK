#ifndef CPP_LANG_BOXING_H
#define CPP_LANG_BOXING_H

#include "Operators.h"

namespace cpp
{
    namespace lang
    {
        JByte boxing(jbyte b);
        jbyte unboxing(JByte b);

        JShort boxing(jshort i);
        jshort unboxing(JShort i);

        JInteger boxing(jint i);
        jint unboxing(JInteger i);

        JLong boxing(jlong l);
        jlong unboxing(JLong l);
        
        JFloat boxing(jfloat f);
        jfloat unboxing(JFloat f);
        
        JDouble boxing(jdouble d);
        jdouble unboxing(JDouble d);

        JCharacter boxing(jchar c);
        jchar unboxing(JCharacter c);

        JBoolean boxing(jboolean b);
        jboolean unboxing(JBoolean b);
        
        template <typename T> 
        auto boxing(T value)
        {
            return boxing(value);
        }
        
        template <typename T>
        JArray<T> boxing(jarray<T>& a) 
        {
            return jnew<jarray<T>>(a);
        }
        
        template <typename T>
        jarray<T> unboxing(JArray<T> a)
        {
            return *a;
        }
        
        template <typename T>
        jarray<T> jarray_unboxing(JObject obj) 
        {
            return unboxing(std::static_pointer_cast<jarray<T>>(obj));
        }
    }
}

using cpp::lang::boxing;
using cpp::lang::unboxing;
using cpp::lang::jarray_unboxing;

#endif // CPP_LANG_BOXING_H
