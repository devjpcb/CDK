#include "Byte.h"
#include "String.h"
#include "Integer.h"
#include "Boxing.h"
#include "NumberFormatException.h"

namespace cpp
{
    namespace lang
    {
        Byte::Byte() : value(0)
        {
        }

        Byte::Byte(jbyte pvalue) : value(pvalue)
        {
        }

        Byte::Byte(JString s) : value(parseByte(s, 10))
        {
        }

        Byte::~Byte()
        {
        }

        jbyte Byte::byteValue()
        {
            return value;
        }

        jint Byte::compare(jbyte x, jbyte y)
        {
            return x - y;
        }

        jint Byte::compareTo(JByte anotherByte)
        {
            return compare(value, anotherByte->value);
        }

        JByte Byte::decode(JString nm)
        {
            jint i = unboxing(Integer::decode(nm));
            if(i < MIN_VALUE || i > MAX_VALUE)
                throw NumberFormatException(jstr(u"Value ") + boxing(i) + jstr(u" out of range from input ") + nm);
            return valueOf((jbyte)i);
        }

        jdouble Byte::doubleValue()
        {
            return value;
        }

        jboolean Byte::equals(JObject obj)
        {
            JByte bobj = std::dynamic_pointer_cast<Byte>(obj);
            if(bobj != nullptr) {
                return value == bobj->value;
            }

            return false;
        }

        jfloat Byte::floatValue()
        {
            return value;
        }

        jint Byte::hashCode()
        {
            return hashCode(value);
        }

        jint Byte::hashCode(jbyte value)
        {
            return value;
        }

        jint Byte::intValue()
        {
            return value;
        }

        jlong Byte::longValue()
        {
            return value;
        }

        jbyte Byte::parseByte(JString s)
        {
            return parseByte(s, 10);
        }

        jbyte Byte::parseByte(JString s, jint radix)
        {
            jint i = Integer::parseInt(s, radix);
            if(i < MIN_VALUE || i > MAX_VALUE)
                throw NumberFormatException(jstr(u"Value out of range. Value:\"") + s + jstr(u"\" Radix:") + boxing(radix));
            return (jbyte)i;
        }

        jshort Byte::shortValue()
        {
            return value;
        }

        JString Byte::toString()
        {
            return Integer::toString(static_cast<jint>(value));
        }

        JString Byte::toString(jbyte b)
        {
            return Integer::toString(static_cast<jint>(b), 10);
        }

        jint Byte::toUnsignedInt(jbyte x)
        {
            return x & 0xFF;
        }

        jlong Byte::toUnsignedLong(jbyte x)
        {
            return x & 0xFF;
        }

        JByte Byte::valueOf(jbyte b)
        {
            const jint offset = 128;
            return ByteCache::cache[static_cast<jint>(b + offset)];
        }

        JByte Byte::valueOf(JString s)
        {
            return valueOf(s, 10);
        }

        JByte Byte::valueOf(JString s, jint radix)
        {
            return valueOf(parseByte(s, radix));
        }

        const jbyte Byte::MIN_VALUE = -128;
        const jbyte Byte::MAX_VALUE = 127;
        const jint Byte::SIZE = 8;
        const jint Byte::BYTES = SIZE / Byte::SIZE;
        
        jarray<JByte> Byte::ByteCache::cache(-(-128) + 127 + 1);
        
        jboolean Byte::ByteCache::isStaticInitializationBlock = StaticInitializationBlock();

        jboolean Byte::ByteCache::StaticInitializationBlock()
        {
            for(jint i = 0; i < cache.length(); i++)
                cache[i] = jnew<Byte>(static_cast<jbyte>(i - 128));
            return true;
        }
    }
}
