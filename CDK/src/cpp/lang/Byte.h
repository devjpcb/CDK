#ifndef CPP_LANG_BYTE_H
#define CPP_LANG_BYTE_H

#include "Array.h"
#include "Number.h"
#include "Comparable.h"

namespace cpp
{
    namespace lang
    {
        class Byte : public Number, public Comparable<JByte>
        {
        public:
            Byte();
            Byte(jbyte pvalue);
            Byte(JString s);
            ~Byte();

            jbyte byteValue();
            static jint compare(jbyte x, jbyte y);
            jint compareTo(JByte anotherByte);
            static JByte decode(JString nm);
            jdouble doubleValue();
            jboolean equals(JObject obj);
            jfloat floatValue();
            jint hashCode();
            static jint hashCode(jbyte value);
            jint intValue();
            jlong longValue();
            static jbyte parseByte(JString s);
            static jbyte parseByte(JString s, jint radix);
            jshort shortValue();
            JString toString();
            static JString toString(jbyte b);
            static jint toUnsignedInt(jbyte x);
            static jlong toUnsignedLong(jbyte x);
            static JByte valueOf(jbyte b);
            static JByte valueOf(JString s);
            static JByte valueOf(JString s, jint radix);

            static const jbyte MIN_VALUE;
            static const jbyte MAX_VALUE;
            static const jint SIZE;
            static const jint BYTES;
            
        private:
            jbyte value;
            
            class ByteCache {
            public:
                static jarray<JByte> cache;
                
                static jboolean isStaticInitializationBlock;
                static jboolean StaticInitializationBlock();
            };
        };
    }
}

#endif // CPP_LANG_BYTE_H
