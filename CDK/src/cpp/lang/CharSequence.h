#ifndef CPP_LANG_CHARSEQUENCE_H
#define CPP_LANG_CHARSEQUENCE_H

#include "Object.h"

namespace cpp
{
    namespace lang
    {
        class CharSequence : public virtual Object
        {
        public:
            virtual jchar charAt(jint index) = 0;
            virtual jint length() = 0;
            virtual JCharSequence subSequence(jint start, jint end) = 0;
            virtual JString toString() = 0;
        };
    }
}

#endif // CPP_LANG_CHARSEQUENCE_H
