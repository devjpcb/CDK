#include "Character.h"
#include "CharacterData.h"
#include "IndexOutOfBoundsException.h"
#include "IllegalArgumentException.h"
#include "../util/HashMap.h"

namespace cpp
{
    namespace lang
    {
        Character::Character() : value(0)
        {
        }

        Character::Character(jchar pvalue) : value(pvalue)
        {
        }

        Character::~Character()
        {
        }

        jint Character::charCount(jint codePoint)
        {
            return codePoint >= 65536 ? 2 : 1;
        }

        jchar Character::charValue()
        {
            return value;
        }

        jint Character::codePointAt(jarray<jchar> a, jint index)
        {
            return codePointAtImpl(a, index, a.length());
        }

        jint Character::codePointAt(jarray<jchar> a, jint index, jint limit)
        {
            if(index >= limit || limit < 0 || limit > a.length()) {
                throw IndexOutOfBoundsException();
            }
            return codePointAtImpl(a, index, limit);
        }

        jint Character::codePointAt(JCharSequence seq, jint index)
        {
            jchar c1 = seq->charAt(index);
            if(isHighSurrogate(c1) && ++index < seq->length()) {
                jchar c2 = seq->charAt(index);
                if(isLowSurrogate(c2)) {
                    return toCodePoint(c1, c2);
                }
            }
            return c1;
        }

        jint Character::codePointBefore(jarray<jchar> a, jint index)
        {
            return codePointBeforeImpl(a, index, 0);
        }

        jint Character::codePointBefore(jarray<jchar> a, jint index, jint start)
        {
            if(index <= start || start < 0 || start >= a.length()) {
                throw IndexOutOfBoundsException();
            }
            return codePointBeforeImpl(a, index, start);
        }

        jint Character::codePointBefore(JCharSequence seq, jint index)
        {
            jchar c2 = seq->charAt(--index);
            if(isLowSurrogate(c2) && index > 0) {
                jchar c1 = seq->charAt(--index);
                if(isHighSurrogate(c1)) {
                    return toCodePoint(c1, c2);
                }
            }
            return c2;
        }

        jint Character::codePointCount(jarray<jchar> a, jint offset, jint count)
        {
            if(count > a.length() - offset || offset < 0 || count < 0) {
                throw IndexOutOfBoundsException();
            }
            return codePointCountImpl(a, offset, count);
        }

        jint Character::codePointCount(JCharSequence seq, jint beginIndex, jint endIndex)
        {
            jint length = seq->length();
            if(beginIndex < 0 || endIndex > length || beginIndex > endIndex) {
                throw IndexOutOfBoundsException();
            }
            jint n = endIndex - beginIndex;
            for(jint i = beginIndex; i < endIndex;) {
                if(isHighSurrogate(seq->charAt(i++)) && i < endIndex &&
                   isLowSurrogate(seq->charAt(i))) {
                    n--;
                    i++;
                }
            }
            return n;
        }

        jint Character::compare(jchar x, jchar y)
        {
            return x - y;
        }

        jint Character::compareTo(JCharacter anotherCharacter)
        {
            return compare(value, anotherCharacter->value);
        }

        jint Character::digit(jchar ch, jint radix)
        {
            return digit(static_cast<jint>(ch), radix);
        }

        jint Character::digit(jint codePoint, jint radix)
        {
            return CharacterData::of(codePoint)->digit(codePoint, radix);
        }

        jboolean Character::equals(JObject obj)
        {
            JCharacter cobj = std::dynamic_pointer_cast<Character>(obj);
            if(cobj != nullptr) {
                return value == cobj->value;
            }

            return false;
        }

        jchar Character::forDigit(jint digit, jint radix)
        {
            if((digit >= radix) || (digit < 0)) {
                return u'\0';
            }
            if((radix < Character::MIN_RADIX) || (radix > Character::MAX_RADIX)) {
                return u'\0';
            }
            if(digit < 10) {
                return  static_cast<jchar>(u'0' + digit);
            }
            return static_cast<jchar>(u'a' - 10 + digit);
        }

        jbyte Character::getDirectionality(jchar ch)
        {
            return getDirectionality(static_cast<jint>(ch));
        }

        jbyte Character::getDirectionality(jint codePoint)
        {
            return CharacterData::of(codePoint)->getDirectionality(codePoint);
        }
        
        jint Character::getNumericValue(jchar ch)
        {
            return getNumericValue(static_cast<jint>(ch));
        }
        
        jint Character::getNumericValue(jint codePoint)
        {
            return CharacterData::of(codePoint)->getNumericValue(codePoint);
        }
        
        jint Character::getType(jchar ch)
        {
            return getType(static_cast<int>(ch));
        }
        
        jint Character::getType(jint codePoint)
        {
            return CharacterData::of(codePoint)->getType(codePoint);
        }

        jint Character::hashCode()
        {
            return hashCode(value);
        }

        jint Character::hashCode(jchar value)
        {
            return value;
        }

        jchar Character::highSurrogate(jint codePoint)
        {
            return static_cast<jchar>((codePoint >> 10) + (MIN_HIGH_SURROGATE - (MIN_SUPPLEMENTARY_CODE_POINT >> 10)));
        }
        
        jboolean Character::isAlphabetic(jint codePoint)
        {
            return (((((1 << Character::UPPERCASE_LETTER) | (1 << Character::LOWERCASE_LETTER) | (1 << Character::TITLECASE_LETTER) | (1 << Character::MODIFIER_LETTER) 
                | (1 << Character::OTHER_LETTER) | (1 << Character::LETTER_NUMBER)) >> getType(codePoint)) & 1) != 0) || CharacterData::of(codePoint)->isOtherAlphabetic(codePoint);
        }

        jboolean Character::isBmpCodePoint(jint codePoint)
        {
            return codePoint >> 16 == 0;
        }
        
        jboolean Character::isDefined(jchar ch)
        {
            return isDefined(static_cast<jint>(ch));
        }
        
        jboolean Character::isDefined(jint codePoint)
        {
            return getType(codePoint) != Character::UNASSIGNED;
        }
        
        jboolean Character::isDigit(jchar ch)
        {
            return isDigit(static_cast<jint>(ch));
        }
        
        jboolean Character::isDigit(jint codePoint)
        {
            return getType(codePoint) == Character::DECIMAL_DIGIT_NUMBER;
        }

        jboolean Character::isHighSurrogate(jchar ch)
        {
            return ch >= MIN_HIGH_SURROGATE && ch < (MAX_HIGH_SURROGATE + 1);
        }
        
        jboolean Character::isIdentifierIgnorable(jchar ch)
        {
            return isIdentifierIgnorable(static_cast<jint>(ch));
        }
        
        jboolean Character::isIdentifierIgnorable(jint codePoint)
        {
            return CharacterData::of(codePoint)->isIdentifierIgnorable(codePoint);
        }
        
        jboolean Character::isIdeographic(jint codePoint)
        {
            return CharacterData::of(codePoint)->isIdeographic(codePoint);
        }
        
        jboolean Character::isISOControl(jchar ch)
        {
            return isISOControl(static_cast<jint>(ch));
        }
        
        jboolean Character::isISOControl(jint codePoint)
        {
            return codePoint <= 0x9F && (codePoint >= 0x7F || ((static_cast<juint>(codePoint) >> 5) == 0));
        }
        
        jboolean Character::isJavaIdentifierPart(jchar ch)
        {
            return isJavaIdentifierPart(static_cast<jint>(ch));
        }
        
        jboolean Character::isJavaIdentifierPart(jint codePoint)
        {
            return CharacterData::of(codePoint)->isJavaIdentifierPart(codePoint);
        }
        
        jboolean Character::isJavaIdentifierStart(jchar ch)
        {
            return isJavaIdentifierStart(static_cast<jint>(ch));
        }
        
        jboolean Character::isJavaIdentifierStart(jint codePoint)
        {
            return CharacterData::of(codePoint)->isJavaIdentifierStart(codePoint);
        }
        
        jboolean Character::isJavaLetter(jchar ch)
        {
            return isJavaIdentifierStart(ch);
        }
        
        jboolean Character::isJavaLetterOrDigit(jchar ch)
        {
            return isJavaIdentifierPart(ch);
        }
        
        jboolean Character::isLetter(jchar ch)
        {
            return isLetter(static_cast<jint>(ch));
        }
        
        jboolean Character::isLetter(jint codePoint)
        {
            return ((((1 << Character::UPPERCASE_LETTER) | (1 << Character::LOWERCASE_LETTER) | (1 << Character::TITLECASE_LETTER) | 
                      (1 << Character::MODIFIER_LETTER) | (1 << Character::OTHER_LETTER)) >> getType(codePoint)) & 1) != 0;
        }
        
        jboolean Character::isLetterOrDigit(jchar ch)
        {
            return isLetterOrDigit(static_cast<jint>(ch));
        }
        
        jboolean Character::isLetterOrDigit(jint codePoint)
        {
            return ((((1 << Character::UPPERCASE_LETTER) | (1 << Character::LOWERCASE_LETTER) | (1 << Character::TITLECASE_LETTER) | 
                      (1 << Character::MODIFIER_LETTER) | (1 << Character::OTHER_LETTER) | (1 << Character::DECIMAL_DIGIT_NUMBER)) >> getType(codePoint)) & 1) != 0;
        }
        
        jboolean Character::isLowerCase(jchar ch)
        {
            return isLowerCase(static_cast<jint>(ch));
        }
        
        jboolean Character::isLowerCase(jint codePoint)
        {
            return getType(codePoint) == Character::LOWERCASE_LETTER || CharacterData::of(codePoint)->isOtherLowercase(codePoint);
        }

        jboolean Character::isLowSurrogate(jchar ch)
        {
            return ch >= MIN_LOW_SURROGATE && ch < (MAX_LOW_SURROGATE + 1);
        }
        
        jboolean Character::isMirrored(jchar ch)
        {
            return isMirrored(static_cast<jint>(ch));
        }
        
        jboolean Character::isMirrored(jint codePoint)
        {
            return CharacterData::of(codePoint)->isMirrored(codePoint);
        }
        
        jboolean Character::isSpace(jchar ch)
        {
            return (ch <= 0x0020) && (((((1LL << 0x0009) | (1LL << 0x000A) | (1LL << 0x000C) | (1LL << 0x000D) | (1LL << 0x0020)) >> ch) & 1LL) != 0);
        }
        
        jboolean Character::isSpaceChar(jchar ch)
        {
            return isSpaceChar(static_cast<jint>(ch));
        }
        
        jboolean Character::isSpaceChar(jint codePoint)
        {
            return ((((1 << Character::SPACE_SEPARATOR) | (1 << Character::LINE_SEPARATOR) | (1 << Character::PARAGRAPH_SEPARATOR)) >> getType(codePoint)) & 1) != 0;
        }
        
        jboolean Character::isSupplementaryCodePoint(jint codePoint)
        {
            return codePoint >= MIN_SUPPLEMENTARY_CODE_POINT && codePoint <  MAX_CODE_POINT + 1;
        }

        jboolean Character::isSurrogate(jchar ch)
        {
            return ch >= MIN_SURROGATE && ch < (MAX_SURROGATE + 1);
        }
        
        jboolean Character::isSurrogatePair(jchar high, jchar low)
        {
            return isHighSurrogate(high) && isLowSurrogate(low);
        }
        
        jboolean Character::isTitleCase(jchar ch)
        {
            return isTitleCase(static_cast<jint>(ch));
        }
        
        jboolean Character::isTitleCase(jint codePoint)
        {
            return getType(codePoint) == Character::TITLECASE_LETTER;
        }
        
        jboolean Character::isUnicodeIdentifierPart(jchar ch)
        {
            return isUnicodeIdentifierPart(static_cast<jint>(ch));
        }
        
        jboolean Character::isUnicodeIdentifierPart(jint codePoint)
        {
            return CharacterData::of(codePoint)->isUnicodeIdentifierPart(codePoint);
        }
        
        jboolean Character::isUnicodeIdentifierStart(jchar ch)
        {
            return isUnicodeIdentifierPart(static_cast<jint>(ch));
        }
        
        jboolean Character::isUnicodeIdentifierStart(jint codePoint)
        {
            return CharacterData::of(codePoint)->isUnicodeIdentifierPart(codePoint);
        }
        
        jboolean Character::isUpperCase(jchar ch)
        {
            return isUpperCase(static_cast<jint>(ch));
        }
        
        jboolean Character::isUpperCase(jint codePoint)
        {
            return getType(codePoint) == Character::UPPERCASE_LETTER || CharacterData::of(codePoint)->isOtherUppercase(codePoint);
        }

        jboolean Character::isValidCodePoint(jint codePoint)
        {
            jint plane = codePoint >> 16;
            return plane < ((MAX_CODE_POINT + 1) >> 16);
        }
        
        jboolean Character::isWhitespace(jchar ch)
        {
            return isWhitespace(static_cast<jint>(ch));
        }
        
        jboolean Character::isWhitespace(jint codePoint)
        {
            return CharacterData::of(codePoint)->isWhitespace(codePoint);
        }

        jchar Character::lowSurrogate(jint codePoint)
        {
            return static_cast<jchar>((codePoint & 0x3ff) + MIN_LOW_SURROGATE);
        }

        jint Character::offsetByCodePoints(jarray<jchar> a, jint start, jint count, jint index, jint codePointOffset)
        {
            if(count > a.length() - start || start < 0 || count < 0 || index < start || index > start + count) {
                throw IndexOutOfBoundsException();
            }
            return offsetByCodePointsImpl(a, start, count, index, codePointOffset);
        }
        
        jint Character::offsetByCodePoints(JCharSequence seq, jint index, jint codePointOffset)
        {
            jint length = seq->length();
            
            if (index < 0 || index > length) {
                throw IndexOutOfBoundsException();
            }

            jint x = index;
            if (codePointOffset >= 0) {
                jint i;
                for (i = 0; x < length && i < codePointOffset; i++) {
                    if (isHighSurrogate(seq->charAt(x++)) && x < length && isLowSurrogate(seq->charAt(x))) {
                        x++;
                    }
                }
                if (i < codePointOffset) {
                    throw IndexOutOfBoundsException();
                }
            } else {
                jint i;
                for (i = codePointOffset; x > 0 && i < 0; i++) {
                    if (isLowSurrogate(seq->charAt(--x)) && x > 0 && isHighSurrogate(seq->charAt(x-1))) {
                        x--;
                    }
                }
                if (i < 0) {
                    throw IndexOutOfBoundsException();
                }
            }
            
            return x;
        }
        
        jchar Character::reverseBytes(jchar ch)
        {
            return static_cast<jchar>(((ch & 0xFF00) >> 8) | (ch << 8));
        }
        
        jarray<jchar> Character::toChars(jint codePoint)
        {
            if (isBmpCodePoint(codePoint)) {
                return { static_cast<jchar>(codePoint) };
            } else if (isValidCodePoint(codePoint)) {
                jarray<jchar> result(2);
                toSurrogates(codePoint, result, 0);
                return result;
            } else {
                throw IllegalArgumentException();
            }
        }
        
        jint Character::toChars(jint codePoint, jarray<jchar> dst, jint dstIndex)
        {
            if (isBmpCodePoint(codePoint)) {
                dst[dstIndex] = static_cast<jchar>(codePoint);
                return 1;
            } else if (isValidCodePoint(codePoint)) {
                toSurrogates(codePoint, dst, dstIndex);
                return 2;
            } else {
                throw IllegalArgumentException();
            }
        }

        jint Character::toCodePoint(jchar high, jchar low)
        {
            return ((high << 10) + low) + (MIN_SUPPLEMENTARY_CODE_POINT - (MIN_HIGH_SURROGATE << 10) - MIN_LOW_SURROGATE);
        }

        jchar Character::toLowerCase(jchar ch)
        {
            return static_cast<jchar>(toLowerCase(static_cast<jint>(ch)));
        }

        jint Character::toLowerCase(jint codePoint)
        {
            return CharacterData::of(codePoint)->toLowerCase(codePoint);
        }
        
        JString Character::toString()
        {
            jarray<jchar> buf = { value };
            return String::valueOf(buf);
        }
        
        JString Character::toString(jchar c)
        {
            return String::valueOf(c);
        }
        
        jchar Character::toTitleCase(jchar ch)
        {
            return static_cast<jchar>(toTitleCase(static_cast<jint>(ch)));
        }
        
        jint Character::toTitleCase(jint codePoint)
        {
            return CharacterData::of(codePoint)->toTitleCase(codePoint);
        }

        jchar Character::toUpperCase(jchar ch)
        {
            return static_cast<jchar>(toUpperCase(static_cast<int>(ch)));
        }

        jint Character::toUpperCase(jint codePoint)
        {
            return CharacterData::of(codePoint)->toUpperCase(codePoint);
        }

        JCharacter Character::valueOf(jchar c)
        {
            if (c <= 127) { // must cache
                return CharacterCache::cache[static_cast<jint>(c)];
            }
            return jnew<Character>(c);
        }

        void Character::toSurrogates(jint codePoint, jarray<jchar> dst, jint index)
        {
            dst[index + 1] = lowSurrogate(codePoint);
            dst[index] = highSurrogate(codePoint);
        }

        jint Character::codePointAtImpl(jarray<jchar> a, jint index, jint limit)
        {
            jchar c1 = a[index];
            if(isHighSurrogate(c1) && ++index < limit) {
                jchar c2 = a[index];
                if(isLowSurrogate(c2)) {
                    return toCodePoint(c1, c2);
                }
            }
            return c1;
        }

        jint Character::codePointBeforeImpl(jarray<jchar> a, jint index, jint start)
        {
            jchar c2 = a[--index];
            if(isLowSurrogate(c2) && index > start) {
                jchar c1 = a[--index];
                if(isHighSurrogate(c1)) {
                    return toCodePoint(c1, c2);
                }
            }
            return c2;
        }

        jint Character::codePointCountImpl(jarray<jchar> a, jint offset, jint count)
        {
            jint endIndex = offset + count;
            jint n = count;
            for(jint i = offset; i < endIndex;) {
                if(isHighSurrogate(a[i++]) && i < endIndex && isLowSurrogate(a[i])) {
                    n--;
                    i++;
                }
            }
            return n;
        }

        jint Character::offsetByCodePointsImpl(jarray<jchar> a, jint start, jint count, jint index, jint codePointOffset)
        {
            jint x = index;
            if(codePointOffset >= 0) {
                jint limit = start + count;
                jint i;
                for(i = 0; x < limit && i < codePointOffset; i++) {
                    if(isHighSurrogate(a[x++]) && x < limit && isLowSurrogate(a[x])) {
                        x++;
                    }
                }
                if(i < codePointOffset) {
                    throw IndexOutOfBoundsException();
                }
            }
            else {
                jint i;
                for(i = codePointOffset; x > start && i < 0; i++) {
                    if(isLowSurrogate(a[--x]) && x > start && isHighSurrogate(a[x - 1])) {
                        x--;
                    }
                }
                if(i < 0) {
                    throw IndexOutOfBoundsException();
                }
            }
            return x;
        }

        const jbyte Character::UNASSIGNED = 0;
        const jbyte Character::UPPERCASE_LETTER = 1;
        const jbyte Character::LOWERCASE_LETTER = 2;
        const jbyte Character::TITLECASE_LETTER = 3;
        const jbyte Character::MODIFIER_LETTER = 4;
        const jbyte Character::OTHER_LETTER = 5;
        const jbyte Character::DECIMAL_DIGIT_NUMBER = 9;
        const jbyte Character::LETTER_NUMBER = 10;
        const jbyte Character::SPACE_SEPARATOR = 12;
        const jbyte Character::LINE_SEPARATOR = 13;
        const jbyte Character::PARAGRAPH_SEPARATOR = 14;
        const jbyte Character::DIRECTIONALITY_LEFT_TO_RIGHT = 0;
        const jbyte Character::DIRECTIONALITY_LEFT_TO_RIGHT_EMBEDDING = 14;
        const jbyte Character::DIRECTIONALITY_LEFT_TO_RIGHT_OVERRIDE = 15;
        const jbyte Character::DIRECTIONALITY_POP_DIRECTIONAL_FORMAT = 18;
        const jbyte Character::DIRECTIONALITY_RIGHT_TO_LEFT_EMBEDDING = 16;
        const jbyte Character::DIRECTIONALITY_RIGHT_TO_LEFT_OVERRIDE = 17;
        const jbyte Character::DIRECTIONALITY_UNDEFINED = -1;
        const jint Character::ERROR = 0xFFFFFFFF;
        const jint Character::MAX_CODE_POINT = 0X10FFFF;
        const jchar Character::MAX_HIGH_SURROGATE = 0xDBFF;
        const jchar Character::MAX_LOW_SURROGATE = 0xDFFF;
        const jchar Character::MAX_SURROGATE = MAX_HIGH_SURROGATE;
        const jint Character::MAX_RADIX = 36;
        const jchar Character::MIN_HIGH_SURROGATE = 0xD800;
        const jchar Character::MIN_LOW_SURROGATE = 0xDC00;
        const jint Character::MIN_RADIX = 2;
        const jint Character::MIN_SUPPLEMENTARY_CODE_POINT = 0x010000;
        const jchar Character::MIN_SURROGATE = MAX_LOW_SURROGATE;
        const jbyte Character::PRIVATE_USE = 18;
        
        Character::Subset::Subset(JString name)
        {
            if (name == nullptr) {
                throw NullPointerException(jstr(u"name"));
            }
            
            this->name = name;
        }
        
        jboolean Character::Subset::equals(JObject obj)
        {
            return (shared_from_this() == obj);
        }
        
        jint Character::Subset::hashCode()
        {
            return Object::hashCode();
        }
        
        JString Character::Subset::toString()
        {
            return name;
        }
        
        Character::UnicodeBlock::UnicodeBlock(JString idName) : Subset(idName)
        {
        }
        
        Character::JUnicodeBlock Character::UnicodeBlock::createUnicodeBlock(JString idName)
        {
            JUnicodeBlock unicodeBlock = jnew<UnicodeBlock>(idName);
            map()->put(idName, unicodeBlock);
            return unicodeBlock;
        }
        
        Character::JUnicodeBlock Character::UnicodeBlock::createUnicodeBlock(JString idName, jarray<JString> aliases)
        {
            JUnicodeBlock unicodeBlock = jnew<UnicodeBlock>(idName);
            for (JString alias : aliases)
                map()->put(alias, unicodeBlock);
            return unicodeBlock;
        }
        
        cpp::util::JMap<JString, Character::JUnicodeBlock> Character::UnicodeBlock::map()
        {
            static cpp::util::JMap<JString, Character::JUnicodeBlock> _map = jnew<cpp::util::HashMap<JString, Character::JUnicodeBlock>>(256);
            return _map;
        }
        
        const Character::JUnicodeBlock Character::UnicodeBlock::BASIC_LATIN = Character::UnicodeBlock::createUnicodeBlock(jstr(u"BASIC_LATIN"), jstr(u"BASIC LATIN"), jstr(u"BASICLATIN"));
        const Character::JUnicodeBlock Character::UnicodeBlock::LATIN_1_SUPPLEMENT = Character::UnicodeBlock::createUnicodeBlock(jstr(u"LATIN_1_SUPPLEMENT"), jstr(u"LATIN-1 SUPPLEMENT"), jstr(u"LATIN-1SUPPLEMENT"));
        const Character::JUnicodeBlock Character::UnicodeBlock::LATIN_EXTENDED_A = Character::UnicodeBlock::createUnicodeBlock(jstr(u"LATIN_EXTENDED_A"), jstr(u"LATIN EXTENDED-A"), jstr(u"LATINEXTENDED-A"));
        const Character::JUnicodeBlock Character::UnicodeBlock::LATIN_EXTENDED_B = Character::UnicodeBlock::createUnicodeBlock(jstr(u"LATIN_EXTENDED_B"), jstr(u"LATIN EXTENDED-B"), jstr(u"LATINEXTENDED-B"));
        const Character::JUnicodeBlock Character::UnicodeBlock::IPA_EXTENSIONS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"IPA_EXTENSIONS"), jstr(u"IPA EXTENSIONS"), jstr(u"IPAEXTENSIONS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::SPACING_MODIFIER_LETTERS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"SPACING_MODIFIER_LETTERS"), jstr(u"SPACING MODIFIER LETTERS"), jstr(u"SPACINGMODIFIERLETTERS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::COMBINING_DIACRITICAL_MARKS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"COMBINING_DIACRITICAL_MARKS"), jstr(u"COMBINING DIACRITICAL MARKS"), jstr(u"COMBININGDIACRITICALMARKS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::GREEK = Character::UnicodeBlock::createUnicodeBlock(jstr(u"GREEK"), jstr(u"GREEK AND COPTIC"), jstr(u"GREEKANDCOPTIC"));
        const Character::JUnicodeBlock Character::UnicodeBlock::CYRILLIC = Character::UnicodeBlock::createUnicodeBlock(jstr(u"CYRILLIC"));
        const Character::JUnicodeBlock Character::UnicodeBlock::ARMENIAN = Character::UnicodeBlock::createUnicodeBlock(jstr(u"ARMENIAN"));
        const Character::JUnicodeBlock Character::UnicodeBlock::HEBREW = Character::UnicodeBlock::createUnicodeBlock(jstr(u"HEBREW"));
        const Character::JUnicodeBlock Character::UnicodeBlock::ARABIC = Character::UnicodeBlock::createUnicodeBlock(jstr(u"ARABIC"));
        const Character::JUnicodeBlock Character::UnicodeBlock::DEVANAGARI = Character::UnicodeBlock::createUnicodeBlock(jstr(u"DEVANAGARI"));
        const Character::JUnicodeBlock Character::UnicodeBlock::BENGALI = Character::UnicodeBlock::createUnicodeBlock(jstr(u"BENGALI"));
        const Character::JUnicodeBlock Character::UnicodeBlock::GURMUKHI = Character::UnicodeBlock::createUnicodeBlock(jstr(u"GURMUKHI"));
        const Character::JUnicodeBlock Character::UnicodeBlock::GUJARATI = Character::UnicodeBlock::createUnicodeBlock(jstr(u"GUJARATI"));
        const Character::JUnicodeBlock Character::UnicodeBlock::ORIYA = Character::UnicodeBlock::createUnicodeBlock(jstr(u"ORIYA"));
        const Character::JUnicodeBlock Character::UnicodeBlock::TAMIL = Character::UnicodeBlock::createUnicodeBlock(jstr(u"TAMIL"));
        const Character::JUnicodeBlock Character::UnicodeBlock::TELUGU = Character::UnicodeBlock::createUnicodeBlock(jstr(u"TELUGU"));
        const Character::JUnicodeBlock Character::UnicodeBlock::KANNADA = Character::UnicodeBlock::createUnicodeBlock(jstr(u"KANNADA"));
        const Character::JUnicodeBlock Character::UnicodeBlock::MALAYALAM = Character::UnicodeBlock::createUnicodeBlock(jstr(u"MALAYALAM"));
        const Character::JUnicodeBlock Character::UnicodeBlock::THAI = Character::UnicodeBlock::createUnicodeBlock(jstr(u"THAI"));
        const Character::JUnicodeBlock Character::UnicodeBlock::LAO = Character::UnicodeBlock::createUnicodeBlock(jstr(u"LAO"));
        const Character::JUnicodeBlock Character::UnicodeBlock::TIBETAN = Character::UnicodeBlock::createUnicodeBlock(jstr(u"TIBETAN"));
        const Character::JUnicodeBlock Character::UnicodeBlock::GEORGIAN = Character::UnicodeBlock::createUnicodeBlock(jstr(u"GEORGIAN"));
        const Character::JUnicodeBlock Character::UnicodeBlock::HANGUL_JAMO = Character::UnicodeBlock::createUnicodeBlock(jstr(u"HANGUL_JAMO"));
        const Character::JUnicodeBlock Character::UnicodeBlock::LATIN_EXTENDED_ADDITIONAL = Character::UnicodeBlock::createUnicodeBlock(jstr(u"LATIN_EXTENDED_ADDITIONAL"), jstr(u"LATIN EXTENDED ADDITIONAL"), jstr(u"LATINEXTENDEDADDITIONAL"));
        const Character::JUnicodeBlock Character::UnicodeBlock::GREEK_EXTENDED = Character::UnicodeBlock::createUnicodeBlock(jstr(u"GREEK_EXTENDED"), jstr(u"GREEK EXTENDED"), jstr(u"GREEKEXTENDED"));
        const Character::JUnicodeBlock Character::UnicodeBlock::GENERAL_PUNCTUATION = Character::UnicodeBlock::createUnicodeBlock(jstr(u"GENERAL_PUNCTUATION"), jstr(u"GENERAL PUNCTUATION"), jstr(u"GENERALPUNCTUATION"));
        const Character::JUnicodeBlock Character::UnicodeBlock::SUPERSCRIPTS_AND_SUBSCRIPTS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"SUPERSCRIPTS_AND_SUBSCRIPTS"), jstr(u"SUPERSCRIPTS AND SUBSCRIPTS"), jstr(u"SUPERSCRIPTSANDSUBSCRIPTS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::CURRENCY_SYMBOLS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"CURRENCY_SYMBOLS"), jstr(u"CURRENCY SYMBOLS"), jstr(u"CURRENCYSYMBOLS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::COMBINING_MARKS_FOR_SYMBOLS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"COMBINING_MARKS_FOR_SYMBOLS"), jstr(u"COMBINING DIACRITICAL MARKS FOR SYMBOLS"), jstr(u"COMBININGDIACRITICALMARKSFORSYMBOLS"), jstr(u"COMBINING MARKS FOR SYMBOLS"), jstr(u"COMBININGMARKSFORSYMBOLS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::LETTERLIKE_SYMBOLS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"LETTERLIKE_SYMBOLS"), jstr(u"LETTERLIKE SYMBOLS"), jstr(u"LETTERLIKESYMBOLS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::NUMBER_FORMS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"NUMBER_FORMS"), jstr(u"NUMBER FORMS"), jstr(u"NUMBERFORMS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::ARROWS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"ARROWS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::MATHEMATICAL_OPERATORS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"MATHEMATICAL_OPERATORS"), jstr(u"MATHEMATICAL OPERATORS"), jstr(u"MATHEMATICALOPERATORS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::MISCELLANEOUS_TECHNICAL = Character::UnicodeBlock::createUnicodeBlock(jstr(u"MISCELLANEOUS_TECHNICAL"), jstr(u"MISCELLANEOUS TECHNICAL"), jstr(u"MISCELLANEOUSTECHNICAL"));
        const Character::JUnicodeBlock Character::UnicodeBlock::CONTROL_PICTURES = Character::UnicodeBlock::createUnicodeBlock(jstr(u"CONTROL_PICTURES"), jstr(u"CONTROL PICTURES"), jstr(u"CONTROLPICTURES"));
        const Character::JUnicodeBlock Character::UnicodeBlock::OPTICAL_CHARACTER_RECOGNITION = Character::UnicodeBlock::createUnicodeBlock(jstr(u"OPTICAL_CHARACTER_RECOGNITION"), jstr(u"OPTICAL CHARACTER RECOGNITION"), jstr(u"OPTICALCHARACTERRECOGNITION"));
        const Character::JUnicodeBlock Character::UnicodeBlock::ENCLOSED_ALPHANUMERICS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"ENCLOSED_ALPHANUMERICS"), jstr(u"ENCLOSED ALPHANUMERICS"), jstr(u"ENCLOSEDALPHANUMERICS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::BOX_DRAWING = Character::UnicodeBlock::createUnicodeBlock(jstr(u"BOX_DRAWING"), jstr(u"BOX DRAWING"), jstr(u"BOXDRAWING"));
        const Character::JUnicodeBlock Character::UnicodeBlock::BLOCK_ELEMENTS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"BLOCK_ELEMENTS"), jstr(u"BLOCK ELEMENTS"), jstr(u"BLOCKELEMENTS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::GEOMETRIC_SHAPES = Character::UnicodeBlock::createUnicodeBlock(jstr(u"GEOMETRIC_SHAPES"), jstr(u"GEOMETRIC SHAPES"), jstr(u"GEOMETRICSHAPES"));
        const Character::JUnicodeBlock Character::UnicodeBlock::MISCELLANEOUS_SYMBOLS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"MISCELLANEOUS_SYMBOLS"), jstr(u"MISCELLANEOUS SYMBOLS"), jstr(u"MISCELLANEOUSSYMBOLS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::DINGBATS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"DINGBATS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::CJK_SYMBOLS_AND_PUNCTUATION = Character::UnicodeBlock::createUnicodeBlock(jstr(u"CJK_SYMBOLS_AND_PUNCTUATION"), jstr(u"CJK SYMBOLS AND PUNCTUATION"), jstr(u"CJKSYMBOLSANDPUNCTUATION"));
        const Character::JUnicodeBlock Character::UnicodeBlock::HIRAGANA = Character::UnicodeBlock::createUnicodeBlock(jstr(u"HIRAGANA"));
        const Character::JUnicodeBlock Character::UnicodeBlock::KATAKANA = Character::UnicodeBlock::createUnicodeBlock(jstr(u"KATAKANA"));
        const Character::JUnicodeBlock Character::UnicodeBlock::BOPOMOFO = Character::UnicodeBlock::createUnicodeBlock(jstr(u"BOPOMOFO"));
        const Character::JUnicodeBlock Character::UnicodeBlock::HANGUL_COMPATIBILITY_JAMO = Character::UnicodeBlock::createUnicodeBlock(jstr(u"HANGUL_COMPATIBILITY_JAMO"), jstr(u"HANGUL COMPATIBILITY JAMO"), jstr(u"HANGULCOMPATIBILITYJAMO"));
        const Character::JUnicodeBlock Character::UnicodeBlock::KANBUN = Character::UnicodeBlock::createUnicodeBlock(jstr(u"KANBUN"));
        const Character::JUnicodeBlock Character::UnicodeBlock::ENCLOSED_CJK_LETTERS_AND_MONTHS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"ENCLOSED_CJK_LETTERS_AND_MONTHS"), jstr(u"ENCLOSED CJK LETTERS AND MONTHS"), jstr(u"ENCLOSEDCJKLETTERSANDMONTHS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::CJK_COMPATIBILITY = Character::UnicodeBlock::createUnicodeBlock(jstr(u"CJK_COMPATIBILITY"), jstr(u"CJK COMPATIBILITY"), jstr(u"CJKCOMPATIBILITY"));
        const Character::JUnicodeBlock Character::UnicodeBlock::CJK_UNIFIED_IDEOGRAPHS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"CJK_UNIFIED_IDEOGRAPHS"), jstr(u"CJK UNIFIED IDEOGRAPHS"), jstr(u"CJKUNIFIEDIDEOGRAPHS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::HANGUL_SYLLABLES = Character::UnicodeBlock::createUnicodeBlock(jstr(u"HANGUL_SYLLABLES"), jstr(u"HANGUL SYLLABLES"), jstr(u"HANGULSYLLABLES"));
        const Character::JUnicodeBlock Character::UnicodeBlock::PRIVATE_USE_AREA = Character::UnicodeBlock::createUnicodeBlock(jstr(u"PRIVATE_USE_AREA"), jstr(u"PRIVATE USE AREA"), jstr(u"PRIVATEUSEAREA"));
        const Character::JUnicodeBlock Character::UnicodeBlock::CJK_COMPATIBILITY_IDEOGRAPHS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"CJK_COMPATIBILITY_IDEOGRAPHS"), jstr(u"CJK COMPATIBILITY IDEOGRAPHS"), jstr(u"CJKCOMPATIBILITYIDEOGRAPHS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::ALPHABETIC_PRESENTATION_FORMS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"ALPHABETIC_PRESENTATION_FORMS"), jstr(u"ALPHABETIC PRESENTATION FORMS"), jstr(u"ALPHABETICPRESENTATIONFORMS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::ARABIC_PRESENTATION_FORMS_A = Character::UnicodeBlock::createUnicodeBlock(jstr(u"ARABIC_PRESENTATION_FORMS_A"), jstr(u"ARABIC PRESENTATION FORMS-A"), jstr(u"ARABICPRESENTATIONFORMS-A"));
        const Character::JUnicodeBlock Character::UnicodeBlock::COMBINING_HALF_MARKS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"COMBINING_HALF_MARKS"), jstr(u"COMBINING HALF MARKS"), jstr(u"COMBININGHALFMARKS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::CJK_COMPATIBILITY_FORMS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"CJK_COMPATIBILITY_FORMS"), jstr(u"CJK COMPATIBILITY FORMS"), jstr(u"CJKCOMPATIBILITYFORMS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::SMALL_FORM_VARIANTS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"SMALL_FORM_VARIANTS"), jstr(u"SMALL FORM VARIANTS"), jstr(u"SMALLFORMVARIANTS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::ARABIC_PRESENTATION_FORMS_B = Character::UnicodeBlock::createUnicodeBlock(jstr(u"ARABIC_PRESENTATION_FORMS_B"), jstr(u"ARABIC PRESENTATION FORMS-B"), jstr(u"ARABICPRESENTATIONFORMS-B"));
        const Character::JUnicodeBlock Character::UnicodeBlock::HALFWIDTH_AND_FULLWIDTH_FORMS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"HALFWIDTH_AND_FULLWIDTH_FORMS"), jstr(u"HALFWIDTH AND FULLWIDTH FORMS"), jstr(u"HALFWIDTHANDFULLWIDTHFORMS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::SPECIALS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"SPECIALS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::SURROGATES_AREA = Character::UnicodeBlock::createUnicodeBlock(jstr(u"SURROGATES_AREA"));
        const Character::JUnicodeBlock Character::UnicodeBlock::SYRIAC = Character::UnicodeBlock::createUnicodeBlock(jstr(u"SYRIAC"));
        const Character::JUnicodeBlock Character::UnicodeBlock::THAANA = Character::UnicodeBlock::createUnicodeBlock(jstr(u"THAANA"));
        const Character::JUnicodeBlock Character::UnicodeBlock::SINHALA = Character::UnicodeBlock::createUnicodeBlock(jstr(u"SINHALA"));
        const Character::JUnicodeBlock Character::UnicodeBlock::MYANMAR = Character::UnicodeBlock::createUnicodeBlock(jstr(u"MYANMAR"));
        const Character::JUnicodeBlock Character::UnicodeBlock::ETHIOPIC = Character::UnicodeBlock::createUnicodeBlock(jstr(u"ETHIOPIC"));
        const Character::JUnicodeBlock Character::UnicodeBlock::CHEROKEE = Character::UnicodeBlock::createUnicodeBlock(jstr(u"CHEROKEE"));
        const Character::JUnicodeBlock Character::UnicodeBlock::UNIFIED_CANADIAN_ABORIGINAL_SYLLABICS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"UNIFIED_CANADIAN_ABORIGINAL_SYLLABICS"), jstr(u"UNIFIED CANADIAN ABORIGINAL SYLLABICS"), jstr(u"UNIFIEDCANADIANABORIGINALSYLLABICS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::OGHAM = Character::UnicodeBlock::createUnicodeBlock(jstr(u"OGHAM"));
        const Character::JUnicodeBlock Character::UnicodeBlock::RUNIC = Character::UnicodeBlock::createUnicodeBlock(jstr(u"RUNIC"));
        const Character::JUnicodeBlock Character::UnicodeBlock::KHMER = Character::UnicodeBlock::createUnicodeBlock(jstr(u"KHMER"));
        const Character::JUnicodeBlock Character::UnicodeBlock::MONGOLIAN = Character::UnicodeBlock::createUnicodeBlock(jstr(u"MONGOLIAN"));
        const Character::JUnicodeBlock Character::UnicodeBlock::BRAILLE_PATTERNS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"BRAILLE_PATTERNS"), jstr(u"BRAILLE PATTERNS"), jstr(u"BRAILLEPATTERNS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::CJK_RADICALS_SUPPLEMENT = Character::UnicodeBlock::createUnicodeBlock(jstr(u"CJK_RADICALS_SUPPLEMENT"), jstr(u"CJK RADICALS SUPPLEMENT"), jstr(u"CJKRADICALSSUPPLEMENT"));
        const Character::JUnicodeBlock Character::UnicodeBlock::KANGXI_RADICALS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"KANGXI_RADICALS"), jstr(u"KANGXI RADICALS"), jstr(u"KANGXIRADICALS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::IDEOGRAPHIC_DESCRIPTION_CHARACTERS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"IDEOGRAPHIC_DESCRIPTION_CHARACTERS"), jstr(u"IDEOGRAPHIC DESCRIPTION CHARACTERS"), jstr(u"IDEOGRAPHICDESCRIPTIONCHARACTERS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::BOPOMOFO_EXTENDED = Character::UnicodeBlock::createUnicodeBlock(jstr(u"BOPOMOFO_EXTENDED"), jstr(u"BOPOMOFO EXTENDED"), jstr(u"BOPOMOFOEXTENDED"));
        const Character::JUnicodeBlock Character::UnicodeBlock::CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A = Character::UnicodeBlock::createUnicodeBlock(jstr(u"CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A"), jstr(u"CJK UNIFIED IDEOGRAPHS EXTENSION A"), jstr(u"CJKUNIFIEDIDEOGRAPHSEXTENSIONA"));
        const Character::JUnicodeBlock Character::UnicodeBlock::YI_SYLLABLES = Character::UnicodeBlock::createUnicodeBlock(jstr(u"YI_SYLLABLES"), jstr(u"YI SYLLABLES"), jstr(u"YISYLLABLES"));
        const Character::JUnicodeBlock Character::UnicodeBlock::YI_RADICALS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"YI_RADICALS"), jstr(u"YI RADICALS"), jstr(u"YIRADICALS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::CYRILLIC_SUPPLEMENTARY = Character::UnicodeBlock::createUnicodeBlock(jstr(u"CYRILLIC_SUPPLEMENTARY"), jstr(u"CYRILLIC SUPPLEMENTARY"), jstr(u"CYRILLICSUPPLEMENTARY"), jstr(u"CYRILLIC SUPPLEMENT"), jstr(u"CYRILLICSUPPLEMENT"));
        const Character::JUnicodeBlock Character::UnicodeBlock::TAGALOG = Character::UnicodeBlock::createUnicodeBlock(jstr(u"TAGALOG"));
        const Character::JUnicodeBlock Character::UnicodeBlock::HANUNOO = Character::UnicodeBlock::createUnicodeBlock(jstr(u"HANUNOO"));
        const Character::JUnicodeBlock Character::UnicodeBlock::BUHID = Character::UnicodeBlock::createUnicodeBlock(jstr(u"BUHID"));
        const Character::JUnicodeBlock Character::UnicodeBlock::TAGBANWA = Character::UnicodeBlock::createUnicodeBlock(jstr(u"TAGBANWA"));
        const Character::JUnicodeBlock Character::UnicodeBlock::LIMBU = Character::UnicodeBlock::createUnicodeBlock(jstr(u"LIMBU"));
        const Character::JUnicodeBlock Character::UnicodeBlock::TAI_LE = Character::UnicodeBlock::createUnicodeBlock(jstr(u"TAI_LE"), jstr(u"TAI LE"), jstr(u"TAILE"));
        const Character::JUnicodeBlock Character::UnicodeBlock::KHMER_SYMBOLS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"KHMER_SYMBOLS"), jstr(u"KHMER SYMBOLS"), jstr(u"KHMERSYMBOLS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::PHONETIC_EXTENSIONS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"PHONETIC_EXTENSIONS"), jstr(u"PHONETIC EXTENSIONS"), jstr(u"PHONETICEXTENSIONS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::MISCELLANEOUS_MATHEMATICAL_SYMBOLS_A = Character::UnicodeBlock::createUnicodeBlock(jstr(u"MISCELLANEOUS_MATHEMATICAL_SYMBOLS_A"), jstr(u"MISCELLANEOUS MATHEMATICAL SYMBOLS-A"), jstr(u"MISCELLANEOUSMATHEMATICALSYMBOLS-A"));
        const Character::JUnicodeBlock Character::UnicodeBlock::SUPPLEMENTAL_ARROWS_A = Character::UnicodeBlock::createUnicodeBlock(jstr(u"SUPPLEMENTAL_ARROWS_A"), jstr(u"SUPPLEMENTAL ARROWS-A"), jstr(u"SUPPLEMENTALARROWS-A"));
        const Character::JUnicodeBlock Character::UnicodeBlock::SUPPLEMENTAL_ARROWS_B = Character::UnicodeBlock::createUnicodeBlock(jstr(u"SUPPLEMENTAL_ARROWS_B"), jstr(u"SUPPLEMENTAL ARROWS-B"), jstr(u"SUPPLEMENTALARROWS-B"));
        const Character::JUnicodeBlock Character::UnicodeBlock::MISCELLANEOUS_MATHEMATICAL_SYMBOLS_B = Character::UnicodeBlock::createUnicodeBlock(jstr(u"MISCELLANEOUS_MATHEMATICAL_SYMBOLS_B"), jstr(u"MISCELLANEOUS MATHEMATICAL SYMBOLS-B"), jstr(u"MISCELLANEOUSMATHEMATICALSYMBOLS-B"));
        const Character::JUnicodeBlock Character::UnicodeBlock::SUPPLEMENTAL_MATHEMATICAL_OPERATORS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"SUPPLEMENTAL_MATHEMATICAL_OPERATORS"), jstr(u"SUPPLEMENTAL MATHEMATICAL OPERATORS"), jstr(u"SUPPLEMENTALMATHEMATICALOPERATORS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::MISCELLANEOUS_SYMBOLS_AND_ARROWS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"MISCELLANEOUS_SYMBOLS_AND_ARROWS"), jstr(u"MISCELLANEOUS SYMBOLS AND ARROWS"), jstr(u"MISCELLANEOUSSYMBOLSANDARROWS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::KATAKANA_PHONETIC_EXTENSIONS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"KATAKANA_PHONETIC_EXTENSIONS"), jstr(u"KATAKANA PHONETIC EXTENSIONS"), jstr(u"KATAKANAPHONETICEXTENSIONS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::YIJING_HEXAGRAM_SYMBOLS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"YIJING_HEXAGRAM_SYMBOLS"), jstr(u"YIJING HEXAGRAM SYMBOLS"), jstr(u"YIJINGHEXAGRAMSYMBOLS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::VARIATION_SELECTORS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"VARIATION_SELECTORS"), jstr(u"VARIATION SELECTORS"), jstr(u"VARIATIONSELECTORS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::LINEAR_B_SYLLABARY = Character::UnicodeBlock::createUnicodeBlock(jstr(u"LINEAR_B_SYLLABARY"), jstr(u"LINEAR B SYLLABARY"), jstr(u"LINEARBSYLLABARY"));
        const Character::JUnicodeBlock Character::UnicodeBlock::LINEAR_B_IDEOGRAMS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"LINEAR_B_IDEOGRAMS"), jstr(u"LINEAR B IDEOGRAMS"), jstr(u"LINEARBIDEOGRAMS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::AEGEAN_NUMBERS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"AEGEAN_NUMBERS"), jstr(u"AEGEAN NUMBERS"), jstr(u"AEGEANNUMBERS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::OLD_ITALIC = Character::UnicodeBlock::createUnicodeBlock(jstr(u"OLD_ITALIC"), jstr(u"OLD ITALIC"), jstr(u"OLDITALIC"));
        const Character::JUnicodeBlock Character::UnicodeBlock::GOTHIC = Character::UnicodeBlock::createUnicodeBlock(jstr(u"GOTHIC"));
        const Character::JUnicodeBlock Character::UnicodeBlock::UGARITIC = Character::UnicodeBlock::createUnicodeBlock(jstr(u"UGARITIC"));
        const Character::JUnicodeBlock Character::UnicodeBlock::DESERET = Character::UnicodeBlock::createUnicodeBlock(jstr(u"DESERET"));
        const Character::JUnicodeBlock Character::UnicodeBlock::SHAVIAN = Character::UnicodeBlock::createUnicodeBlock(jstr(u"SHAVIAN"));
        const Character::JUnicodeBlock Character::UnicodeBlock::OSMANYA = Character::UnicodeBlock::createUnicodeBlock(jstr(u"OSMANYA"));
        const Character::JUnicodeBlock Character::UnicodeBlock::CYPRIOT_SYLLABARY = Character::UnicodeBlock::createUnicodeBlock(jstr(u"CYPRIOT_SYLLABARY"), jstr(u"CYPRIOT SYLLABARY"), jstr(u"CYPRIOTSYLLABARY"));
        const Character::JUnicodeBlock Character::UnicodeBlock::BYZANTINE_MUSICAL_SYMBOLS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"BYZANTINE_MUSICAL_SYMBOLS"), jstr(u"BYZANTINE MUSICAL SYMBOLS"), jstr(u"BYZANTINEMUSICALSYMBOLS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::MUSICAL_SYMBOLS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"MUSICAL_SYMBOLS"), jstr(u"MUSICAL SYMBOLS"), jstr(u"MUSICALSYMBOLS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::TAI_XUAN_JING_SYMBOLS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"TAI_XUAN_JING_SYMBOLS"), jstr(u"TAI XUAN JING SYMBOLS"), jstr(u"TAIXUANJINGSYMBOLS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::MATHEMATICAL_ALPHANUMERIC_SYMBOLS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"MATHEMATICAL_ALPHANUMERIC_SYMBOLS"), jstr(u"MATHEMATICAL ALPHANUMERIC SYMBOLS"), jstr(u"MATHEMATICALALPHANUMERICSYMBOLS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B = Character::UnicodeBlock::createUnicodeBlock(jstr(u"CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B"), jstr(u"CJK UNIFIED IDEOGRAPHS EXTENSION B"), jstr(u"CJKUNIFIEDIDEOGRAPHSEXTENSIONB"));
        const Character::JUnicodeBlock Character::UnicodeBlock::CJK_COMPATIBILITY_IDEOGRAPHS_SUPPLEMENT = Character::UnicodeBlock::createUnicodeBlock(jstr(u"CJK_COMPATIBILITY_IDEOGRAPHS_SUPPLEMENT"), jstr(u"CJK COMPATIBILITY IDEOGRAPHS SUPPLEMENT"), jstr(u"CJKCOMPATIBILITYIDEOGRAPHSSUPPLEMENT"));
        const Character::JUnicodeBlock Character::UnicodeBlock::TAGS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"TAGS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::VARIATION_SELECTORS_SUPPLEMENT = Character::UnicodeBlock::createUnicodeBlock(jstr(u"VARIATION_SELECTORS_SUPPLEMENT"), jstr(u"VARIATION SELECTORS SUPPLEMENT"), jstr(u"VARIATIONSELECTORSSUPPLEMENT"));
        const Character::JUnicodeBlock Character::UnicodeBlock::SUPPLEMENTARY_PRIVATE_USE_AREA_A = Character::UnicodeBlock::createUnicodeBlock(jstr(u"SUPPLEMENTARY_PRIVATE_USE_AREA_A"), jstr(u"SUPPLEMENTARY PRIVATE USE AREA-A"), jstr(u"SUPPLEMENTARYPRIVATEUSEAREA-A"));
        const Character::JUnicodeBlock Character::UnicodeBlock::SUPPLEMENTARY_PRIVATE_USE_AREA_B = Character::UnicodeBlock::createUnicodeBlock(jstr(u"SUPPLEMENTARY_PRIVATE_USE_AREA_B"), jstr(u"SUPPLEMENTARY PRIVATE USE AREA-B"), jstr(u"SUPPLEMENTARYPRIVATEUSEAREA-B"));
        const Character::JUnicodeBlock Character::UnicodeBlock::HIGH_SURROGATES = Character::UnicodeBlock::createUnicodeBlock(jstr(u"HIGH_SURROGATES"), jstr(u"HIGH SURROGATES"), jstr(u"HIGHSURROGATES"));
        const Character::JUnicodeBlock Character::UnicodeBlock::HIGH_PRIVATE_USE_SURROGATES = Character::UnicodeBlock::createUnicodeBlock(jstr(u"HIGH_PRIVATE_USE_SURROGATES"), jstr(u"HIGH PRIVATE USE SURROGATES"), jstr(u"HIGHPRIVATEUSESURROGATES"));
        const Character::JUnicodeBlock Character::UnicodeBlock::LOW_SURROGATES = Character::UnicodeBlock::createUnicodeBlock(jstr(u"LOW_SURROGATES"), jstr(u"LOW SURROGATES"), jstr(u"LOWSURROGATES"));
        const Character::JUnicodeBlock Character::UnicodeBlock::ARABIC_SUPPLEMENT = Character::UnicodeBlock::createUnicodeBlock(jstr(u"ARABIC_SUPPLEMENT"), jstr(u"ARABIC SUPPLEMENT"), jstr(u"ARABICSUPPLEMENT"));
        const Character::JUnicodeBlock Character::UnicodeBlock::NKO = Character::UnicodeBlock::createUnicodeBlock(jstr(u"NKO"));
        const Character::JUnicodeBlock Character::UnicodeBlock::SAMARITAN = Character::UnicodeBlock::createUnicodeBlock(jstr(u"SAMARITAN"));
        const Character::JUnicodeBlock Character::UnicodeBlock::MANDAIC = Character::UnicodeBlock::createUnicodeBlock(jstr(u"MANDAIC"));
        const Character::JUnicodeBlock Character::UnicodeBlock::ETHIOPIC_SUPPLEMENT = Character::UnicodeBlock::createUnicodeBlock(jstr(u"ETHIOPIC_SUPPLEMENT"), jstr(u"ETHIOPIC SUPPLEMENT"), jstr(u"ETHIOPICSUPPLEMENT"));
        const Character::JUnicodeBlock Character::UnicodeBlock::UNIFIED_CANADIAN_ABORIGINAL_SYLLABICS_EXTENDED = Character::UnicodeBlock::createUnicodeBlock(jstr(u"UNIFIED_CANADIAN_ABORIGINAL_SYLLABICS_EXTENDED"), jstr(u"UNIFIED CANADIAN ABORIGINAL SYLLABICS EXTENDED"), jstr(u"UNIFIEDCANADIANABORIGINALSYLLABICSEXTENDED"));
        const Character::JUnicodeBlock Character::UnicodeBlock::NEW_TAI_LUE = Character::UnicodeBlock::createUnicodeBlock(jstr(u"NEW_TAI_LUE"), jstr(u"NEW TAI LUE"), jstr(u"NEWTAILUE"));
        const Character::JUnicodeBlock Character::UnicodeBlock::BUGINESE = Character::UnicodeBlock::createUnicodeBlock(jstr(u"BUGINESE"));
        const Character::JUnicodeBlock Character::UnicodeBlock::TAI_THAM = Character::UnicodeBlock::createUnicodeBlock(jstr(u"TAI_THAM"), jstr(u"TAI THAM"), jstr(u"TAITHAM"));
        const Character::JUnicodeBlock Character::UnicodeBlock::BALINESE = Character::UnicodeBlock::createUnicodeBlock(jstr(u"BALINESE"));
        const Character::JUnicodeBlock Character::UnicodeBlock::SUNDANESE = Character::UnicodeBlock::createUnicodeBlock(jstr(u"SUNDANESE"));
        const Character::JUnicodeBlock Character::UnicodeBlock::BATAK = Character::UnicodeBlock::createUnicodeBlock(jstr(u"BATAK"));
        const Character::JUnicodeBlock Character::UnicodeBlock::LEPCHA = Character::UnicodeBlock::createUnicodeBlock(jstr(u"LEPCHA"));
        const Character::JUnicodeBlock Character::UnicodeBlock::OL_CHIKI = Character::UnicodeBlock::createUnicodeBlock(jstr(u"OL_CHIKI"), jstr(u"OL CHIKI"), jstr(u"OLCHIKI"));
        const Character::JUnicodeBlock Character::UnicodeBlock::VEDIC_EXTENSIONS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"VEDIC_EXTENSIONS"), jstr(u"VEDIC EXTENSIONS"), jstr(u"VEDICEXTENSIONS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::PHONETIC_EXTENSIONS_SUPPLEMENT = Character::UnicodeBlock::createUnicodeBlock(jstr(u"PHONETIC_EXTENSIONS_SUPPLEMENT"), jstr(u"PHONETIC EXTENSIONS SUPPLEMENT"), jstr(u"PHONETICEXTENSIONSSUPPLEMENT"));
        const Character::JUnicodeBlock Character::UnicodeBlock::COMBINING_DIACRITICAL_MARKS_SUPPLEMENT = Character::UnicodeBlock::createUnicodeBlock(jstr(u"COMBINING_DIACRITICAL_MARKS_SUPPLEMENT"), jstr(u"COMBINING DIACRITICAL MARKS SUPPLEMENT"), jstr(u"COMBININGDIACRITICALMARKSSUPPLEMENT"));
        const Character::JUnicodeBlock Character::UnicodeBlock::GLAGOLITIC = Character::UnicodeBlock::createUnicodeBlock(jstr(u"GLAGOLITIC"));
        const Character::JUnicodeBlock Character::UnicodeBlock::LATIN_EXTENDED_C = Character::UnicodeBlock::createUnicodeBlock(jstr(u"LATIN_EXTENDED_C"), jstr(u"LATIN EXTENDED-C"), jstr(u"LATINEXTENDED-C"));
        const Character::JUnicodeBlock Character::UnicodeBlock::COPTIC = Character::UnicodeBlock::createUnicodeBlock(jstr(u"COPTIC"));
        const Character::JUnicodeBlock Character::UnicodeBlock::GEORGIAN_SUPPLEMENT = Character::UnicodeBlock::createUnicodeBlock(jstr(u"GEORGIAN_SUPPLEMENT"), jstr(u"GEORGIAN SUPPLEMENT"), jstr(u"GEORGIANSUPPLEMENT"));
        const Character::JUnicodeBlock Character::UnicodeBlock::TIFINAGH = Character::UnicodeBlock::createUnicodeBlock(jstr(u"TIFINAGH"));
        const Character::JUnicodeBlock Character::UnicodeBlock::ETHIOPIC_EXTENDED = Character::UnicodeBlock::createUnicodeBlock(jstr(u"ETHIOPIC_EXTENDED"), jstr(u"ETHIOPIC EXTENDED"), jstr(u"ETHIOPICEXTENDED"));
        const Character::JUnicodeBlock Character::UnicodeBlock::CYRILLIC_EXTENDED_A = Character::UnicodeBlock::createUnicodeBlock(jstr(u"CYRILLIC_EXTENDED_A"), jstr(u"CYRILLIC EXTENDED-A"), jstr(u"CYRILLICEXTENDED-A"));
        const Character::JUnicodeBlock Character::UnicodeBlock::SUPPLEMENTAL_PUNCTUATION = Character::UnicodeBlock::createUnicodeBlock(jstr(u"SUPPLEMENTAL_PUNCTUATION"), jstr(u"SUPPLEMENTAL PUNCTUATION"), jstr(u"SUPPLEMENTALPUNCTUATION"));
        const Character::JUnicodeBlock Character::UnicodeBlock::CJK_STROKES = Character::UnicodeBlock::createUnicodeBlock(jstr(u"CJK_STROKES"), jstr(u"CJK STROKES"), jstr(u"CJKSTROKES"));
        const Character::JUnicodeBlock Character::UnicodeBlock::LISU = Character::UnicodeBlock::createUnicodeBlock(jstr(u"LISU"));
        const Character::JUnicodeBlock Character::UnicodeBlock::VAI = Character::UnicodeBlock::createUnicodeBlock(jstr(u"VAI"));
        const Character::JUnicodeBlock Character::UnicodeBlock::CYRILLIC_EXTENDED_B = Character::UnicodeBlock::createUnicodeBlock(jstr(u"CYRILLIC_EXTENDED_B"), jstr(u"CYRILLIC EXTENDED-B"), jstr(u"CYRILLICEXTENDED-B"));
        const Character::JUnicodeBlock Character::UnicodeBlock::BAMUM = Character::UnicodeBlock::createUnicodeBlock(jstr(u"BAMUM"));
        const Character::JUnicodeBlock Character::UnicodeBlock::MODIFIER_TONE_LETTERS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"MODIFIER_TONE_LETTERS"), jstr(u"MODIFIER TONE LETTERS"), jstr(u"MODIFIERTONELETTERS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::LATIN_EXTENDED_D = Character::UnicodeBlock::createUnicodeBlock(jstr(u"LATIN_EXTENDED_D"), jstr(u"LATIN EXTENDED-D"), jstr(u"LATINEXTENDED-D"));
        const Character::JUnicodeBlock Character::UnicodeBlock::SYLOTI_NAGRI = Character::UnicodeBlock::createUnicodeBlock(jstr(u"SYLOTI_NAGRI"), jstr(u"SYLOTI NAGRI"), jstr(u"SYLOTINAGRI"));
        const Character::JUnicodeBlock Character::UnicodeBlock::COMMON_INDIC_NUMBER_FORMS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"COMMON_INDIC_NUMBER_FORMS"), jstr(u"COMMON INDIC NUMBER FORMS"), jstr(u"COMMONINDICNUMBERFORMS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::PHAGS_PA = Character::UnicodeBlock::createUnicodeBlock(jstr(u"PHAGS_PA"), jstr(u"PHAGS-PA"));
        const Character::JUnicodeBlock Character::UnicodeBlock::SAURASHTRA = Character::UnicodeBlock::createUnicodeBlock(jstr(u"SAURASHTRA"));
        const Character::JUnicodeBlock Character::UnicodeBlock::DEVANAGARI_EXTENDED = Character::UnicodeBlock::createUnicodeBlock(jstr(u"DEVANAGARI_EXTENDED"), jstr(u"DEVANAGARI EXTENDED"), jstr(u"DEVANAGARIEXTENDED"));
        const Character::JUnicodeBlock Character::UnicodeBlock::KAYAH_LI = Character::UnicodeBlock::createUnicodeBlock(jstr(u"KAYAH_LI"), jstr(u"KAYAH LI"), jstr(u"KAYAHLI"));
        const Character::JUnicodeBlock Character::UnicodeBlock::REJANG = Character::UnicodeBlock::createUnicodeBlock(jstr(u"REJANG"));
        const Character::JUnicodeBlock Character::UnicodeBlock::HANGUL_JAMO_EXTENDED_A = Character::UnicodeBlock::createUnicodeBlock(jstr(u"HANGUL_JAMO_EXTENDED_A"), jstr(u"HANGUL JAMO EXTENDED-A"), jstr(u"HANGULJAMOEXTENDED-A"));
        const Character::JUnicodeBlock Character::UnicodeBlock::JAVANESE = Character::UnicodeBlock::createUnicodeBlock(jstr(u"JAVANESE"));
        const Character::JUnicodeBlock Character::UnicodeBlock::CHAM = Character::UnicodeBlock::createUnicodeBlock(jstr(u"CHAM"));
        const Character::JUnicodeBlock Character::UnicodeBlock::MYANMAR_EXTENDED_A = Character::UnicodeBlock::createUnicodeBlock(jstr(u"MYANMAR_EXTENDED_A"), jstr(u"MYANMAR EXTENDED-A"), jstr(u"MYANMAREXTENDED-A"));
        const Character::JUnicodeBlock Character::UnicodeBlock::TAI_VIET = Character::UnicodeBlock::createUnicodeBlock(jstr(u"TAI_VIET"), jstr(u"TAI VIET"), jstr(u"TAIVIET"));
        const Character::JUnicodeBlock Character::UnicodeBlock::ETHIOPIC_EXTENDED_A = Character::UnicodeBlock::createUnicodeBlock(jstr(u"ETHIOPIC_EXTENDED_A"), jstr(u"ETHIOPIC EXTENDED-A"), jstr(u"ETHIOPICEXTENDED-A"));
        const Character::JUnicodeBlock Character::UnicodeBlock::MEETEI_MAYEK = Character::UnicodeBlock::createUnicodeBlock(jstr(u"MEETEI_MAYEK"), jstr(u"MEETEI MAYEK"), jstr(u"MEETEIMAYEK"));
        const Character::JUnicodeBlock Character::UnicodeBlock::HANGUL_JAMO_EXTENDED_B = Character::UnicodeBlock::createUnicodeBlock(jstr(u"HANGUL_JAMO_EXTENDED_B"), jstr(u"HANGUL JAMO EXTENDED-B"), jstr(u"HANGULJAMOEXTENDED-B"));
        const Character::JUnicodeBlock Character::UnicodeBlock::VERTICAL_FORMS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"VERTICAL_FORMS"), jstr(u"VERTICAL FORMS"), jstr(u"VERTICALFORMS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::ANCIENT_GREEK_NUMBERS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"ANCIENT_GREEK_NUMBERS"), jstr(u"ANCIENT GREEK NUMBERS"), jstr(u"ANCIENTGREEKNUMBERS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::ANCIENT_SYMBOLS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"ANCIENT_SYMBOLS"), jstr(u"ANCIENT SYMBOLS"), jstr(u"ANCIENTSYMBOLS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::PHAISTOS_DISC = Character::UnicodeBlock::createUnicodeBlock(jstr(u"PHAISTOS_DISC"), jstr(u"PHAISTOS DISC"), jstr(u"PHAISTOSDISC"));
        const Character::JUnicodeBlock Character::UnicodeBlock::LYCIAN = Character::UnicodeBlock::createUnicodeBlock(jstr(u"LYCIAN"));
        const Character::JUnicodeBlock Character::UnicodeBlock::CARIAN = Character::UnicodeBlock::createUnicodeBlock(jstr(u"CARIAN"));
        const Character::JUnicodeBlock Character::UnicodeBlock::OLD_PERSIAN = Character::UnicodeBlock::createUnicodeBlock(jstr(u"OLD_PERSIAN"), jstr(u"OLD PERSIAN"), jstr(u"OLDPERSIAN"));
        const Character::JUnicodeBlock Character::UnicodeBlock::IMPERIAL_ARAMAIC = Character::UnicodeBlock::createUnicodeBlock(jstr(u"IMPERIAL_ARAMAIC"), jstr(u"IMPERIAL ARAMAIC"), jstr(u"IMPERIALARAMAIC"));
        const Character::JUnicodeBlock Character::UnicodeBlock::PHOENICIAN = Character::UnicodeBlock::createUnicodeBlock(jstr(u"PHOENICIAN"));
        const Character::JUnicodeBlock Character::UnicodeBlock::LYDIAN = Character::UnicodeBlock::createUnicodeBlock(jstr(u"LYDIAN"));
        const Character::JUnicodeBlock Character::UnicodeBlock::KHAROSHTHI = Character::UnicodeBlock::createUnicodeBlock(jstr(u"KHAROSHTHI"));
        const Character::JUnicodeBlock Character::UnicodeBlock::OLD_SOUTH_ARABIAN = Character::UnicodeBlock::createUnicodeBlock(jstr(u"OLD_SOUTH_ARABIAN"), jstr(u"OLD SOUTH ARABIAN"), jstr(u"OLDSOUTHARABIAN"));
        const Character::JUnicodeBlock Character::UnicodeBlock::AVESTAN = Character::UnicodeBlock::createUnicodeBlock(jstr(u"AVESTAN"));
        const Character::JUnicodeBlock Character::UnicodeBlock::INSCRIPTIONAL_PARTHIAN = Character::UnicodeBlock::createUnicodeBlock(jstr(u"INSCRIPTIONAL_PARTHIAN"), jstr(u"INSCRIPTIONAL PARTHIAN"), jstr(u"INSCRIPTIONALPARTHIAN"));
        const Character::JUnicodeBlock Character::UnicodeBlock::INSCRIPTIONAL_PAHLAVI = Character::UnicodeBlock::createUnicodeBlock(jstr(u"INSCRIPTIONAL_PAHLAVI"), jstr(u"INSCRIPTIONAL PAHLAVI"), jstr(u"INSCRIPTIONALPAHLAVI"));
        const Character::JUnicodeBlock Character::UnicodeBlock::OLD_TURKIC = Character::UnicodeBlock::createUnicodeBlock(jstr(u"OLD_TURKIC"), jstr(u"OLD TURKIC"), jstr(u"OLDTURKIC"));
        const Character::JUnicodeBlock Character::UnicodeBlock::RUMI_NUMERAL_SYMBOLS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"RUMI_NUMERAL_SYMBOLS"), jstr(u"RUMI NUMERAL SYMBOLS"), jstr(u"RUMINUMERALSYMBOLS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::BRAHMI = Character::UnicodeBlock::createUnicodeBlock(jstr(u"BRAHMI"));
        const Character::JUnicodeBlock Character::UnicodeBlock::KAITHI = Character::UnicodeBlock::createUnicodeBlock(jstr(u"KAITHI"));
        const Character::JUnicodeBlock Character::UnicodeBlock::CUNEIFORM = Character::UnicodeBlock::createUnicodeBlock(jstr(u"CUNEIFORM"));
        const Character::JUnicodeBlock Character::UnicodeBlock::CUNEIFORM_NUMBERS_AND_PUNCTUATION = Character::UnicodeBlock::createUnicodeBlock(jstr(u"CUNEIFORM_NUMBERS_AND_PUNCTUATION"), jstr(u"CUNEIFORM NUMBERS AND PUNCTUATION"), jstr(u"CUNEIFORMNUMBERSANDPUNCTUATION"));
        const Character::JUnicodeBlock Character::UnicodeBlock::EGYPTIAN_HIEROGLYPHS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"EGYPTIAN_HIEROGLYPHS"), jstr(u"EGYPTIAN HIEROGLYPHS"), jstr(u"EGYPTIANHIEROGLYPHS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::BAMUM_SUPPLEMENT = Character::UnicodeBlock::createUnicodeBlock(jstr(u"BAMUM_SUPPLEMENT"), jstr(u"BAMUM SUPPLEMENT"), jstr(u"BAMUMSUPPLEMENT"));
        const Character::JUnicodeBlock Character::UnicodeBlock::KANA_SUPPLEMENT = Character::UnicodeBlock::createUnicodeBlock(jstr(u"KANA_SUPPLEMENT"), jstr(u"KANA SUPPLEMENT"), jstr(u"KANASUPPLEMENT"));
        const Character::JUnicodeBlock Character::UnicodeBlock::ANCIENT_GREEK_MUSICAL_NOTATION = Character::UnicodeBlock::createUnicodeBlock(jstr(u"ANCIENT_GREEK_MUSICAL_NOTATION"), jstr(u"ANCIENT GREEK MUSICAL NOTATION"), jstr(u"ANCIENTGREEKMUSICALNOTATION"));
        const Character::JUnicodeBlock Character::UnicodeBlock::COUNTING_ROD_NUMERALS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"COUNTING_ROD_NUMERALS"), jstr(u"COUNTING ROD NUMERALS"), jstr(u"COUNTINGRODNUMERALS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::MAHJONG_TILES = Character::UnicodeBlock::createUnicodeBlock(jstr(u"MAHJONG_TILES"), jstr(u"MAHJONG TILES"), jstr(u"MAHJONGTILES"));
        const Character::JUnicodeBlock Character::UnicodeBlock::DOMINO_TILES = Character::UnicodeBlock::createUnicodeBlock(jstr(u"DOMINO_TILES"), jstr(u"DOMINO TILES"), jstr(u"DOMINOTILES"));
        const Character::JUnicodeBlock Character::UnicodeBlock::PLAYING_CARDS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"PLAYING_CARDS"), jstr(u"PLAYING CARDS"), jstr(u"PLAYINGCARDS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::ENCLOSED_ALPHANUMERIC_SUPPLEMENT = Character::UnicodeBlock::createUnicodeBlock(jstr(u"ENCLOSED_ALPHANUMERIC_SUPPLEMENT"), jstr(u"ENCLOSED ALPHANUMERIC SUPPLEMENT"), jstr(u"ENCLOSEDALPHANUMERICSUPPLEMENT"));
        const Character::JUnicodeBlock Character::UnicodeBlock::ENCLOSED_IDEOGRAPHIC_SUPPLEMENT = Character::UnicodeBlock::createUnicodeBlock(jstr(u"ENCLOSED_IDEOGRAPHIC_SUPPLEMENT"), jstr(u"ENCLOSED IDEOGRAPHIC SUPPLEMENT"), jstr(u"ENCLOSEDIDEOGRAPHICSUPPLEMENT"));
        const Character::JUnicodeBlock Character::UnicodeBlock::MISCELLANEOUS_SYMBOLS_AND_PICTOGRAPHS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"MISCELLANEOUS_SYMBOLS_AND_PICTOGRAPHS"), jstr(u"MISCELLANEOUS SYMBOLS AND PICTOGRAPHS"), jstr(u"MISCELLANEOUSSYMBOLSANDPICTOGRAPHS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::EMOTICONS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"EMOTICONS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::TRANSPORT_AND_MAP_SYMBOLS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"TRANSPORT_AND_MAP_SYMBOLS"), jstr(u"TRANSPORT AND MAP SYMBOLS"), jstr(u"TRANSPORTANDMAPSYMBOLS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::ALCHEMICAL_SYMBOLS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"ALCHEMICAL_SYMBOLS"), jstr(u"ALCHEMICAL SYMBOLS"), jstr(u"ALCHEMICALSYMBOLS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::CJK_UNIFIED_IDEOGRAPHS_EXTENSION_C = Character::UnicodeBlock::createUnicodeBlock(jstr(u"CJK_UNIFIED_IDEOGRAPHS_EXTENSION_C"), jstr(u"CJK UNIFIED IDEOGRAPHS EXTENSION C"), jstr(u"CJKUNIFIEDIDEOGRAPHSEXTENSIONC"));
        const Character::JUnicodeBlock Character::UnicodeBlock::CJK_UNIFIED_IDEOGRAPHS_EXTENSION_D = Character::UnicodeBlock::createUnicodeBlock(jstr(u"CJK_UNIFIED_IDEOGRAPHS_EXTENSION_D"), jstr(u"CJK UNIFIED IDEOGRAPHS EXTENSION D"), jstr(u"CJKUNIFIEDIDEOGRAPHSEXTENSIOND"));
        const Character::JUnicodeBlock Character::UnicodeBlock::ARABIC_EXTENDED_A = Character::UnicodeBlock::createUnicodeBlock(jstr(u"ARABIC_EXTENDED_A"), jstr(u"ARABIC EXTENDED-A"), jstr(u"ARABICEXTENDED-A"));
        const Character::JUnicodeBlock Character::UnicodeBlock::SUNDANESE_SUPPLEMENT = Character::UnicodeBlock::createUnicodeBlock(jstr(u"SUNDANESE_SUPPLEMENT"), jstr(u"SUNDANESE SUPPLEMENT"), jstr(u"SUNDANESESUPPLEMENT"));
        const Character::JUnicodeBlock Character::UnicodeBlock::MEETEI_MAYEK_EXTENSIONS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"MEETEI_MAYEK_EXTENSIONS"), jstr(u"MEETEI MAYEK EXTENSIONS"), jstr(u"MEETEIMAYEKEXTENSIONS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::MEROITIC_HIEROGLYPHS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"MEROITIC_HIEROGLYPHS"), jstr(u"MEROITIC HIEROGLYPHS"), jstr(u"MEROITICHIEROGLYPHS"));
        const Character::JUnicodeBlock Character::UnicodeBlock::MEROITIC_CURSIVE = Character::UnicodeBlock::createUnicodeBlock(jstr(u"MEROITIC_CURSIVE"), jstr(u"MEROITIC CURSIVE"), jstr(u"MEROITICCURSIVE"));
        const Character::JUnicodeBlock Character::UnicodeBlock::SORA_SOMPENG = Character::UnicodeBlock::createUnicodeBlock(jstr(u"SORA_SOMPENG"), jstr(u"SORA SOMPENG"), jstr(u"SORASOMPENG"));
        const Character::JUnicodeBlock Character::UnicodeBlock::CHAKMA = Character::UnicodeBlock::createUnicodeBlock(jstr(u"CHAKMA"));
        const Character::JUnicodeBlock Character::UnicodeBlock::SHARADA = Character::UnicodeBlock::createUnicodeBlock(jstr(u"SHARADA"));
        const Character::JUnicodeBlock Character::UnicodeBlock::TAKRI = Character::UnicodeBlock::createUnicodeBlock(jstr(u"TAKRI"));
        const Character::JUnicodeBlock Character::UnicodeBlock::MIAO = Character::UnicodeBlock::createUnicodeBlock(jstr(u"MIAO"));
        const Character::JUnicodeBlock Character::UnicodeBlock::ARABIC_MATHEMATICAL_ALPHABETIC_SYMBOLS = Character::UnicodeBlock::createUnicodeBlock(jstr(u"ARABIC_MATHEMATICAL_ALPHABETIC_SYMBOLS"), jstr(u"ARABIC MATHEMATICAL ALPHABETIC SYMBOLS"), jstr(u"ARABICMATHEMATICALALPHABETICSYMBOLS"));
        
        const jarray<jint> Character::UnicodeBlock::blockStarts = {
            0x0000,   // 0000..007F; Basic Latin
            0x0080,   // 0080..00FF; Latin-1 Supplement
            0x0100,   // 0100..017F; Latin Extended-A
            0x0180,   // 0180..024F; Latin Extended-B
            0x0250,   // 0250..02AF; IPA Extensions
            0x02B0,   // 02B0..02FF; Spacing Modifier Letters
            0x0300,   // 0300..036F; Combining Diacritical Marks
            0x0370,   // 0370..03FF; Greek and Coptic
            0x0400,   // 0400..04FF; Cyrillic
            0x0500,   // 0500..052F; Cyrillic Supplement
            0x0530,   // 0530..058F; Armenian
            0x0590,   // 0590..05FF; Hebrew
            0x0600,   // 0600..06FF; Arabic
            0x0700,   // 0700..074F; Syriac
            0x0750,   // 0750..077F; Arabic Supplement
            0x0780,   // 0780..07BF; Thaana
            0x07C0,   // 07C0..07FF; NKo
            0x0800,   // 0800..083F; Samaritan
            0x0840,   // 0840..085F; Mandaic
            0x0860,   //             unassigned
            0x08A0,   // 08A0..08FF; Arabic Extended-A
            0x0900,   // 0900..097F; Devanagari
            0x0980,   // 0980..09FF; Bengali
            0x0A00,   // 0A00..0A7F; Gurmukhi
            0x0A80,   // 0A80..0AFF; Gujarati
            0x0B00,   // 0B00..0B7F; Oriya
            0x0B80,   // 0B80..0BFF; Tamil
            0x0C00,   // 0C00..0C7F; Telugu
            0x0C80,   // 0C80..0CFF; Kannada
            0x0D00,   // 0D00..0D7F; Malayalam
            0x0D80,   // 0D80..0DFF; Sinhala
            0x0E00,   // 0E00..0E7F; Thai
            0x0E80,   // 0E80..0EFF; Lao
            0x0F00,   // 0F00..0FFF; Tibetan
            0x1000,   // 1000..109F; Myanmar
            0x10A0,   // 10A0..10FF; Georgian
            0x1100,   // 1100..11FF; Hangul Jamo
            0x1200,   // 1200..137F; Ethiopic
            0x1380,   // 1380..139F; Ethiopic Supplement
            0x13A0,   // 13A0..13FF; Cherokee
            0x1400,   // 1400..167F; Unified Canadian Aboriginal Syllabics
            0x1680,   // 1680..169F; Ogham
            0x16A0,   // 16A0..16FF; Runic
            0x1700,   // 1700..171F; Tagalog
            0x1720,   // 1720..173F; Hanunoo
            0x1740,   // 1740..175F; Buhid
            0x1760,   // 1760..177F; Tagbanwa
            0x1780,   // 1780..17FF; Khmer
            0x1800,   // 1800..18AF; Mongolian
            0x18B0,   // 18B0..18FF; Unified Canadian Aboriginal Syllabics Extended
            0x1900,   // 1900..194F; Limbu
            0x1950,   // 1950..197F; Tai Le
            0x1980,   // 1980..19DF; New Tai Lue
            0x19E0,   // 19E0..19FF; Khmer Symbols
            0x1A00,   // 1A00..1A1F; Buginese
            0x1A20,   // 1A20..1AAF; Tai Tham
            0x1AB0,   //             unassigned
            0x1B00,   // 1B00..1B7F; Balinese
            0x1B80,   // 1B80..1BBF; Sundanese
            0x1BC0,   // 1BC0..1BFF; Batak
            0x1C00,   // 1C00..1C4F; Lepcha
            0x1C50,   // 1C50..1C7F; Ol Chiki
            0x1C80,   //             unassigned
            0x1CC0,   // 1CC0..1CCF; Sundanese Supplement
            0x1CD0,   // 1CD0..1CFF; Vedic Extensions
            0x1D00,   // 1D00..1D7F; Phonetic Extensions
            0x1D80,   // 1D80..1DBF; Phonetic Extensions Supplement
            0x1DC0,   // 1DC0..1DFF; Combining Diacritical Marks Supplement
            0x1E00,   // 1E00..1EFF; Latin Extended Additional
            0x1F00,   // 1F00..1FFF; Greek Extended
            0x2000,   // 2000..206F; General Punctuation
            0x2070,   // 2070..209F; Superscripts and Subscripts
            0x20A0,   // 20A0..20CF; Currency Symbols
            0x20D0,   // 20D0..20FF; Combining Diacritical Marks for Symbols
            0x2100,   // 2100..214F; Letterlike Symbols
            0x2150,   // 2150..218F; Number Forms
            0x2190,   // 2190..21FF; Arrows
            0x2200,   // 2200..22FF; Mathematical Operators
            0x2300,   // 2300..23FF; Miscellaneous Technical
            0x2400,   // 2400..243F; Control Pictures
            0x2440,   // 2440..245F; Optical Character Recognition
            0x2460,   // 2460..24FF; Enclosed Alphanumerics
            0x2500,   // 2500..257F; Box Drawing
            0x2580,   // 2580..259F; Block Elements
            0x25A0,   // 25A0..25FF; Geometric Shapes
            0x2600,   // 2600..26FF; Miscellaneous Symbols
            0x2700,   // 2700..27BF; Dingbats
            0x27C0,   // 27C0..27EF; Miscellaneous Mathematical Symbols-A
            0x27F0,   // 27F0..27FF; Supplemental Arrows-A
            0x2800,   // 2800..28FF; Braille Patterns
            0x2900,   // 2900..297F; Supplemental Arrows-B
            0x2980,   // 2980..29FF; Miscellaneous Mathematical Symbols-B
            0x2A00,   // 2A00..2AFF; Supplemental Mathematical Operators
            0x2B00,   // 2B00..2BFF; Miscellaneous Symbols and Arrows
            0x2C00,   // 2C00..2C5F; Glagolitic
            0x2C60,   // 2C60..2C7F; Latin Extended-C
            0x2C80,   // 2C80..2CFF; Coptic
            0x2D00,   // 2D00..2D2F; Georgian Supplement
            0x2D30,   // 2D30..2D7F; Tifinagh
            0x2D80,   // 2D80..2DDF; Ethiopic Extended
            0x2DE0,   // 2DE0..2DFF; Cyrillic Extended-A
            0x2E00,   // 2E00..2E7F; Supplemental Punctuation
            0x2E80,   // 2E80..2EFF; CJK Radicals Supplement
            0x2F00,   // 2F00..2FDF; Kangxi Radicals
            0x2FE0,   //             unassigned
            0x2FF0,   // 2FF0..2FFF; Ideographic Description Characters
            0x3000,   // 3000..303F; CJK Symbols and Punctuation
            0x3040,   // 3040..309F; Hiragana
            0x30A0,   // 30A0..30FF; Katakana
            0x3100,   // 3100..312F; Bopomofo
            0x3130,   // 3130..318F; Hangul Compatibility Jamo
            0x3190,   // 3190..319F; Kanbun
            0x31A0,   // 31A0..31BF; Bopomofo Extended
            0x31C0,   // 31C0..31EF; CJK Strokes
            0x31F0,   // 31F0..31FF; Katakana Phonetic Extensions
            0x3200,   // 3200..32FF; Enclosed CJK Letters and Months
            0x3300,   // 3300..33FF; CJK Compatibility
            0x3400,   // 3400..4DBF; CJK Unified Ideographs Extension A
            0x4DC0,   // 4DC0..4DFF; Yijing Hexagram Symbols
            0x4E00,   // 4E00..9FFF; CJK Unified Ideographs
            0xA000,   // A000..A48F; Yi Syllables
            0xA490,   // A490..A4CF; Yi Radicals
            0xA4D0,   // A4D0..A4FF; Lisu
            0xA500,   // A500..A63F; Vai
            0xA640,   // A640..A69F; Cyrillic Extended-B
            0xA6A0,   // A6A0..A6FF; Bamum
            0xA700,   // A700..A71F; Modifier Tone Letters
            0xA720,   // A720..A7FF; Latin Extended-D
            0xA800,   // A800..A82F; Syloti Nagri
            0xA830,   // A830..A83F; Common Indic Number Forms
            0xA840,   // A840..A87F; Phags-pa
            0xA880,   // A880..A8DF; Saurashtra
            0xA8E0,   // A8E0..A8FF; Devanagari Extended
            0xA900,   // A900..A92F; Kayah Li
            0xA930,   // A930..A95F; Rejang
            0xA960,   // A960..A97F; Hangul Jamo Extended-A
            0xA980,   // A980..A9DF; Javanese
            0xA9E0,   //             unassigned
            0xAA00,   // AA00..AA5F; Cham
            0xAA60,   // AA60..AA7F; Myanmar Extended-A
            0xAA80,   // AA80..AADF; Tai Viet
            0xAAE0,   // AAE0..AAFF; Meetei Mayek Extensions
            0xAB00,   // AB00..AB2F; Ethiopic Extended-A
            0xAB30,   //             unassigned
            0xABC0,   // ABC0..ABFF; Meetei Mayek
            0xAC00,   // AC00..D7AF; Hangul Syllables
            0xD7B0,   // D7B0..D7FF; Hangul Jamo Extended-B
            0xD800,   // D800..DB7F; High Surrogates
            0xDB80,   // DB80..DBFF; High Private Use Surrogates
            0xDC00,   // DC00..DFFF; Low Surrogates
            0xE000,   // E000..F8FF; Private Use Area
            0xF900,   // F900..FAFF; CJK Compatibility Ideographs
            0xFB00,   // FB00..FB4F; Alphabetic Presentation Forms
            0xFB50,   // FB50..FDFF; Arabic Presentation Forms-A
            0xFE00,   // FE00..FE0F; Variation Selectors
            0xFE10,   // FE10..FE1F; Vertical Forms
            0xFE20,   // FE20..FE2F; Combining Half Marks
            0xFE30,   // FE30..FE4F; CJK Compatibility Forms
            0xFE50,   // FE50..FE6F; Small Form Variants
            0xFE70,   // FE70..FEFF; Arabic Presentation Forms-B
            0xFF00,   // FF00..FFEF; Halfwidth and Fullwidth Forms
            0xFFF0,   // FFF0..FFFF; Specials
            0x10000,  // 10000..1007F; Linear B Syllabary
            0x10080,  // 10080..100FF; Linear B Ideograms
            0x10100,  // 10100..1013F; Aegean Numbers
            0x10140,  // 10140..1018F; Ancient Greek Numbers
            0x10190,  // 10190..101CF; Ancient Symbols
            0x101D0,  // 101D0..101FF; Phaistos Disc
            0x10200,  //               unassigned
            0x10280,  // 10280..1029F; Lycian
            0x102A0,  // 102A0..102DF; Carian
            0x102E0,  //               unassigned
            0x10300,  // 10300..1032F; Old Italic
            0x10330,  // 10330..1034F; Gothic
            0x10350,  //               unassigned
            0x10380,  // 10380..1039F; Ugaritic
            0x103A0,  // 103A0..103DF; Old Persian
            0x103E0,  //               unassigned
            0x10400,  // 10400..1044F; Deseret
            0x10450,  // 10450..1047F; Shavian
            0x10480,  // 10480..104AF; Osmanya
            0x104B0,  //               unassigned
            0x10800,  // 10800..1083F; Cypriot Syllabary
            0x10840,  // 10840..1085F; Imperial Aramaic
            0x10860,  //               unassigned
            0x10900,  // 10900..1091F; Phoenician
            0x10920,  // 10920..1093F; Lydian
            0x10940,  //               unassigned
            0x10980,  // 10980..1099F; Meroitic Hieroglyphs
            0x109A0,  // 109A0..109FF; Meroitic Cursive
            0x10A00,  // 10A00..10A5F; Kharoshthi
            0x10A60,  // 10A60..10A7F; Old South Arabian
            0x10A80,  //               unassigned
            0x10B00,  // 10B00..10B3F; Avestan
            0x10B40,  // 10B40..10B5F; Inscriptional Parthian
            0x10B60,  // 10B60..10B7F; Inscriptional Pahlavi
            0x10B80,  //               unassigned
            0x10C00,  // 10C00..10C4F; Old Turkic
            0x10C50,  //               unassigned
            0x10E60,  // 10E60..10E7F; Rumi Numeral Symbols
            0x10E80,  //               unassigned
            0x11000,  // 11000..1107F; Brahmi
            0x11080,  // 11080..110CF; Kaithi
            0x110D0,  // 110D0..110FF; Sora Sompeng
            0x11100,  // 11100..1114F; Chakma
            0x11150,  //               unassigned
            0x11180,  // 11180..111DF; Sharada
            0x111E0,  //               unassigned
            0x11680,  // 11680..116CF; Takri
            0x116D0,  //               unassigned
            0x12000,  // 12000..123FF; Cuneiform
            0x12400,  // 12400..1247F; Cuneiform Numbers and Punctuation
            0x12480,  //               unassigned
            0x13000,  // 13000..1342F; Egyptian Hieroglyphs
            0x13430,  //               unassigned
            0x16800,  // 16800..16A3F; Bamum Supplement
            0x16A40,  //               unassigned
            0x16F00,  // 16F00..16F9F; Miao
            0x16FA0,  //               unassigned
            0x1B000,  // 1B000..1B0FF; Kana Supplement
            0x1B100,  //               unassigned
            0x1D000,  // 1D000..1D0FF; Byzantine Musical Symbols
            0x1D100,  // 1D100..1D1FF; Musical Symbols
            0x1D200,  // 1D200..1D24F; Ancient Greek Musical Notation
            0x1D250,  //               unassigned
            0x1D300,  // 1D300..1D35F; Tai Xuan Jing Symbols
            0x1D360,  // 1D360..1D37F; Counting Rod Numerals
            0x1D380,  //               unassigned
            0x1D400,  // 1D400..1D7FF; Mathematical Alphanumeric Symbols
            0x1D800,  //               unassigned
            0x1EE00,  // 1EE00..1EEFF; Arabic Mathematical Alphabetic Symbols
            0x1EF00,  //               unassigned
            0x1F000,  // 1F000..1F02F; Mahjong Tiles
            0x1F030,  // 1F030..1F09F; Domino Tiles
            0x1F0A0,  // 1F0A0..1F0FF; Playing Cards
            0x1F100,  // 1F100..1F1FF; Enclosed Alphanumeric Supplement
            0x1F200,  // 1F200..1F2FF; Enclosed Ideographic Supplement
            0x1F300,  // 1F300..1F5FF; Miscellaneous Symbols And Pictographs
            0x1F600,  // 1F600..1F64F; Emoticons
            0x1F650,  //               unassigned
            0x1F680,  // 1F680..1F6FF; Transport And Map Symbols
            0x1F700,  // 1F700..1F77F; Alchemical Symbols
            0x1F780,  //               unassigned
            0x20000,  // 20000..2A6DF; CJK Unified Ideographs Extension B
            0x2A6E0,  //               unassigned
            0x2A700,  // 2A700..2B73F; CJK Unified Ideographs Extension C
            0x2B740,  // 2B740..2B81F; CJK Unified Ideographs Extension D
            0x2B820,  //               unassigned
            0x2F800,  // 2F800..2FA1F; CJK Compatibility Ideographs Supplement
            0x2FA20,  //               unassigned
            0xE0000,  // E0000..E007F; Tags
            0xE0080,  //               unassigned
            0xE0100,  // E0100..E01EF; Variation Selectors Supplement
            0xE01F0,  //               unassigned
            0xF0000,  // F0000..FFFFF; Supplementary Private Use Area-A
            0x100000  // 100000..10FFFF; Supplementary Private Use Area-B
        };
        
        const jarray<Character::JUnicodeBlock> Character::UnicodeBlock::blocks = {
            BASIC_LATIN,
            LATIN_1_SUPPLEMENT,
            LATIN_EXTENDED_A,
            LATIN_EXTENDED_B,
            IPA_EXTENSIONS,
            SPACING_MODIFIER_LETTERS,
            COMBINING_DIACRITICAL_MARKS,
            GREEK,
            CYRILLIC,
            CYRILLIC_SUPPLEMENTARY,
            ARMENIAN,
            HEBREW,
            ARABIC,
            SYRIAC,
            ARABIC_SUPPLEMENT,
            THAANA,
            NKO,
            SAMARITAN,
            MANDAIC,
            nullptr,
            ARABIC_EXTENDED_A,
            DEVANAGARI,
            BENGALI,
            GURMUKHI,
            GUJARATI,
            ORIYA,
            TAMIL,
            TELUGU,
            KANNADA,
            MALAYALAM,
            SINHALA,
            THAI,
            LAO,
            TIBETAN,
            MYANMAR,
            GEORGIAN,
            HANGUL_JAMO,
            ETHIOPIC,
            ETHIOPIC_SUPPLEMENT,
            CHEROKEE,
            UNIFIED_CANADIAN_ABORIGINAL_SYLLABICS,
            OGHAM,
            RUNIC,
            TAGALOG,
            HANUNOO,
            BUHID,
            TAGBANWA,
            KHMER,
            MONGOLIAN,
            UNIFIED_CANADIAN_ABORIGINAL_SYLLABICS_EXTENDED,
            LIMBU,
            TAI_LE,
            NEW_TAI_LUE,
            KHMER_SYMBOLS,
            BUGINESE,
            TAI_THAM,
            nullptr,
            BALINESE,
            SUNDANESE,
            BATAK,
            LEPCHA,
            OL_CHIKI,
            nullptr,
            SUNDANESE_SUPPLEMENT,
            VEDIC_EXTENSIONS,
            PHONETIC_EXTENSIONS,
            PHONETIC_EXTENSIONS_SUPPLEMENT,
            COMBINING_DIACRITICAL_MARKS_SUPPLEMENT,
            LATIN_EXTENDED_ADDITIONAL,
            GREEK_EXTENDED,
            GENERAL_PUNCTUATION,
            SUPERSCRIPTS_AND_SUBSCRIPTS,
            CURRENCY_SYMBOLS,
            COMBINING_MARKS_FOR_SYMBOLS,
            LETTERLIKE_SYMBOLS,
            NUMBER_FORMS,
            ARROWS,
            MATHEMATICAL_OPERATORS,
            MISCELLANEOUS_TECHNICAL,
            CONTROL_PICTURES,
            OPTICAL_CHARACTER_RECOGNITION,
            ENCLOSED_ALPHANUMERICS,
            BOX_DRAWING,
            BLOCK_ELEMENTS,
            GEOMETRIC_SHAPES,
            MISCELLANEOUS_SYMBOLS,
            DINGBATS,
            MISCELLANEOUS_MATHEMATICAL_SYMBOLS_A,
            SUPPLEMENTAL_ARROWS_A,
            BRAILLE_PATTERNS,
            SUPPLEMENTAL_ARROWS_B,
            MISCELLANEOUS_MATHEMATICAL_SYMBOLS_B,
            SUPPLEMENTAL_MATHEMATICAL_OPERATORS,
            MISCELLANEOUS_SYMBOLS_AND_ARROWS,
            GLAGOLITIC,
            LATIN_EXTENDED_C,
            COPTIC,
            GEORGIAN_SUPPLEMENT,
            TIFINAGH,
            ETHIOPIC_EXTENDED,
            CYRILLIC_EXTENDED_A,
            SUPPLEMENTAL_PUNCTUATION,
            CJK_RADICALS_SUPPLEMENT,
            KANGXI_RADICALS,
            nullptr,
            IDEOGRAPHIC_DESCRIPTION_CHARACTERS,
            CJK_SYMBOLS_AND_PUNCTUATION,
            HIRAGANA,
            KATAKANA,
            BOPOMOFO,
            HANGUL_COMPATIBILITY_JAMO,
            KANBUN,
            BOPOMOFO_EXTENDED,
            CJK_STROKES,
            KATAKANA_PHONETIC_EXTENSIONS,
            ENCLOSED_CJK_LETTERS_AND_MONTHS,
            CJK_COMPATIBILITY,
            CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A,
            YIJING_HEXAGRAM_SYMBOLS,
            CJK_UNIFIED_IDEOGRAPHS,
            YI_SYLLABLES,
            YI_RADICALS,
            LISU,
            VAI,
            CYRILLIC_EXTENDED_B,
            BAMUM,
            MODIFIER_TONE_LETTERS,
            LATIN_EXTENDED_D,
            SYLOTI_NAGRI,
            COMMON_INDIC_NUMBER_FORMS,
            PHAGS_PA,
            SAURASHTRA,
            DEVANAGARI_EXTENDED,
            KAYAH_LI,
            REJANG,
            HANGUL_JAMO_EXTENDED_A,
            JAVANESE,
            nullptr,
            CHAM,
            MYANMAR_EXTENDED_A,
            TAI_VIET,
            MEETEI_MAYEK_EXTENSIONS,
            ETHIOPIC_EXTENDED_A,
            nullptr,
            MEETEI_MAYEK,
            HANGUL_SYLLABLES,
            HANGUL_JAMO_EXTENDED_B,
            HIGH_SURROGATES,
            HIGH_PRIVATE_USE_SURROGATES,
            LOW_SURROGATES,
            PRIVATE_USE_AREA,
            CJK_COMPATIBILITY_IDEOGRAPHS,
            ALPHABETIC_PRESENTATION_FORMS,
            ARABIC_PRESENTATION_FORMS_A,
            VARIATION_SELECTORS,
            VERTICAL_FORMS,
            COMBINING_HALF_MARKS,
            CJK_COMPATIBILITY_FORMS,
            SMALL_FORM_VARIANTS,
            ARABIC_PRESENTATION_FORMS_B,
            HALFWIDTH_AND_FULLWIDTH_FORMS,
            SPECIALS,
            LINEAR_B_SYLLABARY,
            LINEAR_B_IDEOGRAMS,
            AEGEAN_NUMBERS,
            ANCIENT_GREEK_NUMBERS,
            ANCIENT_SYMBOLS,
            PHAISTOS_DISC,
            nullptr,
            LYCIAN,
            CARIAN,
            nullptr,
            OLD_ITALIC,
            GOTHIC,
            nullptr,
            UGARITIC,
            OLD_PERSIAN,
            nullptr,
            DESERET,
            SHAVIAN,
            OSMANYA,
            nullptr,
            CYPRIOT_SYLLABARY,
            IMPERIAL_ARAMAIC,
            nullptr,
            PHOENICIAN,
            LYDIAN,
            nullptr,
            MEROITIC_HIEROGLYPHS,
            MEROITIC_CURSIVE,
            KHAROSHTHI,
            OLD_SOUTH_ARABIAN,
            nullptr,
            AVESTAN,
            INSCRIPTIONAL_PARTHIAN,
            INSCRIPTIONAL_PAHLAVI,
            nullptr,
            OLD_TURKIC,
            nullptr,
            RUMI_NUMERAL_SYMBOLS,
            nullptr,
            BRAHMI,
            KAITHI,
            SORA_SOMPENG,
            CHAKMA,
            nullptr,
            SHARADA,
            nullptr,
            TAKRI,
            nullptr,
            CUNEIFORM,
            CUNEIFORM_NUMBERS_AND_PUNCTUATION,
            nullptr,
            EGYPTIAN_HIEROGLYPHS,
            nullptr,
            BAMUM_SUPPLEMENT,
            nullptr,
            MIAO,
            nullptr,
            KANA_SUPPLEMENT,
            nullptr,
            BYZANTINE_MUSICAL_SYMBOLS,
            MUSICAL_SYMBOLS,
            ANCIENT_GREEK_MUSICAL_NOTATION,
            nullptr,
            TAI_XUAN_JING_SYMBOLS,
            COUNTING_ROD_NUMERALS,
            nullptr,
            MATHEMATICAL_ALPHANUMERIC_SYMBOLS,
            nullptr,
            ARABIC_MATHEMATICAL_ALPHABETIC_SYMBOLS,
            nullptr,
            MAHJONG_TILES,
            DOMINO_TILES,
            PLAYING_CARDS,
            ENCLOSED_ALPHANUMERIC_SUPPLEMENT,
            ENCLOSED_IDEOGRAPHIC_SUPPLEMENT,
            MISCELLANEOUS_SYMBOLS_AND_PICTOGRAPHS,
            EMOTICONS,
            nullptr,
            TRANSPORT_AND_MAP_SYMBOLS,
            ALCHEMICAL_SYMBOLS,
            nullptr,
            CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B,
            nullptr,
            CJK_UNIFIED_IDEOGRAPHS_EXTENSION_C,
            CJK_UNIFIED_IDEOGRAPHS_EXTENSION_D,
            nullptr,
            CJK_COMPATIBILITY_IDEOGRAPHS_SUPPLEMENT,
            nullptr,
            TAGS,
            nullptr,
            VARIATION_SELECTORS_SUPPLEMENT,
            nullptr,
            SUPPLEMENTARY_PRIVATE_USE_AREA_A,
            SUPPLEMENTARY_PRIVATE_USE_AREA_B
        };
        
        Character::JUnicodeBlock Character::UnicodeBlock::of(jchar c)
        {
            return of(static_cast<jint>(c));
        }
        
        Character::JUnicodeBlock Character::UnicodeBlock::of(jint codePoint)
        {
            if (!isValidCodePoint(codePoint)) {
                throw IllegalArgumentException();
            }

            jint top, bottom, current;
            bottom = 0;
            top = blockStarts.length();
            current = top/2;

            // invariant: top > current >= bottom && codePoint >= unicodeBlockStarts[bottom]
            while (top - bottom > 1) {
                if (codePoint >= blockStarts[current]) {
                    bottom = current;
                } else {
                    top = current;
                }
                current = (top + bottom) / 2;
            }
            return blocks[current];
        }
        
        const Character::JUnicodeBlock Character::UnicodeBlock::forName(JString blockName)
        {
            // TODO: JUnicodeBlock block = map()->get(blockName->toUpperCase(Locale::US));
            JUnicodeBlock block = map()->get(blockName);
            if (block == nullptr) {
                throw IllegalArgumentException();
            }
            return block;
        }
        
        jarray<JCharacter> Character::CharacterCache::cache(127 + 1);

        jboolean Character::CharacterCache::isStaticInitializationBlock = StaticInitializationBlock();
        
        jboolean Character::CharacterCache::StaticInitializationBlock()
        {
            for (jint i = 0; i < cache.length(); i++)
                cache[i] = jnew<Character>(static_cast<jchar>(i));
            return true;
        }
    }
}
