#ifndef CPP_LANG_CHARACTER_H
#define CPP_LANG_CHARACTER_H

#include "Object.h"
#include "Array.h"
#include "Comparable.h"
#include "CharSequence.h"
#include "Enum.h"

namespace cpp
{
    namespace lang
    {
        class Character : public Comparable<JCharacter>
        {
        public:
            class Subset;
            using JSubset = std::shared_ptr<Subset>;
            
            class UnicodeBlock;
            using JUnicodeBlock = std::shared_ptr<UnicodeBlock>;
        
            Character();
            Character(jchar pvalue);
            ~Character();

            static jint charCount(jint codePoint);
            jchar charValue();
            static jint codePointAt(jarray<jchar> a, jint index);
            static jint codePointAt(jarray<jchar> a, jint index, jint limit);
            static jint codePointAt(JCharSequence seq, jint index);
            static jint codePointBefore(jarray<jchar> a, jint index);
            static jint codePointBefore(jarray<jchar> a, jint index, jint start);
            static jint codePointBefore(JCharSequence seq, jint index);
            static jint codePointCount(jarray<jchar> a, jint offset, jint count);
            static jint codePointCount(JCharSequence seq, jint beginIndex, jint endIndex);
            static jint compare(jchar x, jchar y);
            jint compareTo(JCharacter anotherCharacter);
            static jint digit(jchar ch, jint radix);
            static jint digit(jint codePoint, jint radix);
            jboolean equals(JObject obj);
            static jchar forDigit(jint digit, jint radix);
            static jbyte getDirectionality(jchar ch);
            static jbyte getDirectionality(jint codePoint);
            static jint getNumericValue(jchar ch);
            static jint getNumericValue(jint codePoint);
            static jint getType(jchar ch);
            static jint getType(jint codePoint);
            jint hashCode();
            static jint hashCode(jchar value);
            static jchar highSurrogate(jint codePoint);
            static jboolean isAlphabetic(jint codePoint);
            static jboolean isBmpCodePoint(jint codePoint);
            static jboolean isDefined(jchar ch);
            static jboolean isDefined(jint codePoint);
            static jboolean isDigit(jchar ch);
            static jboolean isDigit(jint codePoint);
            static jboolean isHighSurrogate(jchar ch);
            static jboolean isIdentifierIgnorable(jchar ch);
            static jboolean isIdentifierIgnorable(jint codePoint);
            static jboolean isIdeographic(jint codePoint);
            static jboolean isISOControl(jchar ch);
            static jboolean isISOControl(jint codePoint);
            static jboolean isJavaIdentifierPart(jchar ch);
            static jboolean isJavaIdentifierPart(jint codePoint);
            static jboolean isJavaIdentifierStart(jchar ch);
            static jboolean isJavaIdentifierStart(jint codePoint);
            static jboolean isJavaLetter(jchar ch);
            static jboolean isJavaLetterOrDigit(jchar ch);
            static jboolean isLetter(jchar ch);
            static jboolean isLetter(jint codePoint);
            static jboolean isLetterOrDigit(jchar ch);
            static jboolean isLetterOrDigit(jint codePoint);
            static jboolean isLowerCase(jchar ch);
            static jboolean isLowerCase(jint codePoint);
            static jboolean isLowSurrogate(jchar ch);
            static jboolean isMirrored(jchar ch);
            static jboolean isMirrored(jint codePoint);
            static jboolean isSpace(jchar ch);
            static jboolean isSpaceChar(jchar ch);
            static jboolean isSpaceChar(jint codePoint);
            static jboolean isSupplementaryCodePoint(jint codePoint);
            static jboolean isSurrogate(jchar ch);
            static jboolean isSurrogatePair(jchar high, jchar low);
            static jboolean isTitleCase(jchar ch);
            static jboolean isTitleCase(jint codePoint);
            static jboolean isUnicodeIdentifierPart(jchar ch);
            static jboolean isUnicodeIdentifierPart(jint codePoint);
            static jboolean isUnicodeIdentifierStart(jchar ch);
            static jboolean isUnicodeIdentifierStart(jint codePoint);
            static jboolean isUpperCase(jchar ch);
            static jboolean isUpperCase(jint codePoint);
            static jboolean isValidCodePoint(jint codePoint);
            static jboolean isWhitespace(jchar ch);
            static jboolean isWhitespace(jint codePoint);
            static jchar lowSurrogate(jint codePoint);
            static jint offsetByCodePoints(jarray<jchar> a, jint start, jint count, jint index, jint codePointOffset);
            static jint offsetByCodePoints(JCharSequence seq, jint index, jint codePointOffset);
            static jchar reverseBytes(jchar ch);
            static jarray<jchar> toChars(jint codePoint);
            static jint toChars(jint codePoint, jarray<jchar> dst, jint dstIndex);
            static jint toCodePoint(jchar high, jchar low);
            static jchar toLowerCase(jchar ch);
            static jint toLowerCase(jint codePoint);
            JString toString();
            static JString toString(jchar c);
            static jchar toTitleCase(jchar ch);
            static jint toTitleCase(jint codePoint);
            static jchar toUpperCase(jchar ch);
            static jint toUpperCase(jint codePoint);
            static JCharacter valueOf(jchar c);

            static const jbyte UNASSIGNED;
            static const jbyte UPPERCASE_LETTER;
            static const jbyte LOWERCASE_LETTER;
            static const jbyte TITLECASE_LETTER;
            static const jbyte MODIFIER_LETTER;
            static const jbyte OTHER_LETTER;
            static const jbyte DECIMAL_DIGIT_NUMBER;
            static const jbyte LETTER_NUMBER;
            static const jbyte SPACE_SEPARATOR;
            static const jbyte LINE_SEPARATOR;
            static const jbyte PARAGRAPH_SEPARATOR;
            static const jbyte DIRECTIONALITY_LEFT_TO_RIGHT;
            static const jbyte DIRECTIONALITY_LEFT_TO_RIGHT_EMBEDDING;
            static const jbyte DIRECTIONALITY_LEFT_TO_RIGHT_OVERRIDE;
            static const jbyte DIRECTIONALITY_POP_DIRECTIONAL_FORMAT;
            static const jbyte DIRECTIONALITY_RIGHT_TO_LEFT_EMBEDDING;
            static const jbyte DIRECTIONALITY_RIGHT_TO_LEFT_OVERRIDE;
            static const jbyte DIRECTIONALITY_UNDEFINED;
            static const jint ERROR;
            static const jint MAX_CODE_POINT;
            static const jchar MAX_HIGH_SURROGATE;
            static const jchar MAX_LOW_SURROGATE;
            static const jchar MAX_SURROGATE;
            static const jint MAX_RADIX;
            static const jchar MIN_HIGH_SURROGATE;
            static const jchar MIN_LOW_SURROGATE;
            static const jint MIN_RADIX;
            static const jint MIN_SUPPLEMENTARY_CODE_POINT;
            static const jchar MIN_SURROGATE;
            static const jbyte PRIVATE_USE;

            class Subset : public Object, public std::enable_shared_from_this<Subset>
            {
            public:
                Subset(JString name);
                jboolean equals(JObject obj);
                jint hashCode();
                JString toString();
                
            private:
                JString name;
            };

            class UnicodeBlock : public Subset
            {
            public:
                UnicodeBlock(JString idName);
                
                static JUnicodeBlock createUnicodeBlock(JString idName);
                static JUnicodeBlock createUnicodeBlock(JString idName, jarray<JString> aliases);
                
                template<typename... Args>
                static JUnicodeBlock createUnicodeBlock(JString idName, Args&&... aliases) {
                    return createUnicodeBlock(idName, { std::forward<Args>(aliases)... });
                }
                
                static const JUnicodeBlock BASIC_LATIN;
                static const JUnicodeBlock LATIN_1_SUPPLEMENT;
                static const JUnicodeBlock LATIN_EXTENDED_A;
                static const JUnicodeBlock LATIN_EXTENDED_B;
                static const JUnicodeBlock IPA_EXTENSIONS;
                static const JUnicodeBlock SPACING_MODIFIER_LETTERS;
                static const JUnicodeBlock COMBINING_DIACRITICAL_MARKS;
                static const JUnicodeBlock GREEK;
                static const JUnicodeBlock CYRILLIC;
                static const JUnicodeBlock ARMENIAN;
                static const JUnicodeBlock HEBREW;
                static const JUnicodeBlock ARABIC;
                static const JUnicodeBlock DEVANAGARI;
                static const JUnicodeBlock BENGALI;
                static const JUnicodeBlock GURMUKHI;
                static const JUnicodeBlock GUJARATI;
                static const JUnicodeBlock ORIYA;
                static const JUnicodeBlock TAMIL;
                static const JUnicodeBlock TELUGU;
                static const JUnicodeBlock KANNADA;
                static const JUnicodeBlock MALAYALAM;
                static const JUnicodeBlock THAI;
                static const JUnicodeBlock LAO;
                static const JUnicodeBlock TIBETAN;
                static const JUnicodeBlock GEORGIAN;
                static const JUnicodeBlock HANGUL_JAMO;
                static const JUnicodeBlock LATIN_EXTENDED_ADDITIONAL;
                static const JUnicodeBlock GREEK_EXTENDED;
                static const JUnicodeBlock GENERAL_PUNCTUATION;
                static const JUnicodeBlock SUPERSCRIPTS_AND_SUBSCRIPTS;
                static const JUnicodeBlock CURRENCY_SYMBOLS;
                static const JUnicodeBlock COMBINING_MARKS_FOR_SYMBOLS;
                static const JUnicodeBlock LETTERLIKE_SYMBOLS;
                static const JUnicodeBlock NUMBER_FORMS;
                static const JUnicodeBlock ARROWS;
                static const JUnicodeBlock MATHEMATICAL_OPERATORS;
                static const JUnicodeBlock MISCELLANEOUS_TECHNICAL;
                static const JUnicodeBlock CONTROL_PICTURES;
                static const JUnicodeBlock OPTICAL_CHARACTER_RECOGNITION;
                static const JUnicodeBlock ENCLOSED_ALPHANUMERICS;
                static const JUnicodeBlock BOX_DRAWING;
                static const JUnicodeBlock BLOCK_ELEMENTS;
                static const JUnicodeBlock GEOMETRIC_SHAPES;
                static const JUnicodeBlock MISCELLANEOUS_SYMBOLS;
                static const JUnicodeBlock DINGBATS;
                static const JUnicodeBlock CJK_SYMBOLS_AND_PUNCTUATION;
                static const JUnicodeBlock HIRAGANA;
                static const JUnicodeBlock KATAKANA;
                static const JUnicodeBlock BOPOMOFO;
                static const JUnicodeBlock HANGUL_COMPATIBILITY_JAMO;
                static const JUnicodeBlock KANBUN;
                static const JUnicodeBlock ENCLOSED_CJK_LETTERS_AND_MONTHS;
                static const JUnicodeBlock CJK_COMPATIBILITY;
                static const JUnicodeBlock CJK_UNIFIED_IDEOGRAPHS;
                static const JUnicodeBlock HANGUL_SYLLABLES;
                static const JUnicodeBlock PRIVATE_USE_AREA;
                static const JUnicodeBlock CJK_COMPATIBILITY_IDEOGRAPHS;
                static const JUnicodeBlock ALPHABETIC_PRESENTATION_FORMS;
                static const JUnicodeBlock ARABIC_PRESENTATION_FORMS_A;
                static const JUnicodeBlock COMBINING_HALF_MARKS;
                static const JUnicodeBlock CJK_COMPATIBILITY_FORMS;
                static const JUnicodeBlock SMALL_FORM_VARIANTS;
                static const JUnicodeBlock ARABIC_PRESENTATION_FORMS_B;
                static const JUnicodeBlock HALFWIDTH_AND_FULLWIDTH_FORMS;
                static const JUnicodeBlock SPECIALS;
                static const JUnicodeBlock SURROGATES_AREA;
                static const JUnicodeBlock SYRIAC;
                static const JUnicodeBlock THAANA;
                static const JUnicodeBlock SINHALA;
                static const JUnicodeBlock MYANMAR;
                static const JUnicodeBlock ETHIOPIC;
                static const JUnicodeBlock CHEROKEE;
                static const JUnicodeBlock UNIFIED_CANADIAN_ABORIGINAL_SYLLABICS;
                static const JUnicodeBlock OGHAM;
                static const JUnicodeBlock RUNIC;
                static const JUnicodeBlock KHMER;
                static const JUnicodeBlock MONGOLIAN;
                static const JUnicodeBlock BRAILLE_PATTERNS;
                static const JUnicodeBlock CJK_RADICALS_SUPPLEMENT;
                static const JUnicodeBlock KANGXI_RADICALS;
                static const JUnicodeBlock IDEOGRAPHIC_DESCRIPTION_CHARACTERS;
                static const JUnicodeBlock BOPOMOFO_EXTENDED;
                static const JUnicodeBlock CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A;
                static const JUnicodeBlock YI_SYLLABLES;
                static const JUnicodeBlock YI_RADICALS;
                static const JUnicodeBlock CYRILLIC_SUPPLEMENTARY;
                static const JUnicodeBlock TAGALOG;
                static const JUnicodeBlock HANUNOO;
                static const JUnicodeBlock BUHID;
                static const JUnicodeBlock TAGBANWA;
                static const JUnicodeBlock LIMBU;
                static const JUnicodeBlock TAI_LE;
                static const JUnicodeBlock KHMER_SYMBOLS;
                static const JUnicodeBlock PHONETIC_EXTENSIONS;
                static const JUnicodeBlock MISCELLANEOUS_MATHEMATICAL_SYMBOLS_A;
                static const JUnicodeBlock SUPPLEMENTAL_ARROWS_A;
                static const JUnicodeBlock SUPPLEMENTAL_ARROWS_B;
                static const JUnicodeBlock MISCELLANEOUS_MATHEMATICAL_SYMBOLS_B;
                static const JUnicodeBlock SUPPLEMENTAL_MATHEMATICAL_OPERATORS;
                static const JUnicodeBlock MISCELLANEOUS_SYMBOLS_AND_ARROWS;
                static const JUnicodeBlock KATAKANA_PHONETIC_EXTENSIONS;
                static const JUnicodeBlock YIJING_HEXAGRAM_SYMBOLS;
                static const JUnicodeBlock VARIATION_SELECTORS;
                static const JUnicodeBlock LINEAR_B_SYLLABARY;
                static const JUnicodeBlock LINEAR_B_IDEOGRAMS;
                static const JUnicodeBlock AEGEAN_NUMBERS;
                static const JUnicodeBlock OLD_ITALIC;
                static const JUnicodeBlock GOTHIC;
                static const JUnicodeBlock UGARITIC;
                static const JUnicodeBlock DESERET;
                static const JUnicodeBlock SHAVIAN;
                static const JUnicodeBlock OSMANYA;
                static const JUnicodeBlock CYPRIOT_SYLLABARY;
                static const JUnicodeBlock BYZANTINE_MUSICAL_SYMBOLS;
                static const JUnicodeBlock MUSICAL_SYMBOLS;
                static const JUnicodeBlock TAI_XUAN_JING_SYMBOLS;
                static const JUnicodeBlock MATHEMATICAL_ALPHANUMERIC_SYMBOLS;
                static const JUnicodeBlock CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B;
                static const JUnicodeBlock CJK_COMPATIBILITY_IDEOGRAPHS_SUPPLEMENT;
                static const JUnicodeBlock TAGS;
                static const JUnicodeBlock VARIATION_SELECTORS_SUPPLEMENT;
                static const JUnicodeBlock SUPPLEMENTARY_PRIVATE_USE_AREA_A;
                static const JUnicodeBlock SUPPLEMENTARY_PRIVATE_USE_AREA_B;
                static const JUnicodeBlock HIGH_SURROGATES;
                static const JUnicodeBlock HIGH_PRIVATE_USE_SURROGATES;
                static const JUnicodeBlock LOW_SURROGATES;
                static const JUnicodeBlock ARABIC_SUPPLEMENT;
                static const JUnicodeBlock NKO;
                static const JUnicodeBlock SAMARITAN;
                static const JUnicodeBlock MANDAIC;
                static const JUnicodeBlock ETHIOPIC_SUPPLEMENT;
                static const JUnicodeBlock UNIFIED_CANADIAN_ABORIGINAL_SYLLABICS_EXTENDED;
                static const JUnicodeBlock NEW_TAI_LUE;
                static const JUnicodeBlock BUGINESE;
                static const JUnicodeBlock TAI_THAM;
                static const JUnicodeBlock BALINESE;
                static const JUnicodeBlock SUNDANESE;
                static const JUnicodeBlock BATAK;
                static const JUnicodeBlock LEPCHA;
                static const JUnicodeBlock OL_CHIKI;
                static const JUnicodeBlock VEDIC_EXTENSIONS;
                static const JUnicodeBlock PHONETIC_EXTENSIONS_SUPPLEMENT;
                static const JUnicodeBlock COMBINING_DIACRITICAL_MARKS_SUPPLEMENT;
                static const JUnicodeBlock GLAGOLITIC;
                static const JUnicodeBlock LATIN_EXTENDED_C;
                static const JUnicodeBlock COPTIC;
                static const JUnicodeBlock GEORGIAN_SUPPLEMENT;
                static const JUnicodeBlock TIFINAGH;
                static const JUnicodeBlock ETHIOPIC_EXTENDED;
                static const JUnicodeBlock CYRILLIC_EXTENDED_A;
                static const JUnicodeBlock SUPPLEMENTAL_PUNCTUATION;
                static const JUnicodeBlock CJK_STROKES;
                static const JUnicodeBlock LISU;
                static const JUnicodeBlock VAI;
                static const JUnicodeBlock CYRILLIC_EXTENDED_B;
                static const JUnicodeBlock BAMUM;
                static const JUnicodeBlock MODIFIER_TONE_LETTERS;
                static const JUnicodeBlock LATIN_EXTENDED_D;
                static const JUnicodeBlock SYLOTI_NAGRI;
                static const JUnicodeBlock COMMON_INDIC_NUMBER_FORMS;
                static const JUnicodeBlock PHAGS_PA;
                static const JUnicodeBlock SAURASHTRA;
                static const JUnicodeBlock DEVANAGARI_EXTENDED;
                static const JUnicodeBlock KAYAH_LI;
                static const JUnicodeBlock REJANG;
                static const JUnicodeBlock HANGUL_JAMO_EXTENDED_A;
                static const JUnicodeBlock JAVANESE;
                static const JUnicodeBlock CHAM;
                static const JUnicodeBlock MYANMAR_EXTENDED_A;
                static const JUnicodeBlock TAI_VIET;
                static const JUnicodeBlock ETHIOPIC_EXTENDED_A;
                static const JUnicodeBlock MEETEI_MAYEK;
                static const JUnicodeBlock HANGUL_JAMO_EXTENDED_B;
                static const JUnicodeBlock VERTICAL_FORMS;
                static const JUnicodeBlock ANCIENT_GREEK_NUMBERS;
                static const JUnicodeBlock ANCIENT_SYMBOLS;
                static const JUnicodeBlock PHAISTOS_DISC;
                static const JUnicodeBlock LYCIAN;
                static const JUnicodeBlock CARIAN;
                static const JUnicodeBlock OLD_PERSIAN;
                static const JUnicodeBlock IMPERIAL_ARAMAIC;
                static const JUnicodeBlock PHOENICIAN;
                static const JUnicodeBlock LYDIAN;
                static const JUnicodeBlock KHAROSHTHI;
                static const JUnicodeBlock OLD_SOUTH_ARABIAN;
                static const JUnicodeBlock AVESTAN;
                static const JUnicodeBlock INSCRIPTIONAL_PARTHIAN;
                static const JUnicodeBlock INSCRIPTIONAL_PAHLAVI;
                static const JUnicodeBlock OLD_TURKIC;
                static const JUnicodeBlock RUMI_NUMERAL_SYMBOLS;
                static const JUnicodeBlock BRAHMI;
                static const JUnicodeBlock KAITHI;
                static const JUnicodeBlock CUNEIFORM;
                static const JUnicodeBlock CUNEIFORM_NUMBERS_AND_PUNCTUATION;
                static const JUnicodeBlock EGYPTIAN_HIEROGLYPHS;
                static const JUnicodeBlock BAMUM_SUPPLEMENT;
                static const JUnicodeBlock KANA_SUPPLEMENT;
                static const JUnicodeBlock ANCIENT_GREEK_MUSICAL_NOTATION;
                static const JUnicodeBlock COUNTING_ROD_NUMERALS;
                static const JUnicodeBlock MAHJONG_TILES;
                static const JUnicodeBlock DOMINO_TILES;
                static const JUnicodeBlock PLAYING_CARDS;
                static const JUnicodeBlock ENCLOSED_ALPHANUMERIC_SUPPLEMENT;
                static const JUnicodeBlock ENCLOSED_IDEOGRAPHIC_SUPPLEMENT;
                static const JUnicodeBlock MISCELLANEOUS_SYMBOLS_AND_PICTOGRAPHS;
                static const JUnicodeBlock EMOTICONS;
                static const JUnicodeBlock TRANSPORT_AND_MAP_SYMBOLS;
                static const JUnicodeBlock ALCHEMICAL_SYMBOLS;
                static const JUnicodeBlock CJK_UNIFIED_IDEOGRAPHS_EXTENSION_C;
                static const JUnicodeBlock CJK_UNIFIED_IDEOGRAPHS_EXTENSION_D;
                static const JUnicodeBlock ARABIC_EXTENDED_A;
                static const JUnicodeBlock SUNDANESE_SUPPLEMENT;
                static const JUnicodeBlock MEETEI_MAYEK_EXTENSIONS;
                static const JUnicodeBlock MEROITIC_HIEROGLYPHS;
                static const JUnicodeBlock MEROITIC_CURSIVE;
                static const JUnicodeBlock SORA_SOMPENG;
                static const JUnicodeBlock CHAKMA;
                static const JUnicodeBlock SHARADA;
                static const JUnicodeBlock TAKRI;
                static const JUnicodeBlock MIAO;
                static const JUnicodeBlock ARABIC_MATHEMATICAL_ALPHABETIC_SYMBOLS;
                
                static JUnicodeBlock of(jchar c);
                static JUnicodeBlock of(jint codePoint);
                static const JUnicodeBlock forName(JString blockName);
                
            private:
                static cpp::util::JMap<JString, JUnicodeBlock> map();
                static const jarray<jint> blockStarts;
                static const jarray<JUnicodeBlock> blocks;
            };
            
            jenum_begin(UnicodeScript)
            
            jenum_end
            
        private:
            static void toSurrogates(jint codePoint, jarray<jchar> dst, jint index);
            static jint codePointAtImpl(jarray<jchar> a, jint index, jint limit);
            static jint codePointBeforeImpl(jarray<jchar> a, jint index, jint start);
            static jint codePointCountImpl(jarray<jchar> a, jint offset, jint count);
            static jint offsetByCodePointsImpl(jarray<jchar> a, jint start, jint count, jint index, jint codePointOffset);

            jchar value;
            
            class CharacterCache
            {
            public:
                static jarray<JCharacter> cache;
            
                static jboolean isStaticInitializationBlock;
                static jboolean StaticInitializationBlock();
            };

            friend class String;
            friend class AbstractStringBuilder;
        };
    }
}

#endif // CPP_LANG_CHARACTER_H
