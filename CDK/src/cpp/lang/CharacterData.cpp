#include "CharacterData.h"
#include "CharacterData00.h"
#include "CharacterData01.h"
#include "CharacterData02.h"
#include "CharacterData0E.h"
#include "CharacterDataLatin1.h"
#include "CharacterDataPrivateUse.h"
#include "CharacterDataUndefined.h"

namespace cpp
{
    namespace lang
    {
        jint CharacterData::toUpperCaseEx(jint ch)
        {
            return toUpperCase(ch);
        }

        jarray<jchar> CharacterData::toUpperCaseCharArray(jint ch)
        {
            return nullptr;
        }

        jboolean CharacterData::isOtherLowercase(jint ch)
        {
            return false;
        }

        jboolean CharacterData::isOtherUppercase(jint ch)
        {
            return false;
        }

        jboolean CharacterData::isOtherAlphabetic(jint ch)
        {
            return false;
        }

        jboolean CharacterData::isIdeographic(jint ch)
        {
            return false;
        }

        JCharacterData CharacterData::of(jint ch)
        {
            if(ch >> 8 == 0) {
                return CharacterDataLatin1::instance();
            }
            else {
                switch(ch >> 16) {
                    case(0):
                        return CharacterData00::instance();
                    case(1):
                        return CharacterData01::instance();
                    case(2):
                        return CharacterData02::instance();
                    case(14):
                        return CharacterData0E::instance();
                    case(15):
                    case(16):
                        return CharacterDataPrivateUse::instance();
                }
            }

            return CharacterDataUndefined::instance();
        }
    }
}
