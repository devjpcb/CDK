#ifndef CPP_LANG_CHARACTERDATA_H
#define CPP_LANG_CHARACTERDATA_H

#include "Object.h"
#include "Array.h"

namespace cpp
{
    namespace lang
    {
        class CharacterData : public Object
        {
        public:
            virtual jint getProperties(jint ch) = 0;
            virtual jint getType(jint ch) = 0;
            virtual jboolean isWhitespace(jint ch) = 0;
            virtual jboolean isMirrored(jint ch) = 0;
            virtual jboolean isJavaIdentifierStart(jint ch) = 0;
            virtual jboolean isJavaIdentifierPart(jint ch) = 0;
            virtual jboolean isUnicodeIdentifierStart(jint ch) = 0;
            virtual jboolean isUnicodeIdentifierPart(jint ch) = 0;
            virtual jboolean isIdentifierIgnorable(jint ch) = 0;
            virtual jint toLowerCase(jint ch) = 0;
            virtual jint toUpperCase(jint ch) = 0;
            virtual jint toTitleCase(jint ch) = 0;
            virtual jint digit(jint ch, jint radix) = 0;
            virtual jint getNumericValue(jint ch) = 0;
            virtual jbyte getDirectionality(jint ch) = 0;

            virtual jint toUpperCaseEx(jint ch);
            virtual jarray<jchar> toUpperCaseCharArray(jint ch);
            virtual jboolean isOtherLowercase(jint ch);
            virtual jboolean isOtherUppercase(jint ch);
            virtual jboolean isOtherAlphabetic(jint ch);
            virtual jboolean isIdeographic(jint ch);

            static JCharacterData of(jint ch);
        };
    }
}

#endif // CPP_LANG_CHARACTERDATA_H
