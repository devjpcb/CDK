#ifndef CPP_LANG_CHARACTERDATA00_H
#define CPP_LANG_CHARACTERDATA00_H

#include "CharacterData.h"

namespace cpp
{
    namespace lang
    {
        class CharacterData00 : public CharacterData
        {
        public:
            CharacterData00();
            ~CharacterData00();
            
        private:
            virtual jint getProperties(jint ch);
            virtual jint getType(jint ch);
            virtual jboolean isWhitespace(jint ch);
            virtual jboolean isMirrored(jint ch);
            virtual jboolean isJavaIdentifierStart(jint ch);
            virtual jboolean isJavaIdentifierPart(jint ch);
            virtual jboolean isUnicodeIdentifierStart(jint ch);
            virtual jboolean isUnicodeIdentifierPart(jint ch);
            virtual jboolean isIdentifierIgnorable(jint ch);
            virtual jint toLowerCase(jint ch);
            virtual jint toUpperCase(jint ch);
            virtual jint toTitleCase(jint ch);
            virtual jint digit(jint ch, jint radix);
            virtual jint getNumericValue(jint ch);
            virtual jbyte getDirectionality(jint ch);

            jint getPropertiesEx(jint ch);
            virtual jint toUpperCaseEx(jint ch);
            virtual jarray<jchar> toUpperCaseCharArray(jint ch);
            virtual jboolean isOtherLowercase(jint ch);
            virtual jboolean isOtherUppercase(jint ch);
            virtual jboolean isOtherAlphabetic(jint ch);
            virtual jboolean isIdeographic(jint ch);
            jint findInCharMap(jint ch);
            
            static JCharacterData00 instance();
            
            static jarray<jchar> X();
            static jarray<jchar> Y();
            static jarray<jint> A();
            static JString A_DATA();
            static jarray<jchar> B();
            static jarray<jarray<jarray<jchar>>> charMap();
            
            static jboolean isStaticInitializationBlock;
            
            friend class CharacterData;
        };
    }
}

#endif // CPP_LANG_CHARACTERDATA00_H
