#include "CharacterDataLatin1.h"
#include "Character.h"
#include "String.h"
#include <cassert>

namespace cpp
{
    namespace lang
    {
        CharacterDataLatin1::CharacterDataLatin1()
        {
        }

        CharacterDataLatin1::~CharacterDataLatin1()
        {
        }

        jint CharacterDataLatin1::getProperties(jint ch)
        {
            jchar offset = static_cast<jchar>(ch);
            jint props = A()[offset];
            return props;
        }

        jint CharacterDataLatin1::getType(jint ch)
        {
            jint props = getProperties(ch);
            return (props & 0x1F);
        }

        jboolean CharacterDataLatin1::isWhitespace(jint ch)
        {
            jint props = getProperties(ch);
            return ((props & 0x00007000) == 0x00004000);
        }

        jboolean CharacterDataLatin1::isMirrored(jint ch)
        {
            jint props = getProperties(ch);
            return ((props & 0x80000000) != 0);
        }

        jboolean CharacterDataLatin1::isJavaIdentifierStart(jint ch)
        {
            jint props = getProperties(ch);
            return ((props & 0x00007000) >= 0x00005000);
        }

        jboolean CharacterDataLatin1::isJavaIdentifierPart(jint ch)
        {
            jint props = getProperties(ch);
            return ((props & 0x00003000) != 0);
        }

        jboolean CharacterDataLatin1::isUnicodeIdentifierStart(jint ch)
        {
            jint props = getProperties(ch);
            return ((props & 0x00007000) == 0x00007000);
        }

        jboolean CharacterDataLatin1::isUnicodeIdentifierPart(jint ch)
        {
            jint props = getProperties(ch);
            return ((props & 0x00001000) != 0);
        }

        jboolean CharacterDataLatin1::isIdentifierIgnorable(jint ch)
        {
            jint props = getProperties(ch);
            return ((props & 0x00007000) == 0x00001000);
        }

        jint CharacterDataLatin1::toLowerCase(jint ch)
        {
            jint mapChar = ch;
            jint val = getProperties(ch);

            if(((val & 0x00020000) != 0) && ((val & 0x07FC0000) != 0x07FC0000)) {
                jint offset = val << 5>> (5 + 18);
                mapChar = ch + offset;
            }
            return mapChar;
        }

        jint CharacterDataLatin1::toUpperCase(jint ch)
        {
            jint mapChar = ch;
            jint val = getProperties(ch);

            if((val & 0x00010000) != 0) {
                if((val & 0x07FC0000) != 0x07FC0000) {
                    jint offset = val << 5>> (5 + 18);
                    mapChar = ch - offset;
                }
                else if(ch == 0x00B5) {
                    mapChar = 0x039C;
                }
            }
            return mapChar;
        }

        jint CharacterDataLatin1::toTitleCase(jint ch)
        {
            return toUpperCase(ch);
        }

        jint CharacterDataLatin1::digit(jint ch, jint radix)
        {
            jint value = -1;
            if(radix >= Character::MIN_RADIX && radix <= Character::MAX_RADIX) {
                jint val = getProperties(ch);
                jint kind = val & 0x1F;
                if(kind == Character::DECIMAL_DIGIT_NUMBER) {
                    value = (ch + ((val & 0x3E0) >> 5)) & 0x1F;
                }
                else if((val & 0xC00) == 0x00000C00) {
                    value = ((ch + ((val & 0x3E0) >> 5)) & 0x1F) + 10;
                }
            }
            return (value < radix) ? value : -1;
        }

        jint CharacterDataLatin1::getNumericValue(jint ch)
        {
            jint val = getProperties(ch);
            jint retval = -1;

            switch(val & 0xC00) {
                default:
                case(0x00000000):
                    retval = -1;
                    break;
                case(0x00000400):
                    retval = (ch + ((val & 0x3E0) >> 5)) & 0x1F;
                    break;
                case(0x00000800):
                    retval = -2;
                    break;
                case(0x00000C00):
                    retval = ((ch + ((val & 0x3E0) >> 5)) & 0x1F) + 10;
                    break;
            }
            return retval;
        }

        jbyte CharacterDataLatin1::getDirectionality(jint ch)
        {
            jint val = getProperties(ch);
            jbyte directionality = static_cast<jbyte>(((val & 0x78000000) >> 27));

            if(directionality == 0xF) {
                directionality = -1;
            }
            return directionality;
        }

        jint CharacterDataLatin1::getPropertiesEx(jint ch)
        {
            jchar offset = static_cast<jchar>(ch);
            jint props = B()[offset];
            return props;
        }

        jint CharacterDataLatin1::toUpperCaseEx(jint ch)
        {
            jint mapChar = ch;
            jint val = getProperties(ch);

            if((val & 0x00010000) != 0) {
                if((val & 0x07FC0000) != 0x07FC0000) {
                    jint offset = val << 5>> (5 + 18);
                    mapChar = ch - offset;
                }
                else {
                    switch(ch) {
                            // map overflow characters
                        case 0x00B5:
                            mapChar = 0x039C;
                            break;
                        default:
                            mapChar = Character::ERROR;
                            break;
                    }
                }
            }

            return mapChar;
        }

        jarray<jchar> CharacterDataLatin1::toUpperCaseCharArray(jint ch)
        {
            jarray<jchar> upperMap = { static_cast<jchar>(ch) };
            if(ch == 0x00DF) {
                upperMap = sharpsMap();
            }
            return upperMap;
        }

        jboolean CharacterDataLatin1::isOtherLowercase(jint ch)
        {
            int props = getPropertiesEx(ch);
            return (props & 0x0001) != 0;
        }

        jboolean CharacterDataLatin1::isOtherUppercase(jint ch)
        {
            jint props = getPropertiesEx(ch);
            return (props & 0x0002) != 0;
        }

        jboolean CharacterDataLatin1::isOtherAlphabetic(jint ch)
        {
            jint props = getPropertiesEx(ch);
            return (props & 0x0004) != 0;
        }

        jboolean CharacterDataLatin1::isIdeographic(jint ch)
        {
            jint props = getPropertiesEx(ch);
            return (props & 0x0010) != 0;
        }

        JCharacterDataLatin1 CharacterDataLatin1::instance()
        {
            static JCharacterDataLatin1 _instance = jnew<CharacterDataLatin1>();
            return _instance;
        }

        jarray<jchar> CharacterDataLatin1::sharpsMap()
        {
            static jarray<jchar> _sharpsMap = { u'S', u'S' };
            return _sharpsMap;
        }

        jarray<jint> CharacterDataLatin1::A()
        {
            static jarray<jint> _A(256);
            if(!isStaticInitializationBlock) {
                jarray<jchar> data = CharacterDataLatin1::A_DATA()->toCharArray();
                assert(data.length() == (256 * 2));
                jint i = 0, j = 0;
                while(i < (256 * 2)) {
                    jint entry = data[i++] << 16;
                    _A[j++] = entry | data[i++];
                }

                isStaticInitializationBlock = true;
            }
            return _A;
        }

        JString CharacterDataLatin1::A_DATA()
        {
            static JString _A_DATA = jstr(u"\u4800\u100F\u4800\u100F\u4800\u100F\u4800\u100F\u4800\u100F\u4800\u100F\u4800"
                                          u"\u100F\u4800\u100F\u4800\u100F\u5800\u400F\u5000\u400F\u5800\u400F\u6000\u400F"
                                          u"\u5000\u400F\u4800\u100F\u4800\u100F\u4800\u100F\u4800\u100F\u4800\u100F\u4800"
                                          u"\u100F\u4800\u100F\u4800\u100F\u4800\u100F\u4800\u100F\u4800\u100F\u4800\u100F"
                                          u"\u4800\u100F\u4800\u100F\u5000\u400F\u5000\u400F\u5000\u400F\u5800\u400F\u6000"
                                          u"\u400C\u6800\030\u6800\030\u2800\030\u2800\u601A\u2800\030\u6800\030\u6800"
                                          u"\030\uE800\025\uE800\026\u6800\030\u2000\031\u3800\030\u2000\024\u3800\030"
                                          u"\u3800\030\u1800\u3609\u1800\u3609\u1800\u3609\u1800\u3609\u1800\u3609\u1800"
                                          u"\u3609\u1800\u3609\u1800\u3609\u1800\u3609\u1800\u3609\u3800\030\u6800\030"
                                          u"\uE800\031\u6800\031\uE800\031\u6800\030\u6800\030\202\u7FE1\202\u7FE1\202"
                                          u"\u7FE1\202\u7FE1\202\u7FE1\202\u7FE1\202\u7FE1\202\u7FE1\202\u7FE1\202\u7FE1"
                                          u"\202\u7FE1\202\u7FE1\202\u7FE1\202\u7FE1\202\u7FE1\202\u7FE1\202\u7FE1\202"
                                          u"\u7FE1\202\u7FE1\202\u7FE1\202\u7FE1\202\u7FE1\202\u7FE1\202\u7FE1\202\u7FE1"
                                          u"\202\u7FE1\uE800\025\u6800\030\uE800\026\u6800\033\u6800\u5017\u6800\033\201"
                                          u"\u7FE2\201\u7FE2\201\u7FE2\201\u7FE2\201\u7FE2\201\u7FE2\201\u7FE2\201\u7FE2"
                                          u"\201\u7FE2\201\u7FE2\201\u7FE2\201\u7FE2\201\u7FE2\201\u7FE2\201\u7FE2\201"
                                          u"\u7FE2\201\u7FE2\201\u7FE2\201\u7FE2\201\u7FE2\201\u7FE2\201\u7FE2\201\u7FE2"
                                          u"\201\u7FE2\201\u7FE2\201\u7FE2\uE800\025\u6800\031\uE800\026\u6800\031\u4800"
                                          u"\u100F\u4800\u100F\u4800\u100F\u4800\u100F\u4800\u100F\u4800\u100F\u5000\u100F"
                                          u"\u4800\u100F\u4800\u100F\u4800\u100F\u4800\u100F\u4800\u100F\u4800\u100F\u4800"
                                          u"\u100F\u4800\u100F\u4800\u100F\u4800\u100F\u4800\u100F\u4800\u100F\u4800\u100F"
                                          u"\u4800\u100F\u4800\u100F\u4800\u100F\u4800\u100F\u4800\u100F\u4800\u100F\u4800"
                                          u"\u100F\u4800\u100F\u4800\u100F\u4800\u100F\u4800\u100F\u4800\u100F\u4800\u100F"
                                          u"\u3800\014\u6800\030\u2800\u601A\u2800\u601A\u2800\u601A\u2800\u601A\u6800"
                                          u"\034\u6800\030\u6800\033\u6800\034\000\u7005\uE800\035\u6800\031\u4800\u1010"
                                          u"\u6800\034\u6800\033\u2800\034\u2800\031\u1800\u060B\u1800\u060B\u6800\033"
                                          u"\u07FD\u7002\u6800\030\u6800\030\u6800\033\u1800\u050B\000\u7005\uE800\036"
                                          u"\u6800\u080B\u6800\u080B\u6800\u080B\u6800\030\202\u7001\202\u7001\202\u7001"
                                          u"\202\u7001\202\u7001\202\u7001\202\u7001\202\u7001\202\u7001\202\u7001\202"
                                          u"\u7001\202\u7001\202\u7001\202\u7001\202\u7001\202\u7001\202\u7001\202\u7001"
                                          u"\202\u7001\202\u7001\202\u7001\202\u7001\202\u7001\u6800\031\202\u7001\202"
                                          u"\u7001\202\u7001\202\u7001\202\u7001\202\u7001\202\u7001\u07FD\u7002\201\u7002"
                                          u"\201\u7002\201\u7002\201\u7002\201\u7002\201\u7002\201\u7002\201\u7002\201"
                                          u"\u7002\201\u7002\201\u7002\201\u7002\201\u7002\201\u7002\201\u7002\201\u7002"
                                          u"\201\u7002\201\u7002\201\u7002\201\u7002\201\u7002\201\u7002\201\u7002\u6800"
                                          u"\031\201\u7002\201\u7002\201\u7002\201\u7002\201\u7002\201\u7002\201\u7002"
                                          u"\u061D\u7002");

            return _A_DATA;
        }

        jarray<jchar> CharacterDataLatin1::B()
        {
            static jarray<jchar> _B = jstr(u"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
                                           u"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
                                           u"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
                                           u"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
                                           u"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
                                           u"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
                                           u"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
                                           u"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
                                           u"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\001"
                                           u"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\001\000\000\000"
                                           u"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
                                           u"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
                                           u"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
                                           u"\000\000\000\000\000\000\000\000\000")->toCharArray();
            return _B;
        }

        jboolean CharacterDataLatin1::isStaticInitializationBlock = false;
    }
}
