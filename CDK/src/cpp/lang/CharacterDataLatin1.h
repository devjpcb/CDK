#ifndef CPP_LANG_CHARACTERDATALATIN1_H
#define CPP_LANG_CHARACTERDATALATIN1_H

#include "CharacterData.h"

namespace cpp
{

    namespace lang
    {

        class CharacterDataLatin1 : public CharacterData
        {
        public:
            CharacterDataLatin1();
            ~CharacterDataLatin1();

        private:
            virtual jint getProperties(jint ch);
            virtual jint getType(jint ch);
            virtual jboolean isWhitespace(jint ch);
            virtual jboolean isMirrored(jint ch);
            virtual jboolean isJavaIdentifierStart(jint ch);
            virtual jboolean isJavaIdentifierPart(jint ch);
            virtual jboolean isUnicodeIdentifierStart(jint ch);
            virtual jboolean isUnicodeIdentifierPart(jint ch);
            virtual jboolean isIdentifierIgnorable(jint ch);
            virtual jint toLowerCase(jint ch);
            virtual jint toUpperCase(jint ch);
            virtual jint toTitleCase(jint ch);
            virtual jint digit(jint ch, jint radix);
            virtual jint getNumericValue(jint ch);
            virtual jbyte getDirectionality(jint ch);

            jint getPropertiesEx(jint ch);
            virtual jint toUpperCaseEx(jint ch);
            virtual jarray<jchar> toUpperCaseCharArray(jint ch);
            virtual jboolean isOtherLowercase(jint ch);
            virtual jboolean isOtherUppercase(jint ch);
            virtual jboolean isOtherAlphabetic(jint ch);
            virtual jboolean isIdeographic(jint ch);

            static JCharacterDataLatin1 instance();

            static jarray<jchar> sharpsMap();
            static jarray<jint> A();
            static JString A_DATA();
            static jarray<jchar> B();

            static jboolean isStaticInitializationBlock;
            
            friend class CharacterData;
        };
    }
}

#endif // CPP_LANG_CHARACTERDATALATIN1_H
