#include "CharacterDataPrivateUse.h"
#include "Character.h"

namespace cpp
{
    namespace lang
    {
        CharacterDataPrivateUse::CharacterDataPrivateUse()
        {
        }

        CharacterDataPrivateUse::~CharacterDataPrivateUse()
        {
        }

        jint CharacterDataPrivateUse::getProperties(jint ch)
        {
            return 0;
        }

        jint CharacterDataPrivateUse::getType(jint ch)
        {
            return (ch & 0xFFFE) == 0xFFFE ? Character::UNASSIGNED : Character::PRIVATE_USE;
        }

        jboolean CharacterDataPrivateUse::isWhitespace(jint ch)
        {
            return false;
        }

        jboolean CharacterDataPrivateUse::isMirrored(jint ch)
        {
            return false;
        }

        jboolean CharacterDataPrivateUse::isJavaIdentifierStart(jint ch)
        {
            return false;
        }

        jboolean CharacterDataPrivateUse::isJavaIdentifierPart(jint ch)
        {
            return false;
        }

        jboolean CharacterDataPrivateUse::isUnicodeIdentifierStart(jint ch)
        {
            return false;
        }

        jboolean CharacterDataPrivateUse::isUnicodeIdentifierPart(jint ch)
        {
            return false;
        }

        jboolean CharacterDataPrivateUse::isIdentifierIgnorable(jint ch)
        {
            return false;
        }

        jint CharacterDataPrivateUse::toLowerCase(jint ch)
        {
            return ch;
        }

        jint CharacterDataPrivateUse::toUpperCase(jint ch)
        {
            return ch;
        }

        jint CharacterDataPrivateUse::toTitleCase(jint ch)
        {
            return ch;
        }

        jint CharacterDataPrivateUse::digit(jint ch, jint radix)
        {
            return -1;
        }

        jint CharacterDataPrivateUse::getNumericValue(jint ch)
        {
            return -1;
        }

        jbyte CharacterDataPrivateUse::getDirectionality(jint ch)
        {
            return (ch & 0xFFFE) == 0xFFFE ? Character::DIRECTIONALITY_UNDEFINED : Character::DIRECTIONALITY_LEFT_TO_RIGHT;
        }

        JCharacterDataPrivateUse CharacterDataPrivateUse::instance()
        {
            static JCharacterDataPrivateUse _instance = jnew<CharacterDataPrivateUse>();
            return _instance;
        }
    }
}
