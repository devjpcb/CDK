#include "CharacterDataUndefined.h"
#include "Character.h"

namespace cpp
{
    namespace lang
    {
        CharacterDataUndefined::CharacterDataUndefined()
        {
        }

        CharacterDataUndefined::~CharacterDataUndefined()
        {
        }

        jint CharacterDataUndefined::getProperties(jint ch)
        {
            return 0;
        }

        jint CharacterDataUndefined::getType(jint ch)
        {
            return Character::UNASSIGNED;
        }

        jboolean CharacterDataUndefined::isWhitespace(jint ch)
        {
            return false;
        }

        jboolean CharacterDataUndefined::isMirrored(jint ch)
        {
            return false;
        }

        jboolean CharacterDataUndefined::isJavaIdentifierStart(jint ch)
        {
            return false;
        }

        jboolean CharacterDataUndefined::isJavaIdentifierPart(jint ch)
        {
            return false;
        }

        jboolean CharacterDataUndefined::isUnicodeIdentifierStart(jint ch)
        {
            return false;
        }

        jboolean CharacterDataUndefined::isUnicodeIdentifierPart(jint ch)
        {
            return false;
        }

        jboolean CharacterDataUndefined::isIdentifierIgnorable(jint ch)
        {
            return false;
        }

        jint CharacterDataUndefined::toLowerCase(jint ch)
        {
            return ch;
        }

        jint CharacterDataUndefined::toUpperCase(jint ch)
        {
            return ch;
        }

        jint CharacterDataUndefined::toTitleCase(jint ch)
        {
            return ch;
        }

        jint CharacterDataUndefined::digit(jint ch, jint radix)
        {
            return -1;
        }

        jint CharacterDataUndefined::getNumericValue(jint ch)
        {
            return -1;
        }

        jbyte CharacterDataUndefined::getDirectionality(jint ch)
        {
            return Character::DIRECTIONALITY_UNDEFINED;
        }

        JCharacterDataUndefined CharacterDataUndefined::instance()
        {
            static JCharacterDataUndefined _instance = jnew<CharacterDataUndefined>();
            return _instance;
        }
    }
}
