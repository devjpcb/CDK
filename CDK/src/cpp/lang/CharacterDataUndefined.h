#ifndef CPP_LANG_CHARACTERDATAUNDEFINED_H
#define CPP_LANG_CHARACTERDATAUNDEFINED_H

#include "CharacterData.h"

namespace cpp
{
    namespace lang
    {
        class CharacterDataUndefined : public CharacterData
        {
        public:
            CharacterDataUndefined();
            ~CharacterDataUndefined();

        private:
            virtual jint getProperties(jint ch);
            virtual jint getType(jint ch);
            virtual jboolean isWhitespace(jint ch);
            virtual jboolean isMirrored(jint ch);
            virtual jboolean isJavaIdentifierStart(jint ch);
            virtual jboolean isJavaIdentifierPart(jint ch);
            virtual jboolean isUnicodeIdentifierStart(jint ch);
            virtual jboolean isUnicodeIdentifierPart(jint ch);
            virtual jboolean isIdentifierIgnorable(jint ch);
            virtual jint toLowerCase(jint ch);
            virtual jint toUpperCase(jint ch);
            virtual jint toTitleCase(jint ch);
            virtual jint digit(jint ch, jint radix);
            virtual jint getNumericValue(jint ch);
            virtual jbyte getDirectionality(jint ch);

            static JCharacterDataUndefined instance();
            
            friend class CharacterData;
        };
    }
}

#endif // CPP_LANG_CHARACTERDATAUNDEFINED_H
