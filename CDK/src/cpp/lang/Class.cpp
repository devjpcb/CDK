#include "Class.h"
#include "ClassLoader.h"
#include "String.h"
#include "StringBuilder.h"
#include "System.h"
#include "SecurityManager.h"
#include "reflect/Modifier.h"
#include "../../sun/reflect/Reflection.h"
#include "../../sun/misc/VM.h"
#include "../../sun/security/util/SecurityConstants.h"
#include "reflect/builder/ClassBuilder.h"

using namespace cpp::lang::reflect;
using namespace sun::reflect;
using namespace sun::security::util;
using namespace cpp::security;

namespace cpp
{
    namespace lang
    {
        Class::Class() :
            isarray(false), isinterface(false), islocalClass(false), ismemberClass(false), isprimitive(false)
        {

        }

        JClass Class::forName(JString className)
        {
            JClass caller = Reflection::getCallerClass();
            return forName0(className, true, ClassLoader::getClassLoader(caller), caller);
        }

        JClass Class::forName(JString name, jboolean initialize, JClassLoader loader)
        {
            JClass caller = nullptr;
            JSecurityManager sm = System::getSecurityManager();
            if (sm != nullptr) {
                caller = Reflection::getCallerClass();
                if (sun::misc::VM::isSystemDomainLoader(loader)) {
                    JClassLoader ccl = ClassLoader::getClassLoader(caller);
                    if (!sun::misc::VM::isSystemDomainLoader(ccl)) {
                        sm->checkPermission(SecurityConstants::GET_CLASSLOADER_PERMISSION);
                    }
                }
            }
            return forName0(name, initialize, loader, caller);
        }

        JClass Class::forName0(JString name, jboolean initialize, JClassLoader loader, JClass caller)
        {
            jint hash = name->hashCode();
            return cpp::lang::reflect::builder::ClassBuilder::getInternalClassList()[hash];
        }

        JString Class::getCanonicalName()
        {
            if(isArray()) return package->getName() + jstr(u"::") + simpleName + jstr(u"[]");
            return package->getName() + jstr(u"::") + simpleName;
        }

        JClass Class::getComponentType()
        {
            return nullptr;
        }

        jint Class::getModifiers()
        {
            return modifiers;
        }

        JString Class::getName()
        {
            JString name = this->name;
            if (name == nullptr)
                this->name = name = getName0();
            return name;
        }
        
        JString Class::getName0()
        {
            if(isArray()) jstr(u"[L") + package->getName() + jstr(u"::") + simpleName;
            return package->getName() + jstr(u"::") + simpleName;
        }

        JString Class::getSimpleName()
        {
            if(isArray()) return simpleName + jstr(u"[]");
            return simpleName;
        }

        JPackage Class::getPackage()
        {
            return package;
        }

        jboolean Class::isAnnotation()
        {
            return (getModifiers() & ANNOTATION) != 0;
        }

        jboolean Class::isAnonymousClass()
        {
            return jstr(u"")->equals(getSimpleName());
        }

        jboolean Class::isArray()
        {
            return isarray;
        }

        jboolean Class::isEnum()
        {
            return (getModifiers() & ENUM) != 0;
        }

        jboolean Class::isInterface()
        {
            return isinterface;
        }

        jboolean Class::isLocalClass()
        {
            return islocalClass;
        }

        jboolean Class::isMemberClass()
        {
            return ismemberClass;
        }

        jboolean Class::isPrimitive()
        {
            return isprimitive;
        }

        jboolean Class::isSynthetic()
        {
            return (getModifiers() & SYNTHETIC) != 0;
        }

        JObject Class::newInstance()
        {
            return funcNewInstance();
        }

        JString Class::toGenericString()
        {
            if (isPrimitive()) {
                return toString();
            } else {
                JStringBuilder sb = jnew<StringBuilder>();

                jint modifiers = getModifiers() & Modifier::classModifiers();
                if (modifiers != 0) {
                    sb->append(Modifier::toString(modifiers));
                    sb->append(u' ');
                }

                if (isAnnotation()) {
                    sb->append(u'@');
                }
                if (isInterface()) {
                    sb->append(jstr(u"interface"));
                } else {
                    if (isEnum())
                        sb->append(jstr(u"enum"));
                    else
                        sb->append(jstr(u"class"));
                }
                sb->append(u' ');
                sb->append(getName());

                /* TODO:
                TypeVariable<?>[] typeparms = getTypeParameters();
                if (typeparms.length > 0) {
                    boolean first = true;
                    sb->append(u'<');
                    for(TypeVariable<?> typeparm: typeparms) {
                        if (!first)
                            sb->append(u',');
                        sb->append(typeparm.getTypeName());
                        first = false;
                    }
                    sb->append(u'>');
                }
                */

                return sb->toString();
            }
        }

        JString Class::toString()
        {
            return (isInterface() ? jstr(u"interface ") : (isPrimitive() ? jstr(u"") : jstr(u"class "))) + getName();
        }
        
        JClassLoader Class::getClassLoader()
        {
            return nullptr;
        }

        JClassLoader Class::getClassLoader0()
        {
            return classLoader;
        }
        
        JProtectionDomain Class::getProtectionDomain()
        {
            return nullptr;
        }

        const jint Class::ANNOTATION = 0x00002000;
        const jint Class::ENUM       = 0x00004000;
        const jint Class::SYNTHETIC  = 0x00001000;
    }
}
