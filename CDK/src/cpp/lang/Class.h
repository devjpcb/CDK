#ifndef CPP_LANG_CLASS_H
#define CPP_LANG_CLASS_H

#include "Object.h"
#include "Package.h"
#include <atomic>
#include <functional>

namespace cpp
{
    namespace lang
    {
        // TODO: agregar herencia de Object
        class Class : public Object
        {
        public:
            Class();

            static JClass forName(JString className);
            static JClass forName(JString name, jboolean initialize, JClassLoader loader);
            virtual JString getCanonicalName();
            virtual JClass getComponentType();
            virtual jint getModifiers();
            virtual JString getName();
            virtual JString getSimpleName();
            virtual JPackage getPackage();
            virtual jboolean isAnnotation();
            virtual jboolean isAnonymousClass();
            virtual jboolean isArray();
            virtual jboolean isEnum();
            virtual jboolean isInterface() ;
            virtual jboolean isLocalClass();
            virtual jboolean isMemberClass();
            virtual jboolean isPrimitive();
            virtual jboolean isSynthetic();
            virtual JObject newInstance();
            virtual JString toGenericString();
            virtual JString toString();
            virtual JClassLoader getClassLoader();
            virtual cpp::security::JProtectionDomain getProtectionDomain();

        private:
            static JClass forName0(JString name, jboolean initialize, JClassLoader loader, JClass caller);
            JString getName0();
            JClassLoader getClassLoader0();
        
            JString name;
            JString simpleName;
            JPackage package;
            jint modifiers;
            jboolean isarray;
            jboolean isinterface;
            jboolean islocalClass;
            jboolean ismemberClass;
            jboolean isprimitive;
            std::function<JObject()> funcNewInstance;
            JClassLoader classLoader;

            static const jint ANNOTATION;
            static const jint ENUM;
            static const jint SYNTHETIC;

            friend class cpp::lang::reflect::builder::ClassBuilder;
            friend class cpp::lang::ClassLoader;
        };
    }
}

#endif // CPP_LANG_CLASS_H
