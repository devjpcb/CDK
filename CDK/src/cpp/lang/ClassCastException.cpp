#include "ClassCastException.h"
#include "String.h"

namespace cpp
{
    namespace lang
    {
        ClassCastException::ClassCastException() : RuntimeException()
        {
        }
        
        ClassCastException::ClassCastException(JString s) : RuntimeException(s)
        {
        }
        
        ClassCastException::~ClassCastException()
        {
        }
    }
}