#ifndef CPP_LANG_CLASSCASTEXCEPTION_H
#define CPP_LANG_CLASSCASTEXCEPTION_H

#include "RuntimeException.h"

namespace cpp
{
    namespace lang
    {
        class ClassCastException : public RuntimeException
        {
        public:
            ClassCastException();
            ClassCastException(JString s);
            ~ClassCastException();
        };
    }
}

#endif // CPP_LANG_CLASSCASTEXCEPTION_H
