#include "ClassLoader.h"
#include "Class.h"

namespace cpp
{
    namespace lang
    {
        JClassLoader ClassLoader::getClassLoader(JClass caller)
        {
            if (caller == nullptr) {
                return nullptr;
            }

            return caller->getClassLoader0();
        }
    }
}
