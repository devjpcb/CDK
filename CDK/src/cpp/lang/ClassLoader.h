#ifndef CLASSLOADER_H
#define CLASSLOADER_H

#include "Object.h"

namespace cpp
{
    namespace lang
    {
        class ClassLoader : public Object
        {
        public:
            static JClassLoader getClassLoader(JClass caller);
        };
    }
}

#endif // CLASSLOADER_H
