#include "CloneNotSupportedException.h"

namespace cpp
{
    namespace lang
    {
        CloneNotSupportedException::CloneNotSupportedException() : Exception()
        {
            
        }
        
        CloneNotSupportedException::CloneNotSupportedException(JString s) : Exception(s)
        {
            
        }

        CloneNotSupportedException::~CloneNotSupportedException()
        {
            
        }
    }
}
