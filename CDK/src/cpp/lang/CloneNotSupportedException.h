#ifndef CPP_LANG_CLONENOTSUPPORTEDEXCEPTION_H
#define CPP_LANG_CLONENOTSUPPORTEDEXCEPTION_H

#include "Exception.h"

namespace cpp
{
    namespace lang
    {
        class CloneNotSupportedException : public Exception
        {
        public:
            CloneNotSupportedException();
            CloneNotSupportedException(JString s);
            ~CloneNotSupportedException();
        };
    }
}

#endif // CPP_LANG_CLONENOTSUPPORTEDEXCEPTION_H
