#ifndef CPP_LANG_COMPARABLE_H
#define CPP_LANG_COMPARABLE_H

#include "Object.h"

namespace cpp
{
    namespace lang
    {
        template <typename T> 
        class Comparable : public virtual Object
        {
        public:
            virtual jint compareTo(T o) = 0;
        };
    }
}

#endif // CPP_LANG_COMPARABLE_H
