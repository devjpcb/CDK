#include "Double.h"
#include "Math.h"
#include "../../sun/misc/FloatingDecimal.h"

#define jlong_to_jdouble_bits(a)
#define jdouble_to_jlong_bits(a)

using sun::misc::FloatingDecimal;

namespace cpp
{
    namespace lang
    {
        Double::Double() : value(0.0)
        {
        }

        Double::Double(jdouble pvalue) : value(pvalue)
        {
        }
        
        Double::Double(JString s) : value(parseDouble(s))
        {
            
        }

        Double::~Double()
        {
        }

        jbyte Double::byteValue()
        {
            return static_cast<jbyte>(static_cast<jint>(value));
        }

        jint Double::compare(jdouble d1, jdouble d2)
        {
            if(d1 < d2) {
                return -1;
            }
            if(d1 > d2) {
                return 1;
            }
            long l1 = doubleToLongBits(d1);
            long l2 = doubleToLongBits(d2);

            return l1 < l2 ? -1 : l1 == l2 ? 0 : 1;
        }

        jint Double::compareTo(JDouble anotherDouble)
        {
            return compare(value, anotherDouble->value);
        }

        jlong Double::doubleToLongBits(jdouble value)
        {
            jlong l = doubleToRawLongBits(value);
            if(((l & 0x7FF0000000000000) == 9218868437227405312LL) && ((l & 0xFFFFFFFFFFFFF) != 0LL)) {
                l = 9221120237041090560LL;
            }
            return l;
        }

        jlong Double::doubleToRawLongBits(jdouble value)
        {
            union {
                jlong l;
                jdouble d;
            } u;
            jdouble_to_jlong_bits(&value);
            u.d = value;
            return u.l;
        }

        jdouble Double::doubleValue()
        {
            return value;
        }

        jboolean Double::equals(JObject obj)
        {
            JDouble dobj = std::dynamic_pointer_cast<Double>(obj);
            if(dobj != nullptr) {
                return doubleToLongBits(value) == doubleToLongBits(dobj->value);
            }

            return false;
        }

        jfloat Double::floatValue()
        {
            return static_cast<jfloat>(value);
        }

        jint Double::hashCode()
        {
            return hashCode(value);
        }

        jint Double::hashCode(jdouble value)
        {
            jlong bits = doubleToLongBits(value);
            return (jint)(bits ^ (bits >> 32));
        }

        jint Double::intValue()
        {
            return static_cast<jint>(value);
        }

        jboolean Double::isFinite(jdouble d)
        {
            return Math::abs(d) <= Double::MAX_VALUE;
        }

        jboolean Double::isInfinite()
        {
            return isInfinite(value);
        }

        jboolean Double::isInfinite(jdouble v)
        {
            return (v == Double::POSITIVE_INFINITY) || (v == Double::NEGATIVE_INFINITY);
        }

        jboolean Double::isNaN()
        {
            return isNaN(value);
        }

        jboolean Double::isNaN(jdouble v)
        {
            return v != v;
        }

        jdouble Double::longBitsToDouble(jlong bits)
        {
            union {
                jlong l;
                jdouble d;
            } u;
            jlong_to_jdouble_bits(&bits);
            u.l = bits;
            return u.d;
        }

        jlong Double::longValue()
        {
            return value;
        }

        jdouble Double::max(jdouble a, jdouble b)
        {
            return Math::max(a, b);
        }

        jdouble Double::min(jdouble a, jdouble b)
        {
            return Math::min(a, b);
        }
        
        jdouble Double::parseDouble(JString s)
        {
            return FloatingDecimal::parseDouble(s);
        }

        jshort Double::shortValue()
        {
            return static_cast<jshort>(static_cast<jint>(value));
        }

        jdouble Double::sum(jdouble a, jdouble b)
        {
            return a + b;
        }
        
        JString Double::toString()
        {
            return toString(value);
        }
        
        JString Double::toString(jdouble d)
        {
            return FloatingDecimal::toJavaFormatString(d);
        }

        JDouble Double::valueOf(jdouble d)
        {
            return jnew<Double>(d);
        }
        
        JDouble Double::valueOf(JString s)
        {
            return jnew<Double>(parseDouble(s));
        }

        const jdouble Double::NaN = 0.0 / 0.0;
        const jdouble Double::MAX_VALUE = 0x1.fffffffffffffP+1023;
        const jdouble Double::MIN_VALUE = 0x0.0000000000001P-1022;
        const jdouble Double::POSITIVE_INFINITY = 1.0 / 0.0;
        const jdouble Double::NEGATIVE_INFINITY = -1.0 / 0.0;
    }
}
