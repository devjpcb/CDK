#ifndef CPP_LANG_DOUBLE_H
#define CPP_LANG_DOUBLE_H

#include "Number.h"
#include "Comparable.h"

namespace cpp
{
    namespace lang
    {
        class Double : public Number, public Comparable<JDouble>
        {
        public:
            Double();
            Double(jdouble pvalue);
            Double(JString s);
            ~Double();

            jbyte byteValue();
            static jint compare(jdouble d1, jdouble d2);
            jint compareTo(JDouble anotherDouble);
            static jlong doubleToLongBits(jdouble value);
            static jlong doubleToRawLongBits(jdouble value);
            jdouble doubleValue();
            jboolean equals(JObject obj);
            jfloat floatValue();
            jint hashCode();
            static jint hashCode(jdouble value);
            jint intValue();
            static jboolean isFinite(jdouble d);
            jboolean isInfinite();
            static jboolean isInfinite(jdouble v);
            jboolean isNaN();
            static jboolean isNaN(jdouble v);
            static jdouble longBitsToDouble(jlong bits);
            jlong longValue();
            static jdouble max(jdouble a, jdouble b);
            static jdouble min(jdouble a, jdouble b);
            static jdouble parseDouble(JString s);
            jshort shortValue();
            static jdouble sum(jdouble a, jdouble b);
            JString toString();
            static JString toString(jdouble d);
            static JDouble valueOf(jdouble d);
            static JDouble valueOf(JString s);
            
            static const jdouble NaN;
            static const jdouble MAX_VALUE;
            static const jdouble MIN_VALUE;
            static const jdouble POSITIVE_INFINITY;
            static const jdouble NEGATIVE_INFINITY;
        private:
            double value;
        };
    }
}

#endif // CPP_LANG_DOUBLE_H
