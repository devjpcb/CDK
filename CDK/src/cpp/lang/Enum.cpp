#include "Enum.h"

namespace cpp
{
    namespace lang
    {
        Enum::Enum(JString name, jint ordinal) : _name(name), _ordinal(ordinal)
        {
        }

        JString Enum::name() 
        {
            return _name;
        }
        
        jint Enum::ordinal() 
        {
            return _ordinal;
        }
        
        JString Enum::toString() 
        {
            return _name;
        }
    }
}
