#ifndef CPP_LANG_ENUM_H
#define CPP_LANG_ENUM_H

#include "Object.h"
#include "String.h"

namespace cpp
{
    namespace lang
    {
        class Enum : public Object
        {
        public:
            Enum(JString name, jint ordinal);
            
            JString name();
            jint ordinal();
            virtual JString toString();
            
        private:
            JString _name;
            jint _ordinal;
        };
    }
}

#define jenum_begin(enumName) \
    class enumName; \
    using J##enumName = std::shared_ptr<enumName>; \
    class enumName : public cpp::lang::Enum { \
    using EnumType = std::shared_ptr<enumName>; \
    public: enumName(cpp::lang::JString name, jint ordinal) : Enum(name, ordinal) {} \
    
#define jenum_declare(enumElementName) \
    public: static const jint enumElementName; \
    public: static const EnumType J##enumElementName;

#define jenum_end };

#define jenum_define(enumName, enumElementValue, enumElementName) \
    const jint enumName::enumElementName = enumElementValue; \
    const J##enumName enumName::J##enumElementName = jnew<enumName>(cpp::lang::to_jstr(#enumElementName), enumElementValue);
    
#define jenum_define_args(enumName, enumElementName, ...) \
    const jint enumName::enumElementName = enumElementValue; \
    const J##enumName enumName::J##enumElementName = jnew<enumName>(cpp::lang::to_jstr(#enumElementName), enumElementValue, __VA_ARGS__);

#define jenum_switch(enumObj) switch(enumObj->ordinal())

#endif // ENUM_H
