#include "Error.h"

namespace cpp
{
    namespace lang
    {
        Error::Error() : Throwable()
        {
        }

        Error::Error(JString message) : Throwable(message)
        {
        }
        
        Error::Error(JString message, Throwable& cause) : Throwable(message, cause)
        {
        }
        
        Error::Error(JString message, Throwable& cause, jboolean enableSuppression, jboolean writableStackTrace) : Throwable(message, cause, enableSuppression, writableStackTrace)
        {
        }
        
        Error::Error(Throwable& cause) : Throwable(cause)
        {
        }

        Error::~Error()
        {
        }
    }
}
