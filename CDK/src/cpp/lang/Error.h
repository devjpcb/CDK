#ifndef CPP_LANG_ERROR_H
#define CPP_LANG_ERROR_H

#include "Throwable.h"

namespace cpp
{
    namespace lang
    {
        class Error : public Throwable
        {
        public:
            Error();
            Error(JString message);
            Error(JString message, Throwable& cause);
            Error(Throwable& cause);
            ~Error();
            
        protected:
            Error(JString message, Throwable& cause, jboolean enableSuppression, jboolean writableStackTrace);
        };
    }
}

#endif // CPP_LANG_ERROR_H
