#include "Exception.h"

namespace cpp
{
    namespace lang
    {
        Exception::Exception() : Throwable() 
        {
        }

        Exception::Exception(JString message) : Throwable(message)
        {
        }

        Exception::Exception(JString message, Throwable& cause) : Throwable(message, cause)
        {
        }
        
        Exception::Exception(JString message, Throwable& cause, jboolean enableSuppression, jboolean writableStackTrace) : Throwable(message, cause, enableSuppression, writableStackTrace)
        {
        }
        
        Exception::Exception(Throwable& cause) : Throwable(cause)
        {
        }

        Exception::~Exception()
        {
        }
    }
}
