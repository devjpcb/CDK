#ifndef CPP_LANG_EXCEPTION_H
#define CPP_LANG_EXCEPTION_H

#include "Throwable.h"

namespace cpp
{
    namespace lang
    {
        class Exception : public Throwable
        {
        public:
            Exception();
            Exception(JString message);
            Exception(JString message, Throwable& cause);
            Exception(JString message, Throwable& cause, jboolean enableSuppression, jboolean writableStackTrace);
            Exception(Throwable& cause);
            ~Exception();
        };
    }
}

#endif // CPP_LANG_EXCEPTION_H
