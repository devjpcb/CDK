#include "Float.h"
#include "Math.h"
#include "String.h"
#include "../../sun/misc/FloatingDecimal.h"

using sun::misc::FloatingDecimal;

namespace cpp
{
    namespace lang
    {
        Float::Float() : value(0.0f)
        {
        }

        Float::Float(jdouble pvalue) : value(static_cast<jfloat>(pvalue))
        {
        }

        Float::Float(jfloat pvalue) : value(pvalue)
        {
        }
        
        Float::Float(JString s) : value(parseFloat(s))
        {
        }

        Float::~Float()
        {
        }

        jbyte Float::byteValue()
        {
            return static_cast<jbyte>(static_cast<int>(value));
        }

        jint Float::compare(jfloat f1, jfloat f2)
        {
            if(f1 < f2) {
                return -1;
            }
            if(f1 > f2) {
                return 1;
            }
            int i = floatToIntBits(f1);
            int j = floatToIntBits(f2);

            return i < j ? -1 : i == j ? 0 : 1;
        }

        jint Float::compareTo(JFloat anotherFloat)
        {
            return compare(value, anotherFloat->value);
        }

        jdouble Float::doubleValue()
        {
            return value;
        }

        jboolean Float::equals(JObject obj)
        {
            JFloat fobj = std::dynamic_pointer_cast<Float>(obj);
            if(fobj != nullptr) {
                return floatToIntBits(value) == floatToIntBits(fobj->value);
            }

            return false;
        }

        jint Float::floatToIntBits(jfloat value)
        {
            jint i = floatToRawIntBits(value);
            if(((i & 0x7F800000) == 2139095040) && ((i & 0x7FFFFF) != 0)) {
                i = 2143289344;
            }
            return i;
        }

        jint Float::floatToRawIntBits(jfloat value)
        {
            union {
                jint i;
                jfloat f;
            } u;
            u.f = value;
            return u.i;
        }

        jfloat Float::floatValue()
        {
            return value;
        }

        jint Float::hashCode()
        {
            return hashCode(value);
        }

        jint Float::hashCode(jfloat value)
        {
            return floatToIntBits(value);
        }

        jfloat Float::intBitsToFloat(jint bits)
        {
            union {
                jint i;
                jfloat f;
            } u;
            u.i = bits;
            return u.f;
        }

        jint Float::intValue()
        {
            return static_cast<jint>(value);
        }

        jboolean Float::isFinite(jfloat f)
        {
            return Math::abs(f) <= Float::MAX_VALUE;
        }

        jboolean Float::isInfinite()
        {
            return isInfinite(value);
        }

        jboolean Float::isInfinite(jfloat v)
        {
            return (v == Float::POSITIVE_INFINITY) || (v == Float::NEGATIVE_INFINITY);
        }

        jboolean Float::isNaN()
        {
            return isNaN(value);
        }

        jboolean Float::isNaN(jfloat v)
        {
            return v != v;
        }

        jlong Float::longValue()
        {
            return value;
        }

        jfloat Float::max(jfloat a, jfloat b)
        {
            return Math::max(a, b);
        }

        jfloat Float::min(jfloat a, jfloat b)
        {
            return Math::min(a, b);
        }
        
        jfloat Float::parseFloat(JString s)
        {
            return FloatingDecimal::parseFloat(s);
        }

        jshort Float::shortValue()
        {
            return static_cast<jshort>(static_cast<jint>(value));
        }

        jfloat Float::sum(jfloat a, jfloat b)
        {
            return a + b;
        }

        JString Float::toString() 
        {
            return toString(value);
        }
        
        JString Float::toString(jfloat f)
        {
            return FloatingDecimal::toJavaFormatString(f);
        }

        JFloat Float::valueOf(jfloat f)
        {
            return jnew<Float>(f);
        }
        
        JFloat Float::valueOf(JString s)
        {
            return jnew<Float>(parseFloat(s));
        }

        const jfloat Float::NaN =  0.0f / 0.0f;
        const jfloat Float::MAX_VALUE = 0x1.fffffeP+127f;
        const jfloat Float::MIN_VALUE = 0x0.000002P-126f;
        const jfloat Float::POSITIVE_INFINITY = 1.0f / 0.0f;
        const jfloat Float::NEGATIVE_INFINITY = -1.0f / 0.0f;
    }
}
