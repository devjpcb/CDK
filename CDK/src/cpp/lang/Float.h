#ifndef CPP_LANG_FLOAT_H
#define CPP_LANG_FLOAT_H

#include "Number.h"
#include "Comparable.h"

namespace cpp
{
    namespace lang
    {
        class Float : public Number, public Comparable<JFloat>
        {
        public:
            Float();
            Float(jdouble pvalue);
            Float(jfloat pvalue);
            Float(JString s);
            ~Float();

            jbyte byteValue();
            static jint compare(jfloat f1, jfloat f2);
            jint compareTo(JFloat anotherFloat);
            jdouble doubleValue();
            jboolean equals(JObject obj);
            static jint floatToIntBits(jfloat value);
            static jint floatToRawIntBits(jfloat value);
            jfloat floatValue();
            jint hashCode();
            static jint hashCode(jfloat value);
            static jfloat intBitsToFloat(jint bits);
            jint intValue();
            static jboolean isFinite(jfloat f);
            jboolean isInfinite();
            static jboolean isInfinite(jfloat v);
            jboolean isNaN();
            static jboolean isNaN(jfloat v);
            jlong longValue();
            static jfloat max(jfloat a, jfloat b);
            static jfloat min(jfloat a, jfloat b);
            static jfloat parseFloat(JString s);
            jshort shortValue();
            static jfloat sum(jfloat a, jfloat b);
            JString toString();
            static JString toString(jfloat f);
            static JFloat valueOf(jfloat f);
            static JFloat valueOf(JString s);

            static const jfloat NaN;
            static const jfloat MAX_VALUE;
            static const jfloat MIN_VALUE;
            static const jfloat POSITIVE_INFINITY;
            static const jfloat NEGATIVE_INFINITY;

        private:
            jfloat value;
        };
    }
}

#endif // CPP_LANG_FLOAT_H
