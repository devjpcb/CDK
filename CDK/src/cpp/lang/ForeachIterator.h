#ifndef CPP_LANG_FOREACHITERATOR_H
#define CPP_LANG_FOREACHITERATOR_H

#include "../util/Iterator.h"

namespace cpp
{
    namespace lang
    {
        template <typename T>
        class ForeachIterator
        {
        public:
            ForeachIterator(cpp::util::JIterator<T> iterator) : iterator(iterator) {
            }

            jboolean operator != (ForeachIterator<T>& v) {
                return iterator->hasNext();
            }
            
            ForeachIterator<T>& operator ++ () {
                return *this;
            }
            
            T operator * () {
                return iterator->next();
            }

        private:
            cpp::util::JIterator<T> iterator;
        };
    }
}

#endif // CPP_LANG_FOREACHITERATOR_H
