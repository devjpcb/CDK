#include "IllegalArgumentException.h"

namespace cpp
{
    namespace lang
    {
        IllegalArgumentException::IllegalArgumentException() : RuntimeException()
        {
        }

        IllegalArgumentException::IllegalArgumentException(JString s) : RuntimeException(s)
        {
        }

        IllegalArgumentException::IllegalArgumentException(JString message, Throwable& cause)  : RuntimeException(message, cause)
        {
        }

        IllegalArgumentException::IllegalArgumentException(Throwable& cause) : RuntimeException(cause)
        {
        }


        IllegalArgumentException::~IllegalArgumentException()
        {
        }
    }
}
