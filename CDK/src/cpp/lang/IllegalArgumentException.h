#ifndef CPP_LANG_ILLEGALARGUMENTEXCEPTION_H
#define CPP_LANG_ILLEGALARGUMENTEXCEPTION_H

#include "RuntimeException.h"

namespace cpp
{
    namespace lang
    {
        class IllegalArgumentException : public RuntimeException
        {
        public:
            IllegalArgumentException();
            IllegalArgumentException(JString s);
            IllegalArgumentException(JString message, Throwable& cause);
            IllegalArgumentException(Throwable& cause);
            ~IllegalArgumentException();
        };
    }
}

#endif // CPP_LANG_ILLEGALARGUMENTEXCEPTION_H
