#include "IllegalStateException.h"
#include "String.h"

namespace cpp
{
    namespace lang
    {
        IllegalStateException::IllegalStateException() : RuntimeException()
        {
        }

        IllegalStateException::IllegalStateException(JString s) : RuntimeException(s)
        {
        }
        
        IllegalStateException::IllegalStateException(JString message, Throwable& cause) : RuntimeException(message, cause)
        {
        }
        
        IllegalStateException::IllegalStateException(Throwable& cause) : RuntimeException(cause)
        {
        }

        IllegalStateException::~IllegalStateException()
        {
        }
    }
}
