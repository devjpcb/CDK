#ifndef CPP_LANG_ILLEGALSTATEEXCEPTION_H
#define CPP_LANG_ILLEGALSTATEEXCEPTION_H

#include "RuntimeException.h"

namespace cpp
{
    namespace lang
    {
        class IllegalStateException : public RuntimeException
        {
        public:
            IllegalStateException();
            IllegalStateException(JString s);
            IllegalStateException(JString message, Throwable& cause);
            IllegalStateException(Throwable& cause);
            ~IllegalStateException();
        };
    }
}

#endif // CPP_LANG_ILLEGALSTATEEXCEPTION_H
