#include "IllegalThreadStateException.h"

namespace cpp
{
    namespace lang
    {
        IllegalThreadStateException::IllegalThreadStateException() : IllegalArgumentException()
        {
        }
        
        IllegalThreadStateException::IllegalThreadStateException(JString s) : IllegalArgumentException(s)
        {
        }

        IllegalThreadStateException::~IllegalThreadStateException()
        {
        }
    }
}
