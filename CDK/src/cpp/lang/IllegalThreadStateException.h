#ifndef CPP_LANG_ILLEGALTHREADSTATEEXCEPTION_H
#define CPP_LANG_ILLEGALTHREADSTATEEXCEPTION_H

#include "IllegalArgumentException.h"

namespace cpp
{
    namespace lang
    {
        class IllegalThreadStateException : public IllegalArgumentException
        {
        public:
            IllegalThreadStateException();
            IllegalThreadStateException(JString s);
            ~IllegalThreadStateException();
        };
    }
}

#endif // CPP_LANG_ILLEGALTHREADSTATEEXCEPTION_H
