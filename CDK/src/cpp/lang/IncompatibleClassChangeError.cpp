#include "IncompatibleClassChangeError.h"

namespace cpp
{
    namespace lang
    {
        IncompatibleClassChangeError::IncompatibleClassChangeError() : LinkageError()
        {
        }

        IncompatibleClassChangeError::IncompatibleClassChangeError(JString s) : LinkageError(s)
        {
        }
    }
}
