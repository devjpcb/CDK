#ifndef CPP_LANG_INCOMPATIBLECLASSCHANGEERROR_H
#define CPP_LANG_INCOMPATIBLECLASSCHANGEERROR_H

#include "LinkageError.h"

namespace cpp
{
    namespace lang
    {
        class IncompatibleClassChangeError : public LinkageError
        {
        public:
            IncompatibleClassChangeError();
            IncompatibleClassChangeError(JString s);
        };
    }
}

#endif // CPP_LANG_INCOMPATIBLECLASSCHANGEERROR_H
