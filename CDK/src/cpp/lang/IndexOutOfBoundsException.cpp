#include "IndexOutOfBoundsException.h"

namespace cpp
{
    namespace lang
    {
        IndexOutOfBoundsException::IndexOutOfBoundsException() : RuntimeException()
        {
        }
        
        IndexOutOfBoundsException::IndexOutOfBoundsException(JString s) : RuntimeException(s)
        {
        }

        IndexOutOfBoundsException::~IndexOutOfBoundsException()
        {
        }
    }
}
