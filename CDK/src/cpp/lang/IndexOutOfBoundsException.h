#ifndef CPP_LANG_INDEXOUTOFBOUNDSEXCEPTION_H
#define CPP_LANG_INDEXOUTOFBOUNDSEXCEPTION_H

#include "RuntimeException.h"

namespace cpp
{
    namespace lang
    {
        class IndexOutOfBoundsException : public RuntimeException
        {
        public:
            IndexOutOfBoundsException();
            IndexOutOfBoundsException(JString s);
            ~IndexOutOfBoundsException();
        };
    }
}

#endif // CPP_LANG_INDEXOUTOFBOUNDSEXCEPTION_H
