#include "Integer.h"
#include "Long.h"
#include "Math.h"
#include "String.h"
#include "Character.h"
#include "NumberFormatException.h"
#include <cassert>

namespace cpp
{
    namespace lang
    {
        Integer::Integer() : value(0)
        {
        }

        Integer::Integer(jint pvalue) : value(pvalue)
        {
        }

        Integer::Integer(JString s)
        {
            value = parseInt(s, 10);
        }

        Integer::~Integer()
        {
        }

        jint Integer::bitCount(jint i)
        {
            i = i - ((static_cast<unsigned int>(i) >> 1) & 0x55555555);
            i = (i & 0x33333333) + ((static_cast<unsigned int>(i) >> 2) & 0x33333333);
            i = (i + (static_cast<unsigned int>(i) >> 4)) & 0x0f0f0f0f;
            i = i + (static_cast<unsigned int>(i) >> 8);
            i = i + (static_cast<unsigned int>(i) >> 16);
            return i & 0x3f;
        }

        jbyte Integer::byteValue()
        {
            return static_cast<jbyte>(value);
        }

        jint Integer::compare(jint x, jint y)
        {
            return x == y ? 0 : x < y ? -1 : 1;
        }

        jint Integer::compareTo(JInteger anotherInteger)
        {
            return compare(value, anotherInteger->value);
        }

        jint Integer::compareUnsigned(jint x, jint y)
        {
            return compare(x + Integer::MIN_VALUE, y + Integer::MIN_VALUE);
        }

        JInteger Integer::decode(JString nm)
        {
            jint radix = 10;
            jint index = 0;
            jboolean negative = false;
            JInteger result;

            if(nm->length() == 0)
                throw NumberFormatException(jstr(u"Zero length string"));
            jchar firstChar = nm->charAt(0);
            // Handle sign, if present
            if(firstChar == u'-') {
                negative = true;
                index++;
            }
            else if(firstChar == u'+')
                index++;

            // Handle radix specifier, if present
            if(nm->startsWith(jstr(u"0x"), index) || nm->startsWith(jstr(u"0X"), index)) {
                index += 2;
                radix = 16;
            }
            else if(nm->startsWith(jstr(u"#"), index)) {
                index ++;
                radix = 16;
            }
            else if(nm->startsWith(jstr(u"0"), index) && nm->length() > 1 + index) {
                index ++;
                radix = 8;
            }

            if(nm->startsWith(jstr(u"-"), index) || nm->startsWith(jstr(u"+"), index))
                throw NumberFormatException(jstr(u"Sign character in wrong position"));

            try {
                result = Integer::valueOf(nm->substring(index), radix);
                result = negative ? Integer::valueOf(-result->intValue()) : result;
            }
            catch(NumberFormatException& e) {
                // If number is Integer.MIN_VALUE, we'll end up here. The next line
                // handles this case, and causes any genuine format error to be
                // rethrown.
                JString constant = negative ? (jstr(u"-") + nm->substring(index)) : nm->substring(index);
                result = Integer::valueOf(constant, radix);
            }

            return result;
        }

        jint Integer::divideUnsigned(jint dividend, jint divisor)
        {
            return static_cast<jint>(toUnsignedLong(dividend) / toUnsignedLong(divisor));
        }

        jdouble Integer::doubleValue()
        {
            return value;
        }

        jboolean Integer::equals(JObject obj)
        {
            JInteger iobj = std::dynamic_pointer_cast<Integer>(obj);
            if(iobj != nullptr) {
                return value == iobj->value;
            }

            return false;
        }

        jfloat Integer::floatValue()
        {
            return value;
        }

        jint Integer::hashCode()
        {
            return hashCode(value);
        }

        jint Integer::hashCode(jint value)
        {
            return value;
        }

        jint Integer::highestOneBit(jint i)
        {
            i |= (i >> 1);
            i |= (i >> 2);
            i |= (i >> 4);
            i |= (i >> 8);
            i |= (i >> 16);
            return i - (static_cast<unsigned int>(i) >> 1);
        }

        jint Integer::intValue()
        {
            return value;
        }

        jlong Integer::longValue()
        {
            return value;
        }

        jint Integer::lowestOneBit(jint i)
        {
            return i & -i;
        }

        jint Integer::max(jint a, jint b)
        {
            return Math::max(a, b);
        }

        jint Integer::min(jint a, jint b)
        {
            return Math::min(a, b);
        }

        jint Integer::numberOfLeadingZeros(jint i)
        {
            if(i == 0)
                return 32;
            jint n = 1;
            if(static_cast<unsigned int>(i) >> 16 == 0) {
                n += 16;
                i <<= 16;
            }
            if(static_cast<unsigned int>(i) >> 24 == 0) {
                n +=  8;
                i <<=  8;
            }
            if(static_cast<unsigned int>(i) >> 28 == 0) {
                n +=  4;
                i <<=  4;
            }
            if(static_cast<unsigned int>(i) >> 30 == 0) {
                n +=  2;
                i <<=  2;
            }
            n -= static_cast<unsigned int>(i) >> 31;
            return n;
        }

        jint Integer::numberOfTrailingZeros(jint i)
        {
            jint y;
            if(i == 0) return 32;
            jint n = 31;
            y = i << 16;
            if(y != 0) {
                n = n - 16;
                i = y;
            }
            y = i << 8;
            if(y != 0) {
                n = n - 8;
                i = y;
            }
            y = i << 4;
            if(y != 0) {
                n = n - 4;
                i = y;
            }
            y = i << 2;
            if(y != 0) {
                n = n - 2;
                i = y;
            }
            return n - (static_cast<unsigned int>(i << 1) >> 31);
        }

        jint Integer::parseInt(JString s)
        {
            return parseInt(s, 10);
        }

        jint Integer::parseInt(JString s, jint radix)
        {
            if(s == nullptr) {
                throw NumberFormatException(jstr(u"null"));
            }

            if(radix < Character::MIN_RADIX) {
                throw NumberFormatException(jstr(u"radix ") + boxing(radix) + jstr(u" less than Character::MIN_RADIX"));
            }

            if(radix > Character::MAX_RADIX) {
                throw NumberFormatException(jstr(u"radix ") + boxing(radix) + jstr(u" greater than Character::MAX_RADIX"));
            }

            jint result = 0;
            jboolean negative = false;
            jint i = 0, len = s->length();
            jint limit = -Integer::MAX_VALUE;
            jint multmin;
            jint digit;

            if(len > 0) {
                jchar firstChar = s->charAt(0);
                if(firstChar < u'0') { // Possible leading "+" or "-"
                    if(firstChar == u'-') {
                        negative = true;
                        limit = Integer::MIN_VALUE;
                    }
                    else if(firstChar != u'+') {
                        throw NumberFormatException::forInputString(s);
                    }

                    if(len == 1) { // Cannot have lone "+" or "-"
                        throw NumberFormatException::forInputString(s);
                    }
                    i++;
                }
                multmin = limit / radix;
                while(i < len) {
                    // Accumulating negatively avoids surprises near MAX_VALUE
                    digit = Character::digit(s->charAt(i++), radix);
                    if(digit < 0) {
                        throw NumberFormatException::forInputString(s);
                    }
                    if(result < multmin) {
                        throw NumberFormatException::forInputString(s);
                    }
                    result *= radix;
                    if(result < limit + digit) {
                        throw NumberFormatException::forInputString(s);
                    }
                    result -= digit;
                }
            }
            else {
                throw NumberFormatException::forInputString(s);
            }
            return negative ? result : -result;
        }

        jint Integer::parseUnsignedInt(JString s)
        {
            return parseUnsignedInt(s, 10);
        }

        jint Integer::parseUnsignedInt(JString s, jint radix)
        {
            if(s == nullptr)  {
                throw NumberFormatException(jstr(u"null"));
            }

            jint len = s->length();
            if(len > 0) {
                jchar firstChar = s->charAt(0);
                if(firstChar == u'-') {
                    throw NumberFormatException(/* TODO: String.format("Illegal leading minus sign on unsigned string %s.", s)*/);
                }
                else {
                    if(len <= 5 ||  // Integer.MAX_VALUE in Character.MAX_RADIX is 6 digits
                       (radix == 10 && len <= 9)) {  // Integer.MAX_VALUE in base 10 is 10 digits
                        return parseInt(s, radix);
                    }
                    else {
                        jlong ell = Long::parseLong(s, radix);
                        if((ell & 0xffffffff00000000LL) == 0) {
                            return (jint) ell;
                        }
                        else {
                            throw NumberFormatException(/*TODO: String.format("String value %s exceeds range of unsigned int.", s)*/);
                        }
                    }
                }
            }
            else {
                throw NumberFormatException::forInputString(s);
            }
        }

        jint Integer::remainderUnsigned(jint dividend, jint divisor)
        {
            return static_cast<jint>(toUnsignedLong(dividend) % toUnsignedLong(divisor));
        }

        jint Integer::reverse(jint i)
        {
            i = ((i & 0x55555555) << 1) | ((static_cast<unsigned int>(i) >> 1) & 0x55555555);
            i = ((i & 0x33333333) << 2) | ((static_cast<unsigned int>(i) >> 2) & 0x33333333);
            i = ((i & 0x0f0f0f0f) << 4) | ((static_cast<unsigned int>(i) >> 4) & 0x0f0f0f0f);
            i = (i << 24) | ((i & 0xff00) << 8) | ((static_cast<unsigned int>(i) >> 8) & 0xff00) | (static_cast<unsigned int>(i) >> 24);
            return i;
        }

        jint Integer::reverseBytes(jint i)
        {
            return ((static_cast<unsigned int>(i) >> 24)) | ((i >>   8) & 0xFF00) |((i << 8) & 0xFF0000) | ((i << 24));
        }

        jint Integer::rotateLeft(jint i, jint distance)
        {
            return (i << distance) | (static_cast<unsigned int>(i) >> -distance);
        }

        jint Integer::rotateRight(jint i, jint distance)
        {
            return (static_cast<unsigned int>(i) >> distance) | (i << -distance);
        }

        jshort Integer::shortValue()
        {
            return static_cast<jshort>(value);
        }

        jint Integer::signum(jint i)
        {
            return (i >> 31) | (static_cast<unsigned int>(-i) >> 31);
        }

        jint Integer::sum(jint a, jint b)
        {
            return a + b;
        }

        JString Integer::toBinaryString(jint i)
        {
            return toUnsignedString0(i, 1);
        }

        JString Integer::toHexString(jint i)
        {
            return toUnsignedString0(i, 4);
        }

        JString Integer::toOctalString(jint i)
        {
            return toUnsignedString0(i, 3);
        }

        JString Integer::toString()
        {
            return toString(value);
        }

        JString Integer::toString(jint i)
        {
            if(i == Integer::MIN_VALUE)
                return jstr(u"-2147483648");
            jint size = (i < 0) ? stringSize(-i) + 1 : stringSize(i);
            jarray<jchar> buf(size);
            getChars(i, size, buf);
            return jnew<String>(buf, true);
        }

        JString Integer::toString(jint i, jint radix)
        {
            if(radix < Character::MIN_RADIX || radix > Character::MAX_RADIX)
                radix = 10;

            /* Use the faster version */
            if(radix == 10) {
                return toString(i);
            }

            jarray<jchar> buf(33);
            jboolean negative = (i < 0);
            jint charPos = 32;

            if(!negative) {
                i = -i;
            }

            while(i <= -radix) {
                buf[charPos--] = digits[-(i % radix)];
                i = i / radix;
            }
            buf[charPos] = digits[-i];

            if(negative) {
                buf[--charPos] = u'-';
            }

            return jnew<String>(buf, charPos, (33 - charPos));
        }

        jlong Integer::toUnsignedLong(jint x)
        {
            return x & 0xFFFFFFFF;
        }

        JString Integer::toUnsignedString(jint i)
        {
            return Long::toString(toUnsignedLong(i));
        }

        JString Integer::toUnsignedString(jint i, jint radix)
        {
            return Long::toUnsignedString(toUnsignedLong(i), radix);
        }

        JInteger Integer::valueOf(jint i)
        {
            if (i >= IntegerCache::low && i <= IntegerCache::high)
                return IntegerCache::cache[i + (-IntegerCache::low)];
            return jnew<Integer>(i);
        }

        JInteger Integer::valueOf(JString s)
        {
            return valueOf(s, 10);
        }

        JInteger Integer::valueOf(JString s, jint radix)
        {
            return Integer::valueOf(parseInt(s, radix));
        }

        jint Integer::formatUnsignedInt(jint val, jint shift, jarray<jchar> buf, jint offset, jint len)
        {
            jint charPos = len;
            jint radix = 1 << shift;
            jint mask = radix - 1;
            do {
                buf[offset + --charPos] = Integer::digits[val & mask];
                val = static_cast<unsigned int>(val) >> shift;
            }
            while(val != 0 && charPos > 0);

            return charPos;
        }

        JString Integer::toUnsignedString0(jint val, jint shift)
        {
            jint mag = Integer::SIZE - Integer::numberOfLeadingZeros(val);
            jint chars = Math::max(((mag + (shift - 1)) / shift), 1);
            jarray<jchar> buf(chars);

            formatUnsignedInt(val, shift, buf, 0, chars);

            return jnew<String>(buf, true);
        }

        void Integer::getChars(jint i, jint index, jarray<jchar> buf)
        {
            jint q, r;
            jint charPos = index;
            jchar sign = 0;

            if(i < 0) {
                sign = '-';
                i = -i;
            }

            // Generate two digits per iteration
            while(i >= 65536) {
                q = i / 100;
                // really: r = i - (q * 100);
                r = i - ((q << 6) + (q << 5) + (q << 2));
                i = q;
                buf [--charPos] = DigitOnes[r];
                buf [--charPos] = DigitTens[r];
            }

            // Fall thru to fast mode for smaller numbers
            // assert(i <= 65536, i);
            for(;;) {
                q = static_cast<unsigned int>(i * 52429) >> (16 + 3);
                r = i - ((q << 3) + (q << 1));  // r = i-(q*10) ...
                buf [--charPos] = digits [r];
                i = q;
                if(i == 0) break;
            }
            if(sign != 0) {
                buf [--charPos] = sign;
            }
        }

        jint Integer::stringSize(jint x)
        {
            for(jint i = 0; ; i++)
                if(x <= sizeTable[i])
                    return i + 1;
        }

        const jint Integer::MAX_VALUE = 0x7fffffff;
        const jint Integer::MIN_VALUE = 0x80000000;
        const jint Integer::SIZE = 32;

        const jarray<jchar> Integer::digits = {
            u'0', u'1', u'2', u'3', u'4', u'5', u'6', u'7', u'8',
            u'9', u'a', u'b', u'c', u'd', u'e', u'f', u'g', u'h',
            u'i', u'j', u'k', u'l', u'm', u'n', u'o', u'p', u'q',
            u'r', u's', u't', u'u', u'v', u'w', u'x', u'y', u'z'
        };

        const jarray<jchar> Integer::DigitTens = {
            '0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
            '1', '1', '1', '1', '1', '1', '1', '1', '1', '1',
            '2', '2', '2', '2', '2', '2', '2', '2', '2', '2',
            '3', '3', '3', '3', '3', '3', '3', '3', '3', '3',
            '4', '4', '4', '4', '4', '4', '4', '4', '4', '4',
            '5', '5', '5', '5', '5', '5', '5', '5', '5', '5',
            '6', '6', '6', '6', '6', '6', '6', '6', '6', '6',
            '7', '7', '7', '7', '7', '7', '7', '7', '7', '7',
            '8', '8', '8', '8', '8', '8', '8', '8', '8', '8',
            '9', '9', '9', '9', '9', '9', '9', '9', '9', '9',
        };

        const jarray<jchar> Integer::DigitOnes = {
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
        };

        const jarray<jint> Integer::sizeTable = {
            9, 99, 999, 9999, 99999, 999999, 9999999,
            99999999, 999999999, Integer::MAX_VALUE
        };
        
        jint Integer::IntegerCache::low = -128;
        jint Integer::IntegerCache::high = 0;
        jarray<JInteger> Integer::IntegerCache::cache;
                
        jboolean Integer::IntegerCache::isStaticInitializationBlock = StaticInitializationBlock();
        
        jboolean Integer::IntegerCache::StaticInitializationBlock()
        {
            jint h = 127;
            JString integerCacheHighPropValue = jstr(u"0"); // TODO: sun.misc.VM.getSavedProperty("java.lang.Integer.IntegerCache.high");
            if (integerCacheHighPropValue != nullptr) {
                try {
                    jint i = parseInt(integerCacheHighPropValue);
                    i = Math::max(i, 127);
                    // Maximum array size is Integer.MAX_VALUE
                    h = Math::min(i, Integer::MAX_VALUE - (-low) -1);
                } catch(NumberFormatException& nfe) {
                    // If the property cannot be parsed into an int, ignore it.
                }
            }
            high = h;

            cache = jnew_array<Integer>((high - low) + 1);
            jint j = low;
            for(jint k = 0; k < cache.length(); k++)
                cache[k] = jnew<Integer>(j++);

            // range [-128, 127] must be interned (JLS7 5.1.7)
            assert(IntegerCache::high >= 127);
            return true;
        }
    }
}
