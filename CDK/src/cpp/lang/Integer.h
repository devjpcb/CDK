#ifndef CPP_LANG_INTEGER_H
#define CPP_LANG_INTEGER_H

#include "Array.h"
#include "Number.h"
#include "Comparable.h"

namespace cpp
{
    namespace lang
    {
        class Integer : public Number, public Comparable<JInteger>
        {
        public:
            Integer();
            Integer(jint pvalue);
            Integer(JString s);
            ~Integer();

            static jint bitCount(jint i);
            jbyte byteValue();
            static jint compare(jint x, jint y);
            jint compareTo(JInteger anotherInteger);
            static jint compareUnsigned(jint x, jint y);
            static JInteger decode(JString nm);
            static jint divideUnsigned(jint dividend, jint divisor);
            jdouble doubleValue();
            jboolean equals(JObject obj);
            jfloat floatValue();
            jint hashCode();
            static jint hashCode(jint value);
            static jint highestOneBit(jint i);
            jint intValue();
            jlong longValue();
            static jint lowestOneBit(jint i);
            static jint max(jint a, jint b);
            static jint min(jint a, jint b);
            static jint numberOfLeadingZeros(jint i);
            static jint numberOfTrailingZeros(jint i);
            static jint parseInt(JString s);
            static jint parseInt(JString s, jint radix);
            static jint parseUnsignedInt(JString s);
            static jint parseUnsignedInt(JString s, jint radix);
            static jint remainderUnsigned(jint dividend, jint divisor);
            static jint reverse(jint i);
            static jint reverseBytes(jint i);
            static jint rotateLeft(jint i, jint distance);
            static jint rotateRight(jint i, jint distance);
            jshort shortValue();
            static jint signum(jint i);
            static jint sum(jint a, jint b);
            static JString toBinaryString(jint i);
            static JString toHexString(jint i);
            static JString toOctalString(jint i);
            JString toString();
            static JString toString(jint i);
            static JString toString(jint i, jint radix);
            static jlong toUnsignedLong(jint x);
            static JString toUnsignedString(jint i);
            static JString toUnsignedString(jint i, jint radix);
            static JInteger valueOf(jint i);
            static JInteger valueOf(JString s);
            static JInteger valueOf(JString s, jint radix);

            static const jint MAX_VALUE;
            static const jint MIN_VALUE;
            static const jint SIZE;

        private:
            static JString toUnsignedString0(jint val, jint shift);
            static jint formatUnsignedInt(jint val, jint shift, jarray<jchar> buf, jint offset, jint len);
            static void getChars(jint i, jint index, jarray<jchar> buf);
            static jint stringSize(jint x);

            jint value;
            static const jarray<jchar> digits;
            static const jarray<jchar> DigitTens;
            static const jarray<jchar> DigitOnes;
            static const jarray<jint> sizeTable;

            class IntegerCache
            {
            public:
                static jint low;
                static jint high;
                static jarray<JInteger> cache;
                
                static jboolean isStaticInitializationBlock;
                static jboolean StaticInitializationBlock();
            };

            friend class Long;
            friend class AbstractStringBuilder;
        };
    }
}

#endif // CPP_LANG_INTEGER_H
