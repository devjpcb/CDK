#ifndef CPP_LANG_ITERABLE_H
#define CPP_LANG_ITERABLE_H

#include "ForeachIterator.h"
#include "../util/Iterator.h"
#include "../util/Set.h"

namespace cpp
{
    namespace lang
    {
        template <typename T>
        class Iterable : public virtual Object
        {
        public:
            virtual cpp::util::JIterator<T> iterator() = 0;

            cpp::lang::ForeachIterator<T> begin() {
                cpp::lang::ForeachIterator<T> fi(iterator());
                return fi;
            }

            cpp::lang::ForeachIterator<T> end() {
                cpp::lang::ForeachIterator<T> fi(iterator());
                return fi;
            }
        };
    }
}

#endif // CPP_LANG_ITERABLE_H
