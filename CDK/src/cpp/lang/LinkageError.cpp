#include "LinkageError.h"

namespace cpp
{
    namespace lang
    {
        LinkageError::LinkageError() : Error()
        {
        }

        LinkageError::LinkageError(JString s) : Error(s)
        {
            
        }
        
        LinkageError::LinkageError(JString s, Throwable& cause) : Error(s, cause)
        {
            
        }
    }
}
