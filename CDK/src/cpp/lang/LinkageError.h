#ifndef CPP_LANG_LINKAGEERROR_H
#define CPP_LANG_LINKAGEERROR_H

#include "Error.h"

namespace cpp
{
    namespace lang
    {
        class LinkageError : public Error
        {
        public:
            LinkageError();
            LinkageError(JString s);
            LinkageError(JString s, Throwable& cause);
        };
    }
}

#endif // CPP_LANG_LINKAGEERROR_H
