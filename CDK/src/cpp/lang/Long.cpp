#include "Long.h"
#include "Math.h"
#include "Character.h"
#include "String.h"
#include "Integer.h"
#include "NumberFormatException.h"

namespace cpp
{
    namespace lang
    {
        Long::Long() : value(0LL)
        {
        }

        Long::Long(jlong pvalue) : value(pvalue)
        {
        }

        Long::Long(JString s) : value(parseLong(s, 10))
        {
        }

        Long::~Long()
        {
        }

        jint Long::bitCount(jlong i)
        {
            i = i - ((static_cast<julong>(i) >> 1) & 0x5555555555555555LL);
            i = (i & 0x3333333333333333LL) + ((static_cast<julong>(i) >> 2) & 0x3333333333333333LL);
            i = (i + (static_cast<julong>(i) >> 4)) & 0x0f0f0f0f0f0f0f0fLL;
            i = i + (static_cast<julong>(i) >> 8);
            i = i + (static_cast<julong>(i) >> 16);
            i = i + (static_cast<julong>(i) >> 32);
            return static_cast<jint>(i & 0x7f);
        }

        jbyte Long::byteValue()
        {
            return static_cast<jbyte>(static_cast<jint>(value));
        }

        jint Long::compare(jlong x, jlong y)
        {
            return x == y ? 0 : x < y ? -1 : 1;
        }

        jint Long::compareTo(JLong anotherLong)
        {
            return compare(value, anotherLong->value);
        }

        jint Long::compareUnsigned(jlong x, jlong y)
        {
            return compare(x + Long::MIN_VALUE, y + Long::MIN_VALUE);
        }

        JLong Long::decode(JString nm)
        {
            jint radix = 10;
            jint index = 0;
            jboolean negative = false;
            JLong result;

            if(nm->length() == 0)
                throw NumberFormatException(jstr(u"Zero length string"));
            jchar firstChar = nm->charAt(0);
            // Handle sign, if present
            if(firstChar == u'-') {
                negative = true;
                index++;
            }
            else if(firstChar == u'+')
                index++;

            // Handle radix specifier, if present
            if(nm->startsWith(jstr(u"0x"), index) || nm->startsWith(jstr(u"0X"), index)) {
                index += 2;
                radix = 16;
            }
            else if(nm->startsWith(jstr(u"#"), index)) {
                index ++;
                radix = 16;
            }
            else if(nm->startsWith(jstr(u"0"), index) && nm->length() > 1 + index) {
                index ++;
                radix = 8;
            }

            if(nm->startsWith(jstr(u"-"), index) || nm->startsWith(jstr(u"+"), index))
                throw NumberFormatException(jstr(u"Sign character in wrong position"));

            try {
                result = Long::valueOf(nm->substring(index), radix);
                result = negative ? Long::valueOf(-result->longValue()) : result;
            }
            catch(NumberFormatException& e) {
                // If number is Long.MIN_VALUE, we'll end up here. The next line
                // handles this case, and causes any genuine format error to be
                // rethrown.
                JString constant = negative ? (jstr(u"-") + nm->substring(index)) : nm->substring(index);
                result = Long::valueOf(constant, radix);
            }

            return result;
        }

        jdouble Long::doubleValue()
        {
            return value;
        }

        jboolean Long::equals(JObject obj)
        {
            JLong lobj = std::dynamic_pointer_cast<Long>(obj);
            if(lobj != nullptr) {
                return value == lobj->value;
            }

            return false;
        }

        jfloat Long::floatValue()
        {
            return static_cast<jfloat>(value);
        }

        jint Long::hashCode()
        {
            return hashCode(value);
        }

        jint Long::hashCode(jlong value)
        {
            return static_cast<juint>(value ^ (static_cast<julong>(value) >> 32));
        }

        jlong Long::highestOneBit(jlong i)
        {
            i |= (i >>  1);
            i |= (i >>  2);
            i |= (i >>  4);
            i |= (i >>  8);
            i |= (i >> 16);
            i |= (i >> 32);
            return i - (static_cast<julong>(i) >> 1);
        }

        jint Long::intValue()
        {
            return static_cast<jint>(value);
        }

        jlong Long::longValue()
        {
            return value;
        }

        jlong Long::lowestOneBit(jlong i)
        {
            return i & -i;
        }

        jlong Long::max(jlong a, jlong b)
        {
            return Math::max(a, b);
        }

        jlong Long::min(jlong a, jlong b)
        {
            return Math::min(a, b);
        }

        jint Long::numberOfLeadingZeros(jlong i)
        {
            if(i == 0)
                return 64;
            jint n = 1;
            jint x = static_cast<jint>(static_cast<julong>(i) >> 32);
            if(x == 0) {
                n += 32;
                x = static_cast<jint>(i);
            }
            if(static_cast<juint>(x) >> 16 == 0) {
                n += 16;
                x <<= 16;
            }
            if(static_cast<juint>(x) >> 24 == 0) {
                n +=  8;
                x <<=  8;
            }
            if(static_cast<juint>(x) >> 28 == 0) {
                n +=  4;
                x <<=  4;
            }
            if(static_cast<juint>(x) >> 30 == 0) {
                n +=  2;
                x <<=  2;
            }
            n -= static_cast<juint>(x) >> 31;
            return n;
        }

        jint Long::numberOfTrailingZeros(jlong i)
        {
            jint x, y;
            if(i == 0) return 64;
            jint n = 63;
            y = static_cast<jint>(i);
            if(y != 0) {
                n = n - 32;
                x = y;
            }
            else x = static_cast<jint>(static_cast<julong>(i) >> 32);
            y = x << 16;
            if(y != 0) {
                n = n - 16;
                x = y;
            }
            y = x << 8;
            if(y != 0) {
                n = n - 8;
                x = y;
            }
            y = x << 4;
            if(y != 0) {
                n = n - 4;
                x = y;
            }
            y = x << 2;
            if(y != 0) {
                n = n - 2;
                x = y;
            }
            return n - (static_cast<juint>(x << 1) >> 31);
        }

        jlong Long::parseLong(JString s)
        {
            return parseLong(s, 10);
        }

        jlong Long::parseLong(JString s, jint radix)
        {
            if(s == nullptr) {
                throw NumberFormatException(jstr(u"null"));
            }

            if(radix < Character::MIN_RADIX) {
                throw NumberFormatException(jstr(u"radix ") + boxing(radix) + jstr(u" less than Character.MIN_RADIX"));
            }
            if(radix > Character::MAX_RADIX) {
                throw NumberFormatException(jstr(u"radix ") + boxing(radix) + jstr(u" greater than Character.MAX_RADIX"));
            }

            jlong result = 0;
            jboolean negative = false;
            jint i = 0, len = s->length();
            jlong limit = -Long::MAX_VALUE;
            jlong multmin;
            jint digit;

            if(len > 0) {
                char firstChar = s->charAt(0);
                if(firstChar < u'0') {  // Possible leading "+" or "-"
                    if(firstChar == u'-') {
                        negative = true;
                        limit = Long::MIN_VALUE;
                    }
                    else if(firstChar != u'+')
                        throw NumberFormatException::forInputString(s);

                    if(len == 1)  // Cannot have lone "+" or "-"
                        throw NumberFormatException::forInputString(s);
                    i++;
                }
                multmin = limit / radix;
                while(i < len) {
                    // Accumulating negatively avoids surprises near MAX_VALUE
                    digit = Character::digit(s->charAt(i++), radix);
                    if(digit < 0) {
                        throw NumberFormatException::forInputString(s);
                    }
                    if(result < multmin) {
                        throw NumberFormatException::forInputString(s);
                    }
                    result *= radix;
                    if(result < limit + digit) {
                        throw NumberFormatException::forInputString(s);
                    }
                    result -= digit;
                }
            }
            else {
                throw NumberFormatException::forInputString(s);
            }
            return negative ? result : -result;
        }

        jlong Long::parseUnsignedLong(JString s)
        {
            return parseUnsignedLong(s, 10);
        }

        jlong Long::parseUnsignedLong(JString s, jint radix)
        {
            if(s == nullptr)  {
                throw NumberFormatException(jstr(u"null"));
            }

            jint len = s->length();
            if(len > 0) {
                jchar firstChar = s->charAt(0);
                if(firstChar == u'-') {
                    throw NumberFormatException(/* TODO: String.format("Illegal leading minus sign on unsigned string %s.", s)*/);
                }
                else {
                    if(len <= 12 ||  // Long.MAX_VALUE in Character.MAX_RADIX is 13 digits
                       (radix == 10 && len <= 18)) {  // Long.MAX_VALUE in base 10 is 19 digits
                        return parseLong(s, radix);
                    }

                    // No need for range checks on len due to testing above.
                    jlong first = parseLong(s->substring(0, len - 1), radix);
                    jint second = Character::digit(s->charAt(len - 1), radix);
                    if(second < 0) {
                        throw NumberFormatException(jstr(u"Bad digit at end of ") + s);
                    }
                    jlong result = first * radix + second;
                    if(compareUnsigned(result, first) < 0) {
                        /*
                         * The maximum unsigned value, (2^64)-1, takes at
                         * most one more digit to represent than the
                         * maximum signed value, (2^63)-1.  Therefore,
                         * parsing (len - 1) digits will be appropriately
                         * in-range of the signed parsing.  In other
                         * words, if parsing (len -1) digits overflows
                         * signed parsing, parsing len digits will
                         * certainly overflow unsigned parsing.
                         *
                         * The compareUnsigned check above catches
                         * situations where an unsigned overflow occurs
                         * incorporating the contribution of the final
                         * digit.
                         */
                        throw NumberFormatException(/* TODO: String.format("String value %s exceeds range of unsigned long.", s)*/);
                    }
                    return result;
                }
            }
            else {
                throw NumberFormatException::forInputString(s);
            }
        }

        jlong Long::reverse(jlong i)
        {
            i = ((i & 0x5555555555555555LL) << 1) | ((static_cast<julong>(i) >> 1) & 0x5555555555555555LL);
            i = ((i & 0x3333333333333333LL) << 2) | ((static_cast<julong>(i) >> 2) & 0x3333333333333333LL);
            i = ((i & 0x0f0f0f0f0f0f0f0fLL) << 4) | ((static_cast<julong>(i) >> 4) & 0x0f0f0f0f0f0f0f0fLL);
            i = ((i & 0x00ff00ff00ff00ffLL) << 8) | ((static_cast<julong>(i) >> 8) & 0x00ff00ff00ff00ffLL);
            i = (i << 48) | ((i & 0xffff0000LL) << 16) | ((static_cast<julong>(i) >> 16) & 0xffff0000LL) | (static_cast<julong>(i) >> 48);
            return i;
        }

        jlong Long::reverseBytes(jlong i)
        {
            i = ((i & 0x00ff00ff00ff00ffLL) << 8) | ((static_cast<julong>(i) >> 8) & 0x00ff00ff00ff00ffLL);
            return (i << 48) | ((i & 0xffff0000LL) << 16) | ((static_cast<julong>(i) >> 16) & 0xffff0000LL) | (static_cast<julong>(i) >> 48);
        }

        jlong Long::rotateLeft(jlong i, jint distance)
        {
            return (i << distance) | (static_cast<julong>(i) >> -distance);
        }

        jlong Long::rotateRight(jlong i, jint distance)
        {
            return (static_cast<julong>(i) >> distance) | (i << -distance);
        }

        jshort Long::shortValue()
        {
            return static_cast<jshort>(static_cast<jint>(value));
        }

        jint Long::signum(jlong i)
        {
            return static_cast<jint>((i >> 63) | (static_cast<julong>(-i) >> 63));
        }

        jlong Long::sum(jlong a, jlong b)
        {
            return a + b;
        }

        JString Long::toBinaryString(jlong i)
        {
            return toUnsignedString0(i, 1);
        }

        JString Long::toHexString(jlong i)
        {
            return toUnsignedString0(i, 4);
        }

        JString Long::toOctalString(jlong i)
        {
            return toUnsignedString0(i, 3);
        }

        JString Long::toString()
        {
            return toString(value);
        }

        JString Long::toString(jlong i)
        {
            if(i == Long::MIN_VALUE)
                return jstr(u"-9223372036854775808");
            jint size = (i < 0) ? stringSize(-i) + 1 : stringSize(i);
            jarray<jchar> buf(size);
            getChars(i, size, buf);
            return jnew<String>(buf, true);
        }

        JString Long::toString(jlong i, jint radix)
        {
            if(radix < Character::MIN_RADIX || radix > Character::MAX_RADIX)
                radix = 10;
            if(radix == 10)
                return toString(i);
            jarray<jchar> buf(65);
            jint charPos = 64;
            jboolean negative = (i < 0);

            if(!negative) {
                i = -i;
            }

            while(i <= -radix) {
                buf[charPos--] = Integer::digits[static_cast<jint>((-(i % radix)))];
                i = i / radix;
            }
            buf[charPos] = Integer::digits[static_cast<jint>((-i))];

            if(negative) {
                buf[--charPos] = u'-';
            }

            return jnew<String>(buf, charPos, (65 - charPos));
        }

        JString Long::toUnsignedString(jlong i)
        {
            return toUnsignedString(i, 10);
        }

        JString Long::toUnsignedString(jlong i, jint radix)
        {
            if(i >= 0)
                return toString(i, radix);
            else {
                switch(radix) {
                    case 2:
                        return toBinaryString(i);

                    case 4:
                        return toUnsignedString0(i, 2);

                    case 8:
                        return toOctalString(i);

                    case 10:
                        /*
                         * We can get the effect of an unsigned division by 10
                         * on a long value by first shifting right, yielding a
                         * positive value, and then dividing by 5.  This
                         * allows the last digit and preceding digits to be
                         * isolated more quickly than by an initial conversion
                         * to BigInteger.
                         */
                    {
                        jlong quot = (static_cast<julong>(i) >> 1) / 5;
                        jlong rem = i - quot * 10;
                        return toString(quot) + boxing(rem);
                    }
                    case 16:
                        return toHexString(i);

                    case 32:
                        return toUnsignedString0(i, 5);
                }
            }

            // TODO: return toUnsignedBigInteger(i).toString(radix);
            return jstr(u"");
        }

        JLong Long::valueOf(jlong l)
        {
            const jint offset = 128;
            if (l >= -128 && l <= 127) { // will cache
                return LongCache::cache[static_cast<jint>(l + offset)];
            }
            return jnew<Long>(l);
        }

        JLong Long::valueOf(JString s)
        {
            return Long::valueOf(parseLong(s, 10));
        }

        JLong Long::valueOf(JString s, jint radix)
        {
            return Long::valueOf(parseLong(s, radix));
        }

        jint Long::formatUnsignedLong(jlong val, jint shift, jarray<jchar> buf, jint offset, jint len)
        {
            jint charPos = len;
            jint radix = 1 << shift;
            jint mask = radix - 1;
            do {
                buf[offset + --charPos] = Integer::digits[(static_cast<jint>(val)) & mask];
                val >>= shift;
            }
            while(val != 0 && charPos > 0);

            return charPos;
        }

        JString Long::toUnsignedString0(jlong val, jint shift)
        {
            // assert shift > 0 && shift <=5 : "Illegal shift value";
            jint mag = Long::SIZE - Long::numberOfLeadingZeros(val);
            jint chars = Math::max(((mag + (shift - 1)) / shift), 1);
            jarray<jchar> buf(chars);

            formatUnsignedLong(val, shift, buf, 0, chars);
            return jnew<String>(buf, true);
        }

        jint Long::stringSize(jlong x)
        {
            jlong p = 10;
            for(jint i = 1; i < 19; i++) {
                if(x < p)
                    return i;
                p = 10 * p;
            }
            return 19;
        }

        void Long::getChars(jlong i, jint index, jarray<jchar> buf)
        {
            jlong q;
            jint r;
            jint charPos = index;
            jchar sign = 0;

            if(i < 0) {
                sign = '-';
                i = -i;
            }

            // Get 2 digits/iteration using longs until quotient fits into an int
            while(i > Integer::MAX_VALUE) {
                q = i / 100;
                // really: r = i - (q * 100);
                r = (int)(i - ((q << 6) + (q << 5) + (q << 2)));
                i = q;
                buf[--charPos] = Integer::DigitOnes[r];
                buf[--charPos] = Integer::DigitTens[r];
            }

            // Get 2 digits/iteration using ints
            int q2;
            int i2 = (int)i;
            while(i2 >= 65536) {
                q2 = i2 / 100;
                // really: r = i2 - (q * 100);
                r = i2 - ((q2 << 6) + (q2 << 5) + (q2 << 2));
                i2 = q2;
                buf[--charPos] = Integer::DigitOnes[r];
                buf[--charPos] = Integer::DigitTens[r];
            }

            // Fall thru to fast mode for smaller numbers
            // assert(i2 <= 65536, i2);
            for(;;) {
                q2 = (i2 * 52429) >> (16 + 3);
                r = i2 - ((q2 << 3) + (q2 << 1));  // r = i2-(q2*10) ...
                buf[--charPos] = Integer::digits[r];
                i2 = q2;
                if(i2 == 0) break;
            }
            if(sign != 0) {
                buf[--charPos] = sign;
            }
        }

        const jlong Long::MAX_VALUE = 0x7fffffffffffffffL;
        const jlong Long::MIN_VALUE = 0x8000000000000000L;
        const jint Long::SIZE = 64;
        
        jarray<JLong> Long::LongCache::cache(-(-128) + 127 + 1);
            
        jboolean Long::LongCache::isStaticInitializationBlock = StaticInitializationBlock();
        
        jboolean Long::LongCache::StaticInitializationBlock()
        {
            for(jint i = 0; i < cache.length(); i++)
                cache[i] = jnew<Long>(i - 128);
            return true;
        }
    }
}
