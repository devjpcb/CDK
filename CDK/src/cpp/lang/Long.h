#ifndef CPP_LANG_LONG_H
#define CPP_LANG_LONG_H

#include "Array.h"
#include "Number.h"
#include "Comparable.h"

namespace cpp
{
    namespace lang
    {
        class Long : public Number, public Comparable<JLong>
        {
        public:
            Long();
            Long(jlong pvalue);
            Long(JString s);
            ~Long();

            static jint bitCount(jlong i);
            jbyte byteValue();
            static jint compare(jlong x, jlong y);
            jint compareTo(JLong anotherLong);
            static jint compareUnsigned(jlong x, jlong y);
            static JLong decode(JString nm);
            // static jlong divideUnsigned(jlong dividend, jlong divisor);
            jdouble doubleValue();
            jboolean equals(JObject obj);
            jfloat floatValue();
            jint hashCode();
            static jint hashCode(jlong value);
            static jlong highestOneBit(jlong i);
            jint intValue();
            jlong longValue();
            static jlong lowestOneBit(jlong i);
            static jlong max(jlong a, jlong b);
            static jlong min(jlong a, jlong b);
            static jint numberOfLeadingZeros(jlong i);
            static jint numberOfTrailingZeros(jlong i);
            static jlong parseLong(JString s);
            static jlong parseLong(JString s, jint radix);
            static jlong parseUnsignedLong(JString s);
            static jlong parseUnsignedLong(JString s, jint radix);
            //static jlong remainderUnsigned(jlong dividend, jlong divisor);
            static jlong reverse(jlong i);
            static jlong reverseBytes(jlong i);
            static jlong rotateLeft(jlong i, jint distance);
            static jlong rotateRight(jlong i, jint distance);
            jshort shortValue();
            static jint signum(jlong i);
            static jlong sum(jlong a, jlong b);
            static JString toBinaryString(jlong i);
            static JString toHexString(jlong i);
            static JString toOctalString(jlong i);
            JString toString();
            static JString toString(jlong i);
            static JString toString(jlong i, jint radix);
            static JString toUnsignedString(jlong i);
            static JString toUnsignedString(jlong i, jint radix);
            static JLong valueOf(jlong l);
            static JLong valueOf(JString s);
            static JLong valueOf(JString s, jint radix);

            static const jlong MAX_VALUE;
            static const jlong MIN_VALUE;
            static const jint SIZE;

        private:
            static jint formatUnsignedLong(jlong val, jint shift, jarray<jchar> buf, jint offset, jint len);
            static JString toUnsignedString0(jlong val, jint shift);
            static jint stringSize(jlong x);
            static void getChars(jlong i, jint index, jarray<jchar> buf);
                        
            jlong value;
            
            class LongCache
            {
            public:
                static jarray<JLong> cache;
            
                static jboolean isStaticInitializationBlock;
                static jboolean StaticInitializationBlock();
            };
            
            friend class AbstractStringBuilder;
        };
    }
}

#endif // CPP_LANG_LONG_H
