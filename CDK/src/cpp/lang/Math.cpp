#include "Math.h"
#include "StrictMath.h"
#include "Integer.h"
#include "Long.h"
#include "Double.h"
#include "Float.h"
#include "../../sun/misc/DoubleConsts.h"
#include "../../sun/misc/FloatConsts.h"
#include "ArithmeticException.h"
#include <cassert>

using namespace sun::misc;

namespace cpp
{
    namespace lang
    {
        jdouble Math::abs(jdouble a)
        {
            return a <= 0.0 ? 0.0 - a : a;
        }

        jfloat Math::abs(jfloat a)
        {
            return a <= 0.0f ? 0.0f - a : a;
        }
        
        jint Math::abs(jint a)
        {
            return (a < 0) ? -a : a;
        }
        
        jlong Math::abs(jlong a)
        {
            return (a < 0) ? -a : a;
        }
        
        jdouble Math::acos(jdouble a)
        {
            return StrictMath::acos(a);
        }

        jint Math::addExact(jint x, jint y)
        {
            jint r = x + y;
            if (((x ^ r) & (y ^ r)) < 0) {
                throw ArithmeticException(jstr(u"integer overflow"));
            }
            return r;
        }
        
        jlong Math::addExact(jlong x, jlong y)
        {
            jlong r = x + y;
            if (((x ^ r) & (y ^ r)) < 0) {
                throw ArithmeticException(jstr(u"long overflow"));
            }
            return r;
        }
        
        jdouble Math::asin(jdouble a)
        {
            return StrictMath::asin(a);
        }
        
        jdouble Math::atan(jdouble a)
        {
            return StrictMath::atan(a);
        }
        
        jdouble Math::atan2(jdouble y, jdouble x)
        {
            return StrictMath::atan2(y, x);
        }
        
        jdouble Math::cbrt(jdouble a)
        {
            return StrictMath::cbrt(a);
        }
        
        jdouble Math::ceil(jdouble a)
        {
            return StrictMath::ceil(a);
        }
        
        jdouble Math::copySign(jdouble magnitude, jdouble sign)
        {
            return Double::longBitsToDouble((Double::doubleToRawLongBits(sign) & (DoubleConsts::SIGN_BIT_MASK)) |
                                            (Double::doubleToRawLongBits(magnitude) & (DoubleConsts::EXP_BIT_MASK | DoubleConsts::SIGNIF_BIT_MASK)));
        }
        
        jfloat Math::copySign(jfloat magnitude, jfloat sign)
        {
            return Float::intBitsToFloat((Float::floatToRawIntBits(sign) & (FloatConsts::SIGN_BIT_MASK)) |
                                         (Float::floatToRawIntBits(magnitude) & (FloatConsts::EXP_BIT_MASK | FloatConsts::SIGNIF_BIT_MASK)));
        }
        
        jdouble Math::cos(jdouble a)
        {
            return StrictMath::cos(a);
        }
        
        jdouble Math::cosh(jdouble x)
        {
            return StrictMath::cosh(x);
        }
        
        jint Math::decrementExact(jint a)
        {
            if (a == Integer::MIN_VALUE) {
                throw ArithmeticException(jstr(u"integer overflow"));
            }

            return a - 1;
        }
        
        jlong Math::decrementExact(jlong a)
        {
            if (a == Long::MIN_VALUE) {
                throw ArithmeticException(jstr(u"long overflow"));
            }

            return a - 1L;
        }
        
        jdouble Math::exp(jdouble a)
        {
            return StrictMath::exp(a);
        }
        
        jdouble Math::expm1(jdouble x)
        {
            return StrictMath::expm1(x);
        }
        
        jdouble Math::floor(jdouble a)
        {
            return StrictMath::floor(a);
        }
        
        jint Math::floorDiv(jint x, jint y)
        {
            jint r = x / y;
            if ((x ^ y) < 0 && (r * y != x)) {
                r--;
            }
            return r;
        }
        
        jlong Math::floorDiv(jlong x, jlong y)
        {
            jlong r = x / y;
            if ((x ^ y) < 0 && (r * y != x)) {
                r--;
            }
            return r;
        }
        
        jint Math::floorMod(jint x, jint y)
        {
            jint r = x - floorDiv(x, y) * y;
            return r;
        }
        
        jlong Math::floorMod(jlong x, jlong y)
        {
            return x - floorDiv(x, y) * y;
        }
        
        jint Math::getExponent(jdouble d)
        {
            return static_cast<jint>(((Double::doubleToRawLongBits(d) & DoubleConsts::EXP_BIT_MASK) >>
                                      (DoubleConsts::SIGNIFICAND_WIDTH - 1)) - DoubleConsts::EXP_BIAS);
        }
        
        jint Math::getExponent(jfloat f)
        {
            return ((Float::floatToRawIntBits(f) & FloatConsts::EXP_BIT_MASK) >>
                    (FloatConsts::SIGNIFICAND_WIDTH - 1)) - FloatConsts::EXP_BIAS;
        }
        
        jdouble Math::hypot(jdouble x, jdouble y)
        {
            return StrictMath::hypot(x, y);
        }
        
        jdouble Math::IEEEremainder(jdouble f1, jdouble f2)
        {
            return StrictMath::IEEEremainder(f1, f2);
        }
        
        jint Math::incrementExact(jint a)
        {
            if (a == Integer::MAX_VALUE) {
                throw ArithmeticException(jstr(u"integer overflow"));
            }

            return a + 1;
        }
        
        jlong Math::incrementExact(jlong a)
        {
            if (a == Long::MAX_VALUE) {
                throw ArithmeticException(jstr(u"long overflow"));
            }

            return a + 1LL;
        }
        
        jdouble Math::log(jdouble a)
        {
            return StrictMath::log(a);
        }
        
        jdouble Math::log10(jdouble a)
        {
            return StrictMath::log10(a);
        }
        
        jdouble Math::log1p(jdouble x)
        {
            return StrictMath::log1p(x);
        }
            
        jdouble Math::max(jdouble a, jdouble b)
        {
            if(a != a) {
                return a;
            }
            if((a == 0.0) && (b == 0.0)) {
                if(Double::doubleToRawLongBits(a) == negativeZeroDoubleBits) {
                    return b;
                }
            }
            return a >= b ? a : b;
        }

        jfloat Math::max(jfloat a, jfloat b)
        {
            if(a != a) {
                return a;
            }
            if((a == 0.0f) && (b == 0.0f)) {
                if(Float::floatToRawIntBits(a) == negativeZeroFloatBits) {
                    return b;
                }
            }
            return a >= b ? a : b;
        }

        jint Math::max(jint a, jint b)
        {
            return a >= b ? a : b;
        }

        jlong Math::max(jlong a, jlong b)
        {
            return a >= b ? a : b;
        }

        jdouble Math::min(jdouble a, jdouble b)
        {
            if(a != a) {
                return a;
            }
            if((a == 0.0) && (b == 0.0)) {
                if(Double::doubleToRawLongBits(b) == negativeZeroDoubleBits) {
                    return b;
                }
            }
            return a <= b ? a : b;
        }

        jfloat Math::min(jfloat a, jfloat b)
        {
            if(a != a) {
                return a;
            }
            if((a == 0.0f) && (b == 0.0f)) {
                if(Float::floatToRawIntBits(b) == negativeZeroFloatBits) {
                    return b;
                }
            }
            return a <= b ? a : b;
        }

        jint Math::min(jint a, jint b)
        {
            return a <= b ? a : b;
        }

        jlong Math::min(jlong a, jlong b)
        {
            return a <= b ? a : b;
        }
        
        jint Math::multiplyExact(jint x, jint y)
        {
            jlong r = static_cast<jlong>(x) * static_cast<jlong>(y);
            if (static_cast<jint>(r) != r) {
                throw ArithmeticException(jstr(u"integer overflow"));
            }
            return static_cast<jint>(r);
        }
        
        jlong Math::multiplyExact(jlong x, jlong y)
        {
            jlong r = x * y;
            jlong ax = Math::abs(x);
            jlong ay = Math::abs(y);
            if ((static_cast<julong>(ax | ay) >> 31 != 0)) {
               if (((y != 0) && (r / y != x)) ||
                   (x == Long::MIN_VALUE && y == -1)) {
                    throw ArithmeticException(jstr(u"long overflow"));
                }
            }
            return r;
        }
        
        jint Math::negateExact(jint a)
        {
            if (a == Integer::MIN_VALUE) {
                throw ArithmeticException(jstr(u"integer overflow"));
            }

            return -a;
        }
        
        jlong Math::negateExact(jlong a)
        {
            if (a == Long::MIN_VALUE) {
                throw ArithmeticException(jstr(u"long overflow"));
            }

            return -a;
        }
        
        jdouble Math::nextAfter(jdouble start, jdouble direction)
        {
            if(Double::isNaN(start) || Double::isNaN(direction)) {
                return start + direction;
            }
            else if(start == direction) {
                return direction;
            }
            else {
                jlong transducer = Double::doubleToRawLongBits(start + 0.0);
        
        
                if(direction > start) {
                    transducer = transducer + (transducer >= 0L ? 1LL:-1LL);
                }
                else  {
                    assert(direction < start);
                    if(transducer > 0LL)
                        --transducer;
                    else if(transducer < 0LL)
                        ++transducer;
        
                    else
                        transducer = DoubleConsts::SIGN_BIT_MASK | 1LL;
                }
        
                return Double::longBitsToDouble(transducer);
            }
        }
        
        jfloat Math::nextAfter(jfloat start, jdouble direction)
        {
            if(Float::isNaN(start) || Double::isNaN(direction)) {
                return start + static_cast<jfloat>(direction);
            }
            else if(start == direction) {
                return static_cast<jfloat>(direction);
            }
            else {
                jint transducer = Float::floatToRawIntBits(start + 0.0f);
        
                if(direction > start) {
                    transducer = transducer + (transducer >= 0 ? 1:-1);
                }
                else  {
                    assert(direction < start);
                    if(transducer > 0)
                        --transducer;
                    else if(transducer < 0)
                        ++transducer;
                    else
                        transducer = FloatConsts::SIGN_BIT_MASK | 1;
                }
        
                return Float::intBitsToFloat(transducer);
            }
        }
        
        jdouble Math::nextDown(jdouble d)
        {
            if(Double::isNaN(d) || d == Double::NEGATIVE_INFINITY)
                return d;
            else {
                if(d == 0.0)
                    return -Double::MIN_VALUE;
                else
                    return Double::longBitsToDouble(Double::doubleToRawLongBits(d) + ((d > 0.0)?-1LL:+1LL));
            }
        }
        
        jfloat Math::nextDown(jfloat f)
        {
            if (Float::isNaN(f) || f == Float::NEGATIVE_INFINITY)
                return f;
            else {
                if (f == 0.0f)
                    return -Float::MIN_VALUE;
                else
                    return Float::intBitsToFloat(Float::floatToRawIntBits(f) + ((f > 0.0f)?-1:+1));
            }
        }
        
        jdouble Math::nextUp(jdouble d)
        {
            if(Double::isNaN(d) || d == Double::POSITIVE_INFINITY)
                return d;
            else {
                d += 0.0;
                return Double::longBitsToDouble(Double::doubleToRawLongBits(d) +((d >= 0.0)?+1LL:-1LL));
            }
        }
        
        jfloat Math::nextUp(jfloat f)
        {
            if(Float::isNaN(f) || f == FloatConsts::POSITIVE_INFINITY)
                return f;
            else {
                f += 0.0f;
                return Float::intBitsToFloat(Float::floatToRawIntBits(f) + ((f >= 0.0f)?+1:-1));
            }
        }
        
        jdouble Math::pow(jdouble a, jdouble b)
        {
            return StrictMath::pow(a, b);
        }
        
        jdouble Math::random()
        {
            return -1.0;
        }
        
        jdouble Math::rint(jdouble a)
        {
            return StrictMath::rint(a);
        }
        
        jlong Math::round(jdouble a)
        {
            jlong longBits = Double::doubleToRawLongBits(a);
            jlong biasedExp = (longBits & DoubleConsts::EXP_BIT_MASK) >> (DoubleConsts::SIGNIFICAND_WIDTH - 1);
            jlong shift = (DoubleConsts::SIGNIFICAND_WIDTH - 2 + DoubleConsts::EXP_BIAS) - biasedExp;
            if ((shift & -64) == 0) {
                jlong r = ((longBits & DoubleConsts::SIGNIF_BIT_MASK) | (DoubleConsts::SIGNIF_BIT_MASK + 1));
                if (longBits < 0) {
                    r = -r;
                }
                return ((r >> shift) + 1) >> 1;
            } else {
                return static_cast<jlong>(a);
            }
        }
        
        jint Math::round(jfloat a)
        {
            jint intBits = Float::floatToRawIntBits(a);
            jint biasedExp = (intBits & FloatConsts::EXP_BIT_MASK) >> (FloatConsts::SIGNIFICAND_WIDTH - 1);
            jint shift = (FloatConsts::SIGNIFICAND_WIDTH - 2 + FloatConsts::EXP_BIAS) - biasedExp;
            if ((shift & -32) == 0) { // shift >= 0 && shift < 32
                // a is a finite number such that pow(2,-32) <= ulp(a) < 1
                jint r = ((intBits & FloatConsts::SIGNIF_BIT_MASK) | (FloatConsts::SIGNIF_BIT_MASK + 1));
                if (intBits < 0) {
                    r = -r;
                }
                return ((r >> shift) + 1) >> 1;
            } else {
                return static_cast<jint>(a);
            }
        }
        
        jdouble Math::scalb(jdouble d, jint scaleFactor)
        {
            const jint MAX_SCALE = DoubleConsts::MAX_EXPONENT + -DoubleConsts::MIN_EXPONENT + DoubleConsts::SIGNIFICAND_WIDTH + 1;
            jint exp_adjust = 0;
            jint scale_increment = 0;
            jdouble exp_delta = Double::NaN;

            if(scaleFactor < 0) {
                scaleFactor = Math::max(scaleFactor, -MAX_SCALE);
                scale_increment = -512;
                exp_delta = twoToTheDoubleScaleDown;
            }
            else {
                scaleFactor = Math::min(scaleFactor, MAX_SCALE);
                scale_increment = 512;
                exp_delta = twoToTheDoubleScaleUp;
            }

            jint t = static_cast<juint>((scaleFactor >> (9-1))) >> (32 - 9);
            exp_adjust = ((scaleFactor + t) & (512 -1)) - t;

            d *= powerOfTwoD(exp_adjust);
            scaleFactor -= exp_adjust;

            while(scaleFactor != 0) {
                d *= exp_delta;
                scaleFactor -= scale_increment;
            }
            return d;
        }
        
        jfloat Math::scalb(jfloat f, jint scaleFactor)
        {
            const jint MAX_SCALE = FloatConsts::MAX_EXPONENT + -FloatConsts::MIN_EXPONENT + FloatConsts::SIGNIFICAND_WIDTH + 1;

            scaleFactor = Math::max(Math::min(scaleFactor, MAX_SCALE), -MAX_SCALE);

            return static_cast<jfloat>(static_cast<jdouble>(f) * powerOfTwoD(scaleFactor));
        }
        
        jdouble Math::signum(jdouble d)
        {
            return (d == 0.0 || Double::isNaN(d)) ? d:copySign(1.0, d);
        }
        
        jfloat Math::signum(jfloat f)
        {
            return (f == 0.0f || Float::isNaN(f)) ? f:copySign(1.0f, f);
        }
        
        jdouble Math::sin(jdouble a)
        {
            return StrictMath::sin(a);
        }
        
        jdouble Math::sinh(jdouble x)
        {
            return StrictMath::sinh(x);
        }
        
        jdouble Math::sqrt(jdouble a)
        {
            return StrictMath::sqrt(a);
        }
        
        jint Math::subtractExact(jint x, jint y)
        {
            jint r = x - y;
            if (((x ^ y) & (x ^ r)) < 0) {
                throw ArithmeticException(jstr(u"integer overflow"));
            }
            return r;
        }
        
        jlong Math::subtractExact(jlong x, jlong y)
        {
            jlong r = x - y;
            if (((x ^ y) & (x ^ r)) < 0) {
                throw ArithmeticException(jstr(u"long overflow"));
            }
            return r;
        }
        
        jdouble Math::tan(jdouble a)
        {
            return StrictMath::tan(a);
        }
        
        jdouble Math::tanh(jdouble x)
        {
            return StrictMath::tanh(x);
        }
        
        jdouble Math::toDegrees(jdouble angrad)
        {
            return angrad * 180.0 / PI;
        }
        
        jint Math::toIntExact(jlong value)
        {
            if (static_cast<jint>(value) != value) {
                throw ArithmeticException(jstr(u"integer overflow"));
            }
            return static_cast<jint>(value);
        }
        
        jdouble Math::toRadians(jdouble angdeg)
        {
            return angdeg / 180.0 * PI;
        }
        
        jdouble Math::ulp(jdouble d)
        {
            jint exp = getExponent(d);
            
            if (exp == (DoubleConsts::MAX_EXPONENT + 1))
                return Math::abs(d);
            else if (exp == (DoubleConsts::MIN_EXPONENT-1))
                return Double::MIN_VALUE;
            else {
                assert(exp <= DoubleConsts::MAX_EXPONENT && exp >= DoubleConsts::MIN_EXPONENT);
                
                exp = exp - (DoubleConsts::SIGNIFICAND_WIDTH-1);
                if (exp >= DoubleConsts::MIN_EXPONENT) {
                    return powerOfTwoD(exp);
                }
                else {
                    return Double::longBitsToDouble(1L << (exp - (DoubleConsts::MIN_EXPONENT - (DoubleConsts::SIGNIFICAND_WIDTH-1)) ));
                }
            }
        }
        
        jfloat Math::ulp(jfloat f)
        {
            jint exp = getExponent(f);
            
            if (exp == (FloatConsts::MAX_EXPONENT+1)) 
                return Math::abs(f);
            else if(exp == (FloatConsts::MIN_EXPONENT-1))
                return FloatConsts::MIN_VALUE;
            else {
                assert(exp <= FloatConsts::MAX_EXPONENT && exp >= FloatConsts::MIN_EXPONENT);

                exp = exp - (FloatConsts::SIGNIFICAND_WIDTH-1);
                if (exp >= FloatConsts::MIN_EXPONENT) {
                    return powerOfTwoF(exp);
                }
                else {
                    return Float::intBitsToFloat(1 << (exp - (FloatConsts::MIN_EXPONENT - (FloatConsts::SIGNIFICAND_WIDTH-1)) ));
                }
            }
        }
        
        jdouble Math::powerOfTwoD(jint n)
        {
            assert(n >= DoubleConsts::MIN_EXPONENT && n <= DoubleConsts::MAX_EXPONENT);
            return Double::longBitsToDouble((( static_cast<jlong>(n) + static_cast<jlong>(DoubleConsts::EXP_BIAS)) << (DoubleConsts::SIGNIFICAND_WIDTH-1)) & DoubleConsts::EXP_BIT_MASK);
        }
        
        jfloat Math::powerOfTwoF(jint n)
        {
            assert(n >= FloatConsts::MIN_EXPONENT && n <= FloatConsts::MAX_EXPONENT);
            return Float::intBitsToFloat(((n + FloatConsts::EXP_BIAS) << (FloatConsts::SIGNIFICAND_WIDTH-1)) & FloatConsts::EXP_BIT_MASK);
        }
        
        const jdouble Math::E = 2.7182818284590452354;
        const jdouble Math::PI = 3.14159265358979323846;
        const jlong Math::negativeZeroFloatBits  = Float::floatToRawIntBits(-0.0f);
        const jlong Math::negativeZeroDoubleBits = Double::doubleToRawLongBits(-0.0);
        jdouble Math::twoToTheDoubleScaleUp = powerOfTwoD(512);
        jdouble Math::twoToTheDoubleScaleDown = powerOfTwoD(-512);;
    }
}
