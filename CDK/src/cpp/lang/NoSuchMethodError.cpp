#include "NoSuchMethodError.h"

namespace cpp
{

    namespace lang
    {
        NoSuchMethodError::NoSuchMethodError() : IncompatibleClassChangeError()
        {
        }

        NoSuchMethodError::NoSuchMethodError(JString s) : IncompatibleClassChangeError(s)
        {
        }
    }
}
