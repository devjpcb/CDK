#ifndef NOSUCHMETHODERROR_H
#define NOSUCHMETHODERROR_H

#include "IncompatibleClassChangeError.h"

namespace cpp
{
    namespace lang
    {
        class NoSuchMethodError : public IncompatibleClassChangeError
        {
        public:
            NoSuchMethodError();
            NoSuchMethodError(JString s);
        };
    }
}

#endif // NOSUCHMETHODERROR_H
