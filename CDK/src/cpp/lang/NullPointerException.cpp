#include "NullPointerException.h"

namespace cpp
{
    namespace lang
    {
        NullPointerException::NullPointerException() : RuntimeException()
        {
        }
        
        NullPointerException::NullPointerException(JString s) : RuntimeException(s)
        {
        }

        NullPointerException::~NullPointerException()
        {
        }
    }
}
