#ifndef CPP_LANG_NULLPOINTEREXCEPTION_H
#define CPP_LANG_NULLPOINTEREXCEPTION_H

#include "RuntimeException.h"

namespace cpp
{
    namespace lang
    {
        class NullPointerException : public RuntimeException
        {
        public:
            NullPointerException();
            NullPointerException(JString s);
            ~NullPointerException();
        };
    }
}

#endif // CPP_LANG_NULLPOINTEREXCEPTION_H
