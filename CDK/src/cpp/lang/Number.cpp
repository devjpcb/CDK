#include "Number.h"

namespace cpp
{
    namespace lang
    {
        Number::Number()
        {
        }

        Number::~Number()
        {
        }

        jbyte Number::byteValue()
        {
            return (jbyte)intValue();
        }

        jshort Number::shortValue()
        {
            return (jshort)intValue();
        }
    }
}
