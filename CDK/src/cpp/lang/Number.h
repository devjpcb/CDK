#ifndef CPP_LANG_NUMBER_H
#define CPP_LANG_NUMBER_H

#include "Object.h"

namespace cpp
{
    namespace lang
    {
        class Number : public virtual Object
        {
        public:
            Number();
            virtual ~Number();

            virtual jbyte byteValue();
            virtual jdouble doubleValue() = 0;
            virtual jfloat floatValue() = 0;
            virtual jint intValue() = 0;
            virtual jlong longValue() = 0;
            virtual jshort shortValue();
        };
    }
}

#endif // CPP_LANG_NUMBER_H
