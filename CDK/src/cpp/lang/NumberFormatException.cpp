#include "NumberFormatException.h"

namespace cpp
{
    namespace lang
    {
        NumberFormatException::NumberFormatException() : IllegalArgumentException()
        {
        }

        NumberFormatException::NumberFormatException(JString s) : IllegalArgumentException(s)
        {
        }

        NumberFormatException::~NumberFormatException()
        {
        }

        NumberFormatException NumberFormatException::forInputString(JString s)
        {
            return NumberFormatException(jstr(u"For input string: \"") + s + jstr(u"\""));
        }
    }
}
