#ifndef CPP_LANG_NUMBERFORMATEXCEPTION_H
#define CPP_LANG_NUMBERFORMATEXCEPTION_H

#include "IllegalArgumentException.h"

namespace cpp
{
    namespace lang
    {
        class NumberFormatException : public IllegalArgumentException
        {
        public:
            NumberFormatException();
            NumberFormatException(JString s);
            ~NumberFormatException();
            
            static NumberFormatException forInputString(JString s);
        };
    }
}

#endif // CPP_LANG_NUMBERFORMATEXCEPTION_H
