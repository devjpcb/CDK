#include "Object.h"
#include "String.h"
#include "Integer.h"
#include "Class.h"
#include "reflect/Modifier.h"
#include "reflect/builder/ClassBuilder.h"
#include "reflect/builder/PackageBuilder.h"
#include "CloneNotSupportedException.h"
#include "IllegalArgumentException.h"

#include <functional>

using namespace cpp::lang::reflect;
using namespace cpp::lang::reflect::builder;

namespace cpp
{
    namespace lang
    {
        std::atomic_llong Object::counter;

        Object::Object()
        {
            counter++;
            objectId = counter;
        }

        Object::~Object()
        {
        }

        jboolean Object::equals(JObject obj)
        {
            return this == obj.get();
        }

        JClass Object::getClass()
        {
            return clazz;
        }

        jint Object::hashCode()
        {
            int chash = static_cast<int>(std::hash<long long>{}(objectId));
            jint hashCode = std::abs(chash);
            return hashCode;
        }
        
        void Object::notify()
        {
            cv.notify_one();
        }
        
        void Object::notifyAll()
        {
            cv.notify_all();
        }

        JString Object::toString()
        {
            return getClass()->getName() + jstr(u"@") + Integer::toHexString(hashCode());
        }
        
        void Object::wait()
        {
            wait(0);
        }
        
        void Object::wait(jlong timeout)
        {
            std::unique_lock<std::recursive_mutex> lock(internalLock);
            cv.wait_for(lock, std::chrono::milliseconds(timeout));
        }
        
        void Object::wait(jlong timeout, jint nanos)
        {
            if (timeout < 0) {
                throw IllegalArgumentException(jstr(u"timeout value is negative"));
            }

            if (nanos < 0 || nanos > 999999) {
                throw IllegalArgumentException(jstr(u"nanosecond timeout value out of range"));
            }

            if (nanos > 0) {
                timeout++;
            }

            wait(timeout);
        }

        JObject Object::clone()
        {
            throw CloneNotSupportedException();
        }
        
        void Object::finalize()
        {
            
        }
        
        JClass Object::clazz = ClassBuilder::create()
            ->setSimpleName(jstr(u"Object"))
            ->setPackage(
                PackageBuilder::create()
                    ->setName(jstr(u"cpp::lang"))
                    ->getPackage()
            )
            ->setModifiers(Modifier::PUBLIC)
            ->setFuncNewInstance([] { return jnew<Object>(); })
            ->getClass();
    }
}
