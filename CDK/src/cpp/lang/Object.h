#ifndef CPP_LANG_OBJECT_H
#define CPP_LANG_OBJECT_H

#include "../../PrimitiveDataTypes.h"
#include "../../Declarations.h"
#include <mutex>
#include <condition_variable>

namespace cpp
{
    namespace lang
    {
        class Object
        {
        public:
            Object();
            virtual ~Object();

            virtual jboolean equals(JObject obj);
            virtual JClass getClass();
            virtual jint hashCode();
            void notify();
            void notifyAll();
            virtual JString toString();
            virtual void wait();
            virtual void wait(jlong timeout);
            virtual void wait(jlong timeout, jint nanos);

            static JClass clazz;
            
            std::recursive_mutex internalLock;
            
        protected:
            virtual JObject clone();
            virtual void finalize();
            
            std::condition_variable_any cv;

            jlong objectId;

            static std::atomic_llong counter;
            
            template <typename T, typename... Args>
            friend std::shared_ptr<T> jnew(Args&&... args);
        };
    }
}

#endif // CPP_LANG_OBJECT_H
