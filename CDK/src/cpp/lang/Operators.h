#ifndef CPP_LANG_OPERATORS_H
#define CPP_LANG_OPERATORS_H

#include "PostConstructible.h"
#include "AutoCloseable.h"
#include <mutex>

#define jsynchronized_name(A,B) A ## B
#define jsynchronized_preprocessor_name(A,B) jsynchronized_name(A,B)
#define jsynchronized(object) std::lock_guard<std::recursive_mutex> jsynchronized_preprocessor_name(internal_sync_, __LINE__)(object->internalLock)
#define jsynchronized_not_recursive(mutex_object) std::lock_guard<std::mutex> jsynchronized_preprocessor_name(internal_sync_, __LINE__)(mutex_object)

namespace cpp
{
    namespace lang
    {
        // TODO: agregar que tipo sea condicional que se pueda soportar por ejemplo JString o String
        template <typename T, typename... Args>
        inline std::shared_ptr<T> jnew(Args&&... args)
        {
            typedef typename std::remove_const<T>::type tnc;
            std::shared_ptr<tnc> jobj(new tnc(std::forward<Args>(args)...), [](tnc* objPtr) {
                AutoCloseable* acobj = dynamic_cast<AutoCloseable*>(objPtr);
                if (acobj != nullptr) {
                   acobj->close();
                }
                objPtr->finalize();
                delete objPtr;
            });
            std::shared_ptr<PostConstructible> pobj = std::dynamic_pointer_cast<PostConstructible>(jobj);
            if (pobj != nullptr) {
               pobj->postConstruct();
            }
            return jobj;
        }
        
        template <typename T>
        inline jboolean jinstanceof(JObject o)
        {
            std::shared_ptr<T> obj = std::dynamic_pointer_cast<T>(o);
            return obj != nullptr;
        }
    }
}

using cpp::lang::jnew;
using cpp::lang::jinstanceof;

#endif // CPP_LANG_OPERATORS_H
