#include "OutOfMemoryError.h"

namespace cpp
{
    namespace lang
    {
        OutOfMemoryError::OutOfMemoryError() : VirtualMachineError()
        {
        }
        
        OutOfMemoryError::OutOfMemoryError(JString s) : VirtualMachineError(s)
        {
        }

        OutOfMemoryError::~OutOfMemoryError()
        {
        }
    }
}
