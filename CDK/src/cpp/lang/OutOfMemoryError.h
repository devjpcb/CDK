#ifndef CPP_LANG_OUTOFMEMORYERROR_H
#define CPP_LANG_OUTOFMEMORYERROR_H

#include "VirtualMachineError.h"

namespace cpp
{
    namespace lang
    {
        class OutOfMemoryError : public VirtualMachineError
        {
        public:
            OutOfMemoryError();
            OutOfMemoryError(JString s);
            ~OutOfMemoryError();
        };
    }
}

#endif // CPP_LANG_OUTOFMEMORYERROR_H
