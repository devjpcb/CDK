#include "Package.h"
#include "String.h"

namespace cpp
{
    namespace lang
    {
        Package::Package() : name(jstr(u""))
        {

        }

        JString Package::getName()
        {
            return name;
        }
    }
}
