#ifndef CPP_LANG_PACKAGE_H
#define CPP_LANG_PACKAGE_H

#include "Object.h"

namespace cpp
{
    namespace lang
    {
        class Package : public Object
        {
        public:
            Package();
            virtual JString getName();

        private:
            JString name;

            friend class cpp::lang::reflect::builder::PackageBuilder;
        };
    }
}

#endif // CPP_LANG_PACKAGE_H
