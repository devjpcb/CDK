#ifndef POSTCONSTRUCTIBLE_H
#define POSTCONSTRUCTIBLE_H

namespace cpp
{
    namespace lang
    {
        class PostConstructible
        {
        public:
            virtual void postConstruct() = 0;
        };
    }
}

#endif // POSTCONSTRUCTIBLE_H
