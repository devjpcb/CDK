#ifndef CPP_LANG_RUNNABLE_H
#define CPP_LANG_RUNNABLE_H

namespace cpp
{
    namespace lang
    {
        class Runnable : public Object
        {
        public:
            virtual void run() = 0;
        };
    }
}

#endif // CPP_LANG_RUNNABLE_H
