#include "RuntimeException.h"

namespace cpp
{
    namespace lang
    {
        RuntimeException::RuntimeException() : Exception()
        {
        }
        
        RuntimeException::RuntimeException(JString message) : Exception(message)
        {
        }
        
        RuntimeException::RuntimeException(JString message, Throwable& cause) : Exception(message, cause)
        {
        }
        
        RuntimeException::RuntimeException(JString message, Throwable& cause, jboolean enableSuppression, jboolean writableStackTrace) : Exception(message, cause, enableSuppression, writableStackTrace)
        {
        }
        
        RuntimeException::RuntimeException(Throwable& cause) : Exception(cause)
        {
        }

        RuntimeException::~RuntimeException()
        {
        }
    }
}
