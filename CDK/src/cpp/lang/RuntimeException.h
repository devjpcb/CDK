#ifndef CPP_LANG_RUNTIMEEXCEPTION_H
#define CPP_LANG_RUNTIMEEXCEPTION_H

#include "Exception.h"

namespace cpp
{
    namespace lang
    {
        class RuntimeException : public Exception
        {
        public:
            RuntimeException();
            RuntimeException(JString message);
            RuntimeException(JString message, Throwable& cause);
            RuntimeException(JString message, Throwable& cause, jboolean enableSuppression, jboolean writableStackTrace);
            RuntimeException(Throwable& cause);
            ~RuntimeException();
        };
    }
}

#endif // CPP_LANG_RUNTIMEEXCEPTION_H
