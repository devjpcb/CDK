#ifndef RUNTIMEPERMISSION_H
#define RUNTIMEPERMISSION_H

#include "Object.h"

namespace cpp
{
    namespace lang
    {
        class RuntimePermission : public Object
        {
        public:
            RuntimePermission(JString name);
            RuntimePermission(JString name, JString actions);
            ~RuntimePermission();
        };
    }
}

#endif // RUNTIMEPERMISSION_H
