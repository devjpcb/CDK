#include "SecurityManager.h"

namespace cpp
{
    namespace lang
    {
        SecurityManager::SecurityManager()
        {
        }

        SecurityManager::~SecurityManager()
        {
        }
        
        void SecurityManager::checkAccess(JThread t)
        {
            
        }
        
        void SecurityManager::checkAccess(JThreadGroup g)
        {
            
        }
        
        void SecurityManager::checkPermission(JRuntimePermission perm)
        {
            
        }
        
        JThreadGroup SecurityManager::getThreadGroup()
        {
            return nullptr;
        }
        
        void SecurityManager::checkPackageAccess(JString pkg)
        {
            
        }
        
        void SecurityManager::checkPropertiesAccess()
        {
            
        }
        
        void SecurityManager::checkPropertyAccess(JString key)
        {
            
        }
    }
}
