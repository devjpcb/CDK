#ifndef CPP_LANG_SECURITYMANAGER_H
#define CPP_LANG_SECURITYMANAGER_H

#include "Object.h"

namespace cpp
{
    namespace lang
    {
        class SecurityManager : public Object
        {
        public:
            SecurityManager();
            ~SecurityManager();
            
            void checkAccess(JThread t);
            void checkAccess(JThreadGroup g);
            void checkPermission(JRuntimePermission perm);
            static JThreadGroup getThreadGroup();
            void checkPackageAccess(JString pkg);
            void checkPropertiesAccess();
            void checkPropertyAccess(JString key);
        };
    }
}

#endif // CPP_LANG_SECURITYMANAGER_H
