#include "Short.h"
#include "Integer.h"
#include "String.h"
#include "Byte.h"
#include "NumberFormatException.h"

namespace cpp
{
    namespace lang
    {
        Short::Short() : value(0)
        {
        }

        Short::Short(jshort pvalue) : value(pvalue)
        {
        }

        Short::Short(JString s) : value(parseShort(s, 10))
        {
        }

        Short::~Short()
        {
        }

        jbyte Short::byteValue()
        {
            return static_cast<jbyte>(value);
        }

        jint Short::compare(jshort x, jshort y)
        {
            return x - y;
        }

        jint Short::compareTo(JShort anotherShort)
        {
            return compare(value, anotherShort->value);
        }

        JShort Short::decode(JString nm)
        {
            jint i = unboxing(Integer::decode(nm));
            if(i < MIN_VALUE || i > MAX_VALUE)
                throw NumberFormatException(jstr(u"Value ") + boxing(i) + jstr(u" out of range from input ") + nm);
            return valueOf((jshort)i);
        }

        jdouble Short::doubleValue()
        {
            return value;
        }

        jboolean Short::equals(JObject obj)
        {
            JShort sobj = std::dynamic_pointer_cast<Short>(obj);
            if(sobj != nullptr) {
                return value == sobj->value;
            }

            return false;
        }

        jfloat Short::floatValue()
        {
            return value;
        }

        jint Short::hashCode()
        {
            return hashCode(value);
        }

        jint Short::hashCode(jshort value)
        {
            return value;
        }

        jint Short::intValue()
        {
            return value;
        }

        jlong Short::longValue()
        {
            return value;
        }

        jshort Short::parseShort(JString s)
        {
            return parseShort(s, 10);
        }

        jshort Short::parseShort(JString s, jint radix)
        {
            jint i = Integer::parseInt(s, radix);
            if(i < MIN_VALUE || i > MAX_VALUE)
                throw NumberFormatException(jstr(u"Value out of range. Value:\"") + s + jstr(u"\" Radix:") + boxing(radix));
            return (jshort)i;
        }

        jshort Short::reverseBytes(jshort i)
        {
            return static_cast<jshort>(((i & 0xFF00) >> 8 | i << 8));
        }

        jshort Short::shortValue()
        {
            return value;
        }

        JString Short::toString()
        {
            return Integer::toString(static_cast<jint>(value));
        }

        JString Short::toString(jshort s)
        {
            return Integer::toString(static_cast<jint>(s), 10);
        }

        jint Short::toUnsignedInt(jshort x)
        {
            return x & 0xFFFF;
        }

        jlong Short::toUnsignedLong(jshort x)
        {
            return x & 0xFFFF;
        }

        JShort Short::valueOf(jshort s)
        {
            const jint offset = 128;
            jint sAsInt = s;
            if (sAsInt >= -128 && sAsInt <= 127) { // must cache
                return ShortCache::cache[sAsInt + offset];
            }
            return jnew<Short>(s);
        }

        JShort Short::valueOf(JString s)
        {
            return valueOf(s, 10);
        }
        
        JShort Short::valueOf(JString s, jint radix)
        {
            return valueOf(parseShort(s, radix));
        }

        const jint Short::BYTES = SIZE / Byte::SIZE;
        const jshort Short::MAX_VALUE = 32767;
        const jshort Short::MIN_VALUE = -32768;
        const jint Short::SIZE = 16;
        
        
        jarray<JShort> Short::ShortCache::cache(-(-128) + 127 + 1);
                
        jboolean Short::ShortCache::isStaticInitializationBlock = StaticInitializationBlock();
        
        jboolean Short::ShortCache::StaticInitializationBlock() 
        {
            for(int i = 0; i < cache.length(); i++)
                cache[i] = jnew<Short>((jshort)(i - 128));
            return true;
        }
    }
}
