#ifndef CPP_LANG_SHORT_H
#define CPP_LANG_SHORT_H

#include "Number.h"
#include "Comparable.h"
#include "Array.h"

namespace cpp
{
    namespace lang
    {
        class Short : public Number, public Comparable<JShort>
        {
        public:
            Short();
            Short(jshort pvalue);
            Short(JString s);
            ~Short();

            jbyte byteValue();
            static jint compare(jshort x, jshort y);
            jint compareTo(JShort anotherShort);
            static JShort decode(JString nm);
            jdouble doubleValue();
            jboolean equals(JObject obj);
            jfloat floatValue();
            jint hashCode();
            static jint hashCode(jshort value);
            jint intValue();
            jlong longValue();
            static jshort parseShort(JString s);
            static jshort parseShort(JString s, jint radix);
            static jshort reverseBytes(jshort i);
            jshort shortValue();
            JString toString();
            static JString toString(jshort s);
            static jint toUnsignedInt(jshort x);
            static jlong toUnsignedLong(jshort x);
            static JShort valueOf(jshort s);
            static JShort valueOf(JString s);
            static JShort valueOf(JString s, jint radix);

            static const jint BYTES;
            static const jshort MAX_VALUE;
            static const jshort MIN_VALUE;
            static const jint SIZE;
        private:
            jshort value;
            
            class ShortCache 
            {
            public:
                static jarray<JShort> cache;
                
                static jboolean isStaticInitializationBlock;
                static jboolean StaticInitializationBlock();
            };
        };
    }
}

#endif // CPP_LANG_SHORT_H
