#include "StrictMath.h"
#include "Math.h"
#include "Double.h"
#include "Float.h"
#include "../../sun/misc/DoubleConsts.h"
#include <cmath>
#include <cassert>

using namespace sun::misc;

namespace cpp
{
    namespace lang
    {
        jdouble StrictMath::abs(jdouble a)
        {
            return Math::abs(a);
        }

        jfloat StrictMath::abs(jfloat a)
        {
            return Math::abs(a);
        }

        jint StrictMath::abs(jint a)
        {
            return Math::abs(a);
        }

        jlong StrictMath::abs(jlong a)
        {
            return Math::abs(a);
        }

        jdouble StrictMath::acos(jdouble a)
        {
            return std::acos(a);
        }

        jint StrictMath::addExact(jint x, jint y)
        {
            return Math::addExact(x, y);
        }

        jlong StrictMath::addExact(jlong x, jlong y)
        {
            return Math::addExact(x, y);
        }

        jdouble StrictMath::asin(jdouble a)
        {
            return std::asin(a);
        }

        jdouble StrictMath::atan(jdouble a)
        {
            return std::atan(a);
        }

        jdouble StrictMath::atan2(jdouble y, jdouble x)
        {
            return std::atan2(y, x);
        }

        jdouble StrictMath::cbrt(jdouble a)
        {
            return std::cbrt(a);
        }

        jdouble StrictMath::ceil(jdouble a)
        {
            return floorOrCeil(a, -0.0, 1.0, 1.0);
        }

        jdouble StrictMath::copySign(jdouble magnitude, jdouble sign)
        {
            return Math::copySign(magnitude, (Double::isNaN(sign) ? 1.0 : sign));
        }

        jfloat StrictMath::copySign(jfloat magnitude, jfloat sign)
        {
            return Math::copySign(magnitude, (Float::isNaN(sign) ? 1.0f : sign));
        }

        jdouble StrictMath::cos(jdouble a)
        {
            return std::cos(a);
        }

        jdouble StrictMath::cosh(jdouble x)
        {
            return std::cosh(x);
        }

        jdouble StrictMath::exp(jdouble a)
        {
            return std::exp(a);
        }

        jdouble StrictMath::expm1(jdouble x)
        {
            return std::expm1(x);
        }

        jdouble StrictMath::floor(jdouble a)
        {
            return floorOrCeil(a, -1.0, 0.0, -1.0);
        }

        jint StrictMath::floorDiv(jint x, jint y)
        {
            return Math::floorDiv(x, y);
        }

        jlong StrictMath::floorDiv(jlong x, jlong y)
        {
            return Math::floorDiv(x, y);
        }

        jint StrictMath::floorMod(jint x, jint y)
        {
            return Math::floorMod(x, y);
        }

        jlong StrictMath::floorMod(jlong x, jlong y)
        {
            return Math::floorMod(x, y);
        }

        jint StrictMath::getExponent(jdouble d)
        {
            return Math::getExponent(d);
        }

        jint StrictMath::getExponent(jfloat f)
        {
            return Math::getExponent(f);
        }

        jdouble StrictMath::hypot(jdouble x, jdouble y)
        {
            return std::hypot(x, y);
        }

        jdouble StrictMath::IEEEremainder(jdouble f1, jdouble f2)
        {
            return std::remainder(f1, f2);
        }

        jdouble StrictMath::log(jdouble a)
        {
            return std::log(a);
        }

        jdouble StrictMath::log10(jdouble a)
        {
            return std::log10(a);
        }

        jdouble StrictMath::log1p(jdouble x)
        {
            return std::log1p(x);
        }

        jdouble StrictMath::max(jdouble a, jdouble b)
        {
            return Math::max(a, b);
        }

        jfloat StrictMath::max(jfloat a, jfloat b)
        {
            return Math::max(a, b);
        }

        jint StrictMath::max(jint a, jint b)
        {
            return Math::max(a, b);
        }

        jlong StrictMath::max(jlong a, jlong b)
        {
            return Math::max(a, b);
        }

        jdouble StrictMath::min(jdouble a, jdouble b)
        {
            return Math::min(a, b);
        }

        jfloat StrictMath::min(jfloat a, jfloat b)
        {
            return Math::min(a, b);
        }

        jint StrictMath::min(jint a, jint b)
        {
            return Math::min(a, b);
        }

        jlong StrictMath::min(jlong a, jlong b)
        {
            return Math::min(a, b);
        }

        jint StrictMath::multiplyExact(jint x, jint y)
        {
            return Math::multiplyExact(x, y);
        }

        jlong StrictMath::multiplyExact(jlong x, jlong y)
        {
            return Math::multiplyExact(x, y);
        }

        jdouble StrictMath::nextAfter(jdouble start, jdouble direction)
        {
            return Math::nextAfter(start, direction);
        }

        jfloat StrictMath::nextAfter(jfloat start, jdouble direction)
        {
            return Math::nextAfter(start, direction);
        }

        jdouble StrictMath::nextDown(jdouble d)
        {
            return Math::nextDown(d);
        }

        jfloat StrictMath::nextDown(jfloat f)
        {
            return Math::nextDown(f);
        }

        jdouble StrictMath::nextUp(jdouble d)
        {
            return Math::nextUp(d);
        }

        jfloat StrictMath::nextUp(jfloat f)
        {
            return Math::nextUp(f);
        }

        jdouble StrictMath::pow(jdouble a, jdouble b)
        {
            return std::pow(a, b);
        }

        jdouble StrictMath::random()
        {
            return -1.0;
        }

        jdouble StrictMath::rint(jdouble a)
        {
            jdouble twoToThe52 = static_cast<jdouble>(1LL << 52);
            jdouble sign = Math::copySign(1.0, a);
            a = Math::abs(a);
        
            if(a < twoToThe52) { 
                a = ((twoToThe52 + a) - twoToThe52);
            }
        
            return sign * a;
        }

        jlong StrictMath::round(jdouble a)
        {
            return Math::round(a);
        }

        jint StrictMath::round(jfloat a)
        {
            return Math::round(a);
        }

        jdouble StrictMath::scalb(jdouble d, jint scaleFactor)
        {
            return Math::scalb(d, scaleFactor);
        }

        jfloat StrictMath::scalb(jfloat f, jint scaleFactor)
        {
            return Math::scalb(f, scaleFactor);
        }

        jdouble StrictMath::signum(jdouble d)
        {
            return Math::signum(d);
        }

        jfloat StrictMath::signum(jfloat f)
        {
            return Math::signum(f);
        }

        jdouble StrictMath::sin(jdouble a)
        {
            return std::sin(a);
        }

        jdouble StrictMath::sinh(jdouble x)
        {
            return std::sinh(x);
        }

        jdouble StrictMath::sqrt(jdouble a)
        {
            return std::sqrt(a);
        }

        jint StrictMath::subtractExact(jint x, jint y)
        {
            return Math::subtractExact(x, y);
        }

        jlong StrictMath::subtractExact(jlong x, jlong y)
        {
            return Math::subtractExact(x, y);
        }

        jdouble StrictMath::tan(jdouble a)
        {
            return std::tan(a);
        }

        jdouble StrictMath::tanh(jdouble x)
        {
            return std::tanh(x);
        }

        jdouble StrictMath::toDegrees(jdouble angrad)
        {
            return angrad * 180.0 / PI;
        }

        jint StrictMath::toIntExact(jlong value)
        {
            return Math::toIntExact(value);
        }

        jdouble StrictMath::toRadians(jdouble angdeg)
        {
            return angdeg / 180.0 * PI;
        }

        jdouble StrictMath::ulp(jdouble d)
        {
            return Math::ulp(d);
        }

        jfloat StrictMath::ulp(jfloat f)
        {
            return Math::ulp(f);
        }
        
        jdouble StrictMath::floorOrCeil(jdouble a, jdouble negativeBoundary, jdouble positiveBoundary, jdouble sign)
        {
            jint exponent = Math::getExponent(a);

            if (exponent < 0) {
                return ((a == 0.0) ? a : ( (a < 0.0) ?  negativeBoundary : positiveBoundary) );
            } else if (exponent >= 52) {
                return a;
            }

            assert(exponent >= 0 && exponent <= 51);

            jlong doppel = Double::doubleToRawLongBits(a);
            jlong mask   = DoubleConsts::SIGNIF_BIT_MASK >> exponent;

            if ( (mask & doppel) == 0L )
                return a;
            else {
                jdouble result = Double::longBitsToDouble(doppel & (~mask));
                if (sign*a > 0.0)
                    result = result + sign;
                return result;
            }
        }
        
        const jdouble StrictMath::E = 2.7182818284590452354;
        const jdouble StrictMath::PI = 3.14159265358979323846;
    }
}
