#ifndef CPP_LANG_STRICTMATH_H
#define CPP_LANG_STRICTMATH_H

#include "Object.h"

namespace cpp
{
    namespace lang
    {
        class StrictMath
        {
        public:
            static jdouble abs(jdouble a);
            static jfloat abs(jfloat a);
            static jint abs(jint a);
            static jlong abs(jlong a);
            static jdouble acos(jdouble a);
            static jint addExact(jint x, jint y);
            static jlong addExact(jlong x, jlong y);
            static jdouble asin(jdouble a);
            static jdouble atan(jdouble a);
            static jdouble atan2(jdouble y, jdouble x);
            static jdouble cbrt(jdouble a);
            static jdouble ceil(jdouble a);
            static jdouble copySign(jdouble magnitude, jdouble sign);
            static jfloat copySign(jfloat magnitude, jfloat sign);
            static jdouble cos(jdouble a);
            static jdouble cosh(jdouble x);
            static jdouble exp(jdouble a);
            static jdouble expm1(jdouble x);
            static jdouble floor(jdouble a);
            static jint floorDiv(jint x, jint y);
            static jlong floorDiv(jlong x, jlong y);
            static jint floorMod(jint x, jint y);
            static jlong floorMod(jlong x, jlong y);
            static jint getExponent(jdouble d);
            static jint getExponent(jfloat f);
            static jdouble hypot(jdouble x, jdouble y);
            static jdouble IEEEremainder(jdouble f1, jdouble f2);
            static jdouble log(jdouble a);
            static jdouble log10(jdouble a);
            static jdouble log1p(jdouble x);
            static jdouble max(jdouble a, jdouble b);
            static jfloat max(jfloat a, jfloat b);
            static jint max(jint a, jint b);
            static jlong max(jlong a, jlong b);
            static jdouble min(jdouble a, jdouble b);
            static jfloat min(jfloat a, jfloat b);
            static jint min(jint a, jint b);
            static jlong min(jlong a, jlong b);
            static jint multiplyExact(jint x, jint y);
            static jlong multiplyExact(jlong x, jlong y);
            static jdouble nextAfter(jdouble start, jdouble direction);
            static jfloat nextAfter(jfloat start, jdouble direction);
            static jdouble nextDown(jdouble d);
            static jfloat nextDown(jfloat f);
            static jdouble nextUp(jdouble d);
            static jfloat nextUp(jfloat f);
            static jdouble pow(jdouble a, jdouble b);
            static jdouble random();
            static jdouble rint(jdouble a);
            static jlong round(jdouble a);
            static jint round(jfloat a);
            static jdouble scalb(jdouble d, jint scaleFactor);
            static jfloat scalb(jfloat f, jint scaleFactor);
            static jdouble signum(jdouble d);
            static jfloat signum(jfloat f);
            static jdouble sin(jdouble a);
            static jdouble sinh(jdouble x);
            static jdouble sqrt(jdouble a);
            static jint subtractExact(jint x, jint y);
            static jlong subtractExact(jlong x, jlong y);
            static jdouble tan(jdouble a);
            static jdouble tanh(jdouble x);
            static jdouble toDegrees(jdouble angrad);
            static jint toIntExact(jlong value);
            static jdouble toRadians(jdouble angdeg);
            static jdouble ulp(jdouble d);
            static jfloat ulp(jfloat f);
            
            static const jdouble E;
            static const jdouble PI;
        private:
            static jdouble floorOrCeil(jdouble a, jdouble negativeBoundary, jdouble positiveBoundary, jdouble sign);
        };
    }
}

#endif // CPP_LANG_STRICTMATH_H
