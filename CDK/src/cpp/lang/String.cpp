#include "String.h"
#include "Character.h"
#include "Double.h"
#include "Float.h"
#include "Integer.h"
#include "Long.h"
#include "Math.h"
#include "System.h"
#include "../util/Arrays.h"
#include "StringIndexOutOfBoundsException.h"
#include "IllegalArgumentException.h"
#include <locale>
#include <codecvt>

using cpp::util::Arrays;

namespace cpp
{
    namespace lang
    {
        String::String() : hash(0)
        {
            this->value = jstr(u"")->value;
        }
        
        String::String(JString original)
        {
            this->value = original->value;
            this->hash = original->hash;
        }

        String::String(jarray<jbyte> ascii, jint hibyte) : String(ascii, hibyte, 0, ascii.length())
        {
        }

        String::String(jarray<jbyte> ascii, jint hibyte, jint offset, jint count) : hash(0)
        {
            checkBounds(ascii, offset, count);
            jarray<jchar> value(count);

            if(hibyte == 0) {
                for(int i = count; i-- > 0;) {
                    value[i] = (jchar)(ascii[i + offset] & 0xff);
                }
            }
            else {
                hibyte <<= 8;
                for(int i = count; i-- > 0;) {
                    value[i] = (jchar)(hibyte | (ascii[i + offset] & 0xff));
                }
            }

            this->value = value;
        }

        String::String(jarray<jchar> value) : hash(0)
        {
            this->value = Arrays::copyOf(value, value.length());
        }

        String::String(jarray<jchar> value, jint offset, jint count) : hash(0)
        {
            if(offset < 0) {
                throw StringIndexOutOfBoundsException(offset);
            }
            if(count < 0) {
                throw StringIndexOutOfBoundsException(count);
            }
            if(offset > value.length() - count) {
                throw StringIndexOutOfBoundsException(offset + count);
            }

            this->value = Arrays::copyOfRange(value, offset, offset + count);
        }

        String::String(jarray<jint> codePoints, jint offset, jint count) : hash(0)
        {
            if(offset < 0) {
                throw StringIndexOutOfBoundsException(offset);
            }
            if(count < 0) {
                throw StringIndexOutOfBoundsException(count);
            }

            if(offset > codePoints.length() - count) {
                throw StringIndexOutOfBoundsException(offset + count);
            }

            const jint end = offset + count;

            jint n = count;
            for(jint i = offset; i < end; i++) {
                jint c = codePoints[i];
                if(Character::isBmpCodePoint(c))
                    continue;
                else if(Character::isValidCodePoint(c))
                    n++;
                else
                    throw IllegalArgumentException(Integer::toString(c));
            }

            jarray<jchar> v(n);

            for(int i = offset, j = 0; i < end; i++, j++) {
                int c = codePoints[i];
                if(Character::isBmpCodePoint(c))
                    v[j] = (jchar)c;
                else
                    Character::toSurrogates(c, v, j++);
            }

            value = v;
        }
        
        String::String(jarray<jchar> value, jboolean share) : hash(0)
        {
            this->value = value;
        }
        
        String::~String()
        {
        }

        jchar String::charAt(jint index)
        {
            if((index < 0) || (index >= value.length())) {
                throw StringIndexOutOfBoundsException(index);
            }
            return value[index];
        }

        jint String::codePointAt(jint index)
        {
            if((index < 0) || (index >= value.length())) {
                throw StringIndexOutOfBoundsException(index);
            }
            return Character::codePointAtImpl(value, index, value.length());
        }

        jint String::codePointBefore(jint index)
        {
            jint i = index - 1;
            if((i < 0) || (i >= value.length())) {
                throw StringIndexOutOfBoundsException(index);
            }
            return Character::codePointBeforeImpl(value, index, 0);
        }

        jint String::codePointCount(jint beginIndex, jint endIndex)
        {
            if(beginIndex < 0 || endIndex > value.length() || beginIndex > endIndex) {
                throw IndexOutOfBoundsException();
            }
            return Character::codePointCountImpl(value, beginIndex, endIndex - beginIndex);
        }

        jint String::compareTo(JString anotherString)
        {
            jint i = value.length();
            jint j = anotherString->value.length();
            jint k = Math::min(i, j);

            jint m = 0;
            while(m < k) {
                jint n = value[m];
                jint i1 = anotherString->value[m];
                if(n != i1) {
                    return n - i1;
                }
                m++;
            }

            return i - j;
        }

        JString String::concat(JString str)
        {
            jint i = str->length();
            if(i == 0) {
                return shared_from_this();
            }
            jint j = value.length();
            jarray<jchar> arrayOfChar = Arrays::copyOf(value, j + i);
            str->getChars(arrayOfChar, j);
            return jnew<String>(arrayOfChar, true);
        }

        JString String::copyValueOf(jarray<jchar> data)
        {
            return jnew<String>(data);
        }

        JString String::copyValueOf(jarray<jchar> data, jint offset, jint count)
        {
            return jnew<String>(data, offset, count);
        }

        jboolean String::endsWith(JString suffix)
        {
            return startsWith(suffix, value.length() - suffix->value.length());
        }

        jboolean String::equals(JObject anObject)
        {
            if(shared_from_this() == anObject) {
                return true;
            }

            JString anotherString = std::dynamic_pointer_cast<String>(anObject);
            if(anotherString != nullptr) {
                jint n = value.length();
                if(n == anotherString->value.length()) {
                    jint i = 0;
                    while(n-- != 0) {
                        if(value[i] != anotherString->value[i])
                            return false;
                        i++;
                    }
                    return true;
                }
            }

            return false;
        }

        jboolean String::equalsIgnoreCase(JString anotherString)
        {
            return (shared_from_this() == anotherString) ? true : (anotherString->value.length() == value.length()) &&
                   regionMatches(true, 0, anotherString, 0, value.length());
        }

        void String::getChars(jint srcBegin, jint srcEnd, jarray<jchar> dst, jint dstBegin)
        {
            if(srcBegin < 0) {
                throw StringIndexOutOfBoundsException(srcBegin);
            }
            if(srcEnd > value.length()) {
                throw StringIndexOutOfBoundsException(srcEnd);
            }
            if(srcBegin > srcEnd) {
                throw StringIndexOutOfBoundsException(srcEnd - srcBegin);
            }
            System::arraycopy(value, srcBegin, dst, dstBegin, srcEnd - srcBegin);
        }

        jint String::hashCode()
        {
            jint h = hash;
            if(h == 0 && value.length() > 0) {
                for(jint i = 0; i < value.length(); i++) {
                    h = 31 * h + value[i];
                }
                hash = h;
            }
            return h;
        }

        jint String::indexOf(jint ch)
        {
            return indexOf(ch, 0);
        }

        jint String::indexOf(jint ch, jint fromIndex)
        { 
            const jint max = value.length();
            if(fromIndex < 0) {
                fromIndex = 0;
            }
            else if(fromIndex >= max) {
                return -1;
            }

            if(ch < Character::MIN_SUPPLEMENTARY_CODE_POINT) {
                const jarray<jchar> value = this->value;
                for(jint i = fromIndex; i < max; i++) {
                    if(value[i] == ch) {
                        return i;
                    }
                }
                return -1;
            }
            else {
                return indexOfSupplementary(ch, fromIndex);
            }
        }

        jint String::indexOf(JString str)
        {
            return indexOf(str, 0);
        }

        jint String::indexOf(JString str, jint fromIndex)
        {
            return indexOf(value, 0, value.length(), str->value, 0, str->value.length(), fromIndex);
        }
        
        JString String::intern()
        {
            return shared_from_this();
        }

        jboolean String::isEmpty()
        {
            return value.length() == 0;
        }

        jint String::lastIndexOf(jint ch)
        {
            return lastIndexOf(ch, value.length() - 1);
        }

        jint String::lastIndexOf(jint ch, jint fromIndex)
        {
            if(ch < Character::MIN_SUPPLEMENTARY_CODE_POINT) {
                const jarray<jchar> value = this->value;
                jint i = Math::min(fromIndex, value.length() - 1);
                for(; i >= 0; i--) {
                    if(value[i] == ch) {
                        return i;
                    }
                }
                return -1;
            }
            else {
                return lastIndexOfSupplementary(ch, fromIndex);
            }
        }

        jint String::lastIndexOf(JString str)
        {
            return lastIndexOf(str, value.length());
        }

        jint String::lastIndexOf(JString str, jint fromIndex)
        {
            return lastIndexOf(value, 0, value.length(), str->value, 0, str->value.length(), fromIndex);
        }

        jint String::length()
        {
            return value.length();
        }

        const jint String::length() const
        {
            return value.length();
        }

        jint String::offsetByCodePoints(jint index, jint codePointOffset)
        {
            if(index < 0 || index > value.length()) {
                throw IndexOutOfBoundsException();
            }
            return Character::offsetByCodePointsImpl(value, 0, value.length(), index, codePointOffset);
        }

        jboolean String::regionMatches(jboolean ignoreCase, jint toffset, JString other, jint ooffset, jint len)
        { 
            jarray<jchar> ta = value;
            jint to = toffset;
            jarray<jchar> pa = other->value;
            jint po = ooffset;

            if((ooffset < 0) || (toffset < 0) || (toffset > (jlong)value.length() - len) ||
               (ooffset > (jlong)other->value.length() - len)) {
                return false;
            }
            while(len-- > 0) {
                jchar c1 = ta[to++];
                jchar c2 = pa[po++];
                if(c1 == c2) {
                    continue;
                }
                if(ignoreCase) {
                    jchar u1 = Character::toUpperCase(c1);
                    jchar u2 = Character::toUpperCase(c2);
                    
                    if(u1 == u2) {
                        continue;
                    }

                    if(Character::toLowerCase(u1) == Character::toLowerCase(u2)) {
                        continue;
                    }
                }
                return false;
            }
            return true;
        }

        jboolean String::regionMatches(jint toffset, JString other, jint ooffset, jint len)
        {
            jarray<jchar> ta = value;
            jint to = toffset;
            jarray<jchar> pa = other->value;
            jint po = ooffset;

            if((ooffset < 0) || (toffset < 0) || (toffset > (jlong)value.length() - len) ||
               (ooffset > static_cast<jlong>(other->value.length() - len))) {
                return false;
            }
            while(len-- > 0) {
                if(ta[to++] != pa[po++]) {
                    return false;
                }
            }
            return true;
        }

        JString String::replace(jchar oldChar, jchar newChar)
        {
            if(oldChar != newChar) {
                jint len = value.length();
                jint i = -1;
                jarray<jchar> val = value;

                while(++i < len) {
                    if(val[i] == oldChar) {
                        break;
                    }
                }
                if(i < len) {
                    jarray<jchar> buf(len);
                    for(int j = 0; j < i; j++) {
                        buf[j] = val[j];
                    }
                    while(i < len) {
                        jchar c = val[i];
                        buf[i] = (c == oldChar) ? newChar : c;
                        i++;
                    }
                    return jnew<String>(buf, true);
                }
            }

            return shared_from_this();
        }

        jboolean String::startsWith(JString prefix)
        {
            return startsWith(prefix, 0);
        }

        jboolean String::startsWith(JString prefix, jint toffset)
        {
            jarray<jchar> ta = value;
            jint to = toffset;
            jarray<jchar> pa = prefix->value;
            jint po = 0;
            jint pc = prefix->value.length();

            if((toffset < 0) || (toffset > value.length() - pc)) {
                return false;
            }
            while(--pc >= 0) {
                if(ta[to++] != pa[po++]) {
                    return false;
                }
            }
            return true;
        }

        JCharSequence String::subSequence(jint beginIndex, jint endIndex)
        {
            return this->substring(beginIndex, endIndex);
        }

        JString String::substring(jint beginIndex)
        {
            if(beginIndex < 0) {
                throw StringIndexOutOfBoundsException(beginIndex);
            }
            int subLen = value.length() - beginIndex;
            if(subLen < 0) {
                throw StringIndexOutOfBoundsException(subLen);
            }
            return (beginIndex == 0) ? shared_from_this() : jnew<String>(value, beginIndex, subLen);
        }

        JString String::substring(jint beginIndex, jint endIndex)
        {
            if(beginIndex < 0) {
                throw StringIndexOutOfBoundsException(beginIndex);
            }
            if(endIndex > value.length()) {
                throw StringIndexOutOfBoundsException(endIndex);
            }
            jint subLen = endIndex - beginIndex;
            if(subLen < 0) {
                throw StringIndexOutOfBoundsException(subLen);
            }
            return ((beginIndex == 0) && (endIndex == value.length())) ? shared_from_this() : jnew<String>(value, beginIndex, subLen);
        }

        jarray<jchar> String::toCharArray()
        {
            jarray<jchar> result(value.length());
            System::arraycopy(value, 0, result, 0, value.length());
            return result;
        }

        JString String::toString()
        {
            return shared_from_this();
        }

        JString String::trim()
        {
            jint len = value.length();
            jint st = 0;
            jarray<jchar> val = value;

            while((st < len) && (val[st] <= u' ')) {
                st++;
            }
            while((st < len) && (val[len - 1] <= u' ')) {
                len--;
            }
            return ((st > 0) || (len < value.length())) ? substring(st, len) : shared_from_this();
        }

        JString String::valueOf(jboolean b)
        {
            return b ? jstr(u"true") : jstr(u"false");
        }
        
        JString String::valueOf(jchar c)
        {
            jarray<jchar> data = { c };
            return jnew<String>(data, true);
        }
        
        JString String::valueOf(jarray<jchar> data)
        {
            return jnew<String>(data);
        }
        
        JString String::valueOf(jarray<jchar> data, jint offset, jint count)
        {
            return jnew<String>(data, offset, count);
        }
        
        JString String::valueOf(jdouble d)
        {
            return Double::toString(d);
        }
        
        JString String::valueOf(jfloat f)
        {
            return Float::toString(f);
        }
        
        JString String::valueOf(jint i)
        {
            return Integer::toString(i);
        }
        
        JString String::valueOf(jlong l)
        {
            return Long::toString(l);
        }

        JString String::valueOf(JObject obj)
        {
            return (obj == nullptr) ? jstr(u"null") : obj->toString();
        }

        void String::getChars(jarray<jchar> dest, jint destBegin)
        {
            System::arraycopy(value, 0, dest, destBegin, value.length());
        }

        jint String::indexOfSupplementary(jint ch, jint fromIndex)
        {
            if(Character::isValidCodePoint(ch)) {
                const jarray<jchar> value = this->value;
                const jchar hi = Character::highSurrogate(ch);
                const jchar lo = Character::lowSurrogate(ch);
                const jint max = value.length() - 1;
                for(jint i = fromIndex; i < max; i++) {
                    if(value[i] == hi && value[i + 1] == lo) {
                        return i;
                    }
                }
            }
            return -1;
        }

        jint String::indexOf(jarray<jchar> source, jint sourceOffset, jint sourceCount, JString target, jint fromIndex)
        {
            return indexOf(source, sourceOffset, sourceCount, target->value, 0, target->value.length(), fromIndex);
        }

        jint String::indexOf(jarray<jchar> source, jint sourceOffset, jint sourceCount, jarray<jchar> target, jint targetOffset, jint targetCount, jint fromIndex)
        {
            if(fromIndex >= sourceCount) {
                return (targetCount == 0 ? sourceCount : -1);
            }
            if(fromIndex < 0) {
                fromIndex = 0;
            }
            if(targetCount == 0) {
                return fromIndex;
            }

            jchar first = target[targetOffset];
            jint max = sourceOffset + (sourceCount - targetCount);

            for(jint i = sourceOffset + fromIndex; i <= max; i++) {
                /* Look for first character. */
                if(source[i] != first) {
                    while(++i <= max && source[i] != first);
                }

                if(i <= max) {
                    jint j = i + 1;
                    jint end = j + targetCount - 1;
                    for(jint k = targetOffset + 1; j < end && source[j] == target[k]; j++, k++);

                    if(j == end) {
                        return i - sourceOffset;
                    }
                }
            }
            return -1;
        }

        jint String::lastIndexOfSupplementary(jint ch, jint fromIndex)
        {
            if(Character::isValidCodePoint(ch)) {
                const jarray<jchar> value = this->value;
                jchar hi = Character::highSurrogate(ch);
                jchar lo = Character::lowSurrogate(ch);
                jint i = Math::min(fromIndex, value.length() - 2);
                for(; i >= 0; i--) {
                    if(value[i] == hi && value[i + 1] == lo) {
                        return i;
                    }
                }
            }
            return -1;
        }

        jint String::lastIndexOf(jarray<jchar> source, jint sourceOffset, jint sourceCount, JString target, jint fromIndex)
        {
            return lastIndexOf(source, sourceOffset, sourceCount, target->value, 0, target->value.length(), fromIndex);
        }

        jint String::lastIndexOf(jarray<jchar> source, jint sourceOffset, jint sourceCount, jarray<jchar> target, jint targetOffset, jint targetCount, jint fromIndex)
        {
            jint rightIndex = sourceCount - targetCount;
            if(fromIndex < 0) {
                return -1;
            }
            if(fromIndex > rightIndex) {
                fromIndex = rightIndex;
            }

            if(targetCount == 0) {
                return fromIndex;
            }

            jint strLastIndex = targetOffset + targetCount - 1;
            jchar strLastChar = target[strLastIndex];
            jint min = sourceOffset + targetCount - 1;
            jint i = min + fromIndex;

        startSearchForLastChar:
            while(true) {
                while(i >= min && source[i] != strLastChar) {
                    i--;
                }
                if(i < min) {
                    return -1;
                }
                jint j = i - 1;
                jint start = j - (targetCount - 1);
                jint k = strLastIndex - 1;

                while(j > start) {
                    if(source[j--] != target[k--]) {
                        i--;
                        goto startSearchForLastChar;
                    }
                }
                return start - sourceOffset + 1;
            }
        }

        void String::checkBounds(jarray<jbyte> bytes, jint offset, jint length)
        {
            if(length < 0)
                throw StringIndexOutOfBoundsException(length);
            if(offset < 0)
                throw StringIndexOutOfBoundsException(offset);
            if(offset > bytes.length() - length)
                throw StringIndexOutOfBoundsException(offset + length);
        }
        
        JString to_jstr(const std::string& str)
        {
            std::wstring_convert<std::codecvt_utf8_utf16<char16_t>, char16_t> convert;
            std::u16string str16 = convert.from_bytes(str);
            const jint SIZE = static_cast<jint>(str16.length());
            jarray<jchar> newValue(SIZE);

            for(jint index = 0; index < SIZE; index++) {
                newValue[index] = str16[index];
            }

            return jnew<String>(newValue, true);
        }

        JString to_jstr(const std::wstring& str)
        {
            const jint SIZE = str.length();
            jarray<jchar> newValue(SIZE);

            for(jint index = 0; index < SIZE; index++) {
                newValue[index] = str[index];
            }

            return jnew<String>(newValue, true);
        }

        JString to_jstr(const std::u16string& str)
        {
            const jint SIZE = static_cast<jint>(str.length());
            jarray<jchar> newValue(SIZE);

            for(jint index = 0; index < SIZE; index++) {
                newValue[index] = str[index];
            }

            return jnew<String>(newValue, true);
        }
        
        std::string to_cppstr(JString str)
        {
            std::u16string result;

            for(jchar & chr : str->value) {
                result += chr;
            }

            std::wstring_convert<std::codecvt_utf8_utf16<char16_t>, char16_t> convert;
            return convert.to_bytes(result);
        }
        
        std::u16string to_cppstr16(JString str)
        {
            std::u16string result;

            for(jchar & chr : str->value) {
                result += chr;
            }

            return result;
        }
        
        std::wstring to_cppwstr(JString str)
        {
            std::wstring result;

            for(jchar & chr : str->value) {
                result += chr;
            }

            return result;
        }
        
        JString operator + (JString str1, JString str2)
        {
            return str1->concat(str2);
        }
        
        JString operator + (JString str1, JObject str2)
        {
            return str1->concat(str2->toString());
        }
        
        JString operator + (JObject str1, JString str2)
        {
            return str1->toString()->concat(str2);
        }
        
        JString operator + (JString str1, JInteger str2)
        {
            return str1->concat(str2->toString());
        }
        
        JString operator + (JInteger str1, JString str2)
        {
            return str1->toString()->concat(str2);
        }
    }
}
