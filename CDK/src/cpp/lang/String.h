#ifndef CPP_LANG_STRING_H
#define CPP_LANG_STRING_H

#include "Object.h"
#include "Array.h"
#include "Comparable.h"
#include "CharSequence.h"
#include <string>

namespace cpp
{
    namespace lang
    {
        class String : public Comparable<JString>, public CharSequence, public std::enable_shared_from_this<String>
        {
        public:
            String();
            String(JString original);
            String(jarray<jbyte> ascii, jint hibyte);
            String(jarray<jbyte> ascii, jint hibyte, jint offset, jint count);
            String(jarray<jchar> value);
            String(jarray<jchar> value, jint offset, jint count);
            String(jarray<jint> codePoints, jint offset, jint count);
            String(jarray<jchar> value, jboolean share);
            ~String();

            jchar charAt(jint index);
            jint codePointAt(jint index);
            jint codePointBefore(jint index);
            jint codePointCount(jint beginIndex, jint endIndex);
            jint compareTo(JString anotherString);
            JString concat(JString str);
            static JString copyValueOf(jarray<jchar> data);
            static JString copyValueOf(jarray<jchar> data, jint offset, jint count);
            jboolean endsWith(JString suffix);
            jboolean equals(JObject anObject);
            jboolean equalsIgnoreCase(JString anotherString);
            void getChars(jint srcBegin, jint srcEnd, jarray<jchar> dst, jint dstBegin);
            jint hashCode();
            jint indexOf(jint ch);
            jint indexOf(jint ch, jint fromIndex);
            jint indexOf(JString str);
            jint indexOf(JString str, jint fromIndex);
            JString intern();
            jboolean isEmpty();
            jint lastIndexOf(jint ch);
            jint lastIndexOf(jint ch, jint fromIndex);
            jint lastIndexOf(JString str);
            jint lastIndexOf(JString str, jint fromIndex);
            jint length();
            const jint length() const;
            jint offsetByCodePoints(jint index, jint codePointOffset);
            jboolean regionMatches(jboolean ignoreCase, jint toffset, JString other, jint ooffset, jint len);
            jboolean regionMatches(jint toffset, JString other, jint ooffset, jint len);
            JString replace(jchar oldChar, jchar newChar);
            jboolean startsWith(JString prefix);
            jboolean startsWith(JString prefix, jint toffset);
            JCharSequence subSequence(jint beginIndex, jint endIndex);
            JString substring(jint beginIndex);
            JString substring(jint beginIndex, jint endIndex);
            jarray<jchar> toCharArray();
            JString toString();
            JString trim();
            static JString valueOf(jboolean b);
            static JString valueOf(jchar c);
            static JString valueOf(jarray<jchar> data);
            static JString valueOf(jarray<jchar> data, jint offset, jint count);
            static JString valueOf(jdouble d);
            static JString valueOf(jfloat f);
            static JString valueOf(jint i);
            static JString valueOf(jlong l);
            static JString valueOf(JObject obj);

        private:
            void getChars(jarray<jchar> dest, jint destBegin);
            jint indexOfSupplementary(jint ch, jint fromIndex);
            static jint indexOf(jarray<jchar> source, jint sourceOffset, jint sourceCount, JString target, jint fromIndex);
            static jint indexOf(jarray<jchar> source, jint sourceOffset, jint sourceCount, jarray<jchar> target, jint targetOffset, jint targetCount, jint fromIndex);
            jint lastIndexOfSupplementary(jint ch, jint fromIndex);
            static jint lastIndexOf(jarray<jchar> source, jint sourceOffset, jint sourceCount, JString target, jint fromIndex);
            static jint lastIndexOf(jarray<jchar> source, jint sourceOffset, jint sourceCount, jarray<jchar> target, jint targetOffset, jint targetCount, jint fromIndex);
            static void checkBounds(jarray<jbyte> bytes, jint offset, jint length);

            jarray<jchar> value;
            jint hash;

            friend class Integer;
            friend class Long;
            friend class AbstractStringBuilder;
            friend class StringBuffer;

            friend JString to_jstr(const std::string& str);
            friend JString to_jstr(const std::wstring& str);
            friend JString to_jstr(const std::u16string& str);
            
            friend std::string to_cppstr(JString str);
            friend std::u16string to_cppstr16(JString str);
            friend std::wstring to_cppwstr(JString str);
            
            template <jint N> friend JString jstr(const jchar(&nativeStaticString)[N]);
        };
        
        JString to_jstr(const std::string& str);
        JString to_jstr(const std::wstring& str);
        JString to_jstr(const std::u16string& str);
        
        std::string to_cppstr(JString str);
        std::u16string to_cppstr16(JString str);
        std::wstring to_cppwstr(JString str);
        
        template <jint N> 
        JString jstr(const jchar(&nativeStaticString)[N])
        {
            jarray<jchar> value = nativeStaticString;
            return jnew<String>(value, true);
        }
        
        JString operator + (JString str1, JString str2);
        JString operator + (JString str1, JObject str2);
        JString operator + (JObject str1, JString str2);
        JString operator + (JString str1, JInteger str2);
        JString operator + (JInteger str1, JString str2);
    }
}

using cpp::lang::jstr;

#endif // CPP_LANG_STRING_H
