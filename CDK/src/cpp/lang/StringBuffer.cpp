#include "StringBuffer.h"
#include "String.h"
#include "../util/Arrays.h"
#include "StringIndexOutOfBoundsException.h"

using cpp::util::Arrays;

namespace cpp
{
    namespace lang
    {
        StringBuffer::StringBuffer() : AbstractStringBuilder(16)
        {
        }

        StringBuffer::StringBuffer(jint capacity) : AbstractStringBuilder(capacity)
        {
        }

        StringBuffer::StringBuffer(JString str) : AbstractStringBuilder(str->length() + 16)
        {
            append(str);
        }

        StringBuffer::StringBuffer(JCharSequence seq) : StringBuffer(seq->length() + 16)
        {
            append(seq);
        }

        StringBuffer::~StringBuffer()
        {
        }

        jint StringBuffer::length()
        {
            jsynchronized(this);
            return count;
        }

        jint StringBuffer::capacity()
        {
            jsynchronized(this);
            return value.length();
        }

        void StringBuffer::ensureCapacity(jint minimumCapacity)
        {
            jsynchronized(this);
            if(minimumCapacity > value.length()) {
                expandCapacity(minimumCapacity);
            }
        }

        void StringBuffer::trimToSize()
        {
            jsynchronized(this);
            AbstractStringBuilder::trimToSize();
        }

        void StringBuffer::setLength(jint newLength)
        {
            jsynchronized(this);
            toStringCache = nullptr;
            AbstractStringBuilder::setLength(newLength);
        }

        jchar StringBuffer::charAt(jint index)
        {
            jsynchronized(this);
            if((index < 0) || (index >= count))
                throw StringIndexOutOfBoundsException(index);
            return value[index];
        }

        jint StringBuffer::codePointAt(jint index)
        {
            jsynchronized(this);
            return AbstractStringBuilder::codePointAt(index);
        }

        jint StringBuffer::codePointBefore(jint index)
        {
            jsynchronized(this);
            return AbstractStringBuilder::codePointBefore(index);
        }

        jint StringBuffer::codePointCount(jint beginIndex, jint endIndex)
        {
            jsynchronized(this);
            return AbstractStringBuilder::codePointCount(beginIndex, endIndex);
        }

        jint StringBuffer::offsetByCodePoints(jint index, jint codePointOffset)
        {
            jsynchronized(this);
            return AbstractStringBuilder::offsetByCodePoints(index, codePointOffset);
        }

        void StringBuffer::getChars(jint srcBegin, jint srcEnd, jarray<jchar> dst, jint dstBegin)
        {
            jsynchronized(this);
            AbstractStringBuilder::getChars(srcBegin, srcEnd, dst, dstBegin);
        }

        void StringBuffer::setCharAt(jint index, jchar ch)
        {
            jsynchronized(this);
            if((index < 0) || (index >= count))
                throw StringIndexOutOfBoundsException(index);
            toStringCache = nullptr;
            value[index] = ch;
        }

        JAbstractStringBuilder StringBuffer::append(JObject obj)
        {
            jsynchronized(this);
            toStringCache = nullptr;
            AbstractStringBuilder::append(String::valueOf(obj));
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuffer::append(JString str)
        {
            jsynchronized(this);
            toStringCache = nullptr;
            AbstractStringBuilder::append(str);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuffer::append(JStringBuffer sb)
        {
            jsynchronized(this);
            toStringCache = nullptr;
            AbstractStringBuilder::append(sb);
            return shared_from_this();
        }
        
        JAbstractStringBuilder StringBuffer::append(JStringBuilder sb)
        {
            jsynchronized(this);
            toStringCache = nullptr;
            AbstractStringBuilder::append(sb);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuffer::append(JAbstractStringBuilder asb)
        {
            jsynchronized(this);
            toStringCache = nullptr;
            AbstractStringBuilder::append(asb);
            return shared_from_this();
        }

        JAppendable StringBuffer::append(JCharSequence s)
        {
            jsynchronized(this);
            toStringCache = nullptr;
            AbstractStringBuilder::append(s);
            return shared_from_this();
        }

        JAppendable StringBuffer::append(JCharSequence s, jint start, jint end)
        {
            jsynchronized(this);
            toStringCache = nullptr;
            AbstractStringBuilder::append(s, start, end);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuffer::append(jarray<jchar> str)
        {
            jsynchronized(this);
            toStringCache = nullptr;
            AbstractStringBuilder::append(str);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuffer::append(jarray<jchar> str, jint offset, jint len)
        {
            jsynchronized(this);
            toStringCache = nullptr;
            AbstractStringBuilder::append(str, offset, len);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuffer::append(jboolean b)
        {
            jsynchronized(this);
            toStringCache = nullptr;
            AbstractStringBuilder::append(b);
            return shared_from_this();
        }

        JAppendable StringBuffer::append(jchar c)
        {
            jsynchronized(this);
            toStringCache = nullptr;
            AbstractStringBuilder::append(c);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuffer::append(jint i)
        {
            jsynchronized(this);
            toStringCache = nullptr;
            AbstractStringBuilder::append(i);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuffer::appendCodePoint(jint codePoint)
        {
            jsynchronized(this);
            toStringCache = nullptr;
            AbstractStringBuilder::appendCodePoint(codePoint);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuffer::append(jlong lng)
        {
            jsynchronized(this);
            toStringCache = nullptr;
            AbstractStringBuilder::append(lng);
            return shared_from_this();
        }
        
        JAbstractStringBuilder StringBuffer::append(jfloat f)
        {
            jsynchronized(this);
            toStringCache = nullptr;
            AbstractStringBuilder::append(f);
            return shared_from_this();
        }
        
        JAbstractStringBuilder StringBuffer::append(jdouble d)
        {
            jsynchronized(this);
            toStringCache = nullptr;
            AbstractStringBuilder::append(d);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuffer::delete_(jint start, jint end)
        {
            jsynchronized(this);
            toStringCache = nullptr;
            AbstractStringBuilder::delete_(start, end);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuffer::deleteCharAt(jint index)
        {
            jsynchronized(this);
            toStringCache = nullptr;
            AbstractStringBuilder::deleteCharAt(index);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuffer::replace(jint start, jint end, JString str)
        {
            jsynchronized(this);
            toStringCache = nullptr;
            AbstractStringBuilder::replace(start, end, str);
            return shared_from_this();
        }

        JString StringBuffer::substring(jint start)
        {
            jsynchronized(this);
            return substring(start, count);
        }

        JCharSequence StringBuffer::subSequence(jint start, jint end)
        {
            jsynchronized(this);
            return AbstractStringBuilder::substring(start, end);
        }

        JString StringBuffer::substring(jint start, jint end)
        {
            jsynchronized(this);
            return AbstractStringBuilder::substring(start, end);
        }

        JAbstractStringBuilder StringBuffer::insert(jint index, jarray<jchar> str, jint offset, jint len)
        {
            jsynchronized(this);
            toStringCache = nullptr;
            AbstractStringBuilder::insert(index, str, offset, len);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuffer::insert(jint offset, JObject obj)
        {
            jsynchronized(this);
            toStringCache = nullptr;
            AbstractStringBuilder::insert(offset, String::valueOf(obj));
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuffer::insert(jint offset, JString str)
        {
            jsynchronized(this);
            toStringCache = nullptr;
            AbstractStringBuilder::insert(offset, str);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuffer::insert(jint offset, jarray<jchar> str)
        {
            jsynchronized(this);
            toStringCache = nullptr;
            AbstractStringBuilder::insert(offset, str);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuffer::insert(jint dstOffset, JCharSequence s)
        {
            AbstractStringBuilder::insert(dstOffset, s);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuffer::insert(jint dstOffset, JCharSequence s, jint start, jint end)
        {
            jsynchronized(this);
            toStringCache = nullptr;
            AbstractStringBuilder::insert(dstOffset, s, start, end);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuffer::insert(jint offset, jboolean b)
        {
            AbstractStringBuilder::insert(offset, b);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuffer::insert(jint offset, jchar c)
        {
            jsynchronized(this);
            AbstractStringBuilder::insert(offset, c);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuffer::insert(jint offset, jint i)
        {
            AbstractStringBuilder::insert(offset, i);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuffer::insert(jint offset, jlong l)
        {
            AbstractStringBuilder::insert(offset, l);
            return shared_from_this();
        }
        
        JAbstractStringBuilder StringBuffer::insert(jint offset, jfloat f)
        {
            AbstractStringBuilder::insert(offset, f);
            return shared_from_this();
        }
        
        JAbstractStringBuilder StringBuffer::insert(jint offset, jdouble d)
        {
            AbstractStringBuilder::insert(offset, d);
            return shared_from_this();
        }

        jint StringBuffer::indexOf(JString str)
        {
            return AbstractStringBuilder::indexOf(str);
        }

        jint StringBuffer::indexOf(JString str, jint fromIndex)
        {
            jsynchronized(this);
            return AbstractStringBuilder::indexOf(str, fromIndex);
        }

        jint StringBuffer::lastIndexOf(JString str)
        {
            return lastIndexOf(str, count);
        }

        jint StringBuffer::lastIndexOf(JString str, jint fromIndex)
        {
            jsynchronized(this);
            return AbstractStringBuilder::lastIndexOf(str, fromIndex);
        }

        JAbstractStringBuilder StringBuffer::reverse()
        {
            jsynchronized(this);
            toStringCache = nullptr;
            AbstractStringBuilder::reverse();
            return shared_from_this();
        }

        JString StringBuffer::toString()
        {
            if(toStringCache == nullptr) {
                toStringCache = Arrays::copyOfRange(value, 0, count);
            }
            return jnew<String>(toStringCache, true);
        }
    }
}
