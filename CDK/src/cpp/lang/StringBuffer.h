#ifndef CPP_LANG_STRINGBUFFER_H
#define CPP_LANG_STRINGBUFFER_H

#include "AbstractStringBuilder.h"
#include <mutex>

namespace cpp
{
    namespace lang
    {
        class StringBuffer : public AbstractStringBuilder
        {
        public:
            StringBuffer();
            StringBuffer(jint capacity);
            StringBuffer(JString str);
            StringBuffer(JCharSequence seq);
            ~StringBuffer();
            
            jint length();
            jint capacity();
            void ensureCapacity(jint minimumCapacity);
            void trimToSize();
            void setLength(jint newLength);
            jchar charAt(jint index);
            jint codePointAt(jint index);
            jint codePointBefore(jint index);
            jint codePointCount(jint beginIndex, jint endIndex);
            jint offsetByCodePoints(jint index, jint codePointOffset);
            void getChars(jint srcBegin, jint srcEnd, jarray<jchar> dst, jint dstBegin);
            void setCharAt(jint index, jchar ch);
            JAbstractStringBuilder append(JObject obj);
            JAbstractStringBuilder append(JString str);
            JAbstractStringBuilder append(JStringBuffer sb);
            JAbstractStringBuilder append(JStringBuilder sb);
            JAbstractStringBuilder append(JAbstractStringBuilder asb);
            JAppendable append(JCharSequence s);
            JAppendable append(JCharSequence s, jint start, jint end);
            JAbstractStringBuilder append(jarray<jchar> str);
            JAbstractStringBuilder append(jarray<jchar> str, jint offset, jint len);
            JAbstractStringBuilder append(jboolean b);
            JAppendable append(jchar c);
            JAbstractStringBuilder append(jint i);
            JAbstractStringBuilder appendCodePoint(jint codePoint);
            JAbstractStringBuilder append(jlong lng);
            JAbstractStringBuilder append(jfloat f);
            JAbstractStringBuilder append(jdouble d);
            JAbstractStringBuilder delete_(jint start, jint end);
            JAbstractStringBuilder deleteCharAt(jint index);
            JAbstractStringBuilder replace(jint start, jint end, JString str);
            JString substring(jint start);
            JCharSequence subSequence(jint start, jint end);
            JString substring(jint start, jint end);
            JAbstractStringBuilder insert(jint index, jarray<jchar> str, jint offset, jint len);
            JAbstractStringBuilder insert(jint offset, JObject obj);
            JAbstractStringBuilder insert(jint offset, JString str);
            JAbstractStringBuilder insert(jint offset, jarray<jchar> str);
            JAbstractStringBuilder insert(jint dstOffset, JCharSequence s);
            JAbstractStringBuilder insert(jint dstOffset, JCharSequence s, jint start, jint end);
            JAbstractStringBuilder insert(jint offset, jboolean b);
            JAbstractStringBuilder insert(jint offset, jchar c);
            JAbstractStringBuilder insert(jint offset, jint i);
            JAbstractStringBuilder insert(jint offset, jlong l);
            JAbstractStringBuilder insert(jint offset, jfloat f);
            JAbstractStringBuilder insert(jint offset, jdouble d);
            jint indexOf(JString str);
            jint indexOf(JString str, jint fromIndex);
            jint lastIndexOf(JString str);
            jint lastIndexOf(JString str, jint fromIndex);
            JAbstractStringBuilder reverse();
            JString toString();
            
        private:
            jarray<jchar> toStringCache;
        };
    }
}

#endif // CPP_LANG_STRINGBUFFER_H
