#include "StringBuilder.h"
#include "StringBuffer.h"
#include "String.h"

namespace cpp
{
    namespace lang
    {
        StringBuilder::StringBuilder() : AbstractStringBuilder(16)
        {
        }

        StringBuilder::StringBuilder(jint capacity) : AbstractStringBuilder(capacity)
        {
        }

        StringBuilder::StringBuilder(JString str) : AbstractStringBuilder(str->length() + 16)
        {
            append(str);
        }

        StringBuilder::StringBuilder(JCharSequence seq) : StringBuilder(seq->length() + 16)
        {
            append(seq);
        }

        StringBuilder::~StringBuilder()
        {
        }

        JAbstractStringBuilder StringBuilder::append(JObject obj)
        {
            AbstractStringBuilder::append(String::valueOf(obj));
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuilder::append(JString str)
        {
            AbstractStringBuilder::append(str);
            return shared_from_this();
        }
        
        JAbstractStringBuilder StringBuilder::append(JStringBuffer sb)
        {
            AbstractStringBuilder::append(sb);
            return shared_from_this();
        }

        JAppendable StringBuilder::append(JCharSequence s)
        {
            AbstractStringBuilder::append(s);
            return shared_from_this();
        }

        JAppendable StringBuilder::append(JCharSequence s, jint start, jint end)
        {
            AbstractStringBuilder::append(s, start, end);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuilder::append(jarray<jchar> str)
        {
            AbstractStringBuilder::append(str);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuilder::append(jarray<jchar> str, jint offset, jint len)
        {
            AbstractStringBuilder::append(str, offset, len);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuilder::append(jboolean b)
        {
            AbstractStringBuilder::append(b);
            return shared_from_this();
        }

        JAppendable StringBuilder::append(jchar c)
        {
            AbstractStringBuilder::append(c);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuilder::append(jint i)
        {
            AbstractStringBuilder::append(i);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuilder::append(jlong lng)
        {
            AbstractStringBuilder::append(lng);
            return shared_from_this();
        }
        
        JAbstractStringBuilder StringBuilder::append(jfloat f)
        {
            AbstractStringBuilder::append(f);
            return shared_from_this();
        }
        
        JAbstractStringBuilder StringBuilder::append(jdouble d)
        {
            AbstractStringBuilder::append(d);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuilder::appendCodePoint(jint codePoint)
        {
            AbstractStringBuilder::append(codePoint);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuilder::delete_(jint start, jint end)
        {
            AbstractStringBuilder::delete_(start, end);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuilder::deleteCharAt(jint index)
        {
            AbstractStringBuilder::deleteCharAt(index);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuilder::replace(jint start, jint end, JString str)
        {
            AbstractStringBuilder::replace(start, end, str);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuilder::insert(jint index, jarray<jchar> str, jint offset, jint len)
        {
            AbstractStringBuilder::insert(index, str, offset, len);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuilder::insert(jint offset, JObject obj)
        {
            AbstractStringBuilder::insert(offset, obj);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuilder::insert(jint offset, JString str)
        {
            AbstractStringBuilder::insert(offset, str);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuilder::insert(jint offset, jarray<jchar> str)
        {
            AbstractStringBuilder::insert(offset, str);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuilder::insert(jint dstOffset, JCharSequence s)
        {
            AbstractStringBuilder::insert(dstOffset, s);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuilder::insert(jint dstOffset, JCharSequence s, jint start, jint end)
        {
            AbstractStringBuilder::insert(dstOffset, s, start, end);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuilder::insert(jint offset, jboolean b)
        {
            AbstractStringBuilder::insert(offset, b);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuilder::insert(jint offset, jchar c)
        {
            AbstractStringBuilder::insert(offset, c);
            return shared_from_this();
        }

        JAbstractStringBuilder StringBuilder::insert(jint offset, jint i)
        {
            AbstractStringBuilder::insert(offset, i);
            return shared_from_this();
        }
        
        JAbstractStringBuilder StringBuilder::insert(jint offset, jlong l)
        {
            AbstractStringBuilder::insert(offset, l);
            return shared_from_this();
        }
        
        JAbstractStringBuilder StringBuilder::insert(jint offset, jfloat f)
        {
            AbstractStringBuilder::insert(offset, f);
            return shared_from_this();
        }
        
        JAbstractStringBuilder StringBuilder::insert(jint offset, jdouble d)
        {
            AbstractStringBuilder::insert(offset, d);
            return shared_from_this();
        }

        jint StringBuilder::indexOf(JString str)
        {
            return AbstractStringBuilder::indexOf(str);
        }

        jint StringBuilder::indexOf(JString str, jint fromIndex)
        {
            return AbstractStringBuilder::indexOf(str, fromIndex);
        }

        jint StringBuilder::lastIndexOf(JString str)
        {
            return AbstractStringBuilder::lastIndexOf(str);
        }
        
        jint StringBuilder::lastIndexOf(JString str, jint fromIndex)
        {
            return AbstractStringBuilder::lastIndexOf(str, fromIndex);
        }
        
        JAbstractStringBuilder StringBuilder::reverse()
        {
            AbstractStringBuilder::reverse();
            return shared_from_this();
        }
        
        JString StringBuilder::toString()
        {
            return jnew<String>(value, 0, count);
        }
    }
}
