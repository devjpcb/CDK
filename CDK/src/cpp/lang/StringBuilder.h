#ifndef CPP_LANG_STRINGBUILDER_H
#define CPP_LANG_STRINGBUILDER_H

#include "AbstractStringBuilder.h"

namespace cpp
{
    namespace lang
    {
        class StringBuilder : public AbstractStringBuilder
        {
        public:
            StringBuilder();
            StringBuilder(jint capacity);
            StringBuilder(JString str);
            StringBuilder(JCharSequence seq);
            ~StringBuilder();

            JAbstractStringBuilder append(JObject obj);
            JAbstractStringBuilder append(JString str);
            JAbstractStringBuilder append(JStringBuffer sb);
            JAppendable append(JCharSequence s);
            JAppendable append(JCharSequence s, jint start, jint end);
            JAbstractStringBuilder append(jarray<jchar> str);
            JAbstractStringBuilder append(jarray<jchar> str, jint offset, jint len);
            JAbstractStringBuilder append(jboolean b);
            JAppendable append(jchar c);
            JAbstractStringBuilder append(jint i);
            JAbstractStringBuilder append(jlong lng);
            JAbstractStringBuilder append(jfloat f);
            JAbstractStringBuilder append(jdouble d);
            JAbstractStringBuilder appendCodePoint(jint codePoint);
            JAbstractStringBuilder delete_(jint start, jint end);
            JAbstractStringBuilder deleteCharAt(jint index);
            JAbstractStringBuilder replace(jint start, jint end, JString str);
            JAbstractStringBuilder insert(jint index, jarray<jchar> str, jint offset, jint len);
            JAbstractStringBuilder insert(jint offset, JObject obj);
            JAbstractStringBuilder insert(jint offset, JString str);
            JAbstractStringBuilder insert(jint offset, jarray<jchar> str);
            JAbstractStringBuilder insert(jint dstOffset, JCharSequence s);
            JAbstractStringBuilder insert(jint dstOffset, JCharSequence s, jint start, jint end);
            JAbstractStringBuilder insert(jint offset, jboolean b);
            JAbstractStringBuilder insert(jint offset, jchar c);
            JAbstractStringBuilder insert(jint offset, jint i);
            JAbstractStringBuilder insert(jint offset, jlong l);
            JAbstractStringBuilder insert(jint offset, jfloat f);
            JAbstractStringBuilder insert(jint offset, jdouble d);
            jint indexOf(JString str);
            jint indexOf(JString str, jint fromIndex);
            jint lastIndexOf(JString str);
            jint lastIndexOf(JString str, jint fromIndex);
            JAbstractStringBuilder reverse();
            JString toString();
        };
    }
}

#endif // CPP_LANG_STRINGBUILDER_H
