#include "StringIndexOutOfBoundsException.h"

namespace cpp
{
    namespace lang
    {
        StringIndexOutOfBoundsException::StringIndexOutOfBoundsException() : IndexOutOfBoundsException()
        {
        }

        StringIndexOutOfBoundsException::StringIndexOutOfBoundsException(jint index) : IndexOutOfBoundsException(jstr(u"String index out of range: ") + boxing(index))
        {
        }
        
        StringIndexOutOfBoundsException::StringIndexOutOfBoundsException(JString s) : IndexOutOfBoundsException(s)
        {
        }

        StringIndexOutOfBoundsException::~StringIndexOutOfBoundsException()
        {
        }
    }
}
