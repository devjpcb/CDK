#ifndef CPP_LANG_STRINGINDEXOUTOFBOUNDSEXCEPTION_H
#define CPP_LANG_STRINGINDEXOUTOFBOUNDSEXCEPTION_H

#include "IndexOutOfBoundsException.h"

namespace cpp
{
    namespace lang
    {
        class StringIndexOutOfBoundsException : public IndexOutOfBoundsException
        {
        public:
            StringIndexOutOfBoundsException();
            StringIndexOutOfBoundsException(jint index);
            StringIndexOutOfBoundsException(JString s);
            ~StringIndexOutOfBoundsException();
        };
    }
}

#endif // CPP_LANG_STRINGINDEXOUTOFBOUNDSEXCEPTION_H
