#include "System.h"
#include "SecurityManager.h"
#include "RuntimePermission.h"
#include "Class.h"
#include "../security/PrivilegedAction.h"
#include "../security/ProtectionDomain.h"
#include "../security/AllPermission.h"
#include "../security/AccessController.h"
#include "../../sun/misc/SharedSecrets.h"
#include "../../sun/security/util/SecurityConstants.h"
#include "../util/Properties.h"
#include <chrono>
#include <limits>

using namespace std::chrono;
using namespace cpp::io;
using namespace cpp::security;
using namespace cpp::util;
using namespace sun::security::util;

namespace cpp
{
    namespace lang
    {
        void System::setIn(JInputStream in)
        {
            checkIO();
            setIn0(in);
        }

        void System::setOut(JPrintStream out)
        {
            checkIO();
            setOut0(out);
        }

        void System::setErr(JPrintStream err)
        {
            checkIO();
            setErr0(err);
        }

        JConsole System::console()
        {
            if (cons == nullptr) {
                static std::mutex staticLock;
                jsynchronized_not_recursive(staticLock);
                cons = sun::misc::SharedSecrets::getJavaIOAccess()->console();
            }

            return cons;
        }

        void System::checkIO()
        {
            JSecurityManager sm = getSecurityManager();
            if (sm != nullptr) {
                sm->checkPermission(jnew<RuntimePermission>(jstr(u"setIO")));
            }
        }

        void System::setIn0(JInputStream in)
        {

        }

        void System::setOut0(JPrintStream out)
        {

        }
        void System::setErr0(JPrintStream err)
        {

        }
        
        void System::setSecurityManager(JSecurityManager s)
        {
            try {
                s->checkPackageAccess(jstr(u"cpp::lang"));
            } catch (Exception e) {
                // no-op
            }
            setSecurityManager0(s);
        }
        
        JSecurityManager System::getSecurityManager()
        {
            return security;
        }
        
        void System::setSecurityManager0(JSecurityManager s)
        {
            JSecurityManager sm = getSecurityManager();
            if (sm != nullptr) {
                sm->checkPermission(jnew<RuntimePermission>(jstr(u"setSecurityManager")));
            }

            if ((s != nullptr) && (s->getClass()->getClassLoader() != nullptr)) {
                class AnonymousPrivilegedAction : public PrivilegedAction<JObject> 
                {
                public:
                    AnonymousPrivilegedAction(JSecurityManager s) : s(s) { }
                
                    JObject run() 
                    {
                        s->getClass()->getProtectionDomain()->implies(SecurityConstants::ALL_PERMISSION);
                        return nullptr;
                    }
                    
                private:
                    JSecurityManager s;
                };
                
                JPrivilegedAction<JObject> ipa = jnew<AnonymousPrivilegedAction>(s);
                AccessController::doPrivileged(ipa);
            }

            security = s;
        }

        jlong System::currentTimeMillis()
        {
            milliseconds ms = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
            return ms.count();
        }

        jlong System::nanoTime()
        {
            nanoseconds ms = duration_cast<nanoseconds>(system_clock::now().time_since_epoch());
            return ms.count();
        }
        
        jint System::identityHashCode(JObject x)
        {
            jint hashCode = reinterpret_cast<jlong>(x.get());
            return hashCode;
        }
        
        JProperties System::initProperties(JProperties props)
        {
            return nullptr;
        }
        
        JProperties System::getProperties()
        {
            JSecurityManager sm = getSecurityManager();
            if (sm != nullptr) {
                sm->checkPropertiesAccess();
            }

            return props;
        }
        
        JString System::lineSeparator()
        {
            return _lineSeparator;
        }
        
        void System::setProperties(JProperties props)
        {
            JSecurityManager sm = getSecurityManager();
            
            if (sm != nullptr) {
                sm->checkPropertiesAccess();
            }
            
            if (props == nullptr) {
                props = jnew<Properties>();
                initProperties(props);
            }
            
            System::props = props;
        }
        
        JString System::getProperty(JString key)
        {
            checkKey(key);
            JSecurityManager sm = getSecurityManager();
            if (sm != nullptr) {
                sm->checkPropertyAccess(key);
            }

            return props->getProperty(key);
        }
        
        JString System::getProperty(JString key, JString def)
        {
            checkKey(key);
            JSecurityManager sm = getSecurityManager();
            if (sm != nullptr) {
                sm->checkPropertyAccess(key);
            }

            return props->getProperty(key, def);
        }
        
        JString System::setProperty(JString key, JString value)
        {
            checkKey(key);
            JSecurityManager sm = getSecurityManager();
            if (sm != nullptr) {
                //TODO: sm->checkPermission(jnew<PropertyPermission>(key, SecurityConstants::PROPERTY_WRITE_ACTION));
            }

            return std::dynamic_pointer_cast<String>(props->setProperty(key, value));
        }
        
        void System::checkKey(JString key)
        {
            if (key == nullptr) {
                throw NullPointerException(jstr(u"key can't be null"));
            }
            if (key->equals(jstr(u""))) {
                throw IllegalArgumentException(jstr(u"key can't be empty"));
            }
        }
        
        JSecurityManager System::security;
        cpp::io::JConsole System::cons;
        cpp::util::JProperties System::props;
        JString System::_lineSeparator;
    }
}
