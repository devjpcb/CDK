#ifndef CPP_LANG_SYSTEM_H
#define CPP_LANG_SYSTEM_H

#include "Object.h"
#include "NullPointerException.h"
#include "ArrayIndexOutOfBoundsException.h"

namespace cpp
{
    namespace lang
    {
        class System : public Object
        {
        public:
            System() = delete;
            
            static void setIn(cpp::io::JInputStream in);
            static void setOut(cpp::io::JPrintStream out);
            static void setErr(cpp::io::JPrintStream err);
            static cpp::io::JConsole console();
            static void checkIO();
            static void setIn0(cpp::io::JInputStream in);
            static void setOut0(cpp::io::JPrintStream out);
            static void setErr0(cpp::io::JPrintStream err);
            static void setSecurityManager(JSecurityManager s);
            static void setSecurityManager0(JSecurityManager s);
            static jlong currentTimeMillis();
            static JSecurityManager getSecurityManager();
            static jlong nanoTime();
            
            template <typename T>
            static void arraycopy(jarray<T> src, jint srcPos, jarray<T> dest, jint destPos, jint length) {
                if(src == nullptr || dest == nullptr) {
                    throw NullPointerException();
                }

                if(srcPos < 0 || destPos < 0 || length < 0) {
                    throw ArrayIndexOutOfBoundsException();
                }

                if(((length + srcPos) > src.length()) || ((length + destPos) > dest.length())) {
                    throw ArrayIndexOutOfBoundsException();
                }

                if(length == 0) return;

                T* srcPtr = src.begin() + srcPos;
                T* destPtr = dest.begin() + destPos;
                T* endPtr = srcPtr + length;

                while(srcPtr != endPtr) {
                    *destPtr = *srcPtr;
                    srcPtr++;
                    destPtr++;
                }
            }
            
            static jint identityHashCode(JObject x);
            static cpp::util::JProperties getProperties();
            static JString lineSeparator();
            static void setProperties(cpp::util::JProperties props);
            static JString getProperty(JString key);
            static JString getProperty(JString key, JString def);
            static JString setProperty(JString key, JString value);
            
            static void checkKey(JString key);
            
            static cpp::io::JInputStream in;
            static cpp::io::JPrintStream out;
            static cpp::io::JPrintStream err;
            
        private:
            static cpp::util::JProperties initProperties(cpp::util::JProperties props);
        
            static JSecurityManager security;
            static cpp::io::JConsole cons;
            static cpp::util::JProperties props;
            static JString _lineSeparator;
        };
    }
}

#endif // CPP_LANG_SYSTEM_H
