#include "Thread.h"
#include "ThreadGroup.h"
#include "ThreadDeath.h"
#include "System.h"
#include "Runnable.h"
#include "SecurityManager.h"
#include "CloneNotSupportedException.h"
#include "NoSuchMethodError.h"
#include "IllegalArgumentException.h"
#include "IllegalThreadStateException.h"
#include "UnsupportedOperationException.h"
#include "../../sun/misc/VM.h"
#include "../../sun/nio/ch/Interruptible.h"

using namespace cpp::security;
using namespace sun::misc;
using namespace sun::nio::ch;

namespace cpp
{
    namespace lang
    {
        Thread::Thread() : threadStatus(0)
        {
            init(nullptr, nullptr, jstr(u"Thread-") + boxing(nextThreadNum()), 0);
        }
        
        Thread::Thread(JRunnable target) : threadStatus(0)
        {
            init(nullptr, target, jstr(u"Thread-") + boxing(nextThreadNum()), 0);
        }
        
        Thread::Thread(JRunnable target, JAccessControlContext acc) : threadStatus(0)
        {
            init(nullptr, target, jstr(u"Thread-") + boxing(nextThreadNum()), 0, acc);
        }
        
        Thread::Thread(JThreadGroup group, JRunnable target) : threadStatus(0)
        {
            init(group, target, jstr(u"Thread-") + boxing(nextThreadNum()), 0);
        }
        
        Thread::Thread(JString name) : threadStatus(0)
        {
            init(nullptr, nullptr, name, 0);
        }
        
        Thread::Thread(JThreadGroup group, JString name) : threadStatus(0)
        {
            init(group, nullptr, name, 0);
        }
        
        Thread::Thread(JRunnable target, JString name) : threadStatus(0)
        {
            init(nullptr, target, name, 0);
        }
        
        Thread::Thread(JThreadGroup group, JRunnable target, JString name) : threadStatus(0)
        {
            init(group, target, name, 0);
        }
        
        Thread::Thread(JThreadGroup group, JRunnable target, JString name, jlong stackSize) : threadStatus(0)
        {
            init(group, target, name, stackSize);
        }
        
        Thread::Thread(JThreadGroup group, std::nullptr_t null)
        {
            this->group = group;
            name = jstr(u"main");
            threadStatus = VM::JVMTI_THREAD_STATE_RUNNABLE;
            priority = NORM_PRIORITY;
            daemon = false;
            stackSize = 0;
            tid = nextThreadID();
        }

        Thread::~Thread()
        {
        }
        
        jint Thread::activeCount()
        {
            return currentThread()->getThreadGroup()->activeCount();
        }
        
        void Thread::checkAccess()
        {
            JSecurityManager security = System::getSecurityManager();
            if (security != nullptr) {
                security->checkAccess(shared_from_this());
            }
        }
        
        JObject Thread::clone()
        {
            throw CloneNotSupportedException();
        }
        
        jint Thread::countStackFrames()
        {
            //TODO: hay que investigar
            return -1;
        }
        
        JThread Thread::currentThread()
        {
            return getInternalThread();
        }
        
        void Thread::destroy()
        {
            throw NoSuchMethodError();
        }
        
        void Thread::dumpStack()
        {
            //TODO: new Exception("Stack trace").printStackTrace();
        }
        
        jint Thread::enumerate(jarray<JThread> tarray)
        {
            return currentThread()->getThreadGroup()->enumerate(tarray);
        }
        
        Thread::JUncaughtExceptionHandler Thread::getDefaultUncaughtExceptionHandler()
        {
            return nullptr;
        }
        
        jlong Thread::getId()
        {
            return tid;
        }
        
        JString Thread::getName()
        {
            return name;
        }
        
        jint Thread::getPriority()
        {
            return priority;
        }
        
        Thread::State Thread::getState()
        {
            return VM::toThreadState(threadStatus);
        }
        
        JThreadGroup Thread::getThreadGroup()
        {
            return group;
        }
        
        void Thread::interrupt()
        {
            if (shared_from_this() != Thread::currentThread())
                checkAccess();

            {
                jsynchronized(this);
                
                JInterruptible b = blocker;
                if (b != nullptr) {
                    interrupt0();
                    b->interrupt(shared_from_this());
                    return;
                }
            }
            
            interrupt0();
        }
        
        jboolean Thread::interrupted()
        {
            return currentThread()->isInterrupted(true);
        }
        
        jboolean Thread::isAlive()
        {
            //TODO: hay que investigar
            return alive;
        }
        
        jboolean Thread::isDaemon()
        {
            return daemon;
        }
        
        jboolean Thread::isInterrupted()
        {
            return isInterrupted(false);
        }
        
        void Thread::join() 
        {
            join(0);
        }
        
        void Thread::join(jlong millis)
        {
            jlong base = System::currentTimeMillis();
            jlong now = 0;

            if (millis < 0) {
                throw IllegalArgumentException(jstr(u"timeout value is negative"));
            }

            if (millis == 0) {
                while (isAlive()) {
                    wait(0);
                }
            } else {
                while (isAlive()) {
                    jlong delay = millis - now;
                    if (delay <= 0) {
                        break;
                    }
                    wait(delay);
                    now = System::currentTimeMillis() - base;
                }
            }
        }
        
        void Thread::join(jlong millis, jint nanos)
        {
            if (millis < 0) {
                throw IllegalArgumentException(jstr(u"timeout value is negative"));
            }

            if (nanos < 0 || nanos > 999999) {
                throw IllegalArgumentException(jstr(u"nanosecond timeout value out of range"));
            }

            if (nanos >= 500000 || (nanos != 0 && millis == 0)) {
                millis++;
            }

            join(millis);
        }
        
        void Thread::resume()
        {
            checkAccess();
            resume0();
        }
        
        void Thread::run()
        {
            if (target != nullptr) {
                target->run();
            }
        }
        
        void Thread::setDaemon(jboolean on)
        {
            checkAccess();
            if (isAlive()) {
                throw IllegalThreadStateException();
            }
            daemon = on;
        }
        
        void Thread::setName(JString name)
        {
            jsynchronized(this);
            checkAccess();
            if (name == nullptr) {
                throw NullPointerException(jstr(u"name cannot be null"));
            }

            this->name = name;
            if (threadStatus != 0) {
                setNativeName(name);
            }
        }
        
        void Thread::setPriority(jint newPriority)
        {
            JThreadGroup g;
            checkAccess();
            if (newPriority > MAX_PRIORITY || newPriority < MIN_PRIORITY) {
                throw IllegalArgumentException();
            }
            if((g = getThreadGroup()) != nullptr) {
                if (newPriority > g->getMaxPriority()) {
                    newPriority = g->getMaxPriority();
                }
                setPriority0(priority = newPriority);
            }
        }
        
        void Thread::sleep(jlong millis)
        {
            //TODO: hay que investigar
            std::this_thread::sleep_for(std::chrono::milliseconds(millis));
        }
        
        void Thread::sleep(jlong millis, jint nanos)
        {
            if (millis < 0) {
                throw IllegalArgumentException(jstr(u"timeout value is negative"));
            }

            if (nanos < 0 || nanos > 999999) {
                throw IllegalArgumentException(jstr(u"nanosecond timeout value out of range"));
            }

            if (nanos >= 500000 || (nanos != 0 && millis == 0)) {
                millis++;
            }

            sleep(millis);
        }
        
        void Thread::start()
        {
            if (threadStatus != 0)
                throw IllegalThreadStateException();

            group->add(shared_from_this());

            jboolean started = false;
            try {
                start0();
                started = true;
            } catch(...) {}
            
            try {
                if (!started) {
                    group->threadStartFailed(shared_from_this());
                }
            } catch (Throwable& ignore) {}
        }
            
        void Thread::stop()
        {
            JSecurityManager security = System::getSecurityManager();
            if (security != nullptr) {
                checkAccess();
                if (shared_from_this() != Thread::currentThread()) {
                    //TODO: security->checkPermission(SecurityConstants::STOP_THREAD_PERMISSION);
                }
            }
            
            if (threadStatus != 0) {
                resume();
            }

            stop0(jnew<ThreadDeath>());
        }
        
        void Thread::stop(Throwable& obj)
        {
            throw UnsupportedOperationException();
        }
        
        void Thread::suspend()
        {
            checkAccess();
            suspend0();
        }
        
        void Thread::yield()
        {
            //TODO: hay que investigar
            std::this_thread::yield();
        }
        
        void Thread::interrupt0()
        {
            //TODO: hay que investigar
        }
        
        jboolean Thread::isInterrupted(jboolean clearInterrupted)
        {
            //TODO: hay que investigar
            return nullptr;
        }
        
        void Thread::resume0()
        {
            //TODO: hay que investigar
        }
        
        void Thread::setNativeName(JString name)
        {
            //TODO: hay que investigar
        }
        
        void Thread::setPriority0(jint newPriority)
        {
            //TODO: hay que investigar
        }
        
        void Thread::start0()
        {
            //TODO: hay que investigar
            nativeThread = std::move(std::thread([&](){
                addInternalThread(shared_from_this());
                this->alive = true;
                this->threadStatus = VM::JVMTI_THREAD_STATE_RUNNABLE;
                this->run();
                this->alive = false;
                removeInternalThread();
            }));
        }
        
        void Thread::stop0(JObject o)
        {
            //TODO: hay que investigar
        }
        
        void Thread::suspend0()
        {
            //TODO: hay que investigar
        }
        
        void Thread::init(JThreadGroup g, JRunnable target, JString name, jlong stackSize)
        {
            init(g, target, name, stackSize, nullptr);
        }
        
        void Thread::init(JThreadGroup g, JRunnable target, JString name, jlong stackSize, JAccessControlContext acc)
        {
            if (name == nullptr) {
                throw NullPointerException(jstr(u"name cannot be null"));
            }

            this->name = name;

            JThread parent = currentThread();
            JSecurityManager security = System::getSecurityManager();
            if (g == nullptr) {
                if (security != nullptr) {
                    g = security->getThreadGroup();
                }

                if (g == nullptr) {
                    g = parent->getThreadGroup();
                }
            }
            
            g->checkAccess();

            if (security != nullptr) {
                if (isCCLOverridden(getClass())) {
                    //TODO: security->checkPermission(SUBCLASS_IMPLEMENTATION_PERMISSION);
                }
            }

            g->addUnstarted();

            this->group = g;
            this->daemon = parent->isDaemon();
            this->priority = parent->getPriority();
            /*TODO:
            if (security == nullptr || isCCLOverridden(parent->getClass()))
                this->contextClassLoader = parent->getContextClassLoader();
            else
                this->contextClassLoader = parent->contextClassLoader;
            this->inheritedAccessControlContext = acc != nullptr ? acc : AccessController->getContext();
            */
            this->target = target;
            setPriority(priority);
            /* TODO:
            if (parent->inheritableThreadLocals != nullptr)
                this->inheritableThreadLocals = ThreadLocal::createInheritedMap(parent->inheritableThreadLocals);
            */    
            this->stackSize = stackSize;

            tid = nextThreadID();
        }
        
        jint Thread::nextThreadNum()
        {
            static std::mutex staticLock;
            jsynchronized_not_recursive(staticLock);
            return threadInitNumber++;
        }
        
        jlong Thread::nextThreadID()
        {
            static std::mutex staticLock;
            jsynchronized_not_recursive(staticLock);
            return ++threadSeqNumber;
        }
        
        jboolean Thread::isCCLOverridden(JClass cl)
        {
            return nullptr;
        }
        
        void Thread::addInternalThread(JThread internalThread)
        {
            jsynchronized_not_recursive(internalThreadsLock);
            internalThreads[std::this_thread::get_id()] = internalThread;
        }
        
        void Thread::removeInternalThread() 
        {
            jsynchronized_not_recursive(internalThreadsLock);
            internalThreads.erase(std::this_thread::get_id());
        }
        
        JThread Thread::getInternalThread()
        {
            jsynchronized_not_recursive(internalThreadsLock);
            return internalThreads[std::this_thread::get_id()];
        }
        
        const jint Thread::MIN_PRIORITY = 1;
        const jint Thread::NORM_PRIORITY = 5;
        const jint Thread::MAX_PRIORITY = 10;
        
        jint Thread::threadInitNumber = 0;
        jlong Thread::threadSeqNumber = 0;
        
        std::mutex Thread::internalThreadsLock;
        
        std::map<std::thread::id, JThread> Thread::internalThreads;
        
        jboolean Thread::isStaticInitializationBlock = StaticInitializationBlock();

        jboolean Thread::StaticInitializationBlock()
        {
            JThreadGroup systemThreadGroup = jnew<ThreadGroup>();
            JThreadGroup mainThreadGroup = jnew<ThreadGroup>(systemThreadGroup, jstr(u"main"));
            JThread thread = jnew<Thread>(mainThreadGroup, nullptr);
            addInternalThread(thread);
            return true;
        }
    }
}
