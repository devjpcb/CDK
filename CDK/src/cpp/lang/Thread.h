#ifndef CPP_LANG_THREAD_H
#define CPP_LANG_THREAD_H

#include "Object.h"
#include "Throwable.h"
#include <map>
#include <thread>
#include <mutex>

namespace cpp
{
    namespace lang
    {
        class Thread : public Object, public std::enable_shared_from_this<Thread>
        {
        public:
            enum class State {
                NEW,
                RUNNABLE,
                BLOCKED,
                WAITING,
                TIMED_WAITING,
                TERMINATED
            };
        
            Thread();
            Thread(JRunnable target);
            Thread(JRunnable target, cpp::security::JAccessControlContext acc);
            Thread(JThreadGroup group, JRunnable target);
            Thread(JString name);
            Thread(JThreadGroup group, JString name);
            Thread(JRunnable target, JString name);
            Thread(JThreadGroup group, JRunnable target, JString name);
            Thread(JThreadGroup group, JRunnable target, JString name, jlong stackSize);
            Thread(JThreadGroup group, std::nullptr_t null);
            ~Thread();
            
            class UncaughtExceptionHandler : public Object
            {
            public:
                virtual void uncaughtException(JThread t, Throwable& e) = 0;
            };
            
            using JUncaughtExceptionHandler = std::shared_ptr<UncaughtExceptionHandler>;
            
            static jint activeCount();
            void checkAccess();
            jint countStackFrames();
            static JThread currentThread();
            void destroy();
            static void dumpStack();
            static jint enumerate(jarray<JThread> tarray);
            static JUncaughtExceptionHandler getDefaultUncaughtExceptionHandler();
            jlong getId();
            JString getName();
            jint getPriority();
            State getState();
            JThreadGroup getThreadGroup();
            void interrupt();
            static jboolean interrupted();
            jboolean isAlive();
            jboolean isDaemon();
            jboolean isInterrupted();
            void join();
            void join(jlong millis);
            void join(jlong millis, jint nanos);
            void resume();
            void run();
            void setDaemon(jboolean on);
            void setName(JString name);
            void setPriority(jint newPriority);
            static void sleep(jlong millis);
            static void sleep(jlong millis, jint nanos);
            void start();
            void stop();
            void stop(Throwable& obj);
            void suspend();
            void yield();
            
            static const jint MIN_PRIORITY;
            static const jint NORM_PRIORITY;
            static const jint MAX_PRIORITY;
        protected:
            JObject clone();
            
        private:
            void interrupt0();
            jboolean isInterrupted(jboolean clearInterrupted);
            void resume0();
            void setNativeName(JString name);
            void setPriority0(jint newPriority);
            void start0();
            void stop0(JObject o);
            void suspend0();
            void init(JThreadGroup g, JRunnable target, JString name, jlong stackSize);
            void init(JThreadGroup g, JRunnable target, JString name, jlong stackSize, cpp::security::JAccessControlContext acc);
            static jint nextThreadNum();
            static jlong nextThreadID();
            static jboolean isCCLOverridden(JClass cl);
            static void addInternalThread(JThread internalThread);
            static void removeInternalThread();
            static JThread getInternalThread();
            
            jlong tid;
            JString name;
            jint priority;
            JThreadGroup group;
            volatile jint threadStatus;
            sun::nio::ch::JInterruptible blocker;
            jboolean daemon;
            jboolean alive;
            JRunnable target;
            static jint threadInitNumber;
            static jlong threadSeqNumber;
            JClassLoader contextClassLoader;
            cpp::security::JAccessControlContext inheritedAccessControlContext;
            jlong stackSize;
            //TODO: ThreadLocal::ThreadLocalMap inheritableThreadLocals;
            
            std::thread nativeThread;
            
            static std::mutex internalThreadsLock;
            static std::map<std::thread::id, JThread> internalThreads;
            static jboolean isStaticInitializationBlock;
            static jboolean StaticInitializationBlock();
        };
    }
}

#endif // CPP_LANG_THREAD_H
