#ifndef CPP_LANG_THREADDEATH_H
#define CPP_LANG_THREADDEATH_H

#include "Object.h"

namespace cpp
{
    namespace lang
    {
        class ThreadDeath : public Object
        {
        public:
            ThreadDeath();
            ~ThreadDeath();
        };
    }
}

#endif // CPP_LANG_THREADDEATH_H
