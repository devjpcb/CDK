#include "ThreadGroup.h"
#include "IllegalThreadStateException.h"
#include "SecurityManager.h"
#include "Math.h"
#include "ThreadDeath.h"
#include "../util/Arrays.h"
#include "../../sun/misc/VM.h"

using namespace cpp::util;
using namespace sun::misc;

namespace cpp
{
    namespace lang
    {
        ThreadGroup::ThreadGroup() : name(jstr(u"system")), maxPriority(Thread::MAX_PRIORITY), vmAllowSuspension(false), parent(nullptr), 
            destroyed(false), ngroups(0), nthreads(0), nUnstartedThreads(0)
        {
        }

        ThreadGroup::ThreadGroup(JString name) : ThreadGroup(Thread::currentThread()->getThreadGroup(), name)
        {

        }

        ThreadGroup::ThreadGroup(JThreadGroup parent, JString name) : ThreadGroup(checkParentAccess(parent), parent, name)
        {

        }

        ThreadGroup::ThreadGroup(JVoid unused, JThreadGroup parent, JString name) : name(name), maxPriority(parent->maxPriority), daemon(parent->daemon),
            vmAllowSuspension(parent->vmAllowSuspension), parent(parent), destroyed(false), ngroups(0), nthreads(0), nUnstartedThreads(0)
        {
            
        }

        ThreadGroup::~ThreadGroup()
        {
        }
        
        void ThreadGroup::postConstruct()
        {
            if (parent != nullptr)
                parent->add(shared_from_this());
        }
        
        jint ThreadGroup::activeCount()
        {
            jint result;
            jint ngroupsSnapshot;
            jarray<JThreadGroup> groupsSnapshot;
            {
                jsynchronized(this);
                if (destroyed) {
                    return 0;
                }
                result = nthreads;
                ngroupsSnapshot = ngroups;
                if (groups != nullptr) {
                    groupsSnapshot = Arrays::copyOf(groups, ngroupsSnapshot);
                } else {
                    groupsSnapshot = nullptr;
                }
            }
            for (jint i = 0 ; i < ngroupsSnapshot ; i++) {
                result += groupsSnapshot[i]->activeCount();
            }
            return result;
        }
        
        jint ThreadGroup::activeGroupCount()
        {
            jint ngroupsSnapshot;
            jarray<JThreadGroup> groupsSnapshot;
            {
                jsynchronized(this);
                if (destroyed) {
                    return 0;
                }
                ngroupsSnapshot = ngroups;
                if (groups != nullptr) {
                    groupsSnapshot = Arrays::copyOf(groups, ngroupsSnapshot);
                } else {
                    groupsSnapshot = nullptr;
                }
            }
            jint n = ngroupsSnapshot;
            for (jint i = 0 ; i < ngroupsSnapshot ; i++) {
                n += groupsSnapshot[i]->activeGroupCount();
            }
            return n;
        }
        
        jboolean ThreadGroup::allowThreadSuspension(jboolean b)
        {
            vmAllowSuspension = b;
            if (!b) {
                VM::unsuspendSomeThreads();
            }
            return true;
        }
        
        void ThreadGroup::checkAccess()
        {
            JSecurityManager security = System::getSecurityManager();
            if (security != nullptr) {
                security->checkAccess(shared_from_this());
            }
        }
        
        void ThreadGroup::destroy()
        {
            jint ngroupsSnapshot;
            jarray<JThreadGroup> groupsSnapshot;
            {
                jsynchronized(this);
                checkAccess();
                if (destroyed || (nthreads > 0)) {
                    throw IllegalThreadStateException();
                }
                ngroupsSnapshot = ngroups;
                if (groups != nullptr) {
                    groupsSnapshot = Arrays::copyOf(groups, ngroupsSnapshot);
                } else {
                    groupsSnapshot = nullptr;
                }
                if (parent != nullptr) {
                    destroyed = true;
                    ngroups = 0;
                    groups = nullptr;
                    nthreads = 0;
                    threads = nullptr;
                }
            }
            for (int i = 0 ; i < ngroupsSnapshot ; i += 1) {
                groupsSnapshot[i]->destroy();
            }
            if (parent != nullptr) {
                parent->remove(shared_from_this());
            }
        }
        
        jint ThreadGroup::enumerate(jarray<JThread> list)
        {
            checkAccess();
            return enumerate(list, 0, true);
        }

        jint ThreadGroup::enumerate(jarray<JThread> list, jboolean recurse)
        {
            checkAccess();
            return enumerate(list, 0, recurse);
        }
        
        jint ThreadGroup::enumerate(jarray<JThreadGroup> list)
        {
            checkAccess();
            return enumerate(list, 0, true);
        }
        
        jint ThreadGroup::enumerate(jarray<JThreadGroup> list, jboolean recurse)
        {
            checkAccess();
            return enumerate(list, 0, recurse);
        }
        
        jint ThreadGroup::getMaxPriority()
        {
            return maxPriority;
        }
        
        JString ThreadGroup::getName()
        {
            return name;
        }
        
        JThreadGroup ThreadGroup::getParent()
        {
            if (parent != nullptr)
                parent->checkAccess();
            return parent;
        }
        
        void ThreadGroup::interrupt()
        {
            jint ngroupsSnapshot;
            jarray<JThreadGroup> groupsSnapshot;
            {
                jsynchronized(this);
                checkAccess();
                for (jint i = 0 ; i < nthreads ; i++) {
                    threads[i]->interrupt();
                }
                ngroupsSnapshot = ngroups;
                if (groups != nullptr) {
                    groupsSnapshot = Arrays::copyOf(groups, ngroupsSnapshot);
                } else {
                    groupsSnapshot = nullptr;
                }
            }
            for (jint i = 0 ; i < ngroupsSnapshot; i++) {
                groupsSnapshot[i]->interrupt();
            }
        }
        
        jboolean ThreadGroup::isDaemon()
        {
            return daemon;
        }
        
        jboolean ThreadGroup::isDestroyed()
        {
            return destroyed;
        }
        
        jboolean ThreadGroup::parentOf(JThreadGroup g)
        {
            for (; g != nullptr ; g = g->parent) {
                if (g == shared_from_this()) {
                    return true;
                }
            }
            return false;
        }
        
        void ThreadGroup::resume()
        {
            jint ngroupsSnapshot;
            jarray<JThreadGroup> groupsSnapshot;
            {
                jsynchronized(this);
                checkAccess();
                for (jint i = 0 ; i < nthreads ; i++) {
                    threads[i]->resume();
                }
                ngroupsSnapshot = ngroups;
                if (groups != nullptr) {
                    groupsSnapshot = Arrays::copyOf(groups, ngroupsSnapshot);
                } else {
                    groupsSnapshot = nullptr;
                }
            }
            for (jint i = 0 ; i < ngroupsSnapshot ; i++) {
                groupsSnapshot[i]->resume();
            }
        }
        
        void ThreadGroup::setDaemon(jboolean daemon)
        {
            checkAccess();
            this->daemon = daemon;
        }
        
        void ThreadGroup::setMaxPriority(jint pri)
        {
            jint ngroupsSnapshot;
            jarray<JThreadGroup> groupsSnapshot;
            {
                jsynchronized(this);
                checkAccess();
                if (pri < Thread::MIN_PRIORITY || pri > Thread::MAX_PRIORITY) {
                    return;
                }
                maxPriority = (parent != nullptr) ? Math::min(pri, parent->maxPriority) : pri;
                ngroupsSnapshot = ngroups;
                if (groups != nullptr) {
                    groupsSnapshot = Arrays::copyOf(groups, ngroupsSnapshot);
                } else {
                    groupsSnapshot = nullptr;
                }
            }
            for (jint i = 0 ; i < ngroupsSnapshot ; i++) {
                groupsSnapshot[i]->setMaxPriority(pri);
            }
        }
        
        void ThreadGroup::stop()
        {
            if (stopOrSuspend(false))
                Thread::currentThread()->stop();
        }
        
        void ThreadGroup::suspend()
        {
            if (stopOrSuspend(true))
                Thread::currentThread()->suspend();
        }
        
        JString ThreadGroup::toString()
        {
            //TODO: return getClass()->getName() + jstr(u"[name=") + getName() + jstr(u",maxpri=") + maxPriority + jstr(u"]");
            return nullptr;
        }
        
        void ThreadGroup::uncaughtException(JThread t, Throwable& e)
        {
            if (parent != nullptr) {
                parent->uncaughtException(t, e);
            } else {
                Thread::JUncaughtExceptionHandler ueh = Thread::getDefaultUncaughtExceptionHandler();
                if (ueh != nullptr) {
                    ueh->uncaughtException(t, e);
                } 
                /* TODO:
                else if (!(jinstanceof<ThreadDeath>(e))) {
                    System.err.print("Exception in thread \"" + t.getName() + "\" ");
                    e.printStackTrace(System.err);
                }
                */
            }
        }
        
        JVoid ThreadGroup::checkParentAccess(JThreadGroup parent)
        {
            parent->checkAccess();
            return nullptr;
        }
        
        void ThreadGroup::add(JThreadGroup g) 
        {
            jsynchronized(this);
            if (destroyed) {
                throw IllegalThreadStateException();
            }
            if (groups == nullptr) {
                groups = jnew_array<ThreadGroup>(4);
            } else if (ngroups == groups.length()) {
                groups = Arrays::copyOf(groups, ngroups * 2);
            }
            groups[ngroups] = g;

            ngroups++;
        }
        
        void ThreadGroup::remove(JThreadGroup g)
        {
            jsynchronized(this);
            if(destroyed) {
                return;
            }
            for(jint i = 0 ; i < ngroups; i++) {
                if(groups[i] == g) {
                    ngroups -= 1;
                    System::arraycopy(groups, i + 1, groups, i, ngroups - i);
                    groups[ngroups] = nullptr;
                    break;
                }
            }
            if(nthreads == 0) {
                notifyAll();
            }
            if(daemon && (nthreads == 0) &&
               (nUnstartedThreads == 0) && (ngroups == 0)) {
                destroy();
            }
        }
        
        jint ThreadGroup::enumerate(jarray<JThread> list, jint n, jboolean recurse)
        {
            jint ngroupsSnapshot = 0;
            jarray<JThreadGroup> groupsSnapshot = nullptr;
            
            {
                jsynchronized(this);
                if (destroyed) {
                    return 0;
                }
                jint nt = nthreads;
                if (nt > list.length() - n) {
                    nt = list.length() - n;
                }
                for (jint i = 0; i < nt; i++) {
                    if (threads[i]->isAlive()) {
                        list[n++] = threads[i];
                    }
                }
                if (recurse) {
                    ngroupsSnapshot = ngroups;
                    if (groups != nullptr) {
                        groupsSnapshot = Arrays::copyOf(groups, ngroupsSnapshot);
                    } else {
                        groupsSnapshot = nullptr;
                    }
                }
            }
            
            if (recurse) {
                for (jint i = 0 ; i < ngroupsSnapshot ; i++) {
                    n = groupsSnapshot[i]->enumerate(list, n, true);
                }
            }
            return n;
        }
        
        jint ThreadGroup::enumerate(jarray<JThreadGroup> list, jint n, jboolean recurse)
        {
            jint ngroupsSnapshot = 0;
            jarray<JThreadGroup> groupsSnapshot = nullptr;
            
            {
                jsynchronized(this);
                if (destroyed) {
                    return 0;
                }
                jint ng = ngroups;
                if (ng > list.length() - n) {
                    ng = list.length() - n;
                }
                if (ng > 0) {
                    System::arraycopy(groups, 0, list, n, ng);
                    n += ng;
                }
                if (recurse) {
                    ngroupsSnapshot = ngroups;
                    if (groups != nullptr) {
                        groupsSnapshot = Arrays::copyOf(groups, ngroupsSnapshot);
                    } else {
                        groupsSnapshot = nullptr;
                    }
                }
            }
            if (recurse) {
                for (jint i = 0 ; i < ngroupsSnapshot; i++) {
                    n = groupsSnapshot[i]->enumerate(list, n, true);
                }
            }
            return n;
        }
        
        jboolean ThreadGroup::stopOrSuspend(jboolean suspend)
        {
            jboolean suicide = false;
            JThread us = Thread::currentThread();
            jint ngroupsSnapshot;
            jarray<JThreadGroup> groupsSnapshot = nullptr;
            {
                jsynchronized(this);
                checkAccess();
                for (jint i = 0 ; i < nthreads ; i++) {
                    if (threads[i]==us)
                        suicide = true;
                    else if (suspend)
                        threads[i]->suspend();
                    else
                        threads[i]->stop();
                }

                ngroupsSnapshot = ngroups;
                if (groups != nullptr) {
                    groupsSnapshot = Arrays::copyOf(groups, ngroupsSnapshot);
                }
            }
            for (jint i = 0 ; i < ngroupsSnapshot ; i++)
                suicide = groupsSnapshot[i]->stopOrSuspend(suspend) || suicide;

            return suicide;
        }
        
        void ThreadGroup::add(JThread t)
        {
            jsynchronized(this);
            if (destroyed) {
                throw IllegalThreadStateException();
            }
            if (threads == nullptr) {
                threads = jnew_array<Thread>(4);
            } else if (nthreads == threads.length()) {
                threads = Arrays::copyOf(threads, nthreads * 2);
            }
            threads[nthreads] = t;

            nthreads++;

            nUnstartedThreads--;
        }
        
        void ThreadGroup::threadStartFailed(JThread t)
        {
            jsynchronized(this);
            remove(t);
            nUnstartedThreads++;
        }
        
        void ThreadGroup::remove(JThread t)
        {
            jsynchronized(this);
            if (destroyed) {
                return;
            }
            for (jint i = 0 ; i < nthreads ; i++) {
                if (threads[i] == t) {
                    System::arraycopy(threads, i + 1, threads, i, --nthreads - i);
                    threads[nthreads] = nullptr;
                    break;
                }
            }
        }
        
        void ThreadGroup::addUnstarted()
        {
            jsynchronized(this);
            if (destroyed) {
                throw IllegalThreadStateException();
            }
            nUnstartedThreads++;
        }
    }
}
