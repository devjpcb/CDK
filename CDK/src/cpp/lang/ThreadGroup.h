#ifndef CPP_LANG_THREADGROUP_H
#define CPP_LANG_THREADGROUP_H

#include "Thread.h"
#include "PostConstructible.h"

namespace cpp
{
    namespace lang
    {
        class ThreadGroup : public Thread::UncaughtExceptionHandler, public PostConstructible, public std::enable_shared_from_this<ThreadGroup>
        {
        public:
            ThreadGroup();
            ThreadGroup(JString name);
            ThreadGroup(JThreadGroup parent, JString name);
            ThreadGroup(JVoid unused, JThreadGroup parent, JString name);
            ~ThreadGroup();
            
            void postConstruct();
            
            jint activeCount();
            jint activeGroupCount();
            jboolean allowThreadSuspension(jboolean b);
            void checkAccess();
            void destroy();
            jint enumerate(jarray<JThread> list);
            jint enumerate(jarray<JThread> list, jboolean recurse);
            jint enumerate(jarray<JThreadGroup> list);
            jint enumerate(jarray<JThreadGroup> list, jboolean recurse);
            jint getMaxPriority();
            JString getName();
            JThreadGroup getParent();
            void interrupt();
            jboolean isDaemon();
            jboolean isDestroyed();
            jboolean parentOf(JThreadGroup g);
            void resume();
            void setDaemon(jboolean daemon);
            void setMaxPriority(jint pri);
            void stop();
            void suspend();
            JString toString();
            void uncaughtException(JThread t, Throwable& e);

        private:
            static JVoid checkParentAccess(JThreadGroup parent);
            void add(JThreadGroup g);
            void remove(JThreadGroup g);
            jint enumerate(jarray<JThread> list, jint n, jboolean recurse);
            jint enumerate(jarray<JThreadGroup> list, jint n, jboolean recurse);
            jboolean stopOrSuspend(jboolean suspend);
            void add(JThread t);
            void threadStartFailed(JThread t);
            void remove(JThread t);
            void addUnstarted();
        
            JString name;
            jint maxPriority;
            jboolean daemon;
            jboolean vmAllowSuspension;
            JThreadGroup parent;
            jboolean destroyed;
            jint ngroups;
            jarray<JThreadGroup> groups;
            jint nthreads;
            jarray<JThread> threads;
            jint nUnstartedThreads;
            
            // TODO: hay que cambiarlo por el recursivo.
            std::recursive_mutex mtx;
            
            friend class Thread;
        };
    }
}

#endif // CPP_LANG_THREADGROUP_H
