#ifndef CPP_LANG_THREADLOCAL_H
#define CPP_LANG_THREADLOCAL_H

namespace cpp
{
    namespace lang
    {
        class ThreadLocal
        {
        public:
            ThreadLocal();
            ~ThreadLocal();
        };
    }
}

#endif // CPP_LANG_THREADLOCAL_H
