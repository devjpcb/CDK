#include "Throwable.h"

namespace cpp
{
    namespace lang
    {
        Throwable::Throwable()
        {
            fillInStackTrace();
        }

        Throwable::Throwable(JString message)
        {
            fillInStackTrace();
            detailMessage = message;
        }

        Throwable::Throwable(JString message, Throwable& cause)
        {
            detailMessage = message;
            this->cause = &cause;
        }

        Throwable::Throwable(JString message, Throwable& cause, jboolean enableSuppression, jboolean writableStackTrace)
        {
            if(writableStackTrace) {
                fillInStackTrace();
            }
            else {
                // TODO: stackTrace = nullptr;
            }
            detailMessage = message;
            this->cause = &cause;
            if(!enableSuppression) {
                // TODO: suppressedExceptions = null;
            }
        }

        Throwable::Throwable(const Throwable& cause)
        {
            fillInStackTrace();
            detailMessage = cause.toString();
            this->cause = const_cast<Throwable*>(&cause);
        }

        Throwable::~Throwable()
        {
        }

        Throwable& Throwable::fillInStackTrace()
        {
            /* TODO:
            if(stackTrace != null || backtrace != null) {
                fillInStackTrace(0);
                stackTrace = UNASSIGNED_STACK;
            }
            */
            return *this;
        }

        Throwable& Throwable::getCause()
        {
            return (cause == this ? *this : *cause);
        }

        JString Throwable::getLocalizedMessage() const
        {
            return getMessage();
        }

        JString Throwable::getMessage() const
        {
            return detailMessage;
        }

        JString Throwable::toString() const
        {
            JString className = jstr(u"cpp::lang::Throwable");
            JString message = getLocalizedMessage();
            return (!message->isEmpty()) ? (className + jstr(u": ") + message) : className;
        }
    }
}
