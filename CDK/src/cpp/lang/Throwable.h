#ifndef CPP_LANG_THROWABLE_H
#define CPP_LANG_THROWABLE_H

#include "Object.h"
#include "String.h"

namespace cpp
{
    namespace lang
    {
        class Throwable : public Object
        {
        public:
            Throwable();
            Throwable(JString message);
            Throwable(JString message, Throwable& cause);
            Throwable(JString message, Throwable& cause, jboolean enableSuppression, jboolean writableStackTrace);
            Throwable(const Throwable& cause);
            virtual ~Throwable();
            virtual Throwable& fillInStackTrace();
            virtual Throwable& getCause();
            virtual JString getLocalizedMessage() const;
            virtual JString getMessage() const;
            virtual JString toString() const;

        protected:
            JString detailMessage;
            mutable Throwable* cause;
        };
    }
}

#endif // CPP_LANG_THROWABLE_H
