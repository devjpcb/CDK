#include "UnsupportedOperationException.h"

namespace cpp
{
    namespace lang
    {
        UnsupportedOperationException::UnsupportedOperationException() : RuntimeException()
        {
        }

        UnsupportedOperationException::UnsupportedOperationException(JString message) : RuntimeException(message)
        {
        }
        
        UnsupportedOperationException::UnsupportedOperationException(JString message, Throwable& cause) : RuntimeException(message, cause)
        {
        }
        
        UnsupportedOperationException::UnsupportedOperationException(Throwable& cause)  : RuntimeException(cause)
        {
        }

        UnsupportedOperationException::~UnsupportedOperationException()
        {
        }
    }
}
