#ifndef CPP_LANG_UNSUPPORTEDOPERATIONEXCEPTION_H
#define CPP_LANG_UNSUPPORTEDOPERATIONEXCEPTION_H

#include "RuntimeException.h"

namespace cpp
{
    namespace lang
    {
        class UnsupportedOperationException : public RuntimeException
        {
        public:
            UnsupportedOperationException();
            UnsupportedOperationException(JString message);
            UnsupportedOperationException(JString message, Throwable& cause);
            UnsupportedOperationException(Throwable& cause);
            ~UnsupportedOperationException();
        };
    }
}

#endif // CPP_LANG_UNSUPPORTEDOPERATIONEXCEPTION_H
