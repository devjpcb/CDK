#include "VirtualMachineError.h"

namespace cpp
{
    namespace lang
    {
        VirtualMachineError::VirtualMachineError() : Error()
        {
        }

        VirtualMachineError::VirtualMachineError(JString message) : Error(message)
        {
        }
        
        VirtualMachineError::VirtualMachineError(JString message, Throwable& cause) : Error(message, cause)
        {
        }
        
        VirtualMachineError::VirtualMachineError(Throwable& cause) : Error(cause)
        {
        }

        VirtualMachineError::~VirtualMachineError()
        {
        }
    }
}
