#ifndef CPP_LANG_VIRTUALMACHINEERROR_H
#define CPP_LANG_VIRTUALMACHINEERROR_H

#include "Error.h"

namespace cpp
{
    namespace lang
    {
        class VirtualMachineError : public Error
        {
        public:
            VirtualMachineError();
            VirtualMachineError(JString message);
            VirtualMachineError(JString message, Throwable& cause);
            VirtualMachineError(Throwable& cause);
            ~VirtualMachineError();
        };
    }
}

#endif // CPP_LANG_VIRTUALMACHINEERROR_H
