#ifndef CPP_LANG_VOID_H
#define CPP_LANG_VOID_H

namespace cpp
{
    namespace lang
    {
        class Void
        {
        public:
            Void();
            ~Void();
        };
    }
}

#endif // VOID_H
