#ifndef CPP_LANG_ANNOTATION_ANNOTATION_H
#define CPP_LANG_ANNOTATION_ANNOTATION_H

#include "../Object.h"

namespace cpp
{
    namespace lang
    {
        namespace annotation
        {
            class Annotation : public cpp::lang::Object
            {
            public:
                virtual jboolean equals(JObject obj) = 0;
                virtual jint hashCode() = 0;
                virtual JString toString() = 0;
                virtual JClass annotationType() = 0;
            };
        }
    }
}

#endif // CPP_LANG_ANNOTATION_ANNOTATION_H
