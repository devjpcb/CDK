#include "AnnotationFormatError.h"

using namespace cpp::lang;

namespace cpp
{
    namespace lang
    {
        namespace annotation
        {
            AnnotationFormatError::AnnotationFormatError(JString message) : Error(message)
            {
                
            }
            
            AnnotationFormatError::AnnotationFormatError(JString message, cpp::lang::Throwable& cause) : Error(message, cause)
            {
                
            }
            
            AnnotationFormatError::AnnotationFormatError(Throwable& cause) : Error(cause)
            {
                
            }
            
            const jlong AnnotationFormatError::serialVersionUID = -4256701562333669892LL;
        }
    }
}
