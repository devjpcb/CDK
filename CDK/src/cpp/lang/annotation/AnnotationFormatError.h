#ifndef CPP_LANG_ANNOTATION_ANNOTATIONFORMATERROR_H
#define CPP_LANG_ANNOTATION_ANNOTATIONFORMATERROR_H

#include "../Error.h"

namespace cpp
{
    namespace lang
    {
        namespace annotation
        {
            class AnnotationFormatError : public cpp::lang::Error
            {
            public:
                AnnotationFormatError(cpp::lang::JString message);
                AnnotationFormatError(cpp::lang::JString message, cpp::lang::Throwable& cause);
                AnnotationFormatError(cpp::lang::Throwable& cause);
                
            private:
                static const jlong serialVersionUID;
            };
        }
    }
}

#endif // CPP_LANG_ANNOTATION_ANNOTATIONFORMATERROR_H
