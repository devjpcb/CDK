#include "AnnotationTypeMismatchException.h"
#include "../reflect/Method.h"

using namespace cpp::lang;
using namespace cpp::lang::reflect;

namespace cpp
{
    namespace lang
    {
        namespace annotation
        {
            AnnotationTypeMismatchException::AnnotationTypeMismatchException(cpp::lang::reflect::JMethod _element, cpp::lang::JString _foundType) :
                RuntimeException(jstr(u"Incorrectly typed data found for annotation element ") + _element + jstr(u" (Found data of type ") + _foundType + jstr(u")")),
                _element(_element), _foundType(_foundType)
            {
            }

            JMethod AnnotationTypeMismatchException::element()
            {
                return _element;
            }
            
            JString AnnotationTypeMismatchException::foundType()
            {
                return _foundType;
            }
            
            const jlong AnnotationTypeMismatchException::serialVersionUID = 8125925355765570191LL;
        }
    }
}
