#ifndef CPP_LANG_ANNOTATION_ANNOTATIONTYPEMISMATCHEXCEPTION_H
#define CPP_LANG_ANNOTATION_ANNOTATIONTYPEMISMATCHEXCEPTION_H

#include "../RuntimeException.h"

namespace cpp
{
    namespace lang
    {
        namespace annotation
        {
            class AnnotationTypeMismatchException : public cpp::lang::RuntimeException
            {
            public:
                AnnotationTypeMismatchException(cpp::lang::reflect::JMethod _element, cpp::lang::JString _foundType);
                cpp::lang::reflect::JMethod element();
                cpp::lang::JString foundType();
                
            private:
                cpp::lang::reflect::JMethod _element;
                cpp::lang::JString _foundType;
                
                static const jlong serialVersionUID;
            };
        }
    }
}

#endif // CPP_LANG_ANNOTATION_ANNOTATIONTYPEMISMATCHEXCEPTION_H
