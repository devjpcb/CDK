#include "ElementType.h"

namespace cpp
{
    namespace lang
    {
        namespace annotation
        {
            jenum_define(ElementType, 0, ANNOTATION_TYPE)
            jenum_define(ElementType, 1, CONSTRUCTOR)
            jenum_define(ElementType, 2, FIELD)
            jenum_define(ElementType, 3, LOCAL_VARIABLE)
            jenum_define(ElementType, 4, METHOD)
            jenum_define(ElementType, 5, PACKAGE)
            jenum_define(ElementType, 6, PARAMETER)
            jenum_define(ElementType, 7, TYPE)
            jenum_define(ElementType, 8, TYPE_PARAMETER)
            jenum_define(ElementType, 9, TYPE_USE)
        }
    }
}