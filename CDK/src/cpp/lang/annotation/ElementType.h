#ifndef CPP_LANG_ANNOTATION_ELEMENTTYPE_H
#define CPP_LANG_ANNOTATION_ELEMENTTYPE_H

#include "../Enum.h"

namespace cpp
{
    namespace lang
    {
        namespace annotation
        {
            jenum_begin(ElementType)
                jenum_declare(ANNOTATION_TYPE)
                jenum_declare(CONSTRUCTOR)
                jenum_declare(FIELD)
                jenum_declare(LOCAL_VARIABLE)
                jenum_declare(METHOD)
                jenum_declare(PACKAGE)
                jenum_declare(PARAMETER)
                jenum_declare(TYPE)
                jenum_declare(TYPE_PARAMETER)
                jenum_declare(TYPE_USE)
            jenum_end
        }
    }
}

#endif // CPP_LANG_ANNOTATION_ELEMENTTYPE_H
