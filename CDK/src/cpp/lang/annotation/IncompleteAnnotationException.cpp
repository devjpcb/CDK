#include "IncompleteAnnotationException.h"
#include "../Class.h"

using namespace cpp::lang;

namespace cpp
{
    namespace lang
    {
        namespace annotation
        {
            IncompleteAnnotationException::IncompleteAnnotationException(JClass _annotationType, JString _elementName)
                : RuntimeException(_annotationType->getName() + jstr(u" missing element ") + _elementName->toString())
            {
                this->_annotationType = _annotationType;
                this->_elementName = _elementName;
            }

            JClass IncompleteAnnotationException::annotationType()
            {
                return _annotationType;
            }

            JString IncompleteAnnotationException::elementName()
            {
                return _elementName;
            }

            const jlong IncompleteAnnotationException::serialVersionUI = 8445097402741811912L;
        }
    }
}
