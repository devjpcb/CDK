#ifndef INCOMPLETEANNOTATIONEXCEPTION_H
#define INCOMPLETEANNOTATIONEXCEPTION_H

#include "../RuntimeException.h"

namespace cpp
{
    namespace lang
    {
        namespace annotation
        {
            class IncompleteAnnotationException : public cpp::lang::RuntimeException
            {
            public:
                IncompleteAnnotationException(cpp::lang::JClass _annotationType, cpp::lang::JString _elementName);

                cpp::lang::JClass annotationType();
                cpp::lang::JString elementName();
                
            private:
                static const jlong serialVersionUI;
                cpp::lang::JClass _annotationType;
                cpp::lang::JString _elementName;
            };
        }
    }
}

#endif // INCOMPLETEANNOTATIONEXCEPTION_H
