#include "RetentionPolicy.h"

namespace cpp
{
    namespace lang
    {
        namespace annotation
        {
            jenum_define(RetentionPolicy, 0, CLASS)
            jenum_define(RetentionPolicy, 1, RUNTIME)
            jenum_define(RetentionPolicy, 2, SOURCE)
        }
    }
}
