#ifndef CPP_LANG_ANNOTATION_RETENTIONPOLICY_H
#define CPP_LANG_ANNOTATION_RETENTIONPOLICY_H

#include "../Enum.h"

namespace cpp
{
    namespace lang
    {
        namespace annotation
        {
            jenum_begin(RetentionPolicy)
                jenum_declare(CLASS)
                jenum_declare(RUNTIME)
                jenum_declare(SOURCE)
            jenum_end
        }
    }
}

#endif // CPP_LANG_ANNOTATION_RETENTIONPOLICY_H