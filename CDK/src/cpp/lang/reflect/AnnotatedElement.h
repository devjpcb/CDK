#ifndef CPP_LANG_REFLECT_ANNOTATEDELEMENT_H
#define CPP_LANG_REFLECT_ANNOTATEDELEMENT_H

#include "../Object.h"

namespace cpp
{
    namespace lang
    {
        namespace reflect
        {
            class AnnotatedElement : public Object
            {
            public:
                jboolean isAnnotationPresent(JClass annotationClass);
                cpp::lang::annotation::JAnnotation getAnnotation(JClass annotationClass);
                jarray<cpp::lang::annotation::JAnnotation> getAnnotations();
                cpp::lang::annotation::JAnnotation getAnnotationsByType(JClass annotationClass);
                cpp::lang::annotation::JAnnotation getDeclaredAnnotation(JClass annotationClass);
                jarray<cpp::lang::annotation::JAnnotation> getDeclaredAnnotationsByType(JClass annotationClass);
                jarray<cpp::lang::annotation::JAnnotation> getDeclaredAnnotations();
            };
        }
    }
}

#endif // CPP_LANG_REFLECT_ANNOTATEDELEMENT_H
