#include "Member.h"

namespace cpp
{
    namespace lang
    {
        namespace reflect
        {
            const jint Member::PUBLIC = 0;
            const jint Member::DECLARED = 1;
        }
    }
}
