#ifndef CPP_LANG_REFLECT_MEMBER_H
#define CPP_LANG_REFLECT_MEMBER_H

#include "../Object.h"

namespace cpp
{
    namespace lang
    {
        namespace reflect
        {
            class Member : public Object
            {
            public:
                virtual JClass getDeclaringClass() = 0;
                virtual JString getName() = 0;
                virtual jint getModifiers() = 0;
                virtual jboolean isSynthetic() = 0;
                
                static const jint PUBLIC;
                static const jint DECLARED;
            };
        }
    }
}

#endif // CPP_LANG_REFLECT_MEMBER_H
