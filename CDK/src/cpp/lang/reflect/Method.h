#ifndef CPP_LANG_REFLECT_TMETHOD_H
#define CPP_LANG_REFLECT_TMETHOD_H

#include "../Object.h"

namespace cpp
{
    namespace lang
    {
        namespace reflect
        {
            class Method : public cpp::lang::Object
            {
            public:
                Method();
                ~Method();
            };
        }
    }
}

#endif // CPP_LANG_REFLECT_TMETHOD_H
