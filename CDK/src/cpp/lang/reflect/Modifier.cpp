#include "Modifier.h"
#include "../String.h"
#include "../StringBuilder.h"

namespace cpp
{
    namespace lang
    {
        namespace reflect
        {
            jint Modifier::classModifiers()
            {
                return CLASS_MODIFIERS;
            }

            JString Modifier::toString(jint mod)
            {
                JStringBuilder sb = jnew<StringBuilder>();
                jint len;

                if((mod & PUBLIC) != 0)        sb->append(jstr(u"public "));
                if((mod & PROTECTED) != 0)     sb->append(jstr(u"protected "));
                if((mod & PRIVATE) != 0)       sb->append(jstr(u"private "));
                if((mod & ABSTRACT) != 0)      sb->append(jstr(u"abstract "));
                if((mod & STATIC) != 0)        sb->append(jstr(u"static "));
                if((mod & FINAL) != 0)         sb->append(jstr(u"final "));
                if((mod & TRANSIENT) != 0)     sb->append(jstr(u"transient "));
                if((mod & VOLATILE) != 0)      sb->append(jstr(u"volatile "));
                if((mod & SYNCHRONIZED) != 0)  sb->append(jstr(u"synchronized "));
                if((mod & NATIVE) != 0)        sb->append(jstr(u"native "));
                if((mod & STRICT) != 0)        sb->append(jstr(u"strictfp "));
                if((mod & INTERFACE) != 0)     sb->append(jstr(u"interface "));

                if((len = sb->length()) > 0)
                    return sb->toString()->substring(0, len-1);
                return jstr(u"");
            }

            const jint Modifier::PUBLIC = 0x00000001;
            const jint Modifier::PRIVATE = 0x00000002;
            const jint Modifier::PROTECTED = 0x00000004;
            const jint Modifier::STATIC = 0x00000008;
            const jint Modifier::FINAL = 0x00000010;
            const jint Modifier::SYNCHRONIZED = 0x00000020;
            const jint Modifier::VOLATILE = 0x00000040;
            const jint Modifier::TRANSIENT = 0x00000080;
            const jint Modifier::NATIVE = 0x00000100;
            const jint Modifier::INTERFACE = 0x00000200;
            const jint Modifier::ABSTRACT = 0x00000400;
            const jint Modifier::STRICT = 0x00000800;

            const jint Modifier::CLASS_MODIFIERS = PUBLIC | PROTECTED | PRIVATE | ABSTRACT | STATIC | FINAL | STRICT;
        }
    }
}
