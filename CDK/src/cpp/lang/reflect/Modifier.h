#ifndef CPP_LANG_REFLECT_MODIFIER_H
#define CPP_LANG_REFLECT_MODIFIER_H

#include "../Object.h"

namespace cpp
{
    namespace lang
    {
        namespace reflect
        {
            class Modifier : public cpp::lang::Object
            {
            public:
                static jint classModifiers();
                static JString toString(jint mod);
            
                static const jint PUBLIC;
                static const jint PRIVATE;
                static const jint PROTECTED;
                static const jint STATIC;
                static const jint FINAL;
                static const jint SYNCHRONIZED;
                static const jint VOLATILE;
                static const jint TRANSIENT;
                static const jint NATIVE;
                static const jint INTERFACE;
                static const jint ABSTRACT;
                static const jint STRICT;
                
                static const jint CLASS_MODIFIERS;
            };
        }
    }
}

#endif // CPP_LANG_REFLECT_MODIFIER_H
