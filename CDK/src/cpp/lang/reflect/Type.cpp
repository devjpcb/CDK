#include "Type.h"

namespace cpp
{
    namespace lang
    {
        namespace reflect
        {
            JString Type::getTypeName()
            {
                return toString();
            }
        }
    }
}
