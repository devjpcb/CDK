#ifndef CPP_LANG_REFLECT_TYPE_H
#define CPP_LANG_REFLECT_TYPE_H

#include "../Object.h"

namespace cpp
{
    namespace lang
    {
        namespace reflect
        {
            class Type : public Object
            {
            public:
                virtual JString getTypeName();
            };
        }
    }
}

#endif // CPP_LANG_REFLECT_TYPE_H
