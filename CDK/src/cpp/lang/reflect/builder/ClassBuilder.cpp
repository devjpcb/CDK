#include "ClassBuilder.h"
#include "../../Class.h"
#include "../../String.h"

namespace cpp
{
    namespace lang
    {
        namespace reflect
        {
            namespace builder
            {
                ClassBuilder::ClassBuilder()
                {
                    jClass = jnew<Class>();
                }

                ClassBuilder::~ClassBuilder()
                {

                }

                JClassBuilder ClassBuilder::create()
                {
                    return jnew<ClassBuilder>();
                }

                JClassBuilder ClassBuilder::setSimpleName(JString simpleName)
                {
                    jClass->simpleName = simpleName;
                    return shared_from_this();
                }
                
                JClassBuilder ClassBuilder::setPackage(JPackage package)
                {
                    jClass->package = package;
                    return shared_from_this();
                }

                JClassBuilder ClassBuilder::setModifiers(jint modifiers)
                {
                    jClass->modifiers = modifiers;
                    return shared_from_this();
                }

                JClassBuilder ClassBuilder::setIsArray(jboolean isarray)
                {
                    jClass->isarray = isarray;
                    return shared_from_this();
                }

                JClassBuilder ClassBuilder::setIsInterface(jboolean isinterface)
                {
                    jClass->isinterface = isinterface;
                    return shared_from_this();
                }

                JClassBuilder ClassBuilder::setIslocalClass(jboolean islocalClass)
                {
                    jClass->islocalClass = islocalClass;
                    return shared_from_this();
                }

                JClassBuilder ClassBuilder::setIsMemberClass(jboolean ismemberClass)
                {
                    jClass->ismemberClass = ismemberClass;
                    return shared_from_this();
                }

                JClassBuilder ClassBuilder::setIsPrimitive(jboolean isprimitive)
                {
                    jClass->isprimitive = isprimitive;
                    return shared_from_this();
                }

                JClassBuilder ClassBuilder::setFuncNewInstance(std::function<JObject()> funcNewInstance)
                {
                    jClass->funcNewInstance = funcNewInstance;
                    return shared_from_this();
                }

                JClass ClassBuilder::getClass()
                {
                    jint hash = jClass->getName()->hashCode();
                    getInternalClassList()[hash] = jClass;
                    return jClass;
                }

                std::unordered_map<jint,JClass>& ClassBuilder::getInternalClassList()
                {
                    static std::unordered_map<jint, JClass> internalClassList;
                    return internalClassList;
                }
            }
        }
    }
}
