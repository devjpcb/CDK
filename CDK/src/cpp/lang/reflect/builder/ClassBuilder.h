#ifndef CPP_LANG_BUILDER_CLASSBUILDER_H
#define CPP_LANG_BUILDER_CLASSBUILDER_H

#include "../../Object.h"
#include "../../Operators.h"
#include <functional>
#include <unordered_map>

namespace cpp
{
    namespace lang
    {
        namespace reflect
        {
            namespace builder
            {
                class ClassBuilder : public cpp::lang::Object, public std::enable_shared_from_this<ClassBuilder>
                {
                public:
                    ~ClassBuilder();
                    static JClassBuilder create();
                    
                    JClassBuilder setSimpleName(JString simpleName);
                    JClassBuilder setPackage(JPackage package);
                    JClassBuilder setModifiers(jint modifiers);
                    JClassBuilder setIsArray(jboolean isarray);
                    JClassBuilder setIsInterface(jboolean isinterface);
                    JClassBuilder setIslocalClass(jboolean islocalClass);
                    JClassBuilder setIsMemberClass(jboolean ismemberClass);
                    JClassBuilder setIsPrimitive(jboolean isprimitive);
                    JClassBuilder setFuncNewInstance(std::function<JObject()> funcNewInstance);
                    
                    JClass getClass();
                    
                protected:
                    ClassBuilder();

                    static std::unordered_map<jint, JClass>& getInternalClassList();
                    
                private:
                    cpp::lang::JClass jClass;
                    
                    template <typename T, typename... Args>
                    friend std::shared_ptr<T> cpp::lang::jnew(Args&&... args);

                    friend class cpp::lang::Class;
                };
            }
        }
    }
}

#endif // CPP_LANG_BUILDER_CLASSBUILDER_H
