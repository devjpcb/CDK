#include "PackageBuilder.h"
#include "../../Package.h"

namespace cpp
{
    namespace lang
    {
        namespace reflect
        {
            namespace builder
            {
                PackageBuilder::PackageBuilder()
                {
                    jPackage = jnew<Package>();
                }

                PackageBuilder::~PackageBuilder()
                {
                }
                
                JPackageBuilder PackageBuilder::create()
                {
                    return jnew<PackageBuilder>();
                }
                
                JPackageBuilder PackageBuilder::setName(JString name)
                {
                    jPackage->name = name;
                    return shared_from_this();
                }
                
                JPackage PackageBuilder::getPackage()
                {
                    return jPackage;
                }
            }
        }
    }
}
