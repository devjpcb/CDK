#ifndef CPP_LANG_BUILDER_PACKAGEBUILDER_H
#define CPP_LANG_BUILDER_PACKAGEBUILDER_H

#include "../../Object.h"
#include "../../Operators.h"

namespace cpp
{
    namespace lang
    {
        namespace reflect
        {
            namespace builder
            {
                class PackageBuilder : public cpp::lang::Object, public std::enable_shared_from_this<PackageBuilder>
                {
                public:
                    ~PackageBuilder();
                    
                    static JPackageBuilder create();
                    
                    JPackageBuilder setName(JString name);
                    JPackage getPackage();
                    
                protected:
                    PackageBuilder();
                    
                    cpp::lang::JPackage jPackage;
                    
                    template <typename T, typename... Args>
                    friend std::shared_ptr<T> cpp::lang::jnew(Args&&... args);
                };
            }
        }
    }
}

#endif // PACKAGEBUILDER_H
