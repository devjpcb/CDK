#ifndef CPP_SECURITY_ACCESSCONTROLLER_H
#define CPP_SECURITY_ACCESSCONTROLLER_H

#include "../lang/Object.h"

namespace cpp
{
    namespace security
    {
        class AccessController
        {
        public:
            template<typename T>
            static T doPrivileged(JPrivilegedAction<T> action) 
            {
                return nullptr;
            }
        };
    }
}

#endif // CPP_SECURITY_ACCESSCONTROLLER_H
