#ifndef CPP_SECURITY_PRIVILEGEDACTION_H
#define CPP_SECURITY_PRIVILEGEDACTION_H

#include "../lang/Object.h"

namespace cpp
{
    namespace security
    {
        template<typename T> 
        class PrivilegedAction : public cpp::lang::Object
        {
        public:
            virtual T run() = 0;
        };
    }
}

#endif // CPP_SECURITY_PRIVILEGEDACTION_H
