#ifndef CPP_SECURITY_PROTECTIONDOMAIN_H
#define CPP_SECURITY_PROTECTIONDOMAIN_H

#include "../lang/Object.h"

namespace cpp
{
    namespace security
    {
        class ProtectionDomain : public cpp::lang::Object
        {
        public:
            jboolean implies(JPermission permission);
        };
    }
}

#endif // CPP_SECURITY_PROTECTIONDOMAIN_H
