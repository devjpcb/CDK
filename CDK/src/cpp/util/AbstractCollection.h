#ifndef CPP_UTIL_ABSTRACTCOLLECTION_H
#define CPP_UTIL_ABSTRACTCOLLECTION_H

#include "Collection.h"
#include "Arrays.h"
#include "Objects.h"
#include "../lang/StringBuilder.h"
#include "../lang/Integer.h"
#include "../lang/OutOfMemoryError.h"
#include "../lang/UnsupportedOperationException.h"

namespace cpp
{
    namespace util
    {
        template <typename E>
        class AbstractCollection : public virtual cpp::lang::Object, public virtual Collection<E>
        {
        public:
            virtual JIterator<E> iterator() = 0;
            virtual jint size() = 0;

            virtual jboolean isEmpty() {
                return size() == 0;
            }

            virtual jboolean contains(cpp::lang::JObject o) {
                JIterator<E> it = iterator();
                if(o == nullptr) {
                    while(it->hasNext())
                        if(it->next() == nullptr)
                            return true;
                }
                else {
                    while(it->hasNext())
                        if(o->equals(it->next()))
                            return true;
                }
                return false;
            }

            virtual jarray<cpp::lang::JObject> toArray() {
                // Estimate size of array; be prepared to see more or fewer elements
                jarray<cpp::lang::JObject> r(size());
                JIterator<E> it = iterator();
                for(jint i = 0; i < r.length(); i++) {
                    if(! it->hasNext())  // fewer elements than expected
                        return Arrays::copyOf(r, i);
                    r[i] = it->next();
                }
                return it->hasNext() ? finishToArray(r, it) : r;
            }

            virtual jarray<E> toArray(jarray<E> a) {
                // TODO: implementar Class parcialmente
                // se modifico el codigo para que funcionara sin el tipo Class
                jint _size = size();
                jarray<E> r = a.length() >= _size ? a : jarray<E>(_size);
                JIterator<E> it = iterator();

                for (jint i = 0; i < r.length(); i++) {
                    if (! it->hasNext()) { // fewer elements than expected
                        if (a == r) {
                            r[i] = nullptr; // null-terminate
                        } else if (a.length() < i) {
                            return Arrays::copyOf(r, i);
                        } else {
                            cpp::lang::System::arraycopy(r, 0, a, 0, i);
                            if (a.length() > i) {
                                a[i] = nullptr;
                            }
                        }
                        return a;
                    }
                    r[i] = it->next();
                }
                // more elements than expected
                return it->hasNext() ? finishToArray(r, it) : r;
            }

            template <typename T, typename I>
            static jarray<T> finishToArray(jarray<T> r, JIterator<I> it) {
                jint i = r.length();
                while(it->hasNext()) {
                    jint cap = r.length();
                    if(i == cap) {
                        jint newCap = cap + (cap >> 1) + 1;
                        // overflow-conscious code
                        if(newCap - MAX_ARRAY_SIZE > 0)
                            newCap = hugeCapacity(cap + 1);
                        r = Arrays::copyOf(r, newCap);
                    }
                    r[i++] = it->next();
                }
                // trim if overallocated
                return (i == r.length()) ? r : Arrays::copyOf(r, i);
            }

            static jint hugeCapacity(jint minCapacity) {
                if(minCapacity < 0)  // overflow
                    throw cpp::lang::OutOfMemoryError(jstr(u"Required array size too large"));
                return (minCapacity > MAX_ARRAY_SIZE) ? cpp::lang::Integer::MAX_VALUE : MAX_ARRAY_SIZE;
            }

            virtual jboolean add(E e) {
                throw cpp::lang::UnsupportedOperationException();
            }

            virtual jboolean remove(cpp::lang::JObject o) {
                JIterator<E> it = iterator();
                if(o == nullptr) {
                    while(it->hasNext()) {
                        if(it->next() == nullptr) {
                            it->remove();
                            return true;
                        }
                    }
                }
                else {
                    while(it->hasNext()) {
                        if(o->equals(it->next())) {
                            it->remove();
                            return true;
                        }
                    }
                }
                return false;
            }

            virtual jboolean containsAll(JCollection<E> c) {
                for(cpp::lang::JObject e : c)
                    if(!contains(e))
                        return false;
                return true;
            }

            virtual jboolean equals(cpp::lang::JObject o) {
                return Object::equals(o);
            }

            virtual jint hashCode() {
                return Object::hashCode();
            }

            virtual jboolean addAll(JCollection<E> c) {
                jboolean modified = false;
                for(E e : c)
                    if(add(e))
                        modified = true;
                return modified;
            }

            virtual jboolean removeAll(JCollection<E> c) {
                Objects::requireNonNull(c);
                jboolean modified = false;
                JIterator<E> it = iterator();
                while(it->hasNext()) {
                    if(c->contains(it->next())) {
                        it->remove();
                        modified = true;
                    }
                }
                return modified;
            }

            virtual jboolean retainAll(JCollection<E> c) {
                Objects::requireNonNull(c);
                jboolean modified = false;
                JIterator<E> it = iterator();
                while(it->hasNext()) {
                    if(!c->contains(it->next())) {
                        it->remove();
                        modified = true;
                    }
                }
                return modified;
            }

            virtual void clear() {
                JIterator<E> it = iterator();
                while(it->hasNext()) {
                    it->next();
                    it->remove();
                }
            }

            virtual cpp::lang::JString toString() {
                JIterator<E> it = iterator();
                if(! it->hasNext())
                    return jstr(u"[]");

                cpp::lang::JStringBuilder sb = jnew<cpp::lang::StringBuilder>();
                sb->append(u'[');
                for(;;) {
                    E e = it->next();
                    // TODO: sb.append(e == this ? "(this Collection)" : e);
                    if(! it->hasNext())
                        return sb->append(u']')->toString();
                    sb->append(u',')->append(u' ');
                }
            }

        private:
            static const jint MAX_ARRAY_SIZE;
        };

        template <typename E>
        const jint AbstractCollection<E>::MAX_ARRAY_SIZE = cpp::lang::Integer::MAX_VALUE - 8;
    }
}

#endif // CPP_UTIL_ABSTRACTCOLLECTION_H
