#ifndef CPP_UTIL_ABSTRACTLIST_H
#define CPP_UTIL_ABSTRACTLIST_H

#include "AbstractCollection.h"
#include "List.h"
#include "ListIterator.h"
#include "RandomAccess.h"
#include "ConcurrentModificationException.h"
#include "NoSuchElementException.h"
#include "../lang/String.h"
#include "../lang/IllegalArgumentException.h"
#include "../lang/UnsupportedOperationException.h"
#include "../lang/IndexOutOfBoundsException.h"
#include "../lang/IllegalStateException.h"

namespace cpp
{
    namespace util
    {
        template <typename E>
        class SubList;
        
        template <typename E>
        class RandomAccessSubList;
        
        template <typename E>
        class AbstractList : public AbstractCollection<E>, public List<E>, public std::enable_shared_from_this<AbstractList<E>>
        {
        public:
            using std::enable_shared_from_this<AbstractList<E>>::shared_from_this;
        
            AbstractList() : modCount(0) {}
            
            virtual ~AbstractList() {}
            
            void finalize() {
                AbstractCollection<E>::finalize();
            }
            
            virtual jboolean add(E e) {
                add(size(), e);
                return true;
            }
            
            virtual void add(jint index, E element) {
                throw cpp::lang::UnsupportedOperationException();
            }
            
            virtual jboolean addAll(jint index, JCollection<E> c) {
                rangeCheckForAdd(index);
                jboolean modified = false;
                for (E e : c) {
                    add(index++, e);
                    modified = true;
                }
                return modified;
            }
            
            virtual void clear() {
                removeRange(0, size());
            }
            
            virtual jboolean equals(cpp::lang::JObject o) {
                if (o == shared_from_this())
                    return true;
                    
                JList<E> l = std::dynamic_pointer_cast<List<E>>(o);
                if (l == nullptr)
                    return false;

                JListIterator<E> e1 = listIterator();
                JListIterator<E> e2 = l->listIterator();
                while (e1->hasNext() && e2->hasNext()) {
                    E o1 = e1->next();
                    cpp::lang::JObject o2 = e2->next();
                    if (!(o1 == nullptr ? o2 == nullptr : o1->equals(o2)))
                        return false;
                }
                return !(e1->hasNext() || e2->hasNext());
            }
            
            virtual E get(jint index) = 0;
            
            virtual jint hashCode() {
                jint hashCode = 1;
                for (E e : shared_from_this())
                    hashCode = 31 * hashCode + (e == nullptr ? 0 : e->hashCode());
                return hashCode;
            }
            
            virtual jint indexOf(cpp::lang::JObject o) {
                JListIterator<E> it = listIterator();
                if (o == nullptr) {
                    while (it->hasNext())
                        if (it->next() == nullptr)
                            return it->previousIndex();
                } else {
                    while (it->hasNext())
                        if (o->equals(it->next()))
                            return it->previousIndex();
                }
                return -1;
            }
            
            virtual JIterator<E> iterator() {
                return jnew<Itr>(this);
            }
            
            virtual jint lastIndexOf(cpp::lang::JObject o) {
                JListIterator<E> it = listIterator(size());
                if (o == nullptr) {
                    while (it->hasPrevious())
                        if (it->previous() == nullptr)
                            return it->nextIndex();
                } else {
                    while (it->hasPrevious())
                        if (o->equals(it->previous()))
                            return it->nextIndex();
                }
                return -1;
            }
            
            virtual JListIterator<E> listIterator() {
                return listIterator(0);
            }
            
            virtual JListIterator<E> listIterator(jint index) {
                rangeCheckForAdd(index);

                return jnew<ListItr>(index, this);
            }
            
            virtual E remove(jint index) {
                throw cpp::lang::UnsupportedOperationException();
            }
            
            virtual E set(jint index, E element) {
                throw cpp::lang::UnsupportedOperationException();
            }
            
            JList<E> subList(jint fromIndex, jint toIndex) {
                JRandomAccess r = std::dynamic_pointer_cast<RandomAccess>(shared_from_this());
            
                if (r != nullptr)
                    return jnew<RandomAccessSubList<E>>(shared_from_this(), fromIndex, toIndex);
                
                return jnew<SubList<E>>(shared_from_this(), fromIndex, toIndex);
            }
            
            virtual jint size() = 0;
            
            virtual jboolean isEmpty() {
                return AbstractCollection<E>::isEmpty();
            }

            virtual jboolean addAll(JCollection<E> c) {
                return AbstractCollection<E>::addAll(c);
            }

            virtual jboolean containsAll(JCollection<E> c) {
                return AbstractCollection<E>::containsAll(c);
            }

            virtual jboolean remove(cpp::lang::JObject o) {
                return AbstractCollection<E>::remove(o);
            }

            virtual jboolean retainAll(JCollection<E> c) {
                return AbstractCollection<E>::retainAll(c);
            }

            virtual jarray<cpp::lang::JObject> toArray() {
                return AbstractCollection<E>::toArray();
            }

            virtual jarray<E> toArray(jarray<E> a) {
                return AbstractCollection<E>::toArray(a);
            }
            
            virtual jboolean contains(cpp::lang::JObject o) {
                return AbstractCollection<E>::contains(o);
            }
                
            virtual jboolean removeAll(JCollection<E> c) {
                return AbstractCollection<E>::removeAll(c);
            }
            
            // TODO: para evitar ambigüedad
            cpp::lang::ForeachIterator<E> begin() {
                cpp::lang::ForeachIterator<E> fi(iterator());
                return fi;
            }

            cpp::lang::ForeachIterator<E> end() {
                cpp::lang::ForeachIterator<E> fi(iterator());
                return fi;
            }
            
        protected:
            void removeRange(jint fromIndex, jint toIndex) {
                JListIterator<E> it = listIterator(fromIndex);
                for (jint i = 0, n = toIndex - fromIndex; i < n; i++) {
                    it->next();
                    it->remove();
                }
            }
            
            jint modCount;
            
        private:
            class Itr : public Iterator<E>
            {
            public:
                Itr(AbstractList<E> *parent) : cursor(0), lastRet(-1), expectedModCount(parent->modCount), parent(parent) { }
                
                jboolean hasNext() {
                    return cursor != parent->size();
                }

                E next() {
                    checkForComodification();
                    try {
                        jint i = cursor;
                        E next = parent->get(i);
                        lastRet = i;
                        cursor = i + 1;
                        return next;
                    } catch (cpp::lang::IndexOutOfBoundsException& e) {
                        checkForComodification();
                        throw NoSuchElementException();
                    }
                }

                void remove() {
                    if (lastRet < 0)
                        throw cpp::lang::IllegalStateException();
                    checkForComodification();

                    try {
                        parent->remove(lastRet);
                        if (lastRet < cursor)
                            cursor--;
                        lastRet = -1;
                        expectedModCount = parent->modCount;
                    } catch (cpp::lang::IndexOutOfBoundsException& e) {
                        throw ConcurrentModificationException();
                    }
                }
                
            protected:
                void checkForComodification() {
                    if (parent->modCount != expectedModCount)
                        throw ConcurrentModificationException();
                }
                
                jint cursor;
                jint lastRet;
                jint expectedModCount;
                
                AbstractList<E> *parent;
            };
            
            class ListItr : public Itr, public ListIterator<E> 
            {
            public:
                ListItr(jint index, AbstractList<E> *parent) : Itr(parent) {
                    cursor = index;
                }
                
                void finalize() {
                    Itr::finalize();
                }

                jboolean hasPrevious() {
                    return cursor != 0;
                }

                E previous() {
                    checkForComodification();
                    try {
                        jint i = cursor - 1;
                        E previous = parent->get(i);
                        lastRet = cursor = i;
                        return previous;
                    } catch (cpp::lang::IndexOutOfBoundsException& e) {
                        checkForComodification();
                        throw NoSuchElementException();
                    }
                }

                jint nextIndex() {
                    return cursor;
                }

                jint previousIndex() {
                    return cursor-1;
                }

                void set(E e) {
                    if (lastRet < 0)
                        throw cpp::lang::IllegalStateException();
                    checkForComodification();

                    try {
                        parent->set(lastRet, e);
                        expectedModCount = parent->modCount;
                    } catch (cpp::lang::IndexOutOfBoundsException& ex) {
                        throw ConcurrentModificationException();
                    }
                }

                void add(E e) {
                    checkForComodification();

                    try {
                        jint i = cursor;
                        parent->add(i, e);
                        lastRet = -1;
                        cursor = i + 1;
                        expectedModCount = parent->modCount;
                    } catch (cpp::lang::IndexOutOfBoundsException& ex) {
                        throw ConcurrentModificationException();
                    }
                }
                
                jboolean hasNext() {
                    return Itr::hasNext();
                }
                
                E next() {
                    return Itr::next();
                }
                
                void remove() {
                    Itr::remove();
                }
                
            protected:
                using Itr::cursor;
                using Itr::lastRet;
                using Itr::expectedModCount;
                using Itr::checkForComodification;
                using Itr::parent;
            };
            
            cpp::lang::JString outOfBoundsMsg(jint index) {
                return jstr(u"Index: ") + boxing(index) + jstr(u", Size: ") + boxing(size());
            }
        
            void rangeCheckForAdd(jint index) {
                if (index < 0 || index > size())
                    throw cpp::lang::IndexOutOfBoundsException(outOfBoundsMsg(index));
            }
            
            template <typename EE>
            friend class SubList;
        };
        
        template <typename T>
        cpp::lang::ForeachIterator<T> begin(JAbstractList<T> iterator)
        {
            return iterator->begin();
        }
        
        template <typename T>
        cpp::lang::ForeachIterator<T> end(JAbstractList<T> iterator)
        {
            return iterator->end();
        }
        
        template <typename E>
        class SubList : public AbstractList<E> 
        {
        public:
            using std::enable_shared_from_this<AbstractList<E>>::shared_from_this;
        
            SubList(JAbstractList<E> list, jint fromIndex, jint toIndex) {
                if (fromIndex < 0)
                    throw cpp::lang::IndexOutOfBoundsException(jstr(u"fromIndex = ") + boxing(fromIndex));
                if (toIndex > list->size())
                    throw cpp::lang::IndexOutOfBoundsException(jstr(u"toIndex = ") + boxing(toIndex));
                if (fromIndex > toIndex)
                    throw cpp::lang::IllegalArgumentException(jstr(u"fromIndex(") + boxing(fromIndex) + jstr(u") > toIndex(") + boxing(toIndex) + jstr(u")"));
                l = list;
                offset = fromIndex;
                _size = toIndex - fromIndex;
                this->modCount = l->modCount;
            }
        
            E set(jint index, E element) {
                rangeCheck(index);
                checkForComodification();
                return l->set(index + offset, element);
            }

            E get(jint index) {
                rangeCheck(index);
                checkForComodification();
                return l->get(index + offset);
            }

            jint size() {
                checkForComodification();
                return _size;
            }

            void add(jint index, E element) {
                rangeCheckForAdd(index);
                checkForComodification();
                l->add(index + offset, element);
                this->modCount = l->modCount;
                _size++;
            }

            E remove(jint index) {
                rangeCheck(index);
                checkForComodification();
                E result = l->remove(index + offset);
                this->modCount = l->modCount;
                _size--;
                return result;
            }

            jboolean addAll(JCollection<E> c) {
                return addAll(_size, c);
            }

            jboolean addAll(jint index, JCollection<E> c) {
                rangeCheckForAdd(index);
                jint cSize = c->size();
                if (cSize==0)
                    return false;

                checkForComodification();
                l->addAll(offset + index, c);
                this->modCount = l->modCount;
                _size += cSize;
                return true;
            }

            JIterator<E> iterator() {
                return listIterator();
            }

            JListIterator<E> listIterator(jint index) {
                checkForComodification();
                rangeCheckForAdd(index);

                return jnew<SubListIterator>(this, index);
            }

            JList<E> subList(jint fromIndex, jint toIndex) {
                return jnew<SubList<E>>(shared_from_this(), fromIndex, toIndex);
            }

            void rangeCheck(jint index) {
                if (index < 0 || index >= _size)
                    throw cpp::lang::IndexOutOfBoundsException(outOfBoundsMsg(index));
            }

            void rangeCheckForAdd(jint index) {
                if (index < 0 || index > _size)
                    throw cpp::lang::IndexOutOfBoundsException(outOfBoundsMsg(index));
            }

            cpp::lang::JString outOfBoundsMsg(jint index) {
                return jstr(u"Index: ") + boxing(index) + jstr(u", Size: ") + boxing(_size);
            }

            void checkForComodification() {
                if (this->modCount != l->modCount)
                    throw ConcurrentModificationException();
            }
            
            jboolean contains(cpp::lang::JObject o) {
                return AbstractCollection<E>::contains(o);
            }
            
            jboolean removeAll(JCollection<E> c) {
                return AbstractCollection<E>::removeAll(c);
            }
            
        protected:
            void removeRange(jint fromIndex, jint toIndex) {
                checkForComodification();
                l->removeRange(fromIndex + offset, toIndex + offset);
                this->modCount = l->modCount;
                size -= (toIndex-fromIndex);
            }
            
        private:
            class SubListIterator : public ListIterator<E> 
            {
            public:
                SubListIterator(SubList<E> *parent, jint index) : parent(parent)
                {
                    i = parent->l->listIterator(index + parent->offset);
                }

                jboolean hasNext() {
                    return nextIndex() < parent->_size;
                }

                E next() {
                    if (hasNext())
                        return i->next();
                    else
                        throw NoSuchElementException();
                }

                jboolean hasPrevious() {
                    return previousIndex() >= 0;
                }

                E previous() {
                    if (hasPrevious())
                        return i->previous();
                    else
                        throw NoSuchElementException();
                }

                jint nextIndex() {
                    return i->nextIndex() - parent->offset;
                }

                jint previousIndex() {
                    return i->previousIndex() - parent->offset;
                }

                void remove() {
                    i->remove();
                    parent->modCount = parent->l->modCount;
                    parent->_size--;
                }

                void set(E e) {
                    i->set(e);
                }

                void add(E e) {
                    i->add(e);
                    parent->modCount = parent->l->modCount;
                    parent->_size++;
                }
                
            private:
                SubList<E> *parent;
                JListIterator<E> i;
            };
            
            JAbstractList<E> l;
            jint offset;
            jint _size;
            using AbstractList<E>::modCount;
            using AbstractList<E>::listIterator;
            
            template <typename EE>
            friend class AbstractList;
            
            template <typename EE>
            friend class RandomAccessSubList;
        };
        
        template <typename E>
        class RandomAccessSubList : public SubList<E>, public RandomAccess
        {
        public:
            using std::enable_shared_from_this<AbstractList<E>>::shared_from_this;
        
            RandomAccessSubList(JAbstractList<E> list, jint fromIndex, jint toIndex) : SubList<E>(list, fromIndex, toIndex) { }
            
            void finalize() {
                SubList<E>::finalize();
            }
            
            JList<E> subList(jint fromIndex, jint toIndex) {
                return jnew<RandomAccessSubList<E>>(shared_from_this(), fromIndex, toIndex);
            }
            
        private:
            template <typename EE>
            friend class AbstractList;
        };
    }
}

#endif // CPP_UTIL_ABSTRACTLIST_H
