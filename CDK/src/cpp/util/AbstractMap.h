#ifndef CPP_UTIL_ABSTRACTMAP_H
#define CPP_UTIL_ABSTRACTMAP_H

#include "Map.h"
#include "AbstractCollection.h"
#include "AbstractSet.h"
#include "../lang/StringBuilder.h"
#include "../lang/NullPointerException.h"
#include "../lang/ClassCastException.h"
#include "../lang/UnsupportedOperationException.h"

namespace cpp
{
    namespace util
    {
        template <typename K, typename V>
        class AbstractMap : public virtual cpp::lang::Object, public Map<K, V>
        {
        public:
            virtual ~AbstractMap() {}

            virtual jint size() {
                return entrySet()->size();
            }

            virtual jboolean isEmpty() {
                return size() == 0;
            }

            virtual jboolean containsValue(cpp::lang::JObject value) {
                JIterator<typename Map<K, V>::JEntry> i = entrySet()->iterator();
                if(value == nullptr) {
                    while(i->hasNext()) {
                        typename Map<K, V>::JEntry e = i->next();
                        if(e->getValue() == nullptr)
                            return true;
                    }
                }
                else {
                    while(i->hasNext()) {
                        typename Map<K, V>::JEntry e = i->next();
                        if(value->equals(e->getValue()))
                            return true;
                    }
                }
                return false;
            }

            virtual jboolean containsKey(cpp::lang::JObject key) {
                JIterator<typename Map<K, V>::JEntry> i = entrySet()->iterator();
                if(key == nullptr) {
                    while(i->hasNext()) {
                        typename Map<K, V>::JEntry e = i->next();
                        if(e->getKey() == nullptr)
                            return true;
                    }
                }
                else {
                    while(i->hasNext()) {
                        typename Map<K, V>::JEntry e = i->next();
                        if(key->equals(e->getKey()))
                            return true;
                    }
                }
                return false;
            }

            virtual V get(std::shared_ptr<cpp::lang::Object> key) {
                JIterator<typename Map<K, V>::JEntry> i = entrySet()->iterator();
                if(key == nullptr) {
                    while(i->hasNext()) {
                        typename Map<K, V>::JEntry e = i->next();
                        if(e->getKey() == nullptr)
                            return e->getValue();
                    }
                }
                else {
                    while(i->hasNext()) {
                        typename Map<K, V>::JEntry e = i->next();
                        if(key->equals(e->getKey()))
                            return e->getValue();
                    }
                }
                return nullptr;
            }

            virtual V put(K key, V value) {
                throw cpp::lang::UnsupportedOperationException();
            }

            virtual V remove(std::shared_ptr<cpp::lang::Object> key) {
                JIterator<typename Map<K, V>::JEntry> i = entrySet()->iterator();
                typename Map<K, V>::JEntry correctEntry = nullptr;
                if(key == nullptr) {
                    while(correctEntry == nullptr && i->hasNext()) {
                        typename Map<K, V>::JEntry e = i->next();
                        if(e->getKey() == nullptr)
                            correctEntry = e;
                    }
                }
                else {
                    while(correctEntry == nullptr && i->hasNext()) {
                        typename Map<K, V>::JEntry e = i->next();
                        if(key->equals(e->getKey()))
                            correctEntry = e;
                    }
                }

                V oldValue = nullptr;
                if(correctEntry != nullptr) {
                    oldValue = correctEntry->getValue();
                    i->remove();
                }
                return oldValue;
            }

            void putAll(JMap<K, V> m) {
                for(typename Map<K, V>::JEntry e : m->entrySet())
                    put(e->getKey(), e->getValue());
            }

            void clear() {
                entrySet()->clear();
            }

            virtual JSet<typename Map<K, V>::JEntry> entrySet() = 0;

            virtual JSet<K> keySet() {
                if(_keySet == nullptr) {
                    _keySet = jnew<AnonymousAbstractSet>(this);
                }
                return _keySet;
            }

            virtual JCollection<V> values() {
                if(_values == nullptr) {
                    _values = jnew<AnonymousAbstractCollection>(this);
                }
                return _values;
            }

            virtual jboolean equals(cpp::lang::JObject o) {
                if(o.get() == this)
                    return true;

                JMap<K, V> m = std::dynamic_pointer_cast<Map<K, V>>(o);
                if(m == nullptr)
                    return false;

                if(m->size() != size())
                    return false;

                try {
                    JIterator<typename Map<K, V>::JEntry> i = entrySet()->iterator();
                    while(i->hasNext()) {
                        typename Map<K, V>::JEntry e = i->next();
                        K key = e->getKey();
                        V value = e->getValue();
                        if(value == nullptr) {
                            if(!(m->get(key) == nullptr && m->containsKey(key)))
                                return false;
                        }
                        else {
                            if(!value->equals(m->get(key)))
                                return false;
                        }
                    }
                }
                catch(cpp::lang::ClassCastException& unused) {
                    return false;
                }
                catch(cpp::lang::NullPointerException& unused) {
                    return false;
                }

                return true;
            }

            virtual jint hashCode() {
                jint h = 0;
                JIterator<typename Map<K, V>::JEntry> i = entrySet()->iterator();
                while(i->hasNext())
                    h += i->next()->hashCode();
                return h;
            }

            virtual cpp::lang::JString toString() {
                JIterator<typename Map<K, V>::JEntry> i = entrySet()->iterator();
                if(! i->hasNext())
                    return jstr(u"{}");

                cpp::lang::JStringBuilder sb = jnew<cpp::lang::StringBuilder>();
                sb->append(u'{');
                for(;;) {
                    typename Map<K, V>::JEntry e = i->next();
                    K key = e->getKey();
                    V value = e->getValue();
                    // TODO: sb.append(key   == this ? jstr(u"(this Map)") : key);
                    sb->append(u'=');
                    // TODO: sb.append(value == this ? jstr(u"(this Map)") : value);
                    if(!i->hasNext())
                        return sb->append(u'}')->toString();
                    sb->append(u',')->append(u' ');
                }
            }

        protected:
            class AnonymousIteratorSet : public Iterator<K>
            {
            public:
                AnonymousIteratorSet(AbstractMap<K, V> *parent) : i(parent->entrySet()->iterator()) { }

                virtual jboolean hasNext() {
                    return i->hasNext();
                }

                virtual K next() {
                    return i->next()->getKey();
                }

                virtual void remove() {
                    i->remove();
                }

            private:
                JIterator<typename Map<K, V>::JEntry> i;
            };

            class AnonymousIteratorCollection : public Iterator<V>
            {
            public:
                AnonymousIteratorCollection(AbstractMap<K, V> *parent) : i(parent->entrySet()->iterator()) { }

                virtual jboolean hasNext() {
                    return i->hasNext();
                }

                virtual V next() {
                    return i->next()->getValue();
                }

                virtual void remove()  {
                    i->remove();
                }

            private:
                JIterator<typename Map<K, V>::JEntry> i;
            };

            class AnonymousAbstractSet : public AbstractSet<K>
            {
            public:
                AnonymousAbstractSet(AbstractMap<K, V> *parent) : parent(parent) { }

                virtual JIterator<K> iterator() {
                    return jnew<AnonymousIteratorSet>(parent);
                }

                virtual jint size() {
                    return parent->size();
                }

                jboolean isEmpty() {
                    return parent->isEmpty();
                }

                void clear() {
                    parent->clear();
                }

                jboolean contains(cpp::lang::JObject k) {
                    return parent->containsKey(k);
                }

            private:
                AbstractMap<K, V> *parent;
            };

            class AnonymousAbstractCollection : public AbstractCollection<V>
            {
            public:
                AnonymousAbstractCollection(AbstractMap<K, V> *parent) : parent(parent) { }
                
                virtual JIterator<V> iterator() {
                    return jnew<AnonymousIteratorCollection>(parent);
                }

                virtual jint size() {
                    return parent->size();
                }

                jboolean isEmpty() {
                    return parent->isEmpty();
                }

                void clear() {
                    parent->clear();
                }

                jboolean contains(cpp::lang::JObject v) {
                    return parent->containsValue(v);
                }

            private:
                AbstractMap<K, V> *parent;
            };

            AbstractMap() {}

        protected:
            JSet<K> _keySet;
            JCollection<V> _values;
        };
    }
}

#endif // CPP_UTIL_ABSTRACTMAP_H
