#ifndef CPP_UTIL_ABSTRACTSET_H
#define CPP_UTIL_ABSTRACTSET_H

#include "AbstractCollection.h"
#include "Set.h"
#include "../lang/ClassCastException.h"

namespace cpp
{
    namespace util
    {
        template <typename E>
        class AbstractSet : public AbstractCollection<E>, public Set<E>
        {
        public:
            virtual JIterator<E> iterator() = 0;
            virtual jint size() = 0;

            virtual jboolean equals(cpp::lang::JObject o) {
                if(o.get() == this)
                    return true;

                JCollection<E> c = std::dynamic_pointer_cast<Collection<E>>(o);
                if(c == nullptr)
                    return false;

                if(c->size() != size())
                    return false;

                try {
                    return containsAll(c);
                }
                catch(cpp::lang::ClassCastException& unused)   {
                    return false;
                }
                catch(cpp::lang::NullPointerException& unused) {
                    return false;
                }
            }

            virtual jint hashCode() {
                jint h = 0;
                JIterator<E> i = iterator();
                while(i->hasNext()) {
                    E obj = i->next();
                    if(obj != nullptr)
                        h += obj->hashCode();
                }
                return h;
            }

            virtual jboolean removeAll(JCollection<E> c) {
                Objects::requireNonNull(c);
                jboolean modified = false;

                if(size() > c->size()) {
                    for(JIterator<E> i = c->iterator(); i->hasNext();)
                        modified |= remove(i->next());
                }
                else {
                    for(JIterator<E> i = iterator(); i->hasNext();) {
                        if(c->contains(i->next())) {
                            i->remove();
                            modified = true;
                        }
                    }
                }
                return modified;
            }
            
            virtual void clear() {
                AbstractCollection<E>::clear();
            }
            
            virtual jboolean contains(cpp::lang::JObject o) {
                return AbstractCollection<E>::contains(o);
            }
            
            virtual jboolean isEmpty() {
                return AbstractCollection<E>::isEmpty();
            }

            virtual jboolean add(E e) {
                return AbstractCollection<E>::add(e);
            }

            virtual jboolean addAll(JCollection<E> c) {
                return AbstractCollection<E>::addAll(c);
            }

            virtual jboolean containsAll(JCollection<E> c) {
                return AbstractCollection<E>::containsAll(c);
            }

            virtual jboolean remove(cpp::lang::JObject o) {
                return AbstractCollection<E>::remove(o);
            }

            virtual jboolean retainAll(JCollection<E> c) {
                return AbstractCollection<E>::retainAll(c);
            }

            virtual jarray<cpp::lang::JObject> toArray() {
                return AbstractCollection<E>::toArray();
            }

            virtual jarray<E> toArray(jarray<E> a) {
                return AbstractCollection<E>::toArray(a);
            }
        };
    }
}

#endif // CPP_UTIL_ABSTRACTSET_H
