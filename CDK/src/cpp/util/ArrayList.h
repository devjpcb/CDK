#ifndef ARRAYLIST_H
#define ARRAYLIST_H

#include "AbstractList.h"
#include "RandomAccess.h"
#include "../lang/Math.h"
#include "../lang/System.h"

namespace cpp
{
    namespace util
    {
        template <typename E>
        class ArrayList : public AbstractList<E>, public RandomAccess
        {
        public:
            using std::enable_shared_from_this<AbstractList<E>>::shared_from_this;
        
            ArrayList() : AbstractList<E>(), _size(0) {
                this->_elementData = EMPTY_ELEMENTDATA;
            }
            
            ArrayList(jint initialCapacity) : _size(0) {
                if (initialCapacity < 0)
                    throw cpp::lang::IllegalArgumentException(jstr(u"Illegal Capacity: ") + boxing(initialCapacity));
                this->_elementData.create(initialCapacity);
            }
            
            ArrayList(JCollection<E> c) {
                /* TODO: se implemento sin el uso de Class*/
                _elementData = c->toArray();
                _size = _elementData.length();
                // c.toArray might (incorrectly) not return Object[] (see 6260652)
                // if (_elementData.getClass() != Object[].class)
                    // _elementData = Arrays.copyOf(_elementData, size, Object[].class);
            }
            
            ~ArrayList() {}
            
            void finalize() {
                AbstractList<E>::finalize();
            }
            
            jboolean add(E e) {
                ensureCapacityInternal(_size + 1);  // Increments modCount!!
                _elementData[_size++] = e;
                return true;
            }
            
            void add(jint index, E element) {
                rangeCheckForAdd(index);

                ensureCapacityInternal(_size + 1);  // Increments modCount!!
                cpp::lang::System::arraycopy(_elementData, index, _elementData, index + 1, _size - index);
                _elementData[index] = element;
                _size++;
            }
            
            jboolean addAll(JCollection<E> c) {
                jarray<cpp::lang::JObject> a = c->toArray();
                jint numNew = a.length();
                ensureCapacityInternal(_size + numNew);  // Increments modCount
                cpp::lang::System::arraycopy(a, 0, _elementData, _size, numNew);
                _size += numNew;
                return numNew != 0;
            }

            jboolean addAll(jint index, JCollection<E> c) {
                rangeCheckForAdd(index);

                jarray<cpp::lang::JObject> a = c->toArray();
                jint numNew = a.length();
                ensureCapacityInternal(_size + numNew);  // Increments modCount

                jint numMoved = _size - index;
                if (numMoved > 0)
                    cpp::lang::System::arraycopy(_elementData, index, _elementData, index + numNew, numMoved);

                cpp::lang::System::arraycopy(a, 0, _elementData, index, numNew);
                _size += numNew;
                return numNew != 0;
            }
            
            void clear() {
                modCount++;

                // clear to let GC do its work
                for (jint i = 0; i < _size; i++)
                    _elementData[i] = nullptr;

                _size = 0;
            }
            
            jboolean contains(cpp::lang::JObject o) {
                return indexOf(o) >= 0;
            }
            
            void ensureCapacity(jint minCapacity) {
                jint minExpand = (elementData != EMPTY_ELEMENTDATA)
                    // any size if real element table
                    ? 0
                    // larger than default for empty table. It's already supposed to be
                    // at default size.
                    : DEFAULT_CAPACITY;

                if (minCapacity > minExpand) {
                    ensureExplicitCapacity(minCapacity);
                }
            }
            
            E get(jint index) {
                rangeCheck(index);

                return elementData(index);
            }
            
            jint indexOf(cpp::lang::JObject o) {
                if (o == nullptr) {
                    for (jint i = 0; i < _size; i++)
                        if (_elementData[i] == nullptr)
                            return i;
                } else {
                    for (jint i = 0; i < _size; i++)
                        if (o->equals(_elementData[i]))
                            return i;
                }
                return -1;
            }
            
            jboolean isEmpty() {
                return _size == 0;
            }
            
            JIterator<E> iterator() {
                return jnew<Itr>(this);
            }
            
            jint lastIndexOf(cpp::lang::JObject o) {
                if (o == nullptr) {
                    for (jint i = _size - 1; i >= 0; i--)
                        if (_elementData[i] == nullptr)
                            return i;
                } else {
                    for (jint i = _size - 1; i >= 0; i--)
                        if (o->equals(_elementData[i]))
                            return i;
                }
                return -1;
            }
            
            JListIterator<E> listIterator() {
                return jnew<ListItr>(this, 0);
            }
            
            JListIterator<E> listIterator(jint index) {
                if (index < 0 || index > _size)
                    throw cpp::lang::IndexOutOfBoundsException(jstr(u"Index: ") + boxing(index));
                return jnew<ListItr>(this, index);
            }
            
            E remove(jint index) {
                rangeCheck(index);

                modCount++;
                E oldValue = elementData(index);

                jint numMoved = _size - index - 1;
                if (numMoved > 0)
                    cpp::lang::System::arraycopy(_elementData, index + 1, _elementData, index, numMoved);
                _elementData[--_size] = nullptr; // clear to let GC do its work

                return oldValue;
            }
            
            jboolean remove(cpp::lang::JObject o) {
                if (o == nullptr) {
                    for (jint index = 0; index < _size; index++)
                        if (_elementData[index] == nullptr) {
                            fastRemove(index);
                            return true;
                        }
                } else {
                    for (jint index = 0; index < _size; index++)
                        if (o->equals(_elementData[index])) {
                            fastRemove(index);
                            return true;
                        }
                }
                return false;
            }
            
            jboolean removeAll(JCollection<E> c) {
                Objects::requireNonNull(c);
                return batchRemove(c, false);
            }
            
            jboolean retainAll(JCollection<E> c) {
                Objects::requireNonNull(c);
                return batchRemove(c, true);
            }
            
            E set(jint index, E element) {
                rangeCheck(index);

                E oldValue = elementData(index);
                _elementData[index] = element;
                return oldValue;
            }
            
            jint size() {
                return _size;
            }

            JList<E> subList(jint fromIndex, jint toIndex) {
                subListRangeCheck(fromIndex, toIndex, _size);
                return jnew<SubList>(shared_from_this(), 0, fromIndex, toIndex);
            }
            
            jarray<cpp::lang::JObject> toArray() {
                return Arrays::copyOf(_elementData, _size);
            }
            
            jarray<E> toArray(jarray<E> a) {
                if (a.length() < _size)
                    // Make a new array of a's runtime type, but my contents:
                    return jarray_dynamic_pointer_cast<typename E::element_type>(Arrays::copyOf(_elementData, _size));
                cpp::lang::System::arraycopy(jarray_dynamic_pointer_cast<typename E::element_type>(_elementData), 0, a, 0, _size);
                if (a.length() > _size)
                    a[_size] = nullptr;
                return a;
            }
            
            void trimToSize() {
                modCount++;
                if (_size < _elementData.length()) {
                    _elementData = Arrays::copyOf(_elementData, size);
                }
            }
            
        protected:
            void removeRange(jint fromIndex, jint toIndex) {
                modCount++;
                jint numMoved = _size - toIndex;
                cpp::lang::System::arraycopy(_elementData, toIndex, _elementData, fromIndex, numMoved);

                // clear to let GC do its work
                jint newSize = _size - (toIndex-fromIndex);
                for (jint i = newSize; i < _size; i++) {
                    _elementData[i] = nullptr;
                }
                _size = newSize;
            }

            
        private:
            class Itr : public Iterator<E> 
            {
            public:
                Itr(ArrayList<E> *parent) : cursor(0), lastRet(-1), expectedModCount(parent->modCount), parent(parent) {}
            
                jboolean hasNext() {
                    return cursor != parent->_size;
                }

                E next() {
                    checkForComodification();
                    jint i = cursor;
                    if (i >= parent->_size)
                        throw NoSuchElementException();
                    jarray<cpp::lang::JObject> elementData = parent->_elementData;
                    if (i >= elementData.length())
                        throw ConcurrentModificationException();
                    cursor = i + 1;
                    return std::dynamic_pointer_cast<typename E::element_type>(elementData[lastRet = i]);
                }

                void remove() {
                    if (lastRet < 0)
                        throw cpp::lang::IllegalStateException();
                    checkForComodification();

                    try {
                        parent->remove(lastRet);
                        cursor = lastRet;
                        lastRet = -1;
                        expectedModCount = parent->modCount;
                    } catch (cpp::lang::IndexOutOfBoundsException& ex) {
                        throw ConcurrentModificationException();
                    }
                }

                void checkForComodification() {
                    if (parent->modCount != expectedModCount)
                        throw ConcurrentModificationException();
                }
                
                jint cursor;
                jint lastRet;
                jint expectedModCount;
                ArrayList<E> *parent;
            };
            
            class ListItr : public Itr, public ListIterator<E> 
            {
            public:
                ListItr(ArrayList<E> *parent, jint index) : Itr(parent) {
                    cursor = index;
                }
                
                void finalize() {
                    Itr::finalize();
                }

                jboolean hasPrevious() {
                    return cursor != 0;
                }

                jint nextIndex() {
                    return cursor;
                }

                jint previousIndex() {
                    return cursor - 1;
                }

                E previous() {
                    checkForComodification();
                    jint i = cursor - 1;
                    if (i < 0)
                        throw NoSuchElementException();
                    jarray<cpp::lang::JObject> elementData = parent->_elementData;
                    if (i >= elementData.length())
                        throw ConcurrentModificationException();
                    cursor = i;
                    return std::dynamic_pointer_cast<typename E::element_type>(elementData[lastRet = i]);
                }

                void set(E e) {
                    if (lastRet < 0)
                        throw cpp::lang::IllegalStateException();
                    checkForComodification();

                    try {
                        parent->set(lastRet, e);
                    } catch (cpp::lang::IndexOutOfBoundsException& ex) {
                        throw ConcurrentModificationException();
                    }
                }

                void add(E e) {
                    checkForComodification();

                    try {
                        jint i = cursor;
                        parent->add(i, e);
                        cursor = i + 1;
                        lastRet = -1;
                        expectedModCount = parent->modCount;
                    } catch (cpp::lang::IndexOutOfBoundsException& ex) {
                        throw ConcurrentModificationException();
                    }
                }
                
                jboolean hasNext() {
                    return Itr::hasNext();
                }
                
                E next() {
                    return Itr::next();
                }
                
                void remove() {
                    Itr::remove();
                }
                
                using Itr::cursor;
                using Itr::lastRet;
                using Itr::expectedModCount;
                using Itr::parent;
                using Itr::checkForComodification;
            };
            
            class SubList : public AbstractList<E>, public RandomAccess 
            {
            public:
                SubList(JAbstractList<E> parent, jint offset, jint fromIndex, jint toIndex) {
                    this->ArrayList_this = std::dynamic_pointer_cast<ArrayList<E>>(parent);
                    this->parent = parent;
                    this->parentOffset = fromIndex;
                    this->offset = offset + fromIndex;
                    this->_size = toIndex - fromIndex;
                    this->modCount = ArrayList_this->modCount;
                }
                
                void finalize() {
                    AbstractList<E>::finalize();
                }

                E set(jint index, E e) {
                    rangeCheck(index);
                    checkForComodification();
                    E oldValue = ArrayList_this->elementData(offset + index);
                    ArrayList_this->_elementData[offset + index] = e;
                    return oldValue;
                }

                E get(jint index) {
                    rangeCheck(index);
                    checkForComodification();
                    return ArrayList_this->elementData(offset + index);
                }

                jint size() {
                    checkForComodification();
                    return this->_size;
                }

                void add(jint index, E e) {
                    rangeCheckForAdd(index);
                    checkForComodification();
                    parent->add(parentOffset + index, e);
                    this->modCount = ArrayList_this->modCount;
                    this->_size++;
                }

                E remove(jint index) {
                    rangeCheck(index);
                    checkForComodification();
                    E result = parent->remove(parentOffset + index);
                    this->modCount = ArrayList_this->modCount;
                    this->_size--;
                    return result;
                }

                void removeRange(jint fromIndex, jint toIndex) {
                    checkForComodification();
                    parent->removeRange(parentOffset + fromIndex, parentOffset + toIndex);
                    this->modCount = parent->modCount;
                    this->size -= toIndex - fromIndex;
                }

                jboolean addAll(JCollection<E> c) {
                    return addAll(this->_size, c);
                }

                jboolean addAll(jint index, JCollection<E> c) {
                    rangeCheckForAdd(index);
                    jint cSize = c->size();
                    if (cSize == 0)
                        return false;

                    checkForComodification();
                    parent->addAll(parentOffset + index, c);
                    this->modCount = ArrayList_this->modCount;
                    this->_size += cSize;
                    return true;
                }

                JIterator<E> iterator() {
                    return AbstractList<E>::listIterator();
                }

                JListIterator<E> listIterator(jint index) {
                    checkForComodification();
                    rangeCheckForAdd(index);
                    jint offset = this->offset;
                    
                    return jnew<SubListListIterator>(this, ArrayList_this, index, offset);
                }

                JList<E> subList(jint fromIndex, jint toIndex) {
                    ArrayList_this->subListRangeCheck(fromIndex, toIndex, _size);
                    return jnew<SubList>(shared_from_this(), offset, fromIndex, toIndex);
                }

                void rangeCheck(jint index) {
                    if (index < 0 || index >= this->_size)
                        throw cpp::lang::IndexOutOfBoundsException(outOfBoundsMsg(index));
                }

                void rangeCheckForAdd(jint index) {
                    if (index < 0 || index > this->_size)
                        throw cpp::lang::IndexOutOfBoundsException(outOfBoundsMsg(index));
                }

                cpp::lang::JString outOfBoundsMsg(jint index) {
                    return jstr(u"Index: ") + boxing(index) + jstr(u", Size: ") + boxing(this->_size);
                }

                void checkForComodification() {
                    if (ArrayList_this->modCount != this->modCount)
                        throw ConcurrentModificationException();
                }
                
            private:
                class SubListListIterator : public ListIterator<E> 
                {
                public:
                    SubListListIterator(SubList *SubList_this, JArrayList<E> ArrayList_this, jint index, jint offset) 
                        : SubList_this(SubList_this), ArrayList_this(ArrayList_this), cursor(index), lastRet(-1), expectedModCount(ArrayList_this->modCount), offset(offset)
                    {
                    }
                
                    jboolean hasNext() {
                        return cursor != SubList_this->_size;
                    }

                    E next() {
                        checkForComodification();
                        jint i = cursor;
                        if (i >= SubList_this->_size)
                            throw NoSuchElementException();
                        jarray<cpp::lang::JObject> elementData = ArrayList_this->_elementData;
                        if (offset + i >= elementData.length())
                            throw ConcurrentModificationException();
                        cursor = i + 1;
                        return std::dynamic_pointer_cast<typename E::element_type>(elementData[offset + (lastRet = i)]);
                    }

                    jboolean hasPrevious() {
                        return cursor != 0;
                    }

                    E previous() {
                        checkForComodification();
                        jint i = cursor - 1;
                        if (i < 0)
                            throw NoSuchElementException();
                        jarray<cpp::lang::JObject> elementData = ArrayList_this->_elementData;
                        if (offset + i >= elementData.length())
                            throw ConcurrentModificationException();
                        cursor = i;
                        return std::dynamic_pointer_cast<typename E::element_type>(elementData[offset + (lastRet = i)]);
                    }

                    jint nextIndex() {
                        return cursor;
                    }

                    jint previousIndex() {
                        return cursor - 1;
                    }

                    void remove() {
                        if (lastRet < 0)
                            throw cpp::lang::IllegalStateException();
                        checkForComodification();

                        try {
                            SubList_this->remove(lastRet);
                            cursor = lastRet;
                            lastRet = -1;
                            expectedModCount = ArrayList_this->modCount;
                        } catch (cpp::lang::IndexOutOfBoundsException& ex) {
                            throw ConcurrentModificationException();
                        }
                    }

                    void set(E e) {
                        if (lastRet < 0)
                            throw cpp::lang::IllegalStateException();
                        checkForComodification();

                        try {
                            ArrayList_this->set(offset + lastRet, e);
                        } catch (cpp::lang::IndexOutOfBoundsException& ex) {
                            throw ConcurrentModificationException();
                        }
                    }

                    void add(E e) {
                        checkForComodification();

                        try {
                            jint i = cursor;
                            SubList_this->add(i, e);
                            cursor = i + 1;
                            lastRet = -1;
                            expectedModCount = ArrayList_this->modCount;
                        } catch (cpp::lang::IndexOutOfBoundsException& ex) {
                            throw ConcurrentModificationException();
                        }
                    }

                    void checkForComodification() {
                        if (expectedModCount != ArrayList_this->modCount)
                            throw ConcurrentModificationException();
                    }
                    
                private:
                    SubList *SubList_this;
                    JArrayList<E> ArrayList_this;
                    jint cursor;
                    jint lastRet;
                    jint expectedModCount;
                    jint offset;
                };
            
                JArrayList<E> ArrayList_this;
                JAbstractList<E> parent;
                jint parentOffset;
                jint offset;
                jint _size;
                using AbstractList<E>::modCount;
            };
        
            void ensureCapacityInternal(jint minCapacity) {
                if (_elementData == EMPTY_ELEMENTDATA) {
                    minCapacity = cpp::lang::Math::max(DEFAULT_CAPACITY, minCapacity);
                }

                ensureExplicitCapacity(minCapacity);
            }

            void ensureExplicitCapacity(jint minCapacity) {
                modCount++;

                // overflow-conscious code
                if (minCapacity - _elementData.length() > 0)
                    grow(minCapacity);
            }
            
            void grow(jint minCapacity) {
                // overflow-conscious code
                jint oldCapacity = _elementData.length();
                jint newCapacity = oldCapacity + (oldCapacity >> 1);
                if (newCapacity - minCapacity < 0)
                    newCapacity = minCapacity;
                if (newCapacity - MAX_ARRAY_SIZE > 0)
                    newCapacity = hugeCapacity(minCapacity);
                // minCapacity is usually close to size, so this is a win:
                _elementData = Arrays::copyOf(_elementData, newCapacity);
            }
            
            jint hugeCapacity(jint minCapacity) {
                if (minCapacity < 0) // overflow
                    throw cpp::lang::OutOfMemoryError();
                return (minCapacity > MAX_ARRAY_SIZE) ?
                    cpp::lang::Integer::MAX_VALUE : MAX_ARRAY_SIZE;
            }
            
            cpp::lang::JString outOfBoundsMsg(jint index) {
                return jstr(u"Index: ") + boxing(index) + jstr(u", Size: ") + boxing(_size);
            }
            
            void rangeCheck(jint index) {
                if (index >= _size)
                    throw cpp::lang::IndexOutOfBoundsException(outOfBoundsMsg(index));
            }
            
            void rangeCheckForAdd(jint index) {
                if (index > _size || index < 0)
                    throw cpp::lang::IndexOutOfBoundsException(outOfBoundsMsg(index));
            }
            
            void subListRangeCheck(jint fromIndex, jint toIndex, jint size) {
                if (fromIndex < 0)
                    throw cpp::lang::IndexOutOfBoundsException(jstr(u"fromIndex = ") + boxing(fromIndex));
                if (toIndex > size)
                    throw cpp::lang::IndexOutOfBoundsException(jstr(u"toIndex = ") + boxing(toIndex));
                if (fromIndex > toIndex)
                    throw cpp::lang::IllegalArgumentException(jstr(u"fromIndex(") + boxing(fromIndex) + jstr(u") > toIndex(") + boxing(toIndex) + jstr(u")"));
            }
            
            E elementData(jint index) {
                return std::dynamic_pointer_cast<typename E::element_type>(_elementData[index]);
            }
            
            void fastRemove(jint index) {
                modCount++;
                jint numMoved = _size - index - 1;
                if (numMoved > 0)
                    cpp::lang::System::arraycopy(_elementData, index + 1, _elementData, index, numMoved);
                _elementData[--_size] = nullptr; // clear to let GC do its work
            }
            
            jboolean batchRemove(JCollection<E> c, jboolean complement) {
                jarray<cpp::lang::JObject> elementData = this->_elementData;
                jint r = 0, w = 0;
                jboolean modified = false;
                
                for (; r < _size; r++)
                    if (c->contains(elementData[r]) == complement)
                        elementData[w++] = elementData[r];

                if (r != _size) {
                    cpp::lang::System::arraycopy(elementData, r, elementData, w, _size - r);
                    w += _size - r;
                }
                
                if (w != _size) {
                    // clear to let GC do its work
                    for (jint i = w; i < _size; i++)
                        elementData[i] = nullptr;
                    modCount += _size - w;
                    _size = w;
                    modified = true;
                }

                return modified;
            }
        
            static const jint DEFAULT_CAPACITY;
            static const jarray<cpp::lang::JObject> EMPTY_ELEMENTDATA;
            static const jint MAX_ARRAY_SIZE;
            jarray<cpp::lang::JObject> _elementData;
            jint _size;
            using AbstractList<E>::modCount;
        };
        
        template <typename E>
        const jint ArrayList<E>::DEFAULT_CAPACITY = 10;
        
        template <typename E>
        const jarray<cpp::lang::JObject> ArrayList<E>::EMPTY_ELEMENTDATA(0);
        
        template <typename E>
        const jint ArrayList<E>::MAX_ARRAY_SIZE = cpp::lang::Integer::MAX_VALUE - 8;
        
        template <typename T>
        cpp::lang::ForeachIterator<T> begin(JArrayList<T> iterator)
        {
            return iterator->begin();
        }
        
        template <typename T>
        cpp::lang::ForeachIterator<T> end(JArrayList<T> iterator)
        {
            return iterator->end();
        }
    }
}

#endif // ARRAYLIST_H
