#include "Arrays.h"
#include "../lang/String.h"
#include "../lang/Integer.h"
#include "../lang/IllegalArgumentException.h"
#include "../lang/ArrayIndexOutOfBoundsException.h"

using namespace cpp::lang;

namespace cpp
{
    namespace util
    {
        jarray<jchar> Arrays::copyOf(jarray<jchar> original, jint newLength)
        {
            jarray<jchar> copy(newLength);
            System::arraycopy(original, 0, copy, 0, Math::min(original.length(), newLength));

            return copy;
        }

        jarray<jint> Arrays::copyOf(jarray<jint> original, jint newLength)
        {
            jarray<jint> copy(newLength);
            System::arraycopy(original, 0, copy, 0, Math::min(original.length(), newLength));
            return copy;
        }

        jarray<jchar> Arrays::copyOfRange(jarray<jchar> original, jint from, jint to)
        {
            jint newLength = to - from;
            if(newLength < 0) {
                throw IllegalArgumentException(boxing(from) + jstr(u" > ") + boxing(to));
            }
            jarray<jchar> copy(newLength);
            System::arraycopy(original, from, copy, 0, Math::min(original.length() - from, newLength));

            return copy;
        }

        void Arrays::fill(jarray<jchar> a, jint fromIndex, jint toIndex, jchar val)
        {
            rangeCheck(a.length(), fromIndex, toIndex);
            for(jint i = fromIndex; i < toIndex; i++)
                a[i] = val;
        }

        void Arrays::fill(jarray<jint> a, jint fromIndex, jint toIndex, jint val)
        {
            rangeCheck(a.length(), fromIndex, toIndex);
            for(jint i = fromIndex; i < toIndex; i++)
                a[i] = val;
        }

        void Arrays::rangeCheck(jint arrayLength, jint fromIndex, jint toIndex)
        {
            if(fromIndex > toIndex) {
                throw IllegalArgumentException(jstr(u"fromIndex(") + boxing(fromIndex) + jstr(u") > toIndex(") + boxing(toIndex) + jstr(u")"));
            }
            if(fromIndex < 0) {
                throw ArrayIndexOutOfBoundsException(fromIndex);
            }
            if(toIndex > arrayLength) {
                throw ArrayIndexOutOfBoundsException(toIndex);
            }
        }
    }
}
