#ifndef CPP_UTIL_ARRAYS_H
#define CPP_UTIL_ARRAYS_H

#include "../../PrimitiveDataTypes.h"
#include "../lang/Array.h"
#include "../lang/Math.h"
#include "../lang/System.h"

namespace cpp
{
    namespace util
    {
        class Arrays
        {
        public:
            Arrays() = delete;

            static jarray<jchar> copyOf(jarray<jchar> original, jint newLength);
            static jarray<jint> copyOf(jarray<jint> original, jint newLength);

            template<typename T>
            static jarray<T> copyOf(jarray<T> original, jint newLength) {
                // TODO: hay que pensar como implementar Class Parcialmente.
                // implementación temporal
                jarray<T> copy(newLength);
                cpp::lang::System::arraycopy(original, 0, copy, 0, cpp::lang::Math::min(original.length(), newLength));
                return copy;
            }

            static jarray<jchar> copyOfRange(jarray<jchar> original, jint from, jint to);
            static void fill(jarray<jchar> a, jint fromIndex, jint toIndex, jchar val);
            static void fill(jarray<jint> a, jint fromIndex, jint toIndex, jint val);

        private:
            static void rangeCheck(jint arrayLength, jint fromIndex, jint toIndex);
        };
    }
}

#endif // CPP_UTIL_ARRAYS_H
