#ifndef CPP_UTIL_COLLECTION_H
#define CPP_UTIL_COLLECTION_H

#include "../lang/Iterable.h"
#include "../lang/Array.h"

namespace cpp
{
    namespace util
    {
        template <typename E> 
        class Collection : public cpp::lang::Iterable<E>
        {
        public:
            virtual jboolean add(E e) = 0;
            virtual jboolean addAll(JCollection<E> c) = 0;
            virtual void clear() = 0;
            virtual jboolean contains(cpp::lang::JObject o) = 0;
            virtual jboolean containsAll(JCollection<E> c) = 0;
            virtual jboolean equals(cpp::lang::JObject o) = 0;
            virtual jint hashCode() = 0;
            virtual jboolean isEmpty() = 0;
            virtual JIterator<E> iterator() = 0;
            virtual jboolean remove(cpp::lang::JObject o) = 0;
            virtual jboolean removeAll(JCollection<E> c) = 0;
            virtual jboolean retainAll(JCollection<E> c) = 0;
            virtual jint size() = 0;
            virtual jarray<cpp::lang::JObject> toArray() = 0;
            virtual jarray<E> toArray(jarray<E> a) = 0;
        };
        
        template <typename T>
        cpp::lang::ForeachIterator<T> begin(JCollection<T> iterator)
        {
            return iterator->begin();
        }
        
        template <typename T>
        cpp::lang::ForeachIterator<T> end(JCollection<T> iterator)
        {
            return iterator->end();
        }
    }
}

#endif // CPP_UTIL_COLLECTION_H
