#ifndef CPP_UTIL_COLLECTIONS_H
#define CPP_UTIL_COLLECTIONS_H

#include "Set.h"
#include "Collection.h"
#include "Enumeration.h"
#include "Iterator.h"

namespace cpp
{
    namespace util
    {
        class Collections
        {
        public:
            template<typename T>
            static JSet<T> synchronizedSet(JSet<T> s) {
                return s;//TODO: new SynchronizedSet<>(s);
            }
            
            template<typename T>
            static JSet<T> synchronizedSet(JSet<T> s, cpp::lang::JObject mutex) {
                return s;//TODO: new SynchronizedSet<>(s, mutex);
            }
            
            template<typename T>
            static JCollection<T> synchronizedCollection(JCollection<T> c) {
                return c;//TODO: SynchronizedCollection<>(c);
            }
            
            template<typename T>
            static JCollection<T> synchronizedCollection(JCollection<T> c, cpp::lang::JObject mutex) {
                return c;//TODO: SynchronizedCollection<>(c, mutex);
            }
            
            template<typename T>
            static JEnumeration<T> emptyEnumeration() {
                return nullptr;//(Enumeration<T>) EmptyEnumeration.EMPTY_ENUMERATION;
            }
            
            template<typename T>
            static JIterator<T> emptyIterator() {
                return nullptr;//(Iterator<T>) EmptyIterator.EMPTY_ITERATOR;
            }
        };
    }
}

#endif // CPP_UTIL_COLLECTIONS_H
