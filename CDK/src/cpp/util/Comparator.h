#ifndef CPP_UTIL_COMPARATOR_H
#define CPP_UTIL_COMPARATOR_H

#include "../lang/Object.h"

namespace cpp
{
    namespace util
    {
        template <typename T>
        class Comparator : public cpp::lang::Object
        {
        public:
            virtual int compare(T o1, T o2) = 0;
            virtual jboolean equals(cpp::lang::JObject obj) = 0;
        };
    }
}

#endif // CPP_UTIL_COMPARATOR_H
