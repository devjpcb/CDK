#include "ConcurrentModificationException.h"

using namespace cpp::lang;

namespace cpp
{
    namespace util
    {
        ConcurrentModificationException::ConcurrentModificationException() : RuntimeException()
        {
        }

        ConcurrentModificationException::ConcurrentModificationException(JString message) : RuntimeException(message)
        {
        }
        
        ConcurrentModificationException::ConcurrentModificationException(Throwable& cause) : RuntimeException(cause)
        {
        }
        
        ConcurrentModificationException::ConcurrentModificationException(JString message, Throwable& cause) : RuntimeException(message, cause)
        {
        }

        ConcurrentModificationException::~ConcurrentModificationException()
        {
        }
    }
}
