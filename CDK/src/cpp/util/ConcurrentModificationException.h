#ifndef CPP_UTIL_CONCURRENTMODIFICATIONEXCEPTION_H
#define CPP_UTIL_CONCURRENTMODIFICATIONEXCEPTION_H

#include "../lang/RuntimeException.h"

namespace cpp
{
    namespace util
    {
        class ConcurrentModificationException : public cpp::lang::RuntimeException
        {
        public:
            ConcurrentModificationException();
            ConcurrentModificationException(cpp::lang::JString message);
            ConcurrentModificationException(Throwable& cause);
            ConcurrentModificationException(cpp::lang::JString message, Throwable& cause);
            ~ConcurrentModificationException();
        };
    }
}

#endif // CPP_UTIL_CONCURRENTMODIFICATIONEXCEPTION_H
