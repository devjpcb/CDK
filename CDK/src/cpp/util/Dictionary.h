#ifndef CPP_UTIL_DICTIONARY_H
#define CPP_UTIL_DICTIONARY_H

#include "../lang/Object.h"

namespace cpp
{
    namespace util
    {
        template<typename K, typename V>
        class Dictionary : public virtual cpp::lang::Object
        {
        public:
            virtual jint size() = 0;
            virtual jboolean isEmpty() = 0;
            virtual JEnumeration<K> keys() = 0;
            virtual JEnumeration<V> elements() = 0;
            virtual V get(cpp::lang::JObject key) = 0;
            virtual V put(K key, V value) = 0;
            virtual V remove(cpp::lang::JObject key) = 0;
        };
    }
}

#endif // CPP_UTIL_DICTIONARY_H
