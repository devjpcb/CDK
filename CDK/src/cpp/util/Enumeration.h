#ifndef CPP_UTIL_ENUMERATION_H
#define CPP_UTIL_ENUMERATION_H

#include "../lang/Object.h"

namespace cpp
{
    namespace util
    {
        template<typename E>
        class Enumeration : public virtual cpp::lang::Object
        {
        public:
            virtual jboolean hasMoreElements() = 0;
            virtual E nextElement() = 0;
        };
    }
}

#endif // CPP_UTIL_ENUMERATION_H
