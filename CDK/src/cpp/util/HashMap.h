#ifndef CPP_UTIL_HASHMAP_H
#define CPP_UTIL_HASHMAP_H

#include "AbstractMap.h"
#include "LinkedHashMap.h"
#include "../lang/Float.h"
#include "../lang/IllegalArgumentException.h"
#include "../lang/IllegalStateException.h"
#include "ConcurrentModificationException.h"
#include "NoSuchElementException.h"

namespace cpp
{
    namespace util
    {
        template <typename K, typename V>
        class HashMap : public AbstractMap<K, V>, public std::enable_shared_from_this<HashMap<K,V>>
        {
        public:
            using std::enable_shared_from_this<HashMap<K,V>>::shared_from_this;
        
            HashMap() : _size(0), modCount(0), threshold(0), loadFactor(DEFAULT_LOAD_FACTOR) {}

            HashMap(jint initialCapacity) : HashMap(initialCapacity, DEFAULT_LOAD_FACTOR) {}

            HashMap(jint initialCapacity, jfloat loadFactor) : HashMap() {
                if(initialCapacity < 0)
                    throw cpp::lang::IllegalArgumentException(jstr(u"Illegal initial capacity: ") + boxing(initialCapacity));
                if(initialCapacity > MAXIMUM_CAPACITY)
                    initialCapacity = MAXIMUM_CAPACITY;
                if(loadFactor <= 0 || cpp::lang::Float::isNaN(loadFactor))
                    throw cpp::lang::IllegalArgumentException(jstr(u"Illegal load factor: ") + boxing(loadFactor));
                this->loadFactor = loadFactor;
                this->threshold = tableSizeFor(initialCapacity);
            }

            void clear() {
                jarray<JNode> tab;
                modCount++;
                if((tab = table) != nullptr && _size > 0) {
                    _size = 0;
                    for(jint i = 0; i < tab.length(); ++i)
                        tab[i] = nullptr;
                }
            }
            
            jboolean containsKey(cpp::lang::JObject key) {
                return getNode(hash(key), key) != nullptr;
            }
            
            jboolean containsValue(cpp::lang::JObject value) {
                jarray<JNode> tab; V v;
                if ((tab = table) != nullptr && _size > 0) {
                    for (int i = 0; i < tab.length(); ++i) {
                        for (JNode e = tab[i]; e != nullptr; e = e->next) {
                            if ((v = e->value) == value ||
                                (value != nullptr && value->equals(v)))
                                return true;
                        }
                    }
                }
                return false;
            }
            
            JSet<typename Map<K, V>::JEntry> entrySet() {
                JSet<typename Map<K,V>::JEntry> es;
                return (es = _entrySet) == nullptr ? (_entrySet = jnew<EntrySet>(this)) : es;
            }

            V get(cpp::lang::JObject key) {
                JNode e;
                return (e = getNode(hash(key), key)) == nullptr ? nullptr : e->value;
            }
            
            V getOrDefault(cpp::lang::JObject key, V defaultValue) {
                JNode e;
                return (e = getNode(hash(key), key)) == nullptr ? defaultValue : e->value;
            }
            
            jboolean isEmpty() {
                return _size == 0;
            }
            
            JSet<K> keySet() {
                JSet<K> ks;
                return (ks = _keySet) == nullptr ? (_keySet = jnew<KeySet>(this)) : ks;
            }

            V put(K key, V value) {
                return putVal(hash(key), key, value, false, true);
            }
            
            void putAll(JMap<K, V> m) {
                putMapEntries(m, true);
            }
            
            V putIfAbsent(K key, V value) {
                return putVal(hash(key), key, value, true, true);
            }
            
            V remove(cpp::lang::JObject key) {
                JNode e;
                return (e = removeNode(hash(key), key, nullptr, false, true)) == nullptr ? nullptr : e->value;
            }
            
            jboolean remove(cpp::lang::JObject key, cpp::lang::JObject value) {
                return removeNode(hash(key), key, value, true, true) != nullptr;
            }
            
            V replace(K key, V value) {
                JNode e;
                if ((e = getNode(hash(key), key)) != nullptr) {
                    V oldValue = e->value;
                    e->value = value;
                    afterNodeAccess(e);
                    return oldValue;
                }
                return nullptr;
            }
            
            jboolean replace(K key, V oldValue, V newValue) {
                JNode e; V v;
                if ((e = getNode(hash(key), key)) != nullptr &&
                    ((v = e->value) == oldValue || (v != nullptr && v->equals(oldValue)))) {
                    e->value = newValue;
                    afterNodeAccess(e);
                    return true;
                }
                return false;
            }

            jint size() {
                return _size;
            }
            
            JCollection<V> values() {
                JCollection<V> vs;
                return (vs = _values) == nullptr ? (_values = jnew<Values>(this)) : vs;
            }
            
        private:
            class Node;
            using JNode = std::shared_ptr<Node>;

            class Node : public Map<K, V>::Entry, public std::enable_shared_from_this<Node>
            {
            public:
                using std::enable_shared_from_this<Node>::shared_from_this;
            
                Node(jint hash, K key, V value, JNode next) : hash(hash), key(key), value(value), next(next) {}

                K getKey() {
                    return key;
                }

                V getValue() {
                    return value;
                }

                cpp::lang::JString toString() {
                    return key + jstr(u"=") + value;
                }

                jint hashCode() {
                    return Objects::hashCode(key) ^ Objects::hashCode(value);
                }

                V setValue(V newValue) {
                    V oldValue = value;
                    value = newValue;
                    return oldValue;
                }

                jboolean equals(cpp::lang::JObject o) {
                    typename Map<K, V>::JEntry e = std::dynamic_pointer_cast<typename Map<K, V>::Entry>(o);
                    
                    if(e == shared_from_this())
                        return true;

                    if(e != nullptr) {
                        if(Objects::equals(key, e->getKey()) &&
                           Objects::equals(value, e->getValue()))
                            return true;
                    }
                    return false;
                }

            protected:
                jint hash;
                K key;
                V value;
                JNode next;

                friend class HashMap;
            };

            class TreeNode;
            using JTreeNode = std::shared_ptr<TreeNode>;

            class TreeNode : public LinkedHashMap<K, V>::Entry
            {
            public:
                using std::enable_shared_from_this<Node>::shared_from_this;
                
                TreeNode(jint hash, K key, V val, JNode next) : LinkedHashMap<K, V>::Entry(hash, key, val, next) { }

                JTreeNode root() {
                    for(JTreeNode r = std::dynamic_pointer_cast<TreeNode>(shared_from_this()), p;;) {
                        if((p = r->parent) == nullptr)
                            return r;
                        r = p;
                    }
                }
                
                static void moveRootToFront(jarray<JNode> tab, JTreeNode root) {
                    jint n;
                    if (root != nullptr && tab != nullptr && (n = tab.length()) > 0) {
                        jint index = (n - 1) & root->hash;
                        JTreeNode first = std::dynamic_pointer_cast<TreeNode>(tab[index]);
                        if (root != first) {
                            JNode rn;
                            tab[index] = root;
                            JTreeNode rp = root->prev;
                            if ((rn = root->next) != nullptr) {
                                JTreeNode trn = std::dynamic_pointer_cast<TreeNode>(rn);
                                trn->prev = rp;
                            }
                            if (rp != nullptr)
                                rp->next = rn;
                            if (first != nullptr)
                                first->prev = root;
                            root->next = first;
                            root->prev = nullptr;
                        }
                        // TODO: assert checkInvariants(root);
                    }
                }
                
                // TODO: Cuando la clase Class este implementada cambiar kc
                JTreeNode find(jint h, cpp::lang::JObject k, std::nullptr_t kc) {
                    JTreeNode p = std::dynamic_pointer_cast<TreeNode>(shared_from_this());
                    do {
                        //TODO: original jint ph, dir; K pk;
                        jint ph; K pk;
                        JTreeNode pl = p->left, pr = p->right, q;
                        if ((ph = p->hash) > h)
                            p = pl;
                        else if (ph < h)
                            p = pr;
                        else if ((pk = p->key) == k || (k != nullptr && k->equals(pk)))
                            return p;
                        else if (pl == nullptr)
                            p = pr;
                        else if (pr == nullptr)
                            p = pl;
                        // TODO: cuando este implementada la clase Class
                        //else if ((kc != null ||
                        //          (kc = comparableClassFor(k)) != null) &&
                        //         (dir = compareComparables(kc, k, pk)) != 0)
                        //    p = (dir < 0) ? pl : pr;
                        else if ((q = pr->find(h, k, kc)) != nullptr)
                            return q;
                        else
                            p = pl;
                    } while (p != nullptr);
                    return nullptr;
                }
                
                JTreeNode getTreeNode(jint h, cpp::lang::JObject k) {
                    return ((parent != nullptr) ? root() : std::dynamic_pointer_cast<TreeNode>(shared_from_this())->find(h, k, nullptr));
                }
                
                void treeify(jarray<JNode> tab) {
                    JTreeNode root = nullptr;
                    for (JTreeNode x = std::dynamic_pointer_cast<TreeNode>(shared_from_this()), next; x != nullptr; x = next) {
                        next = std::dynamic_pointer_cast<TreeNode>(x->next);
                        x->left = x->right = nullptr;
                        if (root == nullptr) {
                            x->parent = nullptr;
                            x->red = false;
                            root = x;
                        }
                        else {
                            K k = x->key;
                            jint h = x->hash;
                            // TODO: Class<?> kc = null;
                            for (JTreeNode p = root;;) {
                                jint dir, ph;
                                K pk = p->key;
                                if ((ph = p->hash) > h)
                                    dir = -1;
                                else if (ph < h)
                                    dir = 1;
                                // TODO: cuando este implementada la clase Class
                                //else if ((kc == null &&
                                //          (kc = comparableClassFor(k)) == null) ||
                                //         (dir = compareComparables(kc, k, pk)) == 0)
                                //    dir = tieBreakOrder(k, pk);

                                JTreeNode xp = p;
                                if ((p = (dir <= 0) ? p->left : p->right) == nullptr) {
                                    x->parent = xp;
                                    if (dir <= 0)
                                        xp->left = x;
                                    else
                                        xp->right = x;
                                    root = balanceInsertion(root, x);
                                    break;
                                }
                            }
                        }
                    }
                    
                    moveRootToFront(tab, root);
                }
                
                JNode untreeify(JHashMap<K,V> map) {
                    JNode hd = nullptr, tl = nullptr;
                    for (JNode q = std::dynamic_pointer_cast<TreeNode>(shared_from_this()); q != nullptr; q = q->next) {
                        JNode p = map->replacementNode(q, nullptr);
                        if (tl == nullptr)
                            hd = p;
                        else
                            tl->next = p;
                        tl = p;
                    }
                    return hd;
                }


                JTreeNode putTreeVal(JHashMap<K,V> map, jarray<JNode> tab, jint h, K k, V v) {
                    // TODO: Class<?> kc = null;
                    // jboolean searched = false;
                    JTreeNode _root = (parent != nullptr) ? root() : std::dynamic_pointer_cast<TreeNode>(shared_from_this());
                    for (JTreeNode p = _root;;) {
                        jint dir, ph; K pk;
                        if ((ph = p->hash) > h)
                            dir = -1;
                        else if (ph < h)
                            dir = 1;
                        else if ((pk = p->key) == k || (pk != nullptr && k->equals(pk)))
                            return p;
                        /* TODO:
                        else if ((kc == null &&
                                  (kc = comparableClassFor(k)) == null) ||
                                 (dir = compareComparables(kc, k, pk)) == 0) {
                            if (!searched) {
                                TreeNode<K,V> q, ch;
                                searched = true;
                                if (((ch = p.left) != null &&
                                     (q = ch.find(h, k, kc)) != null) ||
                                    ((ch = p.right) != null &&
                                     (q = ch.find(h, k, kc)) != null))
                                    return q;
                            }
                            dir = tieBreakOrder(k, pk);
                        }
                        */
                        JTreeNode xp = p;
                        if ((p = (dir <= 0) ? p->left : p->right) == nullptr) {
                            JNode xpn = xp->next;
                            JTreeNode x = map->newTreeNode(h, k, v, xpn);
                            if (dir <= 0)
                                xp->left = x;
                            else
                                xp->right = x;
                            xp->next = x;
                            x->parent = x->prev = xp;
                            if (xpn != nullptr)
                                (std::dynamic_pointer_cast<TreeNode>(xpn))->prev = x;
                            moveRootToFront(tab, balanceInsertion(_root, x));
                            return nullptr;
                        }
                    }
                }
                
                void removeTreeNode(JHashMap<K,V> map, jarray<JNode> tab, jboolean movable) {
                    jint n;
                    if (tab == nullptr || (n = tab.length()) == 0)
                        return;
                    jint index = (n - 1) & shared_from_this()->hash;
                    JTreeNode first = std::dynamic_pointer_cast<TreeNode>(tab[index]), root = first, rl;
                    JTreeNode succ = std::dynamic_pointer_cast<TreeNode>(next), pred = prev;
                    if (pred == nullptr)
                        tab[index] = first = succ;
                    else
                        pred->next = succ;
                    if (succ != nullptr)
                        succ->prev = pred;
                    if (first == nullptr)
                        return;
                    if (root->parent != nullptr)
                        root = root->root();
                    if (root == nullptr || root->right == nullptr ||
                        (rl = root->left) == nullptr || rl->left == nullptr) {
                        tab[index] = first->untreeify(map);  // too small
                        return;
                    }
                    JTreeNode p = std::dynamic_pointer_cast<TreeNode>(shared_from_this()), pl = left, pr = right, replacement;
                    if (pl != nullptr && pr != nullptr) {
                        JTreeNode s = pr, sl;
                        while ((sl = s->left) != nullptr) // find successor
                            s = sl;
                        jboolean c = s->red; s->red = p->red; p->red = c; // swap colors
                        JTreeNode sr = s->right;
                        JTreeNode pp = p->parent;
                        if (s == pr) { // p was s's direct parent
                            p->parent = s;
                            s->right = p;
                        }
                        else {
                            JTreeNode sp = s->parent;
                            if ((p->parent = sp) != nullptr) {
                                if (s == sp->left)
                                    sp->left = p;
                                else
                                    sp->right = p;
                            }
                            if ((s->right = pr) != nullptr)
                                pr->parent = s;
                        }
                        p->left = nullptr;
                        if ((p->right = sr) != nullptr)
                            sr->parent = p;
                        if ((s->left = pl) != nullptr)
                            pl->parent = s;
                        if ((s->parent = pp) == nullptr)
                            root = s;
                        else if (p == pp->left)
                            pp->left = s;
                        else
                            pp->right = s;
                        if (sr != nullptr)
                            replacement = sr;
                        else
                            replacement = p;
                    }
                    else if (pl != nullptr)
                        replacement = pl;
                    else if (pr != nullptr)
                        replacement = pr;
                    else
                        replacement = p;
                    if (replacement != p) {
                        JTreeNode pp = replacement->parent = p->parent;
                        if (pp == nullptr)
                            root = replacement;
                        else if (p == pp->left)
                            pp->left = replacement;
                        else
                            pp->right = replacement;
                        p->left = p->right = p->parent = nullptr;
                    }

                    JTreeNode r = p->red ? root : balanceDeletion(root, replacement);

                    if (replacement == p) {  // detach
                        JTreeNode pp = p->parent;
                        p->parent = nullptr;
                        if (pp != nullptr) {
                            if (p == pp->left)
                                pp->left = nullptr;
                            else if (p == pp->right)
                                pp->right = nullptr;
                        }
                    }
                    if (movable)
                        moveRootToFront(tab, r);
                }

                void split(JHashMap<K,V> map, jarray<JNode> tab, jint index, jint bit) {
                    JTreeNode b = std::dynamic_pointer_cast<TreeNode>(shared_from_this());
                    // Relink into lo and hi lists, preserving order
                    JTreeNode loHead = nullptr, loTail = nullptr;
                    JTreeNode hiHead = nullptr, hiTail = nullptr;
                    jint lc = 0, hc = 0;
                    for (JTreeNode e = b, next; e != nullptr; e = next) {
                        next = std::dynamic_pointer_cast<TreeNode>(e->next);
                        e->next = nullptr;
                        if ((e->hash & bit) == 0) {
                            if ((e->prev = loTail) == nullptr)
                                loHead = e;
                            else
                                loTail->next = e;
                            loTail = e;
                            ++lc;
                        }
                        else {
                            if ((e->prev = hiTail) == nullptr)
                                hiHead = e;
                            else
                                hiTail->next = e;
                            hiTail = e;
                            ++hc;
                        }
                    }

                    if (loHead != nullptr) {
                        if (lc <= UNTREEIFY_THRESHOLD)
                            tab[index] = loHead->untreeify(map);
                        else {
                            tab[index] = loHead;
                            if (hiHead != nullptr) // (else is already treeified)
                                loHead->treeify(tab);
                        }
                    }
                    if (hiHead != nullptr) {
                        if (hc <= UNTREEIFY_THRESHOLD)
                            tab[index + bit] = hiHead->untreeify(map);
                        else {
                            tab[index + bit] = hiHead;
                            if (loHead != nullptr)
                                hiHead->treeify(tab);
                        }
                    }
                }
                
                static JTreeNode rotateLeft(JTreeNode root, JTreeNode p) {
                    JTreeNode r, pp, rl;
                    if (p != nullptr && (r = p->right) != nullptr) {
                        if ((rl = p->right = r->left) != nullptr)
                            rl->parent = p;
                        if ((pp = r->parent = p->parent) == nullptr)
                            (root = r)->red = false;
                        else if (pp->left == p)
                            pp->left = r;
                        else
                            pp->right = r;
                        r->left = p;
                        p->parent = r;
                    }
                    return root;
                }
                
                static JTreeNode rotateRight(JTreeNode root, JTreeNode p) {
                    JTreeNode l, pp, lr;
                    if (p != nullptr && (l = p->left) != nullptr) {
                        if ((lr = p->left = l->right) != nullptr)
                            lr->parent = p;
                        if ((pp = l->parent = p->parent) == nullptr)
                            (root = l)->red = false;
                        else if (pp->right == p)
                            pp->right = l;
                        else
                            pp->left = l;
                        l->right = p;
                        p->parent = l;
                    }
                    return root;
                }
                
                static JTreeNode balanceInsertion(JTreeNode root, JTreeNode x) {
                    x->red = true;
                    for (JTreeNode xp, xpp, xppl, xppr;;) {
                        if ((xp = x->parent) == nullptr) {
                            x->red = false;
                            return x;
                        }
                        else if (!xp->red || (xpp = xp->parent) == nullptr)
                            return root;
                        if (xp == (xppl = xpp->left)) {
                            if ((xppr = xpp->right) != nullptr && xppr->red) {
                                xppr->red = false;
                                xp->red = false;
                                xpp->red = true;
                                x = xpp;
                            }
                            else {
                                if (x == xp->right) {
                                    root = rotateLeft(root, x = xp);
                                    xpp = (xp = x->parent) == nullptr ? nullptr : xp->parent;
                                }
                                if (xp != nullptr) {
                                    xp->red = false;
                                    if (xpp != nullptr) {
                                        xpp->red = true;
                                        root = rotateRight(root, xpp);
                                    }
                                }
                            }
                        }
                        else {
                            if (xppl != nullptr && xppl->red) {
                                xppl->red = false;
                                xp->red = false;
                                xpp->red = true;
                                x = xpp;
                            }
                            else {
                                if (x == xp->left) {
                                    root = rotateRight(root, x = xp);
                                    xpp = (xp = x->parent) == nullptr ? nullptr : xp->parent;
                                }
                                if (xp != nullptr) {
                                    xp->red = false;
                                    if (xpp != nullptr) {
                                        xpp->red = true;
                                        root = rotateLeft(root, xpp);
                                    }
                                }
                            }
                        }
                    }
                }
                
                static JTreeNode balanceDeletion(JTreeNode root, JTreeNode x) {
                    for (JTreeNode xp, xpl, xpr;;)  {
                        if (x == nullptr || x == root)
                            return root;
                        else if ((xp = x->parent) == nullptr) {
                            x->red = false;
                            return x;
                        }
                        else if (x->red) {
                            x->red = false;
                            return root;
                        }
                        else if ((xpl = xp->left) == x) {
                            if ((xpr = xp->right) != nullptr && xpr->red) {
                                xpr->red = false;
                                xp->red = true;
                                root = rotateLeft(root, xp);
                                xpr = (xp = x->parent) == nullptr ? nullptr : xp->right;
                            }
                            if (xpr == nullptr)
                                x = xp;
                            else {
                                JTreeNode sl = xpr->left, sr = xpr->right;
                                if ((sr == nullptr || !sr->red) &&
                                    (sl == nullptr || !sl->red)) {
                                    xpr->red = true;
                                    x = xp;
                                }
                                else {
                                    if (sr == nullptr || !sr->red) {
                                        if (sl != nullptr)
                                            sl->red = false;
                                        xpr->red = true;
                                        root = rotateRight(root, xpr);
                                        xpr = (xp = x->parent) == nullptr ?
                                            nullptr : xp->right;
                                    }
                                    if (xpr != nullptr) {
                                        xpr->red = (xp == nullptr) ? false : xp->red;
                                        if ((sr = xpr->right) != nullptr)
                                            sr->red = false;
                                    }
                                    if (xp != nullptr) {
                                        xp->red = false;
                                        root = rotateLeft(root, xp);
                                    }
                                    x = root;
                                }
                            }
                        }
                        else { // symmetric
                            if (xpl != nullptr && xpl->red) {
                                xpl->red = false;
                                xp->red = true;
                                root = rotateRight(root, xp);
                                xpl = (xp = x->parent) == nullptr ? nullptr : xp->left;
                            }
                            if (xpl == nullptr)
                                x = xp;
                            else {
                                JTreeNode sl = xpl->left, sr = xpl->right;
                                if ((sl == nullptr || !sl->red) &&
                                    (sr == nullptr || !sr->red)) {
                                    xpl->red = true;
                                    x = xp;
                                }
                                else {
                                    if (sl == nullptr || !sl->red) {
                                        if (sr != nullptr)
                                            sr->red = false;
                                        xpl->red = true;
                                        root = rotateLeft(root, xpl);
                                        xpl = (xp = x->parent) == nullptr ?
                                            nullptr : xp->left;
                                    }
                                    if (xpl != nullptr) {
                                        xpl->red = (xp == nullptr) ? false : xp->red;
                                        if ((sl = xpl->left) != nullptr)
                                            sl->red = false;
                                    }
                                    if (xp != nullptr) {
                                        xp->red = false;
                                        root = rotateRight(root, xp);
                                    }
                                    x = root;
                                }
                            }
                        }
                    }
                }

            protected:
                JTreeNode parent;  // red-black tree links
                JTreeNode left;
                JTreeNode right;
                JTreeNode prev;    // needed to unlink next upon deletion
                jboolean red;
                using HashMap<K, V>::Node::next;

                friend class HashMap;
            };
            
            class HashIterator {
            public:
                HashIterator(HashMap<K,V> *parent) : parent(parent) {
                    expectedModCount = parent->modCount;
                    jarray<JNode> t = parent->table;
                    current = next = nullptr;
                    index = 0;
                    if (t != nullptr && parent->_size > 0) { // advance to first entry
                        do {} while (index < t.length() && (next = t[index++]) == nullptr);
                    }
                }

                jboolean hasNext() {
                    return next != nullptr;
                }

                JNode nextNode() {
                    jarray<JNode> t;
                    JNode e = next;
                    if (parent->modCount != expectedModCount)
                        throw ConcurrentModificationException();
                    if (e == nullptr)
                        throw NoSuchElementException();
                    if ((next = (current = e)->next) == nullptr && (t = parent->table) != nullptr) {
                        do {} while (index < t.length() && (next = t[index++]) == nullptr);
                    }
                    return e;
                }

                void remove() {
                    JNode p = current;
                    if (p == nullptr)
                        throw cpp::lang::IllegalStateException();
                    if (parent->modCount != expectedModCount)
                        throw ConcurrentModificationException();
                    current = nullptr;
                    K key = p->key;
                    removeNode(parent->hash(key), key, nullptr, false, false);
                    expectedModCount = parent->modCount;
                }
                
            private:
                JNode next;        // next entry to return
                JNode current;     // current entry
                jint expectedModCount;  // for fast-fail
                jint index;             // current slot
                HashMap<K,V> *parent;
            };
            
            class KeyIterator : public HashIterator, public Iterator<K> {
            public:
                KeyIterator(HashMap<K,V> *parent) : HashIterator(parent) {}
            
                K next() { 
                    return HashIterator::nextNode()->key; 
                }
                
                jboolean hasNext() {
                    return HashIterator::hasNext();
                }
            };
            
            class ValueIterator : public HashIterator, public Iterator<V> {
            public:
                ValueIterator(HashMap<K,V> *parent) : HashIterator(parent) {}
            
                V next() { 
                    return HashIterator::nextNode()->value; 
                }
                
                jboolean hasNext() {
                    return HashIterator::hasNext();
                }
            };
            
            class EntryIterator : public HashIterator, public Iterator<typename Map<K,V>::JEntry> {
            public:
                EntryIterator(HashMap<K,V> *parent) : HashIterator(parent) {}
            
                typename Map<K,V>::JEntry next() { 
                    return HashIterator::nextNode(); 
                }
                
                jboolean hasNext() {
                    return HashIterator::hasNext();
                }
            };
            
            class Values : public AbstractCollection<V> {
            public:
                Values(HashMap<K,V> *parent) : parent(parent) {}
                
                jint size() {
                    return parent->_size;
                }
                
                void clear() { 
                    parent->clear(); 
                }
                
                JIterator<V> iterator() { 
                    return jnew<ValueIterator>(parent); 
                }
                
                jboolean contains(cpp::lang::JObject o) {
                    return parent->containsValue(o);
                }
                
            private:
                HashMap<K,V> *parent;
            };
            
            class KeySet : public AbstractSet<K> {
            public:
                KeySet(HashMap<K,V> *parent) : parent(parent) {}
                
                jint size() {
                    return parent->_size;
                }
                
                void clear() {
                    parent->clear();
                }
                
                JIterator<K> iterator() {
                    return jnew<KeyIterator>(parent);
                }
                
                jboolean contains(cpp::lang::JObject o) {
                    return parent->containsKey(o);
                }
                
                jboolean remove(cpp::lang::JObject key) {
                    return parent->removeNode(parent->hash(key), key, nullptr, false, true) != nullptr;
                }
                
            private:
                HashMap<K,V> *parent;
            };
            
            class EntrySet : public AbstractSet<typename Map<K,V>::JEntry> {
            public:
                EntrySet(HashMap<K,V> *parent) : parent(parent) {}
            
                jint size() {
                    return parent->_size;
                }
                
                void clear() {
                    parent->clear();
                }
                
                JIterator<typename Map<K,V>::JEntry> iterator() {
                    return jnew<EntryIterator>(parent);
                }
                
                jboolean contains(cpp::lang::JObject o) {
                    typename Map<K, V>::JEntry e = std::dynamic_pointer_cast<typename Map<K, V>::Entry>(o); 
                    if (e == nullptr)
                        return false;
                    cpp::lang::JObject key = e->getKey();
                    JNode candidate = parent->getNode(parent->hash(key), key);
                    return candidate != nullptr && candidate->equals(e);
                }
                
                jboolean remove(cpp::lang::JObject o) {
                    typename Map<K, V>::JEntry e = std::dynamic_pointer_cast<typename Map<K, V>::Entry>(o);
                    if (e != nullptr) {
                        cpp::lang::JObject key = e->getKey();
                        cpp::lang::JObject value = e->getValue();
                        return parent->removeNode(parent->hash(key), key, value, true, true) != nullptr;
                    }
                    return false;
                }
            private:
                HashMap<K,V> *parent;
            };
            
            void putMapEntries(JMap<K, V> m, jboolean evict) {
                jint s = m->size();
                if (s > 0) {
                    if (table == nullptr) { // pre-size
                        jfloat ft = (static_cast<jfloat>(s) / loadFactor) + 1.0F;
                        jint t = ((ft < static_cast<jfloat>(MAXIMUM_CAPACITY)) ? static_cast<jint>(ft) : MAXIMUM_CAPACITY);
                        if (t > threshold)
                            threshold = tableSizeFor(t);
                    }
                    else if (s > threshold)
                        resize();
                    for (typename Map<K, V>::JEntry e : m->entrySet()) {
                        K key = e->getKey();
                        V value = e->getValue();
                        putVal(hash(key), key, value, false, evict);
                    }
                }
            }
            
            jint tableSizeFor(jint cap) {
                jint n = cap - 1;
                n |= static_cast<juint>(n) >> 1;
                n |= static_cast<juint>(n) >> 2;
                n |= static_cast<juint>(n) >> 4;
                n |= static_cast<juint>(n) >> 8;
                n |= static_cast<juint>(n) >> 16;
                return (n < 0) ? 1 : (n >= MAXIMUM_CAPACITY) ? MAXIMUM_CAPACITY : n + 1;
            }

            V putVal(jint hash, K key, V value, jboolean onlyIfAbsent, jboolean evict) {
                jarray<JNode> tab;
                JNode p;
                jint n, i;
                if((tab = table) == nullptr || (n = tab.length()) == 0)
                    n = (tab = resize()).length();
                if((p = tab[i = (n - 1) & hash]) == nullptr)
                    tab[i] = newNode(hash, key, value, nullptr);
                else {
                    JNode e;
                    K k;

                    JTreeNode tnp = std::dynamic_pointer_cast<TreeNode>(p);

                    if(p->hash == hash &&
                       ((k = p->key) == key || (key != nullptr && key->equals(k))))
                        e = p;
                    else if(tnp != nullptr)
                        e = std::dynamic_pointer_cast<Node>(tnp->putTreeVal(shared_from_this(), tab, hash, key, value));
                    else {
                        for(int binCount = 0; ; ++binCount) {
                            if((e = p->next) == nullptr) {
                                p->next = newNode(hash, key, value, nullptr);
                                if(binCount >= TREEIFY_THRESHOLD - 1)  // -1 for 1st
                                    treeifyBin(tab, hash);
                                break;
                            }
                            if(e->hash == hash &&
                               ((k = e->key) == key || (key != nullptr && key->equals(k))))
                                break;
                            p = e;
                        }
                    }
                    if(e != nullptr) {  // existing mapping for key
                        V oldValue = e->value;
                        if(!onlyIfAbsent || oldValue == nullptr)
                            e->value = value;
                        afterNodeAccess(e);
                        return oldValue;
                    }
                }
                ++modCount;
                if(++_size > threshold)
                    resize();
                afterNodeInsertion(evict);
                return nullptr;
            }

            jint hash(cpp::lang::JObject key) {
                if (key == nullptr)
                    return 0;
                jint h = key->hashCode();
                return (h ^ (static_cast<juint>(h) >> 16));
            }

            jarray<JNode> resize() {
                jarray<JNode> oldTab = table;
                jint oldCap = (oldTab == nullptr) ? 0 : oldTab.length();
                jint oldThr = threshold;
                jint newCap, newThr = 0;
                if(oldCap > 0) {
                    if(oldCap >= MAXIMUM_CAPACITY) {
                        threshold = cpp::lang::Integer::MAX_VALUE;
                        return oldTab;
                    }
                    else if((newCap = oldCap << 1) < MAXIMUM_CAPACITY &&
                            oldCap >= DEFAULT_INITIAL_CAPACITY)
                        newThr = oldThr << 1; // double threshold
                }
                else if(oldThr > 0)  // initial capacity was placed in threshold
                    newCap = oldThr;
                else {               // zero initial threshold signifies using defaults
                    newCap = DEFAULT_INITIAL_CAPACITY;
                    newThr = static_cast<jint>(DEFAULT_LOAD_FACTOR * DEFAULT_INITIAL_CAPACITY);
                }
                if(newThr == 0) {
                    jfloat ft = static_cast<jfloat>(newCap * loadFactor);
                    newThr = (newCap < MAXIMUM_CAPACITY && ft < static_cast<jfloat>(MAXIMUM_CAPACITY) ?
                              static_cast<jint>(ft) : cpp::lang::Integer::MAX_VALUE);
                }
                threshold = newThr;
                //TODO: @SuppressWarnings( {"rawtypes", "unchecked"})
                jarray<JNode> newTab(newCap);
                table = newTab;
                if(oldTab != nullptr) {
                    for(jint j = 0; j < oldCap; ++j) {
                        JNode e;
                        if((e = oldTab[j]) != nullptr) {
                            oldTab[j] = nullptr;

                            JTreeNode te = std::dynamic_pointer_cast<TreeNode>(e);

                            if(e->next == nullptr)
                                newTab[e->hash & (newCap - 1)] = e;
                            else if(te != nullptr)
                                te->split(shared_from_this(), newTab, j, oldCap);
                            else { // preserve order
                                JNode loHead = nullptr, loTail = nullptr;
                                JNode hiHead = nullptr, hiTail = nullptr;
                                JNode next;
                                do {
                                    next = e->next;
                                    if((e->hash & oldCap) == 0) {
                                        if(loTail == nullptr)
                                            loHead = e;
                                        else
                                            loTail->next = e;
                                        loTail = e;
                                    }
                                    else {
                                        if(hiTail == nullptr)
                                            hiHead = e;
                                        else
                                            hiTail->next = e;
                                        hiTail = e;
                                    }
                                }
                                while((e = next) != nullptr);
                                if(loTail != nullptr) {
                                    loTail->next = nullptr;
                                    newTab[j] = loHead;
                                }
                                if(hiTail != nullptr) {
                                    hiTail->next = nullptr;
                                    newTab[j + oldCap] = hiHead;
                                }
                            }
                        }
                    }
                }

                return newTab;
            }

            JNode getNode(jint hash, cpp::lang::JObject key) {
                jarray<JNode> tab;
                JNode first, e;
                jint n;
                K k;

                if((tab = table) != nullptr && (n = tab.length()) > 0 &&
                (first = tab[(n - 1) & hash]) != nullptr) {
                    if(first->hash == hash &&  // always check first node
                       ((k = first->key) == key || (key != nullptr && key->equals(k))))
                        return first;
                    if((e = first->next) != nullptr) {
                        JTreeNode tnFirst = std::dynamic_pointer_cast<TreeNode>(first);

                        if(tnFirst != nullptr)
                            return tnFirst->getTreeNode(hash, key);
                        do {
                            if(e->hash == hash &&
                               ((k = e->key) == key || (key != nullptr && key->equals(k))))
                                return e;
                        }
                        while((e = e->next) != nullptr);
                    }
                }
                return nullptr;
            }

            JNode newNode(jint hash, K key, V value, JNode next) {
                return jnew<Node>(hash, key, value, next);
            }
            
            JNode replacementNode(JNode p, JNode next) {
                return jnew<Node>(p->hash, p->key, p->value, next);
            }
            
            JTreeNode newTreeNode(jint hash, K key, V value, JNode next) {
                return jnew<TreeNode>(hash, key, value, next);
            }
            
            JTreeNode replacementTreeNode(JNode p, JNode next) {
                return jnew<TreeNode>(p->hash, p->key, p->value, next);
            }
            
            JNode removeNode(jint hash, cpp::lang::JObject key, cpp::lang::JObject value, jboolean matchValue, jboolean movable) {
                jarray<JNode> tab; JNode p; jint n, index;
                if ((tab = table) != nullptr && (n = tab.length()) > 0 &&
                    (p = tab[index = (n - 1) & hash]) != nullptr) {
                    JNode node = nullptr, e; K k; V v;
                    if (p->hash == hash &&
                        ((k = p->key) == key || (key != nullptr && key->equals(k))))
                        node = p;
                    else if ((e = p->next) != nullptr) {
                        JTreeNode tnp = std::dynamic_pointer_cast<TreeNode>(p);
                        if (tnp != nullptr)
                            node = tnp->getTreeNode(hash, key);
                        else {
                            do {
                                if (e->hash == hash &&
                                    ((k = e->key) == key ||
                                     (key != nullptr && key->equals(k)))) {
                                    node = e;
                                    break;
                                }
                                p = e;
                            } while ((e = e->next) != nullptr);
                        }
                    }
                    if (node != nullptr && (!matchValue || (v = node->value) == value ||
                                         (value != nullptr && value->equals(v)))) {
                        JTreeNode tnnode = std::dynamic_pointer_cast<TreeNode>(p);
                        if (tnnode != nullptr)
                            tnnode->removeTreeNode(shared_from_this(), tab, movable);
                        else if (node == p)
                            tab[index] = node->next;
                        else
                            p->next = node->next;
                        ++modCount;
                        --_size;
                        afterNodeRemoval(node);
                        return node;
                    }
                }
                return nullptr;
            }

            void treeifyBin(jarray<JNode> tab, jint hash) {
                jint n, index;
                JNode e;
                if(tab == nullptr || (n = tab.length()) < MIN_TREEIFY_CAPACITY)
                    resize();
                else if((e = tab[index = (n - 1) & hash]) != nullptr) {
                    JTreeNode hd = nullptr, tl = nullptr;
                    do {
                        JTreeNode p = replacementTreeNode(e, nullptr);
                        if(tl == nullptr)
                            hd = p;
                        else {
                            p->prev = tl;
                            tl->next = p;
                        }
                        tl = p;
                    }
                    while((e = e->next) != nullptr);
                    if((tab[index] = hd) != nullptr)
                        hd->treeify(tab);
                }
            }

            void afterNodeAccess(JNode p) {}
            void afterNodeInsertion(jboolean evict) {}
            void afterNodeRemoval(JNode p) {}

            jarray<JNode> table;
            JSet<typename Map<K, V>::JEntry> _entrySet;
            jint _size;
            jint modCount;
            jint threshold;
            jfloat loadFactor;
            using AbstractMap<K, V>::_keySet;
            using AbstractMap<K, V>::_values;

            static const jint DEFAULT_INITIAL_CAPACITY;
            static const jint MAXIMUM_CAPACITY;
            static const jfloat DEFAULT_LOAD_FACTOR;
            static const jint TREEIFY_THRESHOLD;
            static const jint UNTREEIFY_THRESHOLD;
            static const jint MIN_TREEIFY_CAPACITY;
            
            friend class LinkedHashMap<K, V>;
        };

        template <typename K, typename V>
        const jint HashMap<K, V>::DEFAULT_INITIAL_CAPACITY = 1 << 4;

        template <typename K, typename V>
        const jint HashMap<K, V>::MAXIMUM_CAPACITY = 1 << 30;

        template <typename K, typename V>
        const jfloat HashMap<K, V>::DEFAULT_LOAD_FACTOR = 0.75f;

        template <typename K, typename V>
        const jint HashMap<K, V>::TREEIFY_THRESHOLD = 8;

        template <typename K, typename V>
        const jint HashMap<K, V>::UNTREEIFY_THRESHOLD = 6;

        template <typename K, typename V>
        const jint HashMap<K, V>::MIN_TREEIFY_CAPACITY = 64;
    }
}

#endif // CPP_UTIL_HASHMAP_H
