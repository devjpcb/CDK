#ifndef CPP_UTIL_HASHTABLE_H
#define CPP_UTIL_HASHTABLE_H

#include "Dictionary.h"
#include "Map.h"
#include "AbstractSet.h"
#include "Iterator.h"
#include "Enumeration.h"
#include "Collections.h"
#include "NoSuchElementException.h"
#include "ConcurrentModificationException.h"
#include "../lang/Array.h"
#include "../lang/Integer.h"
#include "../lang/Float.h"
#include "../lang/Math.h"
#include "../lang/IllegalArgumentException.h"
#include "../lang/NullPointerException.h"
#include "../lang/UnsupportedOperationException.h"
#include "../lang/IllegalStateException.h"

namespace cpp
{
    namespace util
    {
        template<typename K, typename V>
        class Hashtable : public Dictionary<K, V>, public Map<K,V>, public std::enable_shared_from_this<Hashtable<K,V>>
        {
        public:
            using std::enable_shared_from_this<Hashtable<K,V>>::shared_from_this;
        
            Hashtable(jint initialCapacity, jfloat loadFactor) : count(0), modCount(0) {
                if (initialCapacity < 0)
                    throw cpp::lang::IllegalArgumentException(jstr(u"Illegal Capacity: ") + boxing(initialCapacity));
                if (loadFactor <= 0 || cpp::lang::Float::isNaN(loadFactor))
                    throw cpp::lang::IllegalArgumentException(jstr(u"Illegal Load: ") + boxing<jfloat>(loadFactor));

                if (initialCapacity == 0)
                    initialCapacity = 1;
                    
                this->loadFactor = loadFactor;
                table = jnew_array<typename Hashtable<K,V>::Entry>(initialCapacity);
                threshold = static_cast<jint>(cpp::lang::Math::min(static_cast<jdouble>(initialCapacity * loadFactor), static_cast<jdouble>(MAX_ARRAY_SIZE + 1)));
            }
            
            Hashtable(jint initialCapacity) : Hashtable(initialCapacity, 0.75f) {}
            Hashtable() : Hashtable(11, 0.75f) {}
            
            Hashtable(JMap<K, V> t) : Hashtable(cpp::lang::Math::max(2 * t->size(), 11), 0.75f) {
                putAll(t);
            }
            
            jint size() {
                jsynchronized(this);
                return count;
            }
            
            jboolean isEmpty() {
                return count == 0;
            }
            
            JEnumeration<K> keys() {
                return getEnumeration<K>(KEYS);
            }
            
            JEnumeration<V> elements() {
                return getEnumeration<V>(VALUES);
            }
            
            jboolean contains(cpp::lang::JObject value) {
                jsynchronized(this);
                
                if (value == nullptr) {
                    throw cpp::lang::NullPointerException();
                }
                
                jarray<typename Hashtable<K,V>::JEntry> tab = table;
                
                for (jint i = tab.length() ; i-- > 0 ;) {
                    for (typename Hashtable<K,V>::JEntry e = tab[i] ; e != nullptr; e = e->next) {
                        if (e->value->equals(value)) {
                            return true;
                        }
                    }
                }
                
                return false;
            }
            
            jboolean containsValue(cpp::lang::JObject value) {
                return contains(value);
            }
            
            jboolean containsKey(cpp::lang::JObject key) {
                jsynchronized(this);
                jarray<typename Hashtable<K,V>::JEntry> tab = table;
                jint hash = key->hashCode();
                jint index = (hash & 0x7FFFFFFF) % tab.length();
                for (typename Hashtable<K,V>::JEntry e = tab[index] ; e != nullptr; e = e->next) {
                    if ((e->hash == hash) && e->key->equals(key)) {
                        return true;
                    }
                }
                return false;
            }
            
            V get(cpp::lang::JObject key) {
                jsynchronized(this);
                
                jarray<typename Hashtable<K,V>::JEntry> tab = table;
                jint hash = key->hashCode();
                jint index = (hash & 0x7FFFFFFF) % tab.length();
                for (typename Hashtable<K,V>::JEntry e = tab[index] ; e != nullptr; e = e->next) {
                    if ((e->hash == hash) && e->key->equals(key)) {
                        return e->value;
                    }
                }
                return nullptr;
            }
            
            V put(K key, V value) {
                jsynchronized(this);
                
                if (value == nullptr) {
                    throw cpp::lang::NullPointerException();
                }

                jarray<typename Hashtable<K,V>::JEntry> tab = table;
                jint hash = key->hashCode();
                jint index = (hash & 0x7FFFFFFF) % tab.length();

                typename Hashtable<K,V>::JEntry entry = tab[index];
                for(; entry != nullptr; entry = entry->next) {
                    if ((entry->hash == hash) && entry->key->equals(key)) {
                        V old = entry->value;
                        entry->value = value;
                        return old;
                    }
                }

                addEntry(hash, key, value, index);
                return nullptr;
            }
            
            V remove(cpp::lang::JObject key) {
                jsynchronized(this);
                
                jarray<typename Hashtable<K,V>::JEntry> tab = table;
                jint hash = key->hashCode();
                jint index = (hash & 0x7FFFFFFF) % tab.length();
                
                typename Hashtable<K,V>::JEntry e = tab[index];
                for(typename Hashtable<K,V>::JEntry prev = nullptr; e != nullptr; prev = e, e = e->next) {
                    if ((e->hash == hash) && e->key->equals(key)) {
                        modCount++;
                        if (prev != nullptr) {
                            prev->next = e->next;
                        } else {
                            tab[index] = e->next;
                        }
                        count--;
                        V oldValue = e->value;
                        e->value = nullptr;
                        return oldValue;
                    }
                }
                return nullptr;
            }
            
            void putAll(JMap<K,V> t) {
                jsynchronized(this);
                
                for (typename Map<K,V>::JEntry e : t->entrySet())
                    put(e->getKey(), e->getValue());
            }
            
            void clear() {
                jsynchronized(this);
                
                jarray<typename Hashtable<K,V>::JEntry> tab = table;
                modCount++;
                for (jint index = tab.length(); --index >= 0;)
                    tab[index] = nullptr;
                count = 0;
            }
            
            template<typename T>
            JEnumeration<T> getEnumeration(jint type) {
                if (count == 0) {
                    return Collections::emptyEnumeration<T>();
                } else {
                    return jnew<Enumerator<T>>(this, type, false);
                }
            }

            template<typename T>
            JIterator<T> getIterator(jint type) {
                if (count == 0) {
                    return Collections::emptyIterator<T>();
                } else {
                    return jnew<Enumerator<T>>(this, type, true);
                }
            }
            
            JSet<K> keySet() {
                if (_keySet == nullptr) {
                    cpp::lang::JObject _this = shared_from_this();
                    JSet<K> ks = jnew<Hashtable<K,V>::KeySet>(this);
                    _keySet = Collections::synchronizedSet(ks, _this);
                }
                return _keySet;
            }
            
            JSet<typename Map<K,V>::JEntry> entrySet() {
                if (_entrySet==nullptr) {
                    cpp::lang::JObject _this = shared_from_this();
                    JSet<typename Map<K,V>::JEntry> es = std::dynamic_pointer_cast<Set<typename Map<K,V>::JEntry>>(jnew<Hashtable<K,V>::EntrySet>(this));
                    _entrySet = Collections::synchronizedSet(es, _this);
                }
                return _entrySet;
            }
            
            JCollection<V> values() {
                if (_values==nullptr) {
                    cpp::lang::JObject _this = shared_from_this();
                    JCollection<V> vc = jnew<Hashtable<K,V>::ValueCollection>(this);
                    _values = Collections::synchronizedCollection(vc, _this);
                }
                return _values;
            }
            
            jboolean equals(cpp::lang::JObject o) {
                jsynchronized(this);
                
                if (o == shared_from_this())
                    return true;

                if (!(jinstanceof<Map<K,V>>(o)))
                    return false;
                JMap<K,V> t = std::dynamic_pointer_cast<Map<K,V>>(o);
                if (t->size() != size())
                    return false;

                try {
                    JIterator<typename Map<K,V>::JEntry> i = entrySet()->iterator();
                    while (i->hasNext()) {
                        typename Map<K,V>::JEntry e = i->next();
                        K key = e->getKey();
                        V value = e->getValue();
                        if (value == nullptr) {
                            if (!(t->get(key)==nullptr && t->containsKey(key)))
                                return false;
                        } else {
                            if (!value->equals(t->get(key)))
                                return false;
                        }
                    }
                } catch (cpp::lang::ClassCastException unused)   {
                    return false;
                } catch (cpp::lang::NullPointerException unused) {
                    return false;
                }

                return true;
            }

            jint hashCode() {
                jsynchronized(this);
                jint h = 0;
                if (count == 0 || loadFactor < 0)
                    return h;

                loadFactor = -loadFactor;
                jarray<Hashtable<K,V>::JEntry> tab = table;
                for (Hashtable<K,V>::JEntry entry : tab) {
                    while (entry != nullptr) {
                        h += entry->hashCode();
                        entry = entry->next;
                    }
                }

                loadFactor = -loadFactor; 

                return h;
            }
            
            V getOrDefault(cpp::lang::JObject key, V defaultValue) {
                jsynchronized(this);
                V result = get(key);
                return (nullptr == result) ? defaultValue : result;
            }
            
        protected:
            void rehash() {
                jint oldCapacity = table.length();
                jarray<typename Hashtable<K,V>::JEntry> oldMap = table;

                jint newCapacity = (oldCapacity << 1) + 1;
                if (newCapacity - MAX_ARRAY_SIZE > 0) {
                    if (oldCapacity == MAX_ARRAY_SIZE)
                        return;
                    newCapacity = MAX_ARRAY_SIZE;
                }
                jarray<typename Hashtable<K,V>::JEntry> newMap = jnew_array<typename Hashtable<K,V>::Entry>(newCapacity);

                modCount++;
                threshold = static_cast<jint>(cpp::lang::Math::min(static_cast<jdouble>(newCapacity * loadFactor), static_cast<jdouble>(MAX_ARRAY_SIZE + 1)));
                table = newMap;

                for (jint i = oldCapacity ; i-- > 0 ;) {
                    for (typename Hashtable<K,V>::JEntry old = oldMap[i]; old != nullptr;) {
                        typename Hashtable<K,V>::JEntry e = old;
                        old = old->next;

                        jint index = (e->hash & 0x7FFFFFFF) % newCapacity;
                        e->next = newMap[index];
                        newMap[index] = e;
                    }
                }
            }
            
        private:
            void addEntry(jint hash, K key, V value, jint index) {
                modCount++;

                jarray<typename Hashtable<K,V>::JEntry> tab = table;
                if (count >= threshold) {
                    rehash();

                    tab = table;
                    hash = key->hashCode();
                    index = (hash & 0x7FFFFFFF) % tab.length();
                }

                typename Hashtable<K,V>::JEntry e = tab[index];
                typename Hashtable<K,V>::JEntry ne = jnew<typename Hashtable<K,V>::Entry>(hash, key, value, e);
                tab[index] = ne;
                count++;
            }
            
            
            class KeySet : public AbstractSet<K> {
            public:
                KeySet(Hashtable<K,V>* ht) : ht(ht) {}
            
                JIterator<K> iterator() {
                    return ht->getIterator<K>(ht->KEYS);
                }
                
                jint size() {
                    return ht->count;
                }
                
                jboolean contains(cpp::lang::JObject o) {
                    return ht->containsKey(o);
                }
                
                jboolean remove(cpp::lang::JObject o) {
                    return ht->remove(o) != nullptr;
                }
                
                void clear() {
                    ht->clear();
                }
                
            private:
                Hashtable<K,V>* ht;
            };
            
            using JKeySet = std::shared_ptr<KeySet>;
            
            class EntrySet : public AbstractSet<typename Map<K,V>::JEntry> {
            public: 
                EntrySet(Hashtable<K,V>* ht) : ht(ht) {}
            
                JIterator<typename Map<K,V>::JEntry> iterator() {
                    return ht->getIterator<typename Map<K,V>::JEntry>(ht->ENTRIES);
                }

                jboolean add(typename Map<K,V>::JEntry o) {
                    return AbstractSet<typename Map<K,V>::JEntry>::add(o);
                }

                jboolean contains(cpp::lang::JObject o) {
                    if (!(jinstanceof<typename Map<K,V>::Entry>(o)))
                        return false;
                    typename Map<K,V>::JEntry entry = std::dynamic_pointer_cast<typename Map<K,V>::Entry>(o);
                    cpp::lang::JObject key = entry->getKey();
                    jarray<typename Hashtable<K,V>::JEntry> tab = ht->table;
                    jint hash = key->hashCode();
                    jint index = (hash & 0x7FFFFFFF) % tab.length();

                    for (typename Hashtable<K,V>::JEntry e = tab[index]; e != nullptr; e = e->next)
                        if (e->hash==hash && e->equals(entry))
                            return true;
                    return false;
                }

                jboolean remove(cpp::lang::JObject o) {
                    if (!(jinstanceof<typename Map<K,V>::Entry>(o)))
                        return false;
                    typename Map<K,V>::JEntry entry = std::dynamic_pointer_cast<typename Map<K,V>::Entry>(o);
                    cpp::lang::JObject key = entry->getKey();
                    jarray<typename Hashtable<K,V>::JEntry> tab = ht->table;
                    jint hash = key->hashCode();
                    jint index = (hash & 0x7FFFFFFF) % tab.length();

                    typename Hashtable<K,V>::JEntry e = tab[index];
                    for(typename Hashtable<K,V>::JEntry prev = nullptr; e != nullptr; prev = e, e = e->next) {
                        if (e->hash==hash && e->equals(entry)) {
                            ht->modCount++;
                            if (prev != nullptr)
                                prev->next = e->next;
                            else
                                tab[index] = e->next;

                            ht->count--;
                            e->value = nullptr;
                            return true;
                        }
                    }
                    return false;
                }

                jint size() {
                    return ht->count;
                }

                void clear() {
                    ht->clear();
                }
            private:
                Hashtable<K,V>* ht;
            };
            
            using JEntrySet = std::shared_ptr<EntrySet>;
            
            class ValueCollection : public AbstractCollection<V> {
            public:
                ValueCollection(Hashtable<K,V>* ht) {}
                
                JIterator<V> iterator() {
                    return ht->getIterator<V>(ht->VALUES);
                }
                
                jint size() {
                    return ht->count;
                }
                
                jboolean contains(cpp::lang::JObject o) {
                    return ht->containsValue(o);
                }
                
                void clear() {
                    ht->clear();
                }
                
            private:
                Hashtable<K,V>* ht;
            };
            
            using JValueCollection = std::shared_ptr<ValueCollection>;
            
            class Entry : public Map<K, V>::Entry {
            public:
                Entry(jint hash, K key, V value, typename Hashtable<K,V>::JEntry next) {
                    this->hash = hash;
                    this->key =  key;
                    this->value = value;
                    this->next = next;
                }

                cpp::lang::JObject clone() {
                    return jnew<Entry>(hash, key, value, (next==nullptr ? nullptr : std::dynamic_pointer_cast<Entry>(next->clone())));
                }
                
                K getKey() {
                    return key;
                }

                V getValue() {
                    return value;
                }

                V setValue(V value) {
                    if (value == nullptr)
                        throw cpp::lang::NullPointerException();

                    V oldValue = this->value;
                    this->value = value;
                    return oldValue;
                }

                jboolean equals(cpp::lang::JObject o) {
                    if (!(jinstanceof<typename Map<K,V>::Entry>(o)))
                        return false;
                    typename Map<K,V>::JEntry e = std::dynamic_pointer_cast<typename Map<K,V>::Entry>(o);

                    return (key==nullptr ? e->getKey()==nullptr : key->equals(e->getKey())) &&
                       (value==nullptr ? e->getValue()==nullptr : value->equals(e->getValue()));
                }

                jint hashCode() {
                    return hash ^ Objects::hashCode(value);
                }

                cpp::lang::JString toString() {
                    return key->toString() + jstr(u"=") + value->toString();
                }
                
                jint hash;
                K key;
                V value;
                typename Hashtable<K,V>::JEntry next;
            };
            
            using JEntry = std::shared_ptr<Entry>;
            
            template<typename T>
            class Enumerator : public Enumeration<T>, public Iterator<T> {
            public:
                Enumerator(Hashtable<K,V>* ht, jint type, jboolean iterator) {
                    this->ht = ht;
                    this->table = ht->table;
                    this->index = ht->table.length();
                    this->expectedModCount = ht->modCount;
                    this->type = type;
                    this->iterator = iterator;
                }
                
                jboolean hasMoreElements() {
                    typename Hashtable<K,V>::JEntry e = entry;
                    jint i = index;
                    jarray<typename Hashtable<K,V>::JEntry> t = table;

                    while (e == nullptr && i > 0) {
                        e = t[--i];
                    }
                    entry = e;
                    index = i;
                    return e != nullptr;
                }
                
                T nextElement() {
                    typename Hashtable<K,V>::JEntry et = entry;
                    int i = index;
                    jarray<typename Hashtable<K,V>::JEntry> t = table;

                    while (et == nullptr && i > 0) {
                        et = t[--i];
                    }
                    entry = et;
                    index = i;
                    if (et != nullptr) {
                        typename Hashtable<K,V>::JEntry e = lastReturned = entry;
                        entry = e->next;
                        return type == ht->KEYS ? 
                            std::dynamic_pointer_cast<typename T::element_type>(e->key) : 
                            (type == ht->VALUES ? std::dynamic_pointer_cast<typename T::element_type>(e->value) : 
                                std::dynamic_pointer_cast<typename T::element_type>(e));
                    }
                    
                    throw NoSuchElementException(jstr(u"Hashtable Enumerator"));
                }
                
                jboolean hasNext() {
                    return hasMoreElements();
                }
                
                T next() {
                    if (ht->modCount != expectedModCount)
                        throw ConcurrentModificationException();
                    return nextElement();
                }
                
                void remove() {
                    if (!iterator)
                        throw cpp::lang::UnsupportedOperationException();
                    if (lastReturned == nullptr)
                        throw cpp::lang::IllegalStateException(jstr(u"Hashtable Enumerator"));
                    if (ht->modCount != expectedModCount)
                        throw ConcurrentModificationException();

                     {
                        jsynchronized(ht);
                        
                        jarray<typename Hashtable<K,V>::JEntry> tab = ht->table;
                        jint index = (lastReturned->hash & 0x7FFFFFFF) % tab.length();

                        typename Hashtable<K,V>::JEntry e = tab[index];
                        for(typename Hashtable<K,V>::JEntry prev = nullptr; e != nullptr; prev = e, e = e->next) {
                            if (e == lastReturned) {
                                ht->modCount++;
                                expectedModCount++;
                                if (prev == nullptr)
                                    tab[index] = e->next;
                                else
                                    prev->next = e->next;
                                ht->count--;
                                lastReturned = nullptr;
                                return;
                            }
                        }
                        throw ConcurrentModificationException();
                    }
                }
                
                void finalize() {
                    
                }
                
            protected:
                jint expectedModCount;
            
            private:
                Hashtable<K,V>* ht;
                jarray<typename Hashtable<K,V>::JEntry> table;
                jint index;
                typename Hashtable<K,V>::JEntry entry;
                typename Hashtable<K,V>::JEntry lastReturned;
                jint type;
                jboolean iterator;
            };
            
            template<typename T>
            using JEnumerator = std::shared_ptr<Enumerator<T>>;
            
            jarray<typename Hashtable<K,V>::JEntry> table;
            jint count;
            jint threshold;
            jfloat loadFactor;
            jint modCount;
            static const jlong serialVersionUID;
            static const jint MAX_ARRAY_SIZE;
            JSet<K> _keySet;
            JSet<typename Map<K,V>::JEntry> _entrySet;
            JCollection<V> _values;
            static const jint KEYS;
            static const jint VALUES;
            static const jint ENTRIES;
        };
        
        template<typename K, typename V>
        const jlong Hashtable<K,V>::serialVersionUID = 1421746759512286392LL;
        
        template<typename K, typename V>
        const jint Hashtable<K,V>::MAX_ARRAY_SIZE = cpp::lang::Integer::MAX_VALUE - 8;
        
        template<typename K, typename V>
        const jint Hashtable<K,V>::KEYS = 0;
        
        template<typename K, typename V>
        const jint Hashtable<K,V>::VALUES = 1;
        
        template<typename K, typename V>
        const jint Hashtable<K,V>::ENTRIES = 2;
    }
}

#endif // CPP_UTIL_HASHTABLE_H
