#ifndef CPP_UTIL_ITERATOR_H
#define CPP_UTIL_ITERATOR_H

#include "../lang/Object.h"
#include "../lang/UnsupportedOperationException.h"

namespace cpp
{
    namespace util
    {
        template <typename E>
        class Iterator : public cpp::lang::Object
        {
        public:
            virtual jboolean hasNext() = 0;
            virtual E next() = 0;
            
            virtual void remove() {
                throw cpp::lang::UnsupportedOperationException(jstr(u"remove"));
            }
        };
    }
}

#endif // CPP_UTIL_ITERATOR_H
