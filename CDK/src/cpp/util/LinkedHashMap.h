#ifndef CPP_UTIL_LINKEDHASHMAP_H
#define CPP_UTIL_LINKEDHASHMAP_H

#include "HashMap.h"

namespace cpp
{
    namespace util
    {
        template <typename K, typename V>
        class LinkedHashMap : public HashMap<K, V>
        {
        public:
            LinkedHashMap() {
            }

            ~LinkedHashMap() {
            }

        private:
            class Entry;
            using JEntry = std::shared_ptr<Entry>;
        
            class Entry : public HashMap<K, V>::Node {
            public:
                JEntry before, after;
                Entry(jint hash, K key, V value, typename HashMap<K, V>::JNode next) : HashMap<K, V>::Node(hash, key, value, next) {}
            };
            
            friend class HashMap<K, V>;
        };
    }
}

#endif // CPP_UTIL_LINKEDHASHMAP_H
