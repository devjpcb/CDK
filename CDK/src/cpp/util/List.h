#ifndef CPP_UTIL_LIST_H
#define CPP_UTIL_LIST_H

#include "Collection.h"
#include "ListIterator.h"
#include "Comparator.h"

namespace cpp
{
    namespace util
    {
        template <typename E>
        class List : public virtual Collection<E>
        {
        public:
            virtual jboolean add(E e) = 0;
            virtual void add(jint index, E element) = 0;
            virtual jboolean addAll(JCollection<E> c) = 0;
            virtual jboolean addAll(jint index, JCollection<E> c) = 0;
            virtual void clear() = 0;
            virtual jboolean contains(cpp::lang::JObject o) = 0;
            virtual jboolean containsAll(JCollection<E> c) = 0;
            virtual jboolean equals(cpp::lang::JObject o) = 0;
            virtual E get(jint index) = 0;
            virtual jint hashCode() = 0;
            virtual jint indexOf(cpp::lang::JObject o) = 0;
            virtual jboolean isEmpty() = 0;
            virtual JIterator<E> iterator() = 0;
            virtual jint lastIndexOf(cpp::lang::JObject o) = 0;
            virtual JListIterator<E> listIterator() = 0;
            virtual JListIterator<E> listIterator(jint index) = 0;
            virtual E remove(jint index) = 0;
            virtual jboolean remove(cpp::lang::JObject o) = 0;
            virtual jboolean removeAll(JCollection<E> c) = 0;
            virtual jboolean retainAll(JCollection<E> c) = 0;
            virtual E set(jint index, E element) = 0;
            virtual jint size() = 0;
            
            virtual void sort(JComparator<E> c) {
                //TODO: Collections.sort(this, c);
            }
            
            virtual JList<E> subList(jint fromIndex, jint toIndex) = 0;
            virtual jarray<cpp::lang::JObject> toArray() = 0;
            virtual jarray<E> toArray(jarray<E> a) = 0;
        };
    }
}

#endif // CPP_UTIL_LIST_H
