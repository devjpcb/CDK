#ifndef CPP_UTIL_LISTITERATOR_H
#define CPP_UTIL_LISTITERATOR_H

#include "Iterator.h"

namespace cpp
{
    namespace util
    {
        template <typename E>
        class ListIterator : public Iterator<E>
        {
        public:
            virtual void add(E e) = 0;
            virtual jboolean hasNext() = 0;
            virtual jboolean hasPrevious() = 0;
            virtual E next() = 0;
            virtual jint nextIndex() = 0;
            virtual E previous() = 0;
            virtual jint previousIndex() = 0;
            virtual void remove() = 0;
            virtual void set(E e) = 0;
        };
    }
}

#endif // CPP_UTIL_LISTITERATOR_H
