#ifndef CPP_UTIL_MAP_H
#define CPP_UTIL_MAP_H

#include "../lang/Object.h"
#include "Set.h"
#include "Objects.h"

namespace cpp
{
    namespace util
    {
        template <typename K, typename V> 
        class Map : public virtual cpp::lang::Object
        {
        public:
            class Entry : public cpp::lang::Object
            {
            public:
                virtual jboolean equals(cpp::lang::JObject o) = 0;
                virtual K getKey() = 0;
                virtual V getValue() = 0;
                virtual jint hashCode() = 0;
                virtual V setValue(V value) = 0;
            };
            
            using JEntry = std::shared_ptr<Entry>;

            virtual void clear() = 0;
            virtual jboolean containsKey(cpp::lang::JObject key) = 0;
            virtual jboolean containsValue(cpp::lang::JObject value) = 0;
            virtual JSet<JEntry> entrySet() = 0;
            virtual jboolean equals(cpp::lang::JObject o) = 0;
            virtual V get(cpp::lang::JObject key) = 0;
            
            virtual V getOrDefault(cpp::lang::JObject key, V defaultValue) {
                V v;
                return (((v = get(key)) != nullptr) || containsKey(key)) ? v : defaultValue;
            }
            
            virtual V replace(K key, V value) {
                V curValue;
                if (((curValue = get(key)) != nullptr) || containsKey(key)) {
                    curValue = put(key, value);
                }
                return curValue;
            }
            
            virtual jboolean replace(K key, V oldValue, V newValue) {
                cpp::lang::JObject curValue = get(key);
                if (!Objects::equals(curValue, oldValue) ||
                    (curValue == nullptr && !containsKey(key))) {
                    return false;
                }
                put(key, newValue);
                return true;
            }
            
            virtual jint hashCode() = 0;
            virtual jboolean isEmpty() = 0;
            virtual JSet<K> keySet() = 0;
            virtual V put(K key, V value) = 0;
            virtual void putAll(JMap<K, V> m) = 0;
            
            virtual V putIfAbsent(K key, V value) {
                V v = get(key);
                if (v == nullptr) {
                    v = put(key, value);
                }

                return v;
            }
            
            virtual V remove(cpp::lang::JObject key) = 0;
            
            virtual jboolean remove(cpp::lang::JObject key, cpp::lang::JObject value) {
                cpp::lang::JObject curValue = get(key);
                if (!Objects::equals(curValue, value) ||
                    (curValue == nullptr && !containsKey(key))) {
                    return false;
                }
                remove(key);
                return true;
            }
            
            virtual jint size() = 0;
            virtual JCollection<V> values() = 0;
        };
    }
}

#endif // CPP_UTIL_MAP_H
