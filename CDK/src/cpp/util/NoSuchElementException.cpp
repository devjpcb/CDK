#include "NoSuchElementException.h"

using namespace cpp::lang;

namespace cpp
{
    namespace util
    {
        NoSuchElementException::NoSuchElementException() : RuntimeException()
        {
        }
        
        NoSuchElementException::NoSuchElementException(JString s) : RuntimeException(s)
        {
        }

        NoSuchElementException::~NoSuchElementException()
        {
        }
    }
}
