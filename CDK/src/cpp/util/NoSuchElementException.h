#ifndef NOSUCHELEMENTEXCEPTION_H
#define NOSUCHELEMENTEXCEPTION_H

#include "../lang/RuntimeException.h"

namespace cpp
{
    namespace util
    {
        class NoSuchElementException : public cpp::lang::RuntimeException
        {
        public:
            NoSuchElementException();
            NoSuchElementException(cpp::lang::JString s);
            ~NoSuchElementException();
        };
    }
}

#endif // NOSUCHELEMENTEXCEPTION_H
