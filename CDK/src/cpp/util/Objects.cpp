#include "Objects.h"

using namespace cpp::lang;

namespace cpp
{
    namespace util
    {
        jboolean Objects::equals(JObject a, JObject b)
        {
            return (a == b) || (a != nullptr && a->equals(b));
        }
        
        jint Objects::hashCode(JObject o)
        {
            return o != nullptr ? o->hashCode() : 0;
        }
    }
}
