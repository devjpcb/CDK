#ifndef CPP_UTIL_OBJECTS_H
#define CPP_UTIL_OBJECTS_H

#include "../lang/String.h"
#include "../lang/NullPointerException.h"

namespace cpp
{
    namespace util
    {
        class Objects
        {
        public:
            static jboolean equals(cpp::lang::JObject a, cpp::lang::JObject b);
            static jint hashCode(cpp::lang::JObject o);

            template<typename T>
            static T requireNonNull(T obj) {
                if(obj == nullptr)
                    throw cpp::lang::NullPointerException();
                return obj;
            }

            template<typename T>
            static T requireNonNull(T obj, cpp::lang::JString message) {
                if(obj == nullptr)
                    throw cpp::lang::NullPointerException(message);
                return obj;
            }
        };
    }
}

#endif // CPP_UTIL_OBJECTS_H
