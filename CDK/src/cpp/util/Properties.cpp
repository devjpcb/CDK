#include "Properties.h"

using namespace cpp::lang;

namespace cpp
{
    namespace util
    {
        Properties::Properties() : Properties(nullptr) {}
        
        Properties::Properties(JProperties defaults) : defaults(defaults) {}
        
        JObject Properties::setProperty(JString key, JString value)
        {
            return put(key, value);
        }
        
        JString Properties::getProperty(JString key)
        {
            JObject oval = get(key);
            JString sval = jinstanceof<String>(oval) ? std::dynamic_pointer_cast<String>(oval) : nullptr;
            return ((sval == nullptr) && (defaults != nullptr)) ? defaults->getProperty(key) : sval;
        }
        
        JString Properties::getProperty(JString key, JString defaultValue)
        {
            JString val = getProperty(key);
            return (val == nullptr) ? defaultValue : val;
        }
    }
}
