#ifndef PROPERTIES_H
#define PROPERTIES_H

#include "Hashtable.h"

namespace cpp
{
    namespace util
    {
        class Properties : public Hashtable<cpp::lang::JObject, cpp::lang::JObject>
        {
        public:
            Properties();
            Properties(JProperties defaults);
            
            cpp::lang::JObject setProperty(cpp::lang::JString key, cpp::lang::JString value);
            cpp::lang::JString getProperty(cpp::lang::JString key);
            cpp::lang::JString getProperty(cpp::lang::JString key, cpp::lang::JString defaultValue);
            
        protected:
            JProperties defaults;
        };
    }
}

#endif // PROPERTIES_H
