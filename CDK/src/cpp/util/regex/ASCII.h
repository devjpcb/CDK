#ifndef CPP_UTIL_REGEX_ASCII_H
#define CPP_UTIL_REGEX_ASCII_H

#include "../../lang/Object.h"
#include "../../lang/Array.h"

namespace cpp
{
    namespace util
    {
        namespace regex
        {
            class ASCII
            {
            public:
                ASCII();
                ~ASCII();

            private:
                static jint getType(jint ch);
                static jboolean isType(jint ch, jint type);
                static jboolean isAscii(jint ch);
                static jboolean isAlpha(jint ch);
                static jboolean isDigit(jint ch);
                static jboolean isAlnum(jint ch);
                static jboolean isGraph(jint ch);
                static jboolean isPrint(jint ch);
                static jboolean isPunct(jint ch);
                static jboolean isSpace(jint ch);
                static jboolean isHexDigit(jint ch);
                static jboolean isOctDigit(jint ch);
                static jboolean isCntrl(jint ch);
                static jboolean isLower(jint ch);
                static jboolean isUpper(jint ch);
                static jboolean isWord(jint ch);
                static jint toDigit(jint ch);
                static jint toLower(jint ch);
                static jint toUpper(jint ch);

                static const jint UPPER;
                static const jint LOWER;
                static const jint DIGIT;
                static const jint SPACE;
                static const jint PUNCT;
                static const jint CNTRL;
                static const jint BLANK;
                static const jint HEX;
                static const jint UNDER;
                static const jint _ASCII;
                static const jint ALPHA;
                static const jint ALNUM;
                static const jint GRAPH;
                static const jint WORD;
                static const jint XDIGIT;
                static const jarray<jint> ctype;
                
                friend class Matcher;
                friend class Pattern;
            };
        }
    }
}

#endif // CPP_UTIL_REGEX_ASCII_H
