#ifndef CPP_UTIL_REGEX_MATCHRESULT_H
#define CPP_UTIL_REGEX_MATCHRESULT_H

#include "../../lang/Object.h"

namespace cpp
{
    namespace util
    {
        namespace regex
        {
            class MatchResult : public virtual cpp::lang::Object
            {
            public:
                virtual jint start() = 0;
                virtual jint start(jint group) = 0;
                virtual jint end() = 0;
                virtual jint end(jint group) = 0;
                virtual cpp::lang::JString group() = 0;
                virtual cpp::lang::JString group(jint group) = 0;
                virtual jint groupCount() = 0;
            };
        }
    }
}

#endif // CPP_UTIL_REGEX_MATCHRESULT_H
