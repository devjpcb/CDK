#include "Matcher.h"
#include "ASCII.h"
#include "../../lang/StringBuilder.h"
#include "../../lang/Math.h"
#include "../../lang/Integer.h"
#include "../../util/Objects.h"
#include "../../lang/IllegalArgumentException.h"
#include "../../lang/IllegalStateException.h"
#include "../../lang/IndexOutOfBoundsException.h"
#include "../../lang/NullPointerException.h"

using namespace cpp::lang;

namespace cpp
{
    namespace util
    {
        namespace regex
        {
            Matcher::Matcher() : first(-1), last(0)
            {
            }

            Matcher::~Matcher()
            {
            }

            JMatcher Matcher::appendReplacement(JStringBuffer sb, JString replacement)
            {
                // If no match, return error
                if(first < 0)
                    throw IllegalStateException(jstr(u"No match available"));

                // Process substitution string to replace group references with groups
                jint cursor = 0;
                JStringBuilder result = jnew<StringBuilder>();

                while(cursor < replacement->length()) {
                    jchar nextChar = replacement->charAt(cursor);
                    if(nextChar == u'\\') {
                        cursor++;
                        if(cursor == replacement->length())
                            throw IllegalArgumentException(jstr(u"character to be escaped is missing"));
                        nextChar = replacement->charAt(cursor);
                        result->append(nextChar);
                        cursor++;
                    }
                    else if(nextChar == u'$') {
                        // Skip past $
                        cursor++;
                        // Throw IAE if this "$" is the last character in replacement
                        if(cursor == replacement->length())
                            throw IllegalArgumentException(jstr(u"Illegal group reference: group index is missing"));
                        nextChar = replacement->charAt(cursor);
                        jint refNum = -1;
                        if(nextChar == u'{') {
                            cursor++;
                            JStringBuilder gsb = jnew<StringBuilder>();
                            while(cursor < replacement->length()) {
                                nextChar = replacement->charAt(cursor);
                                if(ASCII::isLower(nextChar) ||
                                   ASCII::isUpper(nextChar) ||
                                   ASCII::isDigit(nextChar)) {
                                    gsb->append(nextChar);
                                    cursor++;
                                }
                                else {
                                    break;
                                }
                            }
                            if(gsb->length() == 0)
                                throw IllegalArgumentException(jstr(u"named capturing group has 0 length name"));
                            if(nextChar != '}')
                                throw IllegalArgumentException(jstr(u"named capturing group is missing trailing '}'"));
                            JString gname = gsb->toString();
                            if(ASCII::isDigit(gname->charAt(0)))
                                throw IllegalArgumentException(jstr(u"capturing group name {") + gname + jstr(u"} starts with digit character"));
                            if(!parentPattern->namedGroups()->containsKey(gname))
                                throw IllegalArgumentException(jstr(u"No group with name {") + gname + jstr(u"}"));
                            refNum = unboxing(parentPattern->namedGroups()->get(gname));
                            cursor++;
                        }
                        else {
                            // The first number is always a group
                            refNum = (jint)nextChar - u'0';
                            if((refNum < 0) || (refNum > 9))
                                throw IllegalArgumentException(jstr(u"Illegal group reference"));
                            cursor++;
                            // Capture the largest legal group string
                            jboolean done = false;
                            while(!done) {
                                if(cursor >= replacement->length()) {
                                    break;
                                }
                                jint nextDigit = replacement->charAt(cursor) - u'0';
                                if((nextDigit < 0) || (nextDigit > 9)) { // not a number
                                    break;
                                }
                                jint newRefNum = (refNum * 10) + nextDigit;
                                if(groupCount() < newRefNum) {
                                    done = true;
                                }
                                else {
                                    refNum = newRefNum;
                                    cursor++;
                                }
                            }
                        }
                        // Append group
                        if(start(refNum) != -1 && end(refNum) != -1)
                            result->append(text, start(refNum), end(refNum));
                    }
                    else {
                        result->append(nextChar);
                        cursor++;
                    }
                }
                // Append the intervening text
                sb->append(text, lastAppendPosition, first);
                // Append the match substitution
                sb->append(result);

                lastAppendPosition = last;
                return shared_from_this();
            }

            JStringBuffer Matcher::appendTail(JStringBuffer sb)
            {
                sb->append(text, lastAppendPosition, getTextLength());
                return sb;
            }

            jint Matcher::end()
            {
                if(first < 0)
                    throw IllegalStateException(jstr(u"No match available"));
                return last;
            }

            jint Matcher::end(jint group)
            {
                if(first < 0)
                    throw IllegalStateException(jstr(u"No match available"));
                if(group < 0 || group > groupCount())
                    throw IndexOutOfBoundsException(jstr(u"No group ") + boxing(group));
                return groups[group * 2 + 1];
            }

            jint Matcher::end(JString name)
            {
                return groups[getMatchedGroupIndex(name) * 2 + 1];
            }

            jboolean Matcher::find()
            {
                jint nextSearchIndex = last;
                if(nextSearchIndex == first)
                    nextSearchIndex++;

                // If next search starts before region, start it at region
                if(nextSearchIndex < from)
                    nextSearchIndex = from;

                // If next search starts beyond region then it fails
                if(nextSearchIndex > to) {
                    for(jint i = 0; i < groups.length(); i++)
                        groups[i] = -1;
                    return false;
                }
                return search(nextSearchIndex);
            }

            jboolean Matcher::find(jint start)
            {
                jint limit = getTextLength();
                if((start < 0) || (start > limit))
                    throw IndexOutOfBoundsException(jstr(u"Illegal start index"));
                reset();
                return search(start);
            }

            JString Matcher::group()
            {
                return group(0);
            }

            JString Matcher::group(jint group)
            {
                if(first < 0)
                    throw IllegalStateException(jstr(u"No match found"));
                if(group < 0 || group > groupCount())
                    throw IndexOutOfBoundsException(jstr(u"No group ") + boxing(group));
                if((groups[group * 2] == -1) || (groups[group * 2 + 1] == -1))
                    return nullptr;
                return getSubSequence(groups[group * 2], groups[group * 2 + 1])->toString();
            }

            JString Matcher::group(JString name)
            {
                jint group = getMatchedGroupIndex(name);
                if((groups[group * 2] == -1) || (groups[group * 2 + 1] == -1))
                    return nullptr;
                return getSubSequence(groups[group * 2], groups[group * 2 + 1])->toString();
            }

            jint Matcher::groupCount()
            {
                return parentPattern->capturingGroupCount - 1;
            }

            jboolean Matcher::hasAnchoringBounds()
            {
                return anchoringBounds;
            }

            jboolean Matcher::hasTransparentBounds()
            {
                return transparentBounds;
            }

            jboolean Matcher::hitEnd()
            {
                return _hitEnd;
            }

            jboolean Matcher::lookingAt()
            {
                return match(from, NOANCHOR);
            }

            jboolean Matcher::matches()
            {
                return match(from, ENDANCHOR);
            }

            JPattern Matcher::pattern()
            {
                return parentPattern;
            }

            JString Matcher::quoteReplacement(JString s)
            {
                if((s->indexOf(u'\\') == -1) && (s->indexOf(u'$') == -1))
                    return s;
                JStringBuilder sb = jnew<StringBuilder>();
                for(int i = 0; i < s->length(); i++) {
                    char c = s->charAt(i);
                    if(c == u'\\' || c == u'$') {
                        sb->append(u'\\');
                    }
                    sb->append(c);
                }
                return sb->toString();
            }

            JMatcher Matcher::region(jint start, jint end)
            {
                if((start < 0) || (start > getTextLength()))
                    throw IndexOutOfBoundsException(jstr(u"start"));
                if((end < 0) || (end > getTextLength()))
                    throw IndexOutOfBoundsException(jstr(u"end"));
                if(start > end)
                    throw IndexOutOfBoundsException(jstr(u"start > end"));
                reset();
                from = start;
                to = end;
                return shared_from_this();
            }

            jint Matcher::regionEnd()
            {
                return to;
            }

            jint Matcher::regionStart()
            {
                return from;
            }

            JString Matcher::replaceAll(JString replacement)
            {
                reset();
                jboolean result = find();
                if(result) {
                    JStringBuffer sb = jnew<StringBuffer>();
                    do {
                        appendReplacement(sb, replacement);
                        result = find();
                    }
                    while(result);
                    appendTail(sb);
                    return sb->toString();
                }
                return text->toString();
            }

            JString Matcher::replaceFirst(JString replacement)
            {
                if(replacement == nullptr)
                    throw NullPointerException(jstr(u"replacement"));
                reset();
                if(!find())
                    return text->toString();
                JStringBuffer sb = jnew<StringBuffer>();
                appendReplacement(sb, replacement);
                appendTail(sb);
                return sb->toString();
            }

            jboolean Matcher::requireEnd()
            {
                return _requireEnd;
            }

            JMatcher Matcher::reset()
            {
                first = -1;
                last = 0;
                oldLast = -1;
                for(jint i = 0; i < groups.length(); i++)
                    groups[i] = -1;
                for(jint i = 0; i < locals.length(); i++)
                    locals[i] = -1;
                lastAppendPosition = 0;
                from = 0;
                to = getTextLength();
                return shared_from_this();
            }

            JMatcher Matcher::reset(JCharSequence input)
            {
                text = input;
                return reset();
            }

            jint Matcher::start()
            {
                if(first < 0)
                    throw IllegalStateException(jstr(u"No match available"));
                return first;
            }

            jint Matcher::start(jint group)
            {
                if(first < 0)
                    throw IllegalStateException(jstr(u"No match available"));
                if(group < 0 || group > groupCount())
                    throw IndexOutOfBoundsException(jstr(u"No group ") + boxing(group));
                return groups[group * 2];
            }

            jint Matcher::start(JString name)
            {
                return groups[getMatchedGroupIndex(name) * 2];
            }

            JString Matcher::toString()
            {

                JStringBuilder sb = jnew<StringBuilder>();
                /* TODO:
                sb.append(u"java.util.regex.Matcher");
                sb.append(JS(u"[pattern=") + JO(pattern()));
                sb.append(u" region=");
                sb.append(JO(regionStart()) + JS(u",") + JO(regionEnd()));
                sb.append(u" lastmatch=");
                if((first >= 0) && (group() != nullptr)) {
                    sb.append(group());
                }
                sb.append(u"]");
                */
                return sb->toString();
            }

            JMatcher Matcher::useAnchoringBounds(jboolean b)
            {
                anchoringBounds = b;
                return shared_from_this();
            }

            JMatcher Matcher::usePattern(JPattern newPattern)
            {
                if(newPattern == nullptr)
                    throw IllegalArgumentException(jstr(u"Pattern cannot be null"));
                parentPattern = newPattern;

                // Reallocate state storage
                jint parentGroupCount = Math::max(newPattern->capturingGroupCount, 10);
                groups = jnew_array<jint>(parentGroupCount * 2);
                locals = jnew_array<jint>(newPattern->localCount);
                for(jint i = 0; i < groups.length(); i++)
                    groups[i] = -1;
                for(jint i = 0; i < locals.length(); i++)
                    locals[i] = -1;
                return shared_from_this();
            }

            JMatcher Matcher::useTransparentBounds(jboolean b)
            {
                transparentBounds = b;
                return shared_from_this();
            }

            Matcher::Matcher(JPattern parent, JCharSequence text)
            {
                this->parentPattern = parent;
                this->text = text;

                // Allocate state storage
                jint parentGroupCount = Math::max(parent->capturingGroupCount, 10);
                groups = jnew_array<jint>(parentGroupCount * 2);
                locals = jnew_array<jint>(parent->localCount);

                // Put fields into initial states
                reset();
            }

            jint Matcher::getTextLength()
            {
                return text->length();
            }

            JCharSequence Matcher::getSubSequence(jint beginIndex, jint endIndex)
            {
                return text->subSequence(beginIndex, endIndex);
            }

            jboolean Matcher::search(jint from)
            {
                this->_hitEnd = false;
                this->_requireEnd = false;
                from = from < 0 ? 0 : from;
                this->first  = from;
                this->oldLast = oldLast < 0 ? from : oldLast;
                for(jint i = 0; i < groups.length(); i++)
                    groups[i] = -1;
                acceptMode = NOANCHOR;
                jboolean result = parentPattern->root->match(shared_from_this(), from, text);
                if(!result)
                    this->first = -1;
                this->oldLast = this->last;
                return result;
            }

            jboolean Matcher::match(jint from, jint anchor)
            {
                this->_hitEnd = false;
                this->_requireEnd = false;
                from = from < 0 ? 0 : from;
                this->first  = from;
                this->oldLast = oldLast < 0 ? from : oldLast;
                for(jint i = 0; i < groups.length(); i++)
                    groups[i] = -1;
                acceptMode = anchor;
                jboolean result = parentPattern->matchRoot->match(shared_from_this(), from, text);
                if(!result)
                    this->first = -1;
                this->oldLast = this->last;
                return result;
            }

            jint Matcher::getMatchedGroupIndex(JString name)
            {
                Objects::requireNonNull(name, jstr(u"Group name"));
                if(first < 0)
                    throw IllegalStateException(jstr(u"No match found"));
                if(!parentPattern->namedGroups()->containsKey(name))
                    throw IllegalArgumentException(jstr(u"No group with name <") + name + jstr(u">"));
                return unboxing(parentPattern->namedGroups()->get(name));
            }

            const jint Matcher::ENDANCHOR = 1;
            const jint Matcher::NOANCHOR = 0;
        }
    }
}
