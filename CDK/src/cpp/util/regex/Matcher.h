#ifndef CPP_UTIL_REGEX_MATCHER_H
#define CPP_UTIL_REGEX_MATCHER_H

#include "../../lang/Object.h"
#include "MatchResult.h"
#include "Pattern.h"
#include "../../lang/String.h"
#include "../../lang/StringBuffer.h"
#include "../../lang/CharSequence.h"

namespace cpp
{
    namespace util
    {
        namespace regex
        {
            class Matcher : public virtual cpp::lang::Object, public MatchResult, public std::enable_shared_from_this<Matcher>
            {
            public:
                Matcher();
                ~Matcher();

                JMatcher appendReplacement(cpp::lang::JStringBuffer sb, cpp::lang::JString replacement);
                cpp::lang::JStringBuffer appendTail(cpp::lang::JStringBuffer sb);
                jint end();
                jint end(jint group);
                jint end(cpp::lang::JString name);
                jboolean find();
                jboolean find(jint start);
                cpp::lang::JString group();
                cpp::lang::JString group(jint group);
                cpp::lang::JString group(cpp::lang::JString name);
                jint groupCount();
                jboolean hasAnchoringBounds();
                jboolean hasTransparentBounds();
                jboolean hitEnd();
                jboolean lookingAt();
                jboolean matches();
                JPattern pattern();
                static cpp::lang::JString quoteReplacement(cpp::lang::JString s);
                JMatcher region(jint start, jint end);
                jint regionEnd();
                jint regionStart();
                cpp::lang::JString replaceAll(cpp::lang::JString replacement);
                cpp::lang::JString replaceFirst(cpp::lang::JString replacement);
                jboolean requireEnd();
                JMatcher reset();
                JMatcher reset(cpp::lang::JCharSequence input);
                jint start();
                jint start(jint group);
                jint start(cpp::lang::JString name);
                // TODO: MatchResult toMatchResult()
                cpp::lang::JString toString();
                JMatcher useAnchoringBounds(jboolean b);
                JMatcher usePattern(JPattern newPattern);
                JMatcher useTransparentBounds(jboolean b);

            private:
                Matcher(JPattern parent, cpp::lang::JCharSequence text);
                
                jint getTextLength();
                cpp::lang::JCharSequence getSubSequence(jint beginIndex, jint endIndex);
                jboolean search(jint from);
                jboolean match(jint from, jint anchor);
                jint getMatchedGroupIndex(cpp::lang::JString name);
            
                JPattern parentPattern;
                jarray<jint> groups;
                jint from, to;
                jint lookbehindTo;
                cpp::lang::JCharSequence text;
                static const jint ENDANCHOR;
                static const jint NOANCHOR;
                jint acceptMode;
                jint first;
                jint last;
                jint oldLast;
                jint lastAppendPosition;
                jarray<jint> locals;
                jboolean _hitEnd;
                jboolean _requireEnd;
                jboolean transparentBounds;
                jboolean anchoringBounds;
                
                friend class Pattern;
            };
        }
    }
}

#endif // CPP_UTIL_REGEX_MATCHER_H
