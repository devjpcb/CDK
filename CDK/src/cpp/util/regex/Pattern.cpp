#include "Pattern.h"
#include "Matcher.h"
#include "ASCII.h"
#include "../ArrayList.h"
#include "../HashMap.h"
#include "../../lang/String.h"
#include "../../lang/StringBuilder.h"
#include "../../lang/Character.h"
#include <cassert>

using namespace cpp::lang;

namespace cpp
{
    namespace util
    {
        namespace regex
        {
            Pattern::Pattern()
            {
            }

            Pattern::~Pattern()
            {
            }

            JPattern Pattern::compile(JString regex)
            {
                //TODO: return std::shared_ptr<Pattern>(new Pattern(regex, 0));
                return nullptr;
            }

            JPattern Pattern::compile(JString regex, jint flags)
            {
                //TODO: rreturn std::shared_ptr<Pattern>(new Pattern(regex, flags));
                return nullptr;
            }

            jint Pattern::flags()
            {
                return _flags;
            }

            JMatcher Pattern::matcher(cpp::lang::JCharSequence input)
            {
                if(!compiled) {
                    //synchronized(this)
                    {
                        jsynchronized(this);
                        if(!compiled)
                            compile();
                    }
                }
                JMatcher m(new Matcher(shared_from_this(), input));
                return m;
            }

            jboolean Pattern::matches(JString regex, JCharSequence input)
            {
                JPattern p = Pattern::compile(regex);
                JMatcher m = p->matcher(input);
                return m->matches();
            }

            JString Pattern::pattern()
            {
                return _pattern;
            }

            JString Pattern::quote(JString s)
            {
                jint slashEIndex = s->indexOf(jstr(u"\\E"));
                if(slashEIndex == -1)
                    return jstr(u"\\Q") + s + jstr(u"\\E");

                JStringBuilder sb = jnew<StringBuilder>(s->length() * 2);
                sb->append(jstr(u"\\Q"));
                slashEIndex = 0;
                jint current = 0;
                while((slashEIndex = s->indexOf(jstr(u"\\E"), current)) != -1) {
                    sb->append(s->substring(current, slashEIndex));
                    current = slashEIndex + 2;
                    sb->append(jstr(u"\\E\\\\E\\Q"));
                }
                sb->append(s->substring(current, s->length()));
                sb->append(jstr(u"\\E"));
                return sb->toString();
            }

            jarray<JString> Pattern::split(JCharSequence input)
            {
                return split(input, 0);
            }

            jarray<JString> Pattern::split(JCharSequence input, jint limit)
            {
                jint index = 0;
                jboolean matchLimited = limit > 0;
                JArrayList<JString> matchList = jnew<ArrayList<JString>>();
                JMatcher m = matcher(input);

                // Add segments before each match found
                while(m->find()) {
                    if(!matchLimited || matchList->size() < limit - 1) {
                        if(index == 0 && index == m->start() && m->start() == m->end()) {
                            // no empty leading substring included for zero-width match
                            // at the beginning of the input char sequence.
                            continue;
                        }
                        JString match = input->subSequence(index, m->start())->toString();
                        matchList->add(match);
                        index = m->end();
                    }
                    else if(matchList->size() == limit - 1) {    // last one
                        JString match = input->subSequence(index, input->length())->toString();
                        matchList->add(match);
                        index = m->end();
                    }
                }

                // If no match was found, return this
                if(index == 0)
                    return jarray<JString> {input->toString()};

                // Add remaining segment
                if(!matchLimited || matchList->size() < limit)
                    matchList->add(input->subSequence(index, input->length())->toString());

                // Construct result
                int resultSize = matchList->size();
                if(limit == 0)
                    while(resultSize > 0 && matchList->get(resultSize-1)->equals(jstr(u"")))
                        resultSize--;
                jarray<JString> result(resultSize);
                return matchList->subList(0, resultSize)->toArray(result);
            }

            Pattern::TreeInfo::TreeInfo()
            {
                reset();
            }

            void Pattern::TreeInfo::reset()
            {
                minLength = 0;
                maxLength = 0;
                maxValid = true;
                deterministic = true;
            }

            Pattern::Node::Node() : next(Pattern::_accept)
            {
            }

            Pattern::Node::~Node() {}

            jboolean Pattern::Node::match(JMatcher matcher, jint i, JCharSequence seq)
            {
                matcher->last = i;
                matcher->groups[0] = matcher->first;
                matcher->groups[1] = matcher->last;
                return true;
            }

            jboolean Pattern::Node::study(JTreeInfo info)
            {
                if(next != nullptr) {
                    return next->study(info);
                }
                else {
                    return info->deterministic;
                }
            }

            jboolean Pattern::LastNode::match(JMatcher matcher, jint i, JCharSequence seq)
            {
                if(matcher->acceptMode == Matcher::ENDANCHOR && i != matcher->to)
                    return false;
                matcher->last = i;
                matcher->groups[0] = matcher->first;
                matcher->groups[1] = matcher->last;
                return true;
            }

            Pattern::Start::Start(JNode node)
            {
                this->next = node;
                JTreeInfo info = jnew<TreeInfo>();
                next->study(info);
                minLength = info->minLength;
            }

            jboolean Pattern::Start::match(JMatcher matcher, jint i, JCharSequence seq)
            {
                if(i > matcher->to - minLength) {
                    matcher->_hitEnd = true;
                    return false;
                }
                jint guard = matcher->to - minLength;
                for(; i <= guard; i++) {
                    if(next->match(matcher, i, seq)) {
                        matcher->first = i;
                        matcher->groups[0] = matcher->first;
                        matcher->groups[1] = matcher->last;
                        return true;
                    }
                }
                matcher->_hitEnd = true;
                return false;
            }

            jboolean Pattern::Start::study(JTreeInfo info)
            {
                next->study(info);
                info->maxValid = false;
                info->deterministic = false;
                return false;
            }

            Pattern::StartS::StartS(JNode node) : Start(node)
            {
            }

            jboolean Pattern::StartS::match(JMatcher matcher, jint i, JCharSequence seq)
            {
                if(i > matcher->to - minLength) {
                    matcher->_hitEnd = true;
                    return false;
                }
                jint guard = matcher->to - minLength;
                while(i <= guard) {
                    //if ((ret = next.match(matcher, i, seq)) || i == guard)
                    if(next->match(matcher, i, seq)) {
                        matcher->first = i;
                        matcher->groups[0] = matcher->first;
                        matcher->groups[1] = matcher->last;
                        return true;
                    }
                    if(i == guard)
                        break;
                    // Optimization to move to the next character. This is
                    // faster than countChars(seq, i, 1).
                    if(Character::isHighSurrogate(seq->charAt(i++))) {
                        if(i < seq->length() &&
                           Character::isLowSurrogate(seq->charAt(i))) {
                            i++;
                        }
                    }
                }
                matcher->_hitEnd = true;
                return false;
            }

            jboolean Pattern::Begin::match(JMatcher matcher, jint i, JCharSequence seq)
            {
                jint fromIndex = (matcher->anchoringBounds) ? matcher->from : 0;
                if(i == fromIndex && next->match(matcher, i, seq)) {
                    matcher->first = i;
                    matcher->groups[0] = i;
                    matcher->groups[1] = matcher->last;
                    return true;
                }
                else {
                    return false;
                }
            }

            jboolean Pattern::End::match(JMatcher matcher, jint i, JCharSequence seq)
            {
                jint endIndex = (matcher->anchoringBounds) ? matcher->to : matcher->getTextLength();
                if(i == endIndex) {
                    matcher->_hitEnd = true;
                    return next->match(matcher, i, seq);
                }

                return false;
            }

            jboolean Pattern::Caret::match(JMatcher matcher, jint i, JCharSequence seq)
            {
                jint startIndex = matcher->from;
                jint endIndex = matcher->to;
                if(!matcher->anchoringBounds) {
                    startIndex = 0;
                    endIndex = matcher->getTextLength();
                }
                // Perl does not match ^ at end of input even after newline
                if(i == endIndex) {
                    matcher->_hitEnd = true;
                    return false;
                }
                if(i > startIndex) {
                    jchar ch = seq->charAt(i-1);
                    if(ch != u'\n' && ch != u'\r' && (ch|1) != u'\u2029' && ch != u'\u0085') {
                        return false;
                    }
                    // Should treat /r/n as one newline
                    if(ch == u'\r' && seq->charAt(i) == u'\n')
                        return false;
                }
                return next->match(matcher, i, seq);
            }

            jboolean Pattern::UnixCaret::match(JMatcher matcher, jint i, JCharSequence seq)
            {
                jint startIndex = matcher->from;
                jint endIndex = matcher->to;
                if(!matcher->anchoringBounds) {
                    startIndex = 0;
                    endIndex = matcher->getTextLength();
                }
                // Perl does not match ^ at end of input even after newline
                if(i == endIndex) {
                    matcher->_hitEnd = true;
                    return false;
                }
                if(i > startIndex) {
                    char ch = seq->charAt(i-1);
                    if(ch != u'\n') {
                        return false;
                    }
                }
                return next->match(matcher, i, seq);
            }

            jboolean Pattern::LastMatch::match(JMatcher matcher, jint i, JCharSequence seq)
            {
                if(i != matcher->oldLast)
                    return false;
                return next->match(matcher, i, seq);
            }

            Pattern::Dollar::Dollar(jboolean mul) : multiline(mul)
            {
            }

            jboolean Pattern::Dollar::match(JMatcher matcher, jint i, JCharSequence seq)
            {
                int endIndex = (matcher->anchoringBounds) ? matcher->to : matcher->getTextLength();
                if(!multiline) {
                    if(i < endIndex - 2)
                        return false;
                    if(i == endIndex - 2) {
                        char ch = seq->charAt(i);
                        if(ch != u'\r')
                            return false;
                        ch = seq->charAt(i + 1);
                        if(ch != u'\n')
                            return false;
                    }
                }
                // Matches before any line terminator; also matches at the
                // end of input
                // Before line terminator:
                // If multiline, we match here no matter what
                // If not multiline, fall through so that the end
                // is marked as hit; this must be a /r/n or a /n
                // at the very end so the end was hit; more input
                // could make this not match here
                if(i < endIndex) {
                    char ch = seq->charAt(i);
                    if(ch == u'\n') {
                        // No match between \r\n
                        if(i > 0 && seq->charAt(i-1) == u'\r')
                            return false;
                        if(multiline)
                            return next->match(matcher, i, seq);
                    }
                    else if(ch == u'\r' || ch == u'\u0085' ||
                            (ch|1) == u'\u2029') {
                        if(multiline)
                            return next->match(matcher, i, seq);
                    }
                    else {   // No line terminator, no match
                        return false;
                    }
                }
                // Matched at current end so hit end
                matcher->_hitEnd = true;
                // If a $ matches because of end of input, then more input
                // could cause it to fail!
                matcher->_requireEnd = true;
                return next->match(matcher, i, seq);
            }

            jboolean Pattern::Dollar::study(JTreeInfo info)
            {
                next->study(info);
                return info->deterministic;
            }

            Pattern::UnixDollar::UnixDollar(jboolean mul) : multiline(mul)
            {
            }

            jboolean Pattern::UnixDollar::match(JMatcher matcher, jint i, JCharSequence seq)
            {
                jint endIndex = (matcher->anchoringBounds) ? matcher->to : matcher->getTextLength();
                if(i < endIndex) {
                    char ch = seq->charAt(i);
                    if(ch == u'\n') {
                        // If not multiline, then only possible to
                        // match at very end or one before end
                        if(multiline == false && i != endIndex - 1)
                            return false;
                        // If multiline return next.match without setting
                        // matcher.hitEnd
                        if(multiline)
                            return next->match(matcher, i, seq);
                    }
                    else {
                        return false;
                    }
                }
                // Matching because at the end or 1 before the end;
                // more input could change this so set hitEnd
                matcher->_hitEnd = true;
                // If a $ matches because of end of input, then more input
                // could cause it to fail!
                matcher->_requireEnd = true;
                return next->match(matcher, i, seq);
            }

            jboolean Pattern::UnixDollar::study(JTreeInfo info)
            {
                next->study(info);
                return info->deterministic;
            }

            jboolean Pattern::LineEnding::match(JMatcher matcher, jint i, JCharSequence seq)
            {
                // (u+000Du+000A|[u+000Au+000Bu+000Cu+000Du+0085u+2028u+2029])
                if(i < matcher->to) {
                    jint ch = seq->charAt(i);
                    if(ch == 0x0A || ch == 0x0B || ch == 0x0C || ch == 0x85 || ch == 0x2028 || ch == 0x2029)
                        return next->match(matcher, i + 1, seq);
                    if(ch == 0x0D) {
                        i++;
                        if(i < matcher->to && seq->charAt(i) == 0x0A)
                            i++;
                        return next->match(matcher, i, seq);
                    }
                }
                else {
                    matcher->_hitEnd = true;
                }

                return false;
            }

            jboolean Pattern::LineEnding::study(JTreeInfo info)
            {
                info->minLength++;
                info->maxLength += 2;
                return next->study(info);
            }

            Pattern::JCharProperty Pattern::CharProperty::complement()
            {
                class ComplementCharProperty : public CharProperty
                {
                public:
                    jboolean isSatisfiedBy(jint ch) {
                        return !this->isSatisfiedBy(ch);
                    }
                };

                return jnew<ComplementCharProperty>();
            }

            jboolean Pattern::CharProperty::match(JMatcher matcher, jint i, JCharSequence seq)
            {
                if(i < matcher->to) {
                    int ch = Character::codePointAt(seq, i);
                    return isSatisfiedBy(ch) && next->match(matcher, i + Character::charCount(ch), seq);
                }
                else {
                    matcher->_hitEnd = true;
                    return false;
                }
            }

            jboolean Pattern::CharProperty::study(JTreeInfo info)
            {
                info->minLength++;
                info->maxLength++;
                return next->study(info);
            }

            jboolean Pattern::All::isSatisfiedBy(jint ch)
            {
                return true;
            }

            jboolean Pattern::Dot::isSatisfiedBy(jint ch)
            {
                return (ch != u'\n' && ch != u'\r'&& (ch|1) != u'\u2029' && ch != u'\u0085');
            }

            jboolean Pattern::UnixDot::isSatisfiedBy(jint ch)
            {
                return ch != u'\n';
            }

            Pattern::Ques::Ques(JNode node, jint type) : atom(node), type(type)
            {
                
            }

            jboolean Pattern::Ques::match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq)
            {
                if (type == GREEDY) {
                    return (atom->match(matcher, i, seq) && next->match(matcher, matcher->last, seq)) || next->match(matcher, i, seq);
                } else if (type == LAZY) {
                    return next->match(matcher, i, seq) || (atom->match(matcher, i, seq) && next->match(matcher, matcher->last, seq));
                } else if (type == POSSESSIVE) {
                    if (atom->match(matcher, i, seq)) i = matcher->last;
                    return next->match(matcher, i, seq);
                }
                    
                return atom->match(matcher, i, seq) && next->match(matcher, matcher->last, seq);
            }

            jboolean Pattern::Ques::study(JTreeInfo info)
            {
                if (type != INDEPENDENT) {
                    jint minL = info->minLength;
                    atom->study(info);
                    info->minLength = minL;
                    info->deterministic = false;
                    return next->study(info);
                } else {
                    atom->study(info);
                    return next->study(info);
                }
            }

            Pattern::GroupCurly::GroupCurly(JNode node, jint cmin, jint cmax, jint type, jint local, jint group, jboolean capture)
            {

            }

            jboolean Pattern::GroupCurly::match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq)
            {
                return false;
            }

            jboolean Pattern::GroupCurly::match0(JMatcher matcher, jint i, jint j, cpp::lang::JCharSequence seq)
            {
                return false;
            }

            jboolean Pattern::GroupCurly::match1(JMatcher matcher, jint i, jint j, cpp::lang::JCharSequence seq)
            {
                return false;
            }

            jboolean Pattern::GroupCurly::match2(JMatcher matcher, jint i, jint j, cpp::lang::JCharSequence seq)
            {
                return false;
            }

            jboolean Pattern::GroupCurly::study(JTreeInfo info)
            {
                return false;
            }

            jboolean Pattern::BmpCharProperty::match(JMatcher matcher, jint i, JCharSequence seq)
            {
                if(i < matcher->to) {
                    return isSatisfiedBy(seq->charAt(i)) && next->match(matcher, i + 1, seq);
                }
                else {
                    matcher->_hitEnd = true;
                    return false;
                }
            }

            Pattern::BitClass::BitClass()
            {

            }

            Pattern::BitClass::BitClass(jarray<jboolean> bits)
            {

            }

            Pattern::JBitClass Pattern::BitClass::add(jint c, jint flags)
            {
                return nullptr;
            }

            jboolean Pattern::BitClass::isSatisfiedBy(jint ch)
            {
                return nullptr;
            }

            Pattern::SingleS::SingleS(jint c) : c(c)
            {
            }

            jboolean Pattern::SingleS::isSatisfiedBy(jint ch)
            {
                return ch == c;
            }

            Pattern::Single::Single(jint c) : c(c)
            {
            }

            jboolean Pattern::Single::isSatisfiedBy(jint ch)
            {
                return ch == c;
            }

            Pattern::SingleI::SingleI(jint lower, jint upper) : lower(lower), upper(upper)
            {
            }

            jboolean Pattern::SingleI::isSatisfiedBy(jint ch)
            {
                return ch == lower || ch == upper;
            }

            Pattern::SingleU::SingleU(jint lower) : lower(lower)
            {
            }

            jboolean Pattern::SingleU::isSatisfiedBy(jint ch)
            {
                return lower == ch || lower == Character::toLowerCase(Character::toUpperCase(ch));
            }

            Pattern::SliceNode::SliceNode(jarray<jint> buf) : buffer(buf)
            {
            }

            jboolean Pattern::SliceNode::study(JTreeInfo info)
            {
                info->minLength += buffer.length();
                info->maxLength += buffer.length();
                return next->study(info);
            }

            Pattern::Slice::Slice(jarray<jint> buf) : SliceNode(buf)
            {
            }

            jboolean Pattern::Slice::match(JMatcher matcher, jint i, JCharSequence seq)
            {
                jarray<jint> buf = buffer;
                jint len = buf.length();
                for(jint j=0; j<len; j++) {
                    if((i+j) >= matcher->to) {
                        matcher->_hitEnd = true;
                        return false;
                    }
                    if(buf[j] != seq->charAt(i+j))
                        return false;
                }
                return next->match(matcher, i+len, seq);
            }

            Pattern::SliceI::SliceI(jarray<jint> buf) : SliceNode(buf)
            {
            }

            jboolean Pattern::SliceI::match(JMatcher matcher, jint i, JCharSequence seq)
            {
                jarray<jint> buf = buffer;
                jint len = buf.length();
                for(jint j=0; j<len; j++) {
                    if((i+j) >= matcher->to) {
                        matcher->_hitEnd = true;
                        return false;
                    }
                    int c = seq->charAt(i+j);
                    if(buf[j] != c && buf[j] != ASCII::toLower(c))
                        return false;
                }
                return next->match(matcher, i+len, seq);
            }

            Pattern::SliceU::SliceU(jarray<jint> buf) : SliceNode(buf)
            {
            }

            jboolean Pattern::SliceU::match(JMatcher matcher, jint i, JCharSequence seq)
            {
                jarray<jint> buf = buffer;
                jint len = buf.length();
                for(jint j = 0; j < len; j++) {
                    if((i + j) >= matcher->to) {
                        matcher->_hitEnd = true;
                        return false;
                    }
                    int c = seq->charAt(i+j);
                    if(buf[j] != c && buf[j] != Character::toLowerCase(Character::toUpperCase(c)))
                        return false;
                }

                return next->match(matcher, i + len, seq);
            }

            Pattern::SliceS::SliceS(jarray<jint> buf) : SliceNode(buf)
            {
            }

            jboolean Pattern::SliceS::SliceS::match(JMatcher matcher, jint i, JCharSequence seq)
            {
                jarray<jint> buf = buffer;
                jint x = i;
                for(jint j = 0; j < buf.length(); j++) {
                    if(x >= matcher->to) {
                        matcher->_hitEnd = true;
                        return false;
                    }
                    jint c = Character::codePointAt(seq, x);
                    if(buf[j] != c)
                        return false;
                    x += Character::charCount(c);
                    if(x > matcher->to) {
                        matcher->_hitEnd = true;
                        return false;
                    }
                }
                return next->match(matcher, x, seq);
            }

            Pattern::SliceIS::SliceIS(jarray<jint> buf) : SliceNode(buf)
            {
            }

            jint Pattern::SliceIS::toLower(jint c)
            {
                return ASCII::toLower(c);
            }

            jboolean Pattern::SliceIS::match(JMatcher matcher, jint i, JCharSequence seq)
            {
                jarray<jint> buf = buffer;
                jint x = i;
                for(jint j = 0; j < buf.length(); j++) {
                    if(x >= matcher->to) {
                        matcher->_hitEnd = true;
                        return false;
                    }
                    jint c = Character::codePointAt(seq, x);
                    if(buf[j] != c && buf[j] != toLower(c))
                        return false;
                    x += Character::charCount(c);
                    if(x > matcher->to) {
                        matcher->_hitEnd = true;
                        return false;
                    }
                }
                return next->match(matcher, x, seq);
            }

            Pattern::SliceUS::SliceUS(jarray<jint> buf) : SliceIS(buf)
            {
            }

            jint Pattern::SliceUS::toLower(jint c)
            {
                return Character::toLowerCase(Character::toUpperCase(c));
            }

            jboolean Pattern::BranchConn::match(JMatcher matcher, jint i, JCharSequence seq)
            {
                return false;
            }

            jboolean Pattern::BranchConn::study(JTreeInfo info)
            {
                return false;
            }

            Pattern::Branch::Branch(JNode first, JNode second, JNode branchConn)
            {

            }

            void Pattern::Branch::add(JNode node)
            {

            }

            jboolean Pattern::Branch::match(JMatcher matcher, jint i, JCharSequence seq)
            {
                return false;
            }

            jboolean Pattern::Branch::study(JTreeInfo info)
            {
                return false;
            }

            Pattern::GroupHead::GroupHead(jint localCount)
            {

            }

            jboolean Pattern::GroupHead::match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq)
            {
                return false;
            }

            jboolean Pattern::GroupHead::matchRef(JMatcher matcher, jint i, cpp::lang::JCharSequence seq)
            {
                return false;
            }

            Pattern::GroupTail::GroupTail(jint localCount, jint groupCount)
            {

            }

            jboolean Pattern::GroupTail::match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq)
            {
                return false;
            }

            Pattern::Prolog::Prolog(JLoop loop)
            {

            }

            jboolean Pattern::Prolog::match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq)
            {
                return false;
            }

            jboolean Pattern::Prolog::study(JTreeInfo info)
            {
                return false;
            }

            Pattern::Loop::Loop(jint countIndex, jint beginIndex)
            {

            }

            jboolean Pattern::Loop::match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq)
            {
                return false;
            }

            jboolean Pattern::Loop::matchInit(JMatcher matcher, jint i, cpp::lang::JCharSequence seq)
            {
                return false;
            }

            jboolean Pattern::Loop::study(JTreeInfo info)
            {
                return false;
            }

            Pattern::LazyLoop::LazyLoop(jint countIndex, jint beginIndex) : Loop(countIndex, beginIndex)
            {

            }

            jboolean Pattern::LazyLoop::match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq)
            {
                return false;
            }

            jboolean Pattern::LazyLoop::matchInit(JMatcher matcher, jint i, cpp::lang::JCharSequence seq)
            {
                return false;
            }

            jboolean Pattern::LazyLoop::study(JTreeInfo info)
            {
                return false;
            }

            Pattern::Pos::Pos(JNode cond)
            {

            }

            jboolean Pattern::Pos::match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq)
            {
                return false;
            }

            Pattern::Neg::Neg(JNode cond)
            {

            }

            jboolean Pattern::Neg::match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq)
            {
                return false;
            }

            Pattern::JNode Pattern::BnM::optimize(JNode node)
            {
                if(!(jinstanceof<Slice>(node))) {
                    return node;
                }

                jarray<jint> src = (std::dynamic_pointer_cast<Slice>(node))->buffer;
                jint patternLength = src.length();
                // The BM algorithm requires a bit of overhead;
                // If the pattern is short don't use it, since
                // a shift larger than the pattern length cannot
                // be used anyway.
                if(patternLength < 4) {
                    return node;
                }
                jint i, j, k;
                jarray<jint> lastOcc(128);
                jarray<jint> optoSft(patternLength);
                // Precalculate part of the bad character shift
                // It is a table for where in the pattern each
                // lower 7-bit value occurs
                for(i = 0; i < patternLength; i++) {
                    lastOcc[src[i]&0x7F] = i + 1;
                }
                // Precalculate the good suffix shift
                // i is the shift amount being considered
                for(i = patternLength; i > 0; i--) {
                    // j is the beginning index of suffix being considered
                    for(j = patternLength - 1; j >= i; j--) {
                        // Testing for good suffix
                        if(src[j] == src[j-i]) {
                            // src[j..len] is a good suffix
                            optoSft[j-1] = i;
                        }
                        else {
                            // No match. The array has already been
                            // filled up with correct values before.
                            goto NEXT;
                        }
                    }
                    // This fills up the remaining of optoSft
                    // any suffix can not have larger shift amount
                    // then its sub-suffix. Why???
                    while(j > 0) {
                        optoSft[--j] = i;
                    }
                }
NEXT:
                // Set the guard value because of unicode compression
                optoSft[patternLength-1] = 1;
                if(jinstanceof<SliceS>(node))
                    return jnew<BnMS>(src, lastOcc, optoSft, node->next);
                return jnew<BnM>(src, lastOcc, optoSft, node->next);
            }

            Pattern::BnM::BnM(jarray<jint> src, jarray<jint> lastOcc, jarray<jint> optoSft, JNode next) :
                buffer(src), lastOcc(lastOcc), optoSft(optoSft)
            {
                this->next = next;
            }

            jboolean Pattern::BnM::match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq)
            {
                jarray<jint> src = buffer;
                jint patternLength = src.length();
                jint last = matcher->to - patternLength;

                // Loop over all possible match positions in text
                while(i <= last) {
                    // Loop over pattern from right to left
                    for(int j = patternLength - 1; j >= 0; j--) {
                        int ch = seq->charAt(i+j);
                        if(ch != src[j]) {
                            // Shift search to the right by the maximum of the
                            // bad character shift and the good suffix shift
                            i += Math::max(j + 1 - lastOcc[ch&0x7F], optoSft[j]);
                            goto NEXT;
                        }
                    }
                    // Entire pattern matched starting at i
                    matcher->first = i;
                    jboolean ret = next->match(matcher, i + patternLength, seq);
                    if(ret) {
                        matcher->first = i;
                        matcher->groups[0] = matcher->first;
                        matcher->groups[1] = matcher->last;
                        return true;
                    }
                    i++;
                }
NEXT:
                // BnM is only used as the leading node in the unanchored case,
                // and it replaced its Start() which always searches to the end
                // if it doesn't find what it's looking for, so hitEnd is true.
                matcher->_hitEnd = true;
                return false;
            }

            jboolean Pattern::BnM::study(JTreeInfo info)
            {
                info->minLength += buffer.length();
                info->maxValid = false;
                return next->study(info);
            }

            Pattern::BnMS::BnMS(jarray<jint> src, jarray<jint> lastOcc, jarray<jint> optoSft, JNode next) : BnM(src, lastOcc, optoSft, next)
            {
                for(jint x = 0; x < buffer.length(); x++) {
                    lengthInChars += Character::charCount(buffer[x]);
                }
            }

            jboolean Pattern::BnMS::match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq)
            {
                jarray<jint> src = buffer;
                jint patternLength = src.length();
                jint last = matcher->to - lengthInChars;

                // Loop over all possible match positions in text
                while(i <= last) {
                    // Loop over pattern from right to left
                    jint ch;
                    for(jint j = countChars(seq, i, patternLength), x = patternLength - 1; j > 0; j -= Character::charCount(ch), x--) {
                        ch = Character::codePointBefore(seq, i+j);
                        if(ch != src[x]) {
                            // Shift search to the right by the maximum of the
                            // bad character shift and the good suffix shift
                            jint n = Math::max(x + 1 - lastOcc[ch&0x7F], optoSft[x]);
                            i += countChars(seq, i, n);
                            goto NEXT;
                        }
                    }
                    // Entire pattern matched starting at i
                    matcher->first = i;
                    jboolean ret = next->match(matcher, i + lengthInChars, seq);
                    if(ret) {
                        matcher->first = i;
                        matcher->groups[0] = matcher->first;
                        matcher->groups[1] = matcher->last;
                        return true;
                    }
                    i += countChars(seq, i, 1);
                }
NEXT:
                matcher->_hitEnd = true;
                return false;
            }

            jboolean Pattern::LookbehindEnd::match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq)
            {
                return false;
            }

            Pattern::Behind::Behind(JNode cond, jint rmax, jint rmin)
            {

            }

            jboolean Pattern::Behind::match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq)
            {
                return false;
            }

            Pattern::BehindS::BehindS(JNode cond, jint rmax, jint rmin) : Behind(cond, rmax, rmin)
            {

            }

            jboolean Pattern::BehindS::match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq)
            {
                return false;
            }

            Pattern::NotBehind::NotBehind(JNode cond, jint rmax, jint rmin)
            {

            }

            jboolean Pattern::NotBehind::match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq)
            {
                return false;
            }

            Pattern::NotBehindS::NotBehindS(JNode cond, jint rmax, jint rmin) : NotBehind(cond, rmax, rmin)
            {

            }

            jboolean Pattern::NotBehindS::match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq)
            {
                return false;
            }

            Pattern::Pattern(JString p, jint f)
            {
                _pattern = p;
                _flags = f;

                // to use UNICODE_CASE if UNICODE_CHARACTER_CLASS present
                if((_flags & UNICODE_CHARACTER_CLASS) != 0)
                    _flags |= UNICODE_CASE;

                // Reset group index count
                capturingGroupCount = 1;
                localCount = 0;

                if(_pattern->length() > 0) {
                    compile();
                }
                else {
                    root = jnew<Start>(lastAccept);
                    matchRoot = lastAccept;
                }
            }

            void Pattern::normalize()
            {
            }

            void Pattern::RemoveQEQuoting()
            {
                const jint pLen = patternLength;
                jint i = 0;
                while(i < pLen - 1) {
                    if(temp[i] != '\\')
                        i += 1;
                    else if(temp[i + 1] != 'Q')
                        i += 2;
                    else
                        break;
                }
                if(i >= pLen - 1)     // No \Q sequence found
                    return;
                jint j = i;
                i += 2;
                jarray<int> newtemp(j + 3*(pLen-i) + 2);
                System::arraycopy(temp, 0, newtemp, 0, j);

                jboolean inQuote = true;
                jboolean beginQuote = true;
                while(i < pLen) {
                    jint c = temp[i++];
                    if(!ASCII::isAscii(c) || ASCII::isAlpha(c)) {
                        newtemp[j++] = c;
                    }
                    else if(ASCII::isDigit(c)) {
                        if(beginQuote) {
                            /*
                             * A unicode escape \[0xu] could be before this quote,
                             * and we don't want this numeric char to processed as
                             * part of the escape.
                             */
                            newtemp[j++] = u'\\';
                            newtemp[j++] = u'x';
                            newtemp[j++] = u'3';
                        }
                        newtemp[j++] = c;
                    }
                    else if(c != u'\\') {
                        if(inQuote) newtemp[j++] = u'\\';
                        newtemp[j++] = c;
                    }
                    else if(inQuote) {
                        if(temp[i] == u'E') {
                            i++;
                            inQuote = false;
                        }
                        else {
                            newtemp[j++] = u'\\';
                            newtemp[j++] = u'\\';
                        }
                    }
                    else {
                        if(temp[i] == u'Q') {
                            i++;
                            inQuote = true;
                            beginQuote = true;
                            continue;
                        }
                        else {
                            newtemp[j++] = c;
                            if(i != pLen)
                                newtemp[j++] = temp[i++];
                        }
                    }

                    beginQuote = false;
                }

                patternLength = j;
                temp = Arrays::copyOf(newtemp, j + 2); // double zero termination
            }

            void Pattern::compile()
            {
                // Handle canonical equivalences
                if(has(CANON_EQ) && !has(LITERAL)) {
                    normalize();
                }
                else {
                    normalizedPattern = _pattern;
                }
                patternLength = normalizedPattern->length();

                // Copy pattern to int array for convenience
                // Use double zero to terminate pattern
                temp = jnew_array<jint>(patternLength + 2);

                hasSupplementary = false;
                jint c, count = 0;
                // Convert all chars into code points
                for(jint x = 0; x < patternLength; x += Character::charCount(c)) {
                    c = normalizedPattern->codePointAt(x);
                    if(isSupplementary(c)) {
                        hasSupplementary = true;
                    }
                    temp[count++] = c;
                }

                patternLength = count;   // patternLength now in code points

                if(!has(LITERAL))
                    RemoveQEQuoting();

                // Allocate all temporary objects here.
                buffer = jnew_array<jint>(32);
                groupNodes = jnew_array<GroupHead>(10);
                _namedGroups = nullptr;

                if(has(LITERAL)) {
                    // Literal pattern handling
                    matchRoot = newSlice(temp, patternLength, hasSupplementary);
                    matchRoot->next = lastAccept;
                }
                else {
                    // Start recursive descent parsing
                    matchRoot = expr(lastAccept);
                    // Check extra pattern characters
                    if(patternLength != _cursor) {
                        if(peek() == u')') {
                            throw error(jstr(u"Unmatched closing ')'"));
                        }
                        else {
                            throw error(jstr(u"Unexpected internal error"));
                        }
                    }
                }

                // Peephole optimization
                if(jinstanceof<Slice>(matchRoot)) {
                    root = BnM::optimize(matchRoot);
                    if(root == matchRoot) {
                        root = hasSupplementary ? jnew<StartS>(matchRoot) : jnew<Start>(matchRoot);
                    }
                }
                else if(jinstanceof<Begin>(matchRoot) || jinstanceof<First>(matchRoot)) {
                    root = matchRoot;
                }
                else {
                    root = hasSupplementary ? jnew<StartS>(matchRoot) : jnew<Start>(matchRoot);
                }

                // Release temporary storage
                temp = nullptr;
                buffer = nullptr;
                groupNodes = nullptr;
                patternLength = 0;
                compiled = true;
            }

            JMap<JString, JInteger> Pattern::namedGroups()
            {
                if(_namedGroups == nullptr)
                    _namedGroups = jnew<HashMap<JString, JInteger>>(2);
                return _namedGroups;
            }

            jboolean Pattern::has(jint f)
            {
                return (_flags & f) != 0;
            }

            void Pattern::accept(jint ch, JString s)
            {

            }

            void Pattern::mark(jint c)
            {
                temp[patternLength] = c;
            }

            jint Pattern::peek()
            {
                return -1;
            }

            jint Pattern::read()
            {
                jint ch = temp[_cursor++];
                if(has(COMMENTS))
                    ch = parsePastWhitespace(ch);
                return ch;
            }

            jint Pattern::next()
            {
                jint ch = temp[++_cursor];
                if(has(COMMENTS))
                    ch = peekPastWhitespace(ch);
                return ch;
            }

            jint Pattern::nextEscaped()
            {
                jint ch = temp[++_cursor];
                return ch;
            }

            jint Pattern::peekPastWhitespace(jint ch)
            {
                while(ASCII::isSpace(ch) || ch == u'#') {
                    while(ASCII::isSpace(ch))
                        ch = temp[++_cursor];
                    if(ch == u'#') {
                        ch = peekPastLine();
                    }
                }
                return ch;
            }

            jint Pattern::parsePastWhitespace(jint ch)
            {
                while(ASCII::isSpace(ch) || ch == u'#') {
                    while(ASCII::isSpace(ch))
                        ch = temp[_cursor++];
                    if(ch == u'#')
                        ch = parsePastLine();
                }
                return ch;
            }

            jint Pattern::parsePastLine()
            {
                jint ch = temp[_cursor++];
                while(ch != 0 && !isLineSeparator(ch))
                    ch = temp[_cursor++];
                return ch;
            }

            jint Pattern::peekPastLine()
            {
                jint ch = temp[++_cursor];
                while(ch != 0 && !isLineSeparator(ch))
                    ch = temp[++_cursor];
                return ch;
            }

            jboolean Pattern::isLineSeparator(jint ch)
            {
                if(has(UNIX_LINES)) {
                    return ch == u'\n';
                }
                else {
                    return (ch == u'\n' ||
                            ch == u'\r' ||
                            (ch|1) == u'\u2029' ||
                            ch == u'\u0085');
                }
            }

            jint Pattern::skip()
            {
                jint i = _cursor;
                jint ch = temp[i+1];
                _cursor = i + 2;
                return ch;
            }

            void Pattern::unread()
            {
                _cursor--;
            }

            PatternSyntaxException Pattern::error(cpp::lang::JString s)
            {
                return PatternSyntaxException(s, normalizedPattern,  _cursor - 1);
            }

            jboolean Pattern::findSupplementary(jint start, jint end)
            {
                for(jint i = start; i < end; i++) {
                    if(isSupplementary(temp[i]))
                        return true;
                }
                return false;
            }

            jboolean Pattern::isSupplementary(jint ch)
            {
                return ch >= Character::MIN_SUPPLEMENTARY_CODE_POINT || Character::isSurrogate((jchar)ch);
            }

            Pattern::JNode Pattern::expr(JNode end)
            {
                JNode prev = nullptr;
                JNode firstTail = nullptr;
                JBranch branch = nullptr;
                JNode branchConn = nullptr;

                for(;;) {
                    JNode node = sequence(end);
                    JNode nodeTail = root;      //double return
                    if(prev == nullptr) {
                        prev = node;
                        firstTail = nodeTail;
                    }
                    else {
                        // Branch
                        if(branchConn == nullptr) {
                            branchConn = jnew<BranchConn>();
                            branchConn->next = end;
                        }
                        if(node == end) {
                            // if the node returned from sequence() is "end"
                            // we have an empty expr, set a null atom into
                            // the branch to indicate to go "next" directly.
                            node = nullptr;
                        }
                        else {
                            // the "tail.next" of each atom goes to branchConn
                            nodeTail->next = branchConn;
                        }
                        if(prev == branch) {
                            branch->add(node);
                        }
                        else {
                            if(prev == end) {
                                prev = nullptr;
                            }
                            else {
                                // replace the "end" with "branchConn" at its tail.next
                                // when put the "prev" into the branch as the first atom.
                                firstTail->next = branchConn;
                            }
                            prev = branch = jnew<Branch>(prev, node, branchConn);
                        }
                    }
                    if(peek() != u'|') {
                        return prev;
                    }
                    next();
                }
            }

            Pattern::JNode Pattern::sequence(JNode end)
            {
                JNode head = nullptr;
                JNode tail = nullptr;
                JNode node = nullptr;

                for(;;) {
                    jint ch = peek();
                    switch(ch) {
                        case u'(':
                            // Because group handles its own closure,
                            // we need to treat it differently
                            node = group0();
                            // Check for comment or flag group
                            if(node == nullptr)
                                continue;
                            if(head == nullptr)
                                head = node;
                            else
                                tail->next = node;
                            // Double return: Tail was returned in root
                            tail = root;
                            continue;
                        case u'[':
                            node = clazz(true);
                            break;
                        case u'\\':
                            ch = nextEscaped();
                            if(ch == u'p' || ch == u'P') {
                                jboolean oneLetter = true;
                                jboolean comp = (ch == u'P');
                                ch = next(); // Consume { if present
                                if(ch != u'{') {
                                    unread();
                                }
                                else {
                                    oneLetter = false;
                                }
                                node = family(oneLetter, comp);
                            }
                            else {
                                unread();
                                node = atom();
                            }
                            break;
                        case u'^':
                            next();
                            if(has(MULTILINE)) {
                                if(has(UNIX_LINES))
                                    node = jnew<UnixCaret>();
                                else
                                    node = jnew<Caret>();
                            }
                            else {
                                node = jnew<Begin>();
                            }
                            break;
                        case u'$':
                            next();
                            if(has(UNIX_LINES))
                                node = jnew<UnixDollar>(has(MULTILINE));
                            else
                                node = jnew<Dollar>(has(MULTILINE));
                            break;
                        case u'.':
                            next();
                            if(has(DOTALL)) {
                                node = jnew<All>();
                            }
                            else {
                                if(has(UNIX_LINES))
                                    node = jnew<UnixDot>();
                                else {
                                    node = jnew<Dot>();
                                }
                            }
                            break;
                        case u'|':
                        case u')':
                            goto LOOP;
                        case u']': // Now interpreting dangling ] and } as literals
                        case u'}':
                            node = atom();
                            break;
                        case u'?':
                        case u'*':
                        case u'+':
                            next();
                            throw error(jstr(u"Dangling meta character '") + boxing(static_cast<jchar>(ch)) + jstr(u"'"));
                        case 0:
                            if(_cursor >= patternLength) {
                                goto LOOP;
                            }
                            // Fall through
                        default:
                            node = atom();
                            break;
                    }

                    node = closure(node);

                    if(head == nullptr) {
                        head = tail = node;
                    }
                    else {
                        tail->next = node;
                        tail = node;
                    }
                }
LOOP:
                if(head == nullptr) {
                    return end;
                }
                tail->next = end;
                root = tail;      //double return
                return head;
            }

            Pattern::JNode Pattern::atom()
            {
                return nullptr;
            }

            Pattern::JCharProperty Pattern::clazz(jboolean consume)
            {
                JCharProperty prev = nullptr;
                JCharProperty node = nullptr;
                JBitClass bits = jnew<BitClass>();
                jboolean include = true;
                jboolean firstInClass = true;
                jint ch = next();
                for(;;) {
                    switch(ch) {
                        case u'^':
                            // Negates if first char in a class, otherwise literal
                            if(firstInClass) {
                                if(temp[_cursor-1] != u'[')
                                    break;
                                ch = next();
                                include = !include;
                                continue;
                            }
                            else {
                                // ^ not first in class, treat as literal
                                break;
                            }
                        case u'[':
                            firstInClass = false;
                            node = clazz(true);
                            if(prev == nullptr)
                                prev = node;
                            else
                                prev = union_(prev, node);
                            ch = peek();
                            continue;
                        case u'&':
                            firstInClass = false;
                            ch = next();
                            if(ch == u'&') {
                                ch = next();
                                JCharProperty rightNode = nullptr;
                                while(ch != u']' && ch != u'&') {
                                    if(ch == u'[') {
                                        if(rightNode == nullptr)
                                            rightNode = clazz(true);
                                        else
                                            rightNode = union_(rightNode, clazz(true));
                                    }
                                    else {   // abc&&def
                                        unread();
                                        rightNode = clazz(false);
                                    }
                                    ch = peek();
                                }
                                if(rightNode != nullptr)
                                    node = rightNode;
                                if(prev == nullptr) {
                                    if(rightNode == nullptr)
                                        throw error(jstr(u"Bad class syntax"));
                                    else
                                        prev = rightNode;
                                }
                                else {
                                    prev = intersection(prev, node);
                                }
                            }
                            else {
                                // treat as a literal &
                                unread();
                                break;
                            }
                            continue;
                        case 0:
                            firstInClass = false;
                            if(_cursor >= patternLength)
                                throw error(jstr(u"Unclosed character class"));
                            break;
                        case u']':
                            firstInClass = false;
                            if(prev != nullptr) {
                                if(consume)
                                    next();
                                return prev;
                            }
                            break;
                        default:
                            firstInClass = false;
                            break;
                    }
                    node = range(bits);
                    if(include) {
                        if(prev == nullptr) {
                            prev = node;
                        }
                        else {
                            if(prev != node)
                                prev = union_(prev, node);
                        }
                    }
                    else {
                        if(prev == nullptr) {
                            prev = node->complement();
                        }
                        else {
                            if(prev != node)
                                prev = setDifference(prev, node);
                        }
                    }
                    ch = peek();
                }
            }

            Pattern::JCharProperty Pattern::range(JBitClass bits)
            {
                return nullptr;
            }

            Pattern::JCharProperty Pattern::family(jboolean singleLetter, jboolean maybeComplement)
            {
                /* TODO:
                next();
                JString name;
                JCharProperty node = nullptr;

                if (singleLetter) {
                    jint c = temp[_cursor];
                    if (!Character::isSupplementaryCodePoint(c)) {
                        name = String::valueOf(static_cast<jchar>(c));
                    } else {
                        name = jnew<String>(temp, _cursor, 1);
                    }
                    read();
                } else {
                    int i = _cursor;
                    mark(u'}');
                    while(read() != u'}') {
                    }
                    mark(u'\000');
                    int j = _cursor;
                    if (j > patternLength)
                        throw error(jstr(u"Unclosed character family"));
                    if (i + 1 >= j)
                        throw error(jstr(u"Empty character family"));
                    name = jnew<String>(temp, i, j-i-1);
                }

                int i = name->indexOf(u'=');
                if (i != -1) {
                    // property construct \p{name=value}
                    JString value = name->substring(i + 1);
                    name = name->substring(0, i)->toLowerCase(Locale::ENGLISH);
                    if (jstr(u"sc")->equals(name) || jstr(u"script")->equals(name)) {
                        node = unicodeScriptPropertyFor(value);
                    } else if (jstr(u"blk")->equals(name) || jstr(u"block")->equals(name)) {
                        node = unicodeBlockPropertyFor(value);
                    } else if (jstr(u"gc")->equals(name) || jstr(u"general_category")->equals(name)) {
                        node = charPropertyNodeFor(value);
                    } else {
                        throw error(jstr(u"Unknown Unicode property {name=<") + name + jstr(u">, ")
                                     + jstr(u"value=<") + value + jstr(u">}"));
                    }
                } else {
                    if (name->startsWith(jstr(u"In"))) {
                        // \p{inBlockName}
                        node = unicodeBlockPropertyFor(name->substring(2));
                    } else if (name->startsWith(jstr(u"Is"))) {
                        // \p{isGeneralCategory} and \p{isScriptName}
                        name = name->substring(2);
                        JUnicodeProp uprop = UnicodeProp::forName(name);
                        if (uprop != nullptr)
                            node = jnew<Utype>(uprop);
                        if (node == nullptr)
                            node = CharPropertyNames::charPropertyFor(name);
                        if (node == nullptr)
                            node = unicodeScriptPropertyFor(name);
                    } else {
                        if (has(UNICODE_CHARACTER_CLASS)) {
                            JUnicodeProp uprop = UnicodeProp::forPOSIXName(name);
                            if (uprop != nullptr)
                                node = jnew<Utype>(uprop);
                        }
                        if (node == nullptr)
                            node = charPropertyNodeFor(name);
                    }
                }
                if (maybeComplement) {
                    if (jinstanceof<Category>(node) || jinstanceof<Block>(node))
                        hasSupplementary = true;
                    node = node->complement();
                }
                return node;
                */
               return nullptr;
            }

            JString Pattern::groupname(jint ch)
            {
                JStringBuilder sb = jnew<StringBuilder>();
                sb->append(Character::toChars(ch));
                while(ASCII::isLower(ch=read()) || ASCII::isUpper(ch) ||
                      ASCII::isDigit(ch)) {
                    sb->append(Character::toChars(ch));
                }
                if(sb->length() == 0)
                    throw error(jstr(u"named capturing group has 0 length name"));
                if(ch != '>')
                    throw error(jstr(u"named capturing group is missing trailing '>'"));
                return sb->toString();
            }

            Pattern::JNode Pattern::group0()
            {
                jboolean capturingGroup = false;
                JNode head = nullptr;
                JNode tail = nullptr;
                jint save = _flags;
                root = nullptr;
                jint ch = next();
                if(ch == u'?') {
                    ch = skip();
                    switch(ch) {
                        case u':':   //  (?:xxx) pure group
                            head = createGroup(true);
                            tail = root;
                            head->next = expr(tail);
                            break;
                        case u'=':   // (?=xxx) and (?!xxx) lookahead
                        case u'!':
                            head = createGroup(true);
                            tail = root;
                            head->next = expr(tail);
                            if(ch == u'=') {
                                head = tail = jnew<Pos>(head);
                            }
                            else {
                                head = tail = jnew<Neg>(head);
                            }
                            break;
                        case u'>':   // (?>xxx)  independent group
                            head = createGroup(true);
                            tail = root;
                            head->next = expr(tail);
                            head = tail = jnew<Ques>(head, INDEPENDENT);
                            break;
                        case u'<': { // (?<xxx)  look behind
                            ch = read();
                            if(ASCII::isLower(ch) || ASCII::isUpper(ch)) {
                                // named captured group
                                JString name = groupname(ch);
                                if(namedGroups()->containsKey(name))
                                    throw error(jstr(u"Named capturing group <") + name
                                                + jstr(u"> is already defined"));
                                capturingGroup = true;
                                head = createGroup(false);
                                tail = root;
                                namedGroups()->put(name, boxing(capturingGroupCount-1));
                                head->next = expr(tail);
                                break;
                            }
                            jint start = _cursor;
                            head = createGroup(true);
                            tail = root;
                            head->next = expr(tail);
                            tail->next = lookbehindEnd;
                            JTreeInfo info = jnew<TreeInfo>();
                            head->study(info);
                            if(info->maxValid == false) {
                                throw error(jstr(u"Look-behind group does not have an obvious maximum length"));
                            }
                            jboolean hasSupplementary = findSupplementary(start, patternLength);
                            if(ch == u'=') {
                                head = tail = (hasSupplementary ?
                                               jnew<BehindS>(head, info->maxLength, info->minLength) :
                                               jnew<Behind>(head, info->maxLength, info->minLength));
                            }
                            else if(ch == u'!') {
                                head = tail = (hasSupplementary ?
                                               jnew<NotBehindS>(head, info->maxLength, info->minLength) :
                                               jnew<NotBehind>(head, info->maxLength, info->minLength));
                            }
                            else {
                                throw error(jstr(u"Unknown look-behind group"));
                            }
                            break;
                        }
                        case u'$':
                        case u'@':
                            throw error(jstr(u"Unknown group type"));
                        default:    // (?xxx:) inlined match flags
                            unread();
                            addFlag();
                            ch = read();
                            if(ch == u')') {
                                return nullptr;    // Inline modifier only
                            }
                            if(ch != u':') {
                                throw error(jstr(u"Unknown inline modifier"));
                            }
                            head = createGroup(true);
                            tail = root;
                            head->next = expr(tail);
                            break;
                    }
                }
                else {   // (xxx) a regular group
                    capturingGroup = true;
                    head = createGroup(false);
                    tail = root;
                    head->next = expr(tail);
                }

                accept(u')', jstr(u"Unclosed group"));
                _flags = save;

                // Check for quantifiers
                JNode node = closure(head);
                if(node == head) {  // No closure
                    root = tail;
                    return node;    // Dual return
                }
                if(head == tail) {  // Zero length assertion
                    root = node;
                    return node;    // Dual return
                }

                if(jinstanceof<Ques>(node)) {
                    JQues ques = std::dynamic_pointer_cast<Ques>(node);
                    if(ques->type == POSSESSIVE) {
                        root = node;
                        return node;
                    }
                    tail->next = jnew<BranchConn>();
                    tail = tail->next;
                    if(ques->type == GREEDY) {
                        head = jnew<Branch>(head, nullptr, tail);
                    }
                    else {   // Reluctant quantifier
                        head = jnew<Branch>(nullptr, head, tail);
                    }
                    root = tail;
                    return head;
                }
                else if(jinstanceof<Curly>(node)) {
                    JCurly curly = std::dynamic_pointer_cast<Curly>(node);
                    if(curly->type == POSSESSIVE) {
                        root = node;
                        return node;
                    }
                    // Discover if the group is deterministic
                    JTreeInfo info = jnew<TreeInfo>();
                    if(head->study(info)) {  // Deterministic
                        JGroupTail temp = std::dynamic_pointer_cast<GroupTail>(tail);
                        head = root = jnew<GroupCurly>(head->next, curly->cmin, curly->cmax, curly->type,
                                                       (std::dynamic_pointer_cast<GroupTail>(tail))->localIndex,
                                                       (std::dynamic_pointer_cast<GroupTail>(tail))->groupIndex,
                                                       capturingGroup);
                        return head;
                    }
                    else {   // Non-deterministic
                        jint temp = (std::dynamic_pointer_cast<GroupHead>(head))->localIndex;
                        JLoop loop;
                        if(curly->type == GREEDY)
                            loop = jnew<Loop>(this->localCount, temp);
                        else  // Reluctant Curly
                            loop = jnew<LazyLoop>(this->localCount, temp);
                        JProlog prolog = jnew<Prolog>(loop);
                        this->localCount += 1;
                        loop->cmin = curly->cmin;
                        loop->cmax = curly->cmax;
                        loop->body = head;
                        tail->next = loop;
                        root = loop;
                        return prolog; // Dual return
                    }
                }
                throw error(jstr(u"Internal logic error"));
            }

            Pattern::JNode Pattern::createGroup(jboolean anonymous)
            {
                jint localIndex = localCount++;
                jint groupIndex = 0;
                if(!anonymous)
                    groupIndex = capturingGroupCount++;
                JGroupHead head = jnew<GroupHead>(localIndex);
                root = jnew<GroupTail>(localIndex, groupIndex);
                if(!anonymous && groupIndex < 10)
                    groupNodes[groupIndex] = head;
                return head;
            }

            void Pattern::addFlag()
            {

            }

            Pattern::JNode Pattern::closure(JNode prev)
            {
                return nullptr;
            }

            const jint Pattern::countChars(cpp::lang::JCharSequence seq, jint index, jint lengthInCodePoints)
            {
                // optimization
                if(lengthInCodePoints == 1 && !Character::isHighSurrogate(seq->charAt(index))) {
                    assert(index >= 0 && index < seq->length());
                    return 1;
                }
                jint length = seq->length();
                jint x = index;
                if(lengthInCodePoints >= 0) {
                    assert(index >= 0 && index < length);
                    for(jint i = 0; x < length && i < lengthInCodePoints; i++) {
                        if(Character::isHighSurrogate(seq->charAt(x++))) {
                            if(x < length && Character::isLowSurrogate(seq->charAt(x))) {
                                x++;
                            }
                        }
                    }
                    return x - index;
                }

                assert(index >= 0 && index <= length);
                if(index == 0) {
                    return 0;
                }
                jint len = -lengthInCodePoints;
                for(jint i = 0; x > 0 && i < len; i++) {
                    if(Character::isLowSurrogate(seq->charAt(--x))) {
                        if(x > 0 && Character::isHighSurrogate(seq->charAt(x-1))) {
                            x--;
                        }
                    }
                }
                return index - x;
            }

            Pattern::JNode Pattern::newSlice(jarray<jint> buf, jint count, jboolean hasSupplementary)
            {
                jarray<jint> tmp(count);

                if(has(CASE_INSENSITIVE)) {
                    if(has(UNICODE_CASE)) {
                        for(jint i = 0; i < count; i++) {
                            tmp[i] = Character::toLowerCase(Character::toUpperCase(buf[i]));
                        }

                        //return hasSupplementary ? jnew<SliceUS>(tmp) : jnew<SliceU>(tmp);
                        if(hasSupplementary)
                            return jnew<SliceUS>(tmp);

                        return jnew<SliceU>(tmp);
                    }
                    for(jint i = 0; i < count; i++) {
                        tmp[i] = ASCII::toLower(buf[i]);
                    }

                    //return hasSupplementary ? jnew<SliceIS>(tmp) : jnew<SliceI>(tmp);
                    if(hasSupplementary)
                        return jnew<SliceIS>(tmp);

                    return jnew<SliceI>(tmp);
                }

                for(jint i = 0; i < count; i++) {
                    tmp[i] = buf[i];
                }

                //return hasSupplementary ? jnew<SliceS>(tmp) : jnew<Slice>(tmp);
                if(hasSupplementary)
                    return jnew<SliceS>(tmp);

                return jnew<Slice>(tmp);
            }

            Pattern::JCharProperty Pattern::union_(JCharProperty lhs, JCharProperty rhs)
            {
                return nullptr;
            }

            Pattern::JCharProperty Pattern::intersection(JCharProperty lhs, JCharProperty rhs)
            {
                return nullptr;
            }

            Pattern::JCharProperty Pattern::setDifference(JCharProperty lhs, JCharProperty rhs)
            {
                return nullptr;
            }

            Pattern::JNode Pattern::_accept = jnew<Pattern::Node>();
            Pattern::JNode Pattern::lastAccept = jnew<Pattern::LastNode>();

            const jint Pattern::UNIX_LINES = 0x01;
            const jint Pattern::CASE_INSENSITIVE = 0x02;
            const jint Pattern::COMMENTS = 0x04;
            const jint Pattern::MULTILINE = 0x08;
            const jint Pattern::LITERAL = 0x10;
            const jint Pattern::DOTALL = 0x20;
            const jint Pattern::UNICODE_CASE = 0x40;
            const jint Pattern::CANON_EQ = 0x80;
            const jint Pattern::UNICODE_CHARACTER_CLASS = 0x100;

            const jint Pattern::MAX_REPS = 0x7FFFFFFF;
            const jint Pattern::GREEDY = 0;
            const jint Pattern::LAZY = 1;
            const jint Pattern::POSSESSIVE = 2;
            const jint Pattern::INDEPENDENT = 3;
        }
    }
}
