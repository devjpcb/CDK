#ifndef CPP_UTIL_REGEX_PATTERN_H
#define CPP_UTIL_REGEX_PATTERN_H

#include "../../lang/Object.h"
#include "../../lang/CharSequence.h"
#include "../../lang/Array.h"
#include "../../lang/Integer.h"
#include "../../lang/Character.h"
#include "../Map.h"
#include "PatternSyntaxException.h"
#include <mutex>

namespace cpp
{
    namespace util
    {
        namespace regex
        {
            class Matcher;

            class Pattern : public cpp::lang::Object, public std::enable_shared_from_this<Pattern>
            {
            public:
                Pattern();
                ~Pattern();

                static JPattern compile(cpp::lang::JString regex);
                static JPattern compile(cpp::lang::JString regex, jint flags);
                jint flags();
                JMatcher matcher(cpp::lang::JCharSequence input);
                static jboolean matches(cpp::lang::JString regex, cpp::lang::JCharSequence input);
                cpp::lang::JString pattern();
                static cpp::lang::JString quote(cpp::lang::JString s);
                jarray<cpp::lang::JString> split(cpp::lang::JCharSequence input);
                jarray<cpp::lang::JString> split(cpp::lang::JCharSequence input, jint limit);
                
                static const jint UNIX_LINES;
                static const jint CASE_INSENSITIVE;
                static const jint COMMENTS;
                static const jint MULTILINE;
                static const jint LITERAL;
                static const jint DOTALL;
                static const jint UNICODE_CASE;
                static const jint CANON_EQ;
                static const jint UNICODE_CHARACTER_CLASS;
                
            private:
                class Node;
                using JNode = std::shared_ptr<Node>;
                
                class TreeInfo;
                using JTreeInfo = std::shared_ptr<TreeInfo>;
                
                class LastNode;
                using JLastNode = std::shared_ptr<LastNode>;
                
                class Start;
                using JStart = std::shared_ptr<Start>;
                
                class StartS;
                using JStartS = std::shared_ptr<StartS>;
                
                class Begin;
                using JBegin = std::shared_ptr<Begin>;
                
                class End;
                using JEnd = std::shared_ptr<End>;
                
                class Caret;
                using JCaret = std::shared_ptr<Caret>;
                
                class UnixCaret;
                using JUnixCaret = std::shared_ptr<UnixCaret>;
                
                class LastMatch;
                using JLastMatch = std::shared_ptr<LastMatch>;
                
                class Dollar;
                using JDollar = std::shared_ptr<Dollar>;
                
                class UnixDollar;
                using JUnixDollar = std::shared_ptr<UnixDollar>;
                
                class LineEnding;
                using JLineEnding = std::shared_ptr<LineEnding>;
                
                class CharProperty;
                using JCharProperty = std::shared_ptr<CharProperty>;
                
                class All;
                using JAll = std::shared_ptr<All>;
                
                class Dot;
                using JDot = std::shared_ptr<Dot>;
                
                class UnixDot;
                using JUnixDot = std::shared_ptr<UnixDot>;
                
                class Ques;
                using JQues = std::shared_ptr<Ques>;
                
                class Curly;
                using JCurly = std::shared_ptr<Curly>;
                
                class GroupCurly;
                using JGroupCurly = std::shared_ptr<GroupCurly>;
                
                class BmpCharProperty;
                using JBmpCharProperty = std::shared_ptr<BmpCharProperty>;
                
                class BitClass;
                using JBitClass = std::shared_ptr<BitClass>;
                
                class SingleS;
                using JSingleS = std::shared_ptr<SingleS>;
                
                class Single;
                using JSingle = std::shared_ptr<Single>;
                
                class SingleI;
                using JSingleI = std::shared_ptr<SingleI>;
                
                class SingleU;
                using JSingleU = std::shared_ptr<SingleU>;
                
                class Category;
                using JCategory = std::shared_ptr<Category>;
                
                class Ctype;
                using JCtype = std::shared_ptr<Ctype>;
                
                class VertWS;
                using JVertWS = std::shared_ptr<VertWS>;
                
                class HorizWS;
                using JHorizWS = std::shared_ptr<HorizWS>;
                
                class SliceNode;
                using JSliceNode = std::shared_ptr<SliceNode>;
                
                class Slice;
                using JSlice = std::shared_ptr<Slice>; 
                
                class SliceI;
                using JSliceI = std::shared_ptr<SliceI>;
                
                class SliceU;
                using JSliceU = std::shared_ptr<SliceU>;
                
                class SliceS;
                using JSliceS = std::shared_ptr<SliceS>;
                
                class SliceIS;
                using JSliceIS = std::shared_ptr<SliceIS>;
                
                class SliceUS;
                using JSliceUS = std::shared_ptr<SliceUS>;
                
                class BranchConn;
                using JBranchConn = std::shared_ptr<BranchConn>;
                
                class Branch;
                using JBranch = std::shared_ptr<Branch>;
                
                class GroupHead;
                using JGroupHead = std::shared_ptr<GroupHead>;
                
                class GroupTail;
                using JGroupTail = std::shared_ptr<GroupTail>;
                
                class Prolog;
                using JProlog = std::shared_ptr<Prolog>;
                
                class Loop;
                using JLoop = std::shared_ptr<Loop>;
                
                class LazyLoop;
                using JLazyLoop = std::shared_ptr<LazyLoop>;
                
                class First;
                using JFirst = std::shared_ptr<First>;
                
                class BnM;
                using JBnM = std::shared_ptr<BnM>;
                
                class BnMS;
                using JBnMS = std::shared_ptr<BnMS>;
                
                class TreeInfo : public cpp::lang::Object
                {
                public:
                    TreeInfo();
                    void reset();

                    jint minLength;
                    jint maxLength;
                    jboolean maxValid;
                    jboolean deterministic;
                };
                
                class Node : public cpp::lang::Object
                {
                public:
                    Node();
                    virtual ~Node();
                    virtual jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                    virtual jboolean study(JTreeInfo info);
                    
                    JNode next;
                };
                
                class LastNode : public Node
                {
                public:
                    virtual jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                };
                
                class Start : public Node
                {
                public:
                    Start(JNode node);
                    virtual jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                    virtual jboolean study(JTreeInfo info);
                    
                    jint minLength;
                };

                class StartS : public Start
                {
                public:
                    StartS(JNode node);
                    virtual jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                };

                class Begin : public Node
                {
                public:
                    virtual jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                };

                class End : public Node
                {
                public:
                    virtual jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                };

                class Caret : public Node
                {
                public:
                    virtual jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                };

                class UnixCaret : public Node
                {
                public:
                    virtual jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                };

                class LastMatch : public Node
                {
                public:
                    virtual jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                };
                
                class Dollar : public Node
                {
                public:
                    Dollar(jboolean mul);
                    virtual jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                    virtual jboolean study(JTreeInfo info);

                private:
                    jboolean multiline;
                };

                class UnixDollar : public Node
                {
                public:
                    UnixDollar(jboolean mul);
                    virtual jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                    virtual jboolean study(JTreeInfo info);

                private:
                    jboolean multiline;
                };
                
                class LineEnding : public Node
                {
                public:
                    virtual jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                    virtual jboolean study(JTreeInfo info);
                };

                class CharProperty : public Node
                {
                public:
                    virtual jboolean isSatisfiedBy(jint ch) = 0;
                    virtual JCharProperty complement();
                    virtual jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                    virtual jboolean study(JTreeInfo info);
                };
                
                class All : public CharProperty 
                {
                public:
                    jboolean isSatisfiedBy(jint ch);
                };
                
                class Dot : public CharProperty 
                {
                public:
                    jboolean isSatisfiedBy(jint ch);
                };
                
                class UnixDot : public CharProperty 
                {
                public:
                    jboolean isSatisfiedBy(jint ch);
                };
                
                class Ques : public Node
                {
                public:
                    Ques(JNode node, jint type);
                    jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                    jboolean study(JTreeInfo info);
                
                    JNode atom;
                    jint type;
                };
                
                class Curly : public Node
                {
                public:
                    Curly(JNode node, jint cmin, jint cmax, jint type);
                    jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                    jboolean match0(JMatcher matcher, jint i, jint j, cpp::lang::JCharSequence seq);
                    jboolean match1(JMatcher matcher, jint i, jint j, cpp::lang::JCharSequence seq);
                    jboolean match2(JMatcher matcher, jint i, jint j, cpp::lang::JCharSequence seq);
                    jboolean study(JTreeInfo info);
                    
                    JNode atom;
                    jint type;
                    jint cmin;
                    jint cmax;
                };
                
                class GroupCurly : public Node
                {
                public:
                    GroupCurly(JNode node, jint cmin, jint cmax, jint type, jint local, jint group, jboolean capture);
                    jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                    jboolean match0(JMatcher matcher, jint i, jint j, cpp::lang::JCharSequence seq);
                    jboolean match1(JMatcher matcher, jint i, jint j, cpp::lang::JCharSequence seq);
                    jboolean match2(JMatcher matcher, jint i, jint j, cpp::lang::JCharSequence seq);
                    jboolean study(JTreeInfo info);
                    
                    JNode atom;
                    jint type;
                    jint cmin;
                    jint cmax;
                    jint localIndex;
                    jint groupIndex;
                    jboolean capture;
                };
                
                class BmpCharProperty : public CharProperty
                {
                public:
                    virtual jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                };
                
                class BitClass : public BmpCharProperty
                {
                public:
                    BitClass();
                    BitClass(jarray<jboolean> bits);
                    JBitClass add(jint c, jint flags);
                    jboolean isSatisfiedBy(jint ch);
                };

                class SingleS : public CharProperty
                {
                public:
                    SingleS(jint c);
                    virtual jboolean isSatisfiedBy(jint ch);
                    
                    const jint c;
                };
                
                class Single : public BmpCharProperty
                {
                public:
                    Single(jint c);
                    virtual jboolean isSatisfiedBy(jint ch);

                private:
                    const jint c;
                };

                class SingleI : public BmpCharProperty
                {
                public:
                    SingleI(jint lower, jint upper);
                    virtual jboolean isSatisfiedBy(jint ch);
                    
                    const jint lower;
                    const jint upper;
                };
                
                class SingleU : public CharProperty
                {
                public:
                    SingleU(jint lower);
                    virtual jboolean isSatisfiedBy(jint ch);
                    
                    const jint lower;
                };
                
                class Block : public CharProperty
                {
                public:
                    Block(cpp::lang::Character::UnicodeBlock block);
                    jboolean isSatisfiedBy(jint ch);
                private:
                    const cpp::lang::Character::UnicodeBlock block;
                };
                
                class Script : public CharProperty
                {
                public:
                    Script(cpp::lang::Character::UnicodeScript script);
                    jboolean isSatisfiedBy(jint ch);
                private:
                    const cpp::lang::Character::UnicodeScript script;
                };

                class Category : public CharProperty
                {
                public:
                    Category(jint typeMask);
                    virtual jboolean isSatisfiedBy(jint ch);
                    
                    const jint typeMask;
                };
                
                /* TODO:
                class Utype : public CharProperty
                {
                public:
                    Utype(UnicodeProp uprop);
                    jboolean isSatisfiedBy(jint ch);
                private:
                    const UnicodeProp uprop;
                };
                */
                
                class Ctype : public BmpCharProperty
                {
                public:
                    Ctype(jint ctype);
                    virtual jboolean isSatisfiedBy(jint ch);
                    
                    const jint ctype;
                };
                
                class VertWS : public BmpCharProperty
                {
                public:
                    virtual jboolean isSatisfiedBy(jint ch);
                };
                
                class HorizWS : public BmpCharProperty
                {
                public:
                    virtual jboolean isSatisfiedBy(jint ch);
                };
                
                class SliceNode : public Node {
                public:
                    SliceNode(jarray<jint> buf);
                    virtual jboolean study(JTreeInfo info);
                    
                    jarray<jint> buffer;
                };
                
                class Slice : public SliceNode {
                public:
                    Slice(jarray<jint> buf);
                    virtual jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                };
                
                class SliceI : public SliceNode
                {
                public:
                    SliceI(jarray<jint> buf);
                    virtual jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                };
                
                class SliceU : public SliceNode
                {
                public:
                    SliceU(jarray<jint> buf);
                    virtual jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                };
                
                class SliceS : public SliceNode 
                {
                public:
                    SliceS(jarray<jint> buf);
                    virtual jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                };
                
                class SliceIS : public SliceNode 
                {
                public:
                    SliceIS(jarray<jint> buf);
                    virtual jint toLower(jint c);
                    virtual jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                };
                
                class SliceUS : public SliceIS
                {
                public:
                    SliceUS(jarray<jint> buf);
                    virtual jint toLower(jint c);
                };
                
                class BranchConn : public Node
                {
                public:
                    jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                    jboolean study(JTreeInfo info);
                };
                
                class Branch : public Node 
                {
                public:
                    Branch(JNode first, JNode second, JNode branchConn);
                    virtual void add(JNode node);
                    virtual jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                    virtual jboolean study(JTreeInfo info);
                };
                
                class GroupHead : public Node 
                {
                public:
                    GroupHead(jint localCount);
                    virtual jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                    virtual jboolean matchRef(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                    
                    jint localIndex;
                };
                
                class GroupTail : public Node
                {
                public:
                    GroupTail(jint localCount, jint groupCount);
                    jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                    
                    jint localIndex;
                    jint groupIndex;
                };
                
                class Prolog : public Node
                {
                public:
                    Prolog(JLoop loop);
                    jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                    jboolean study(JTreeInfo info);
                    
                    JLoop loop;
                };
                
                class Loop : public Node
                {
                public:
                    Loop(jint countIndex, jint beginIndex);
                    jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                    jboolean matchInit(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                    jboolean study(JTreeInfo info);
                    
                    JNode body;
                    jint countIndex;
                    jint beginIndex;
                    jint cmin, cmax;
                };
                
                class LazyLoop : public Loop
                {
                public:
                    LazyLoop(jint countIndex, jint beginIndex);
                    jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                    jboolean matchInit(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                    jboolean study(JTreeInfo info);
                };
                
                class First : public Node 
                {
                public:
                    First(JNode node);
                    virtual jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                    virtual jboolean study(JTreeInfo info);
                };
                
                class Pos : public Node 
                {
                public:
                    Pos(JNode cond);
                    jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                    
                    JNode cond;
                };
                
                class Neg : public Node 
                {
                public:
                    Neg(JNode cond);
                    jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                    
                    JNode cond;
                };
                
                class BnM : public Node 
                {
                public:
                    static JNode optimize(JNode node);
                    BnM(jarray<jint> src, jarray<jint> lastOcc, jarray<jint> optoSft, JNode next);
                    virtual jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                    virtual jboolean study(JTreeInfo info);
                    
                    jarray<jint> buffer;
                    jarray<jint> lastOcc;
                    jarray<jint> optoSft;
                };
                
                class BnMS : public BnM 
                {
                public:
                    BnMS(jarray<jint> src, jarray<jint> lastOcc, jarray<jint> optoSft, JNode next);
                    virtual jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                    
                    jint lengthInChars;
                };
                
                class LookbehindEnd : public Node 
                {
                public:
                    jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                };
                
                JNode lookbehindEnd = jnew<LookbehindEnd>();
                
                class Behind : public Node
                {
                public:
                    Behind(JNode cond, jint rmax, jint rmin);
                    jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                };
                
                class BehindS : public Behind
                {
                public:
                    BehindS(JNode cond, jint rmax, jint rmin);
                    jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                };
                
                class NotBehind : public Node
                {
                public:
                    NotBehind(JNode cond, jint rmax, jint rmin);
                    jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                };
                
                class NotBehindS : public NotBehind
                {
                public:
                    NotBehindS(JNode cond, jint rmax, jint rmin);
                    jboolean match(JMatcher matcher, jint i, cpp::lang::JCharSequence seq);
                };
                
                Pattern(cpp::lang::JString p, jint f);
                
                void normalize();
                void RemoveQEQuoting();
                void compile();
                cpp::util::JMap<cpp::lang::JString, cpp::lang::JInteger> namedGroups();
                jboolean has(jint f);
                void accept(jint ch, cpp::lang::JString s);
                void mark(jint c);
                jint peek();
                jint read();
                jint readEscaped();
                jint next();
                jint nextEscaped();
                jint peekPastWhitespace(jint ch);
                jint parsePastWhitespace(jint ch);
                jint parsePastLine();
                jint peekPastLine();
                jboolean isLineSeparator(jint ch);
                jint skip();
                void unread();
                PatternSyntaxException error(cpp::lang::JString s);
                jboolean findSupplementary(jint start, jint end);
                jboolean isSupplementary(jint ch);
                JNode expr(JNode end);
                JNode sequence(JNode end);
                JNode atom();
                void append(jint ch, jint len);
                JNode ref(jint refNum);
                jint escape(jboolean inclass, jboolean create, jboolean isrange);
                JCharProperty clazz(jboolean consume);
                JCharProperty bitsOrSingle(JBitClass bits, jint ch);
                JCharProperty range(JBitClass bits); 
                JCharProperty family(jboolean singleLetter, jboolean maybeComplement);
                JCharProperty unicodeScriptPropertyFor(cpp::lang::JString name);
                JCharProperty unicodeBlockPropertyFor(cpp::lang::JString name);
                JCharProperty charPropertyNodeFor(cpp::lang::JString name);
                cpp::lang::JString groupname(jint ch);
                JNode group0();
                JNode createGroup(jboolean anonymous);
                void addFlag();
                void subFlag();
                JNode closure(JNode prev);
                jint c();
                jint o();
                jint x();
                jint cursor();
                void setcursor(jint pos);
                jint uxxxx();
                jint u();
                static const jint countChars(cpp::lang::JCharSequence seq, jint index, jint lengthInCodePoints);
                static const int countCodePoints(cpp::lang::JCharSequence seq);
                JCharProperty newSingle(const jint ch);
                JNode newSlice(jarray<jint> buf, jint count, jboolean hasSupplementary);
                JCharProperty union_(JCharProperty lhs, JCharProperty rhs);
                JCharProperty intersection(JCharProperty lhs, JCharProperty rhs);
                JCharProperty setDifference(JCharProperty lhs, JCharProperty rhs);
                
                static const jint MAX_REPS;
                static const jint GREEDY;
                static const jint LAZY;
                static const jint POSSESSIVE;
                static const jint INDEPENDENT;
                
                static JNode _accept;
                static JNode lastAccept;
                cpp::lang::JString _pattern;
                jint _flags;
                volatile jboolean compiled;
                cpp::lang::JString normalizedPattern;
                JNode root;
                JNode matchRoot;
                jarray<jint> buffer;
                cpp::util::JMap<cpp::lang::JString, cpp::lang::JInteger> _namedGroups;
                jarray<JGroupHead> groupNodes;
                jarray<jint> temp;
                jint capturingGroupCount;
                jint localCount;
                jint _cursor;
                jint patternLength;
                jboolean hasSupplementary;

                friend class Matcher;
                
                std::mutex mtx;
            };
        }
    }
}

#endif // CPP_UTIL_REGEX_PATTERN_H
