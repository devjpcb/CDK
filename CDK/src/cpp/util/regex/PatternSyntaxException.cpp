#include "PatternSyntaxException.h"
#include "../../lang/String.h"
#include "../../lang/StringBuffer.h"

using namespace cpp::lang;

namespace cpp
{
    namespace util
    {

        namespace regex
        {
            PatternSyntaxException::PatternSyntaxException(JString desc, JString regex, jint index) : desc(desc), pattern(regex), index(index)
            {
            }

            PatternSyntaxException::~PatternSyntaxException()
            {
            }

            jint PatternSyntaxException::getIndex()
            {
                return index;
            }

            JString PatternSyntaxException::getDescription()
            {
                return desc;
            }

            JString PatternSyntaxException::getPattern()
            {
                return pattern;
            }

            JString PatternSyntaxException::getMessage()
            {
                JStringBuffer sb = jnew<StringBuffer>();
                sb->append(desc);
                if(index >= 0) {
                    sb->append(jstr(u" near index "));
                    sb->append(index);
                }
                // TODO: sb->append(nl);
                sb->append(pattern);
                if(index >= 0) {
                    // TODO: sb->append(nl);
                    for(jint i = 0; i < index; i++) sb->append(u' ');
                    sb->append(u'^');
                }
                return sb->toString();
            }
        }
    }
}
