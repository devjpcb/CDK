#ifndef PATTERNSYNTAXEXCEPTION_H
#define PATTERNSYNTAXEXCEPTION_H

#include "../../lang/IllegalArgumentException.h"

namespace cpp
{
    namespace util
    {
        namespace regex
        {
            class PatternSyntaxException : cpp::lang::IllegalArgumentException
            {
            public:
                PatternSyntaxException(cpp::lang::JString desc, cpp::lang::JString regex, jint index);
                ~PatternSyntaxException();
                
                jint getIndex();
                cpp::lang::JString getDescription();
                cpp::lang::JString getPattern();
                cpp::lang::JString getMessage();
                
            private:
                const cpp::lang::JString desc;
                const cpp::lang::JString pattern;
                const jint index;
            };
        }
    }
}

#endif // PATTERNSYNTAXEXCEPTION_H
