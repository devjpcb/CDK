#include "DoubleConsts.h"

namespace sun
{
    namespace misc
    {
        const jint DoubleConsts::SIGNIFICAND_WIDTH = 53;
        
        const jint DoubleConsts::MAX_EXPONENT = 1023;
        
        const jint DoubleConsts::MIN_EXPONENT = -1022;
        
        const jint DoubleConsts::MIN_SUB_EXPONENT = MIN_EXPONENT - (SIGNIFICAND_WIDTH - 1);
        
        const jint DoubleConsts::EXP_BIAS = 1023;
        
        const jlong DoubleConsts::SIGN_BIT_MASK = 0x8000000000000000LL;
        
        const jlong DoubleConsts::EXP_BIT_MASK = 0x7FF0000000000000LL;
        
        const jlong DoubleConsts::SIGNIF_BIT_MASK = 0x000FFFFFFFFFFFFFLL;
    }
}
