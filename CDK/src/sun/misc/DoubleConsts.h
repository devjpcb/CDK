#ifndef SUN_MISC_DOUBLECONSTS_H
#define SUN_MISC_DOUBLECONSTS_H

#include "../../cpp/lang/Object.h"

namespace sun
{
    namespace misc
    {
        class DoubleConsts
        {
        public:
            DoubleConsts() = delete;
        
            static const jint SIGNIFICAND_WIDTH;
            static const jint MAX_EXPONENT;
            static const jint MIN_EXPONENT;
            static const jint MIN_SUB_EXPONENT;
            static const jint EXP_BIAS;
            static const jlong SIGN_BIT_MASK;
            static const jlong EXP_BIT_MASK;
            static const jlong SIGNIF_BIT_MASK;
        };
    }
}

#endif // SUN_MISC_DOUBLECONSTS_H
