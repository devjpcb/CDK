#include "FDBigInteger.h"
#include "../../cpp/lang/String.h"
#include "../../cpp/lang/Math.h"
#include "../../cpp/lang/Integer.h"
#include "../../cpp/util/Arrays.h"
#include "../../cpp/lang/System.h"
#include "../../cpp/lang/IllegalArgumentException.h"
#include <cassert>

using cpp::lang::String;
using cpp::lang::Math;
using cpp::lang::Integer;
using cpp::util::Arrays;
using cpp::lang::System;
using cpp::lang::IllegalArgumentException;

namespace sun
{
    namespace misc
    {
        FDBigInteger::FDBigInteger()
        {
            data = jnew_array<jint>(0);
            offset = 0;
        }

        FDBigInteger::FDBigInteger(jarray<jint> data, jint offset) : data(data), offset(offset), nWords(data.length()), isImmutable(false)
        {
            trimLeadingZeros();
        }

        FDBigInteger::FDBigInteger(jlong lValue, jarray<jchar> digits, jint kDigits, jint nDigits) : isImmutable(false)
        {
            jint n = Math::max((nDigits + 8) / 9, 2);        // estimate size needed.
            data = jnew_array<jint>(n);      // allocate enough space
            data[0] = static_cast<jint>(lValue);    // starting value
            data[1] = static_cast<jint>(static_cast<julong>(lValue) >> 32);
            offset = 0;
            nWords = 2;
            jint i = kDigits;
            jint limit = nDigits - 5;       // slurp digits 5 at a time.
            jint v;
            while(i < limit) {
                jint ilim = i + 5;
                v = static_cast<jint>(digits[i++]) - static_cast<jint>(u'0');
                while(i < ilim) {
                    v = 10 * v + static_cast<jint>(digits[i++]) - static_cast<jint>(u'0');
                }
                multAddMe(100000, v); // ... where 100000 is 10^5.
            }
            jint factor = 1;
            v = 0;
            while(i < nDigits) {
                v = 10 * v + static_cast<jint>(digits[i++]) - static_cast<jint>(u'0');
                factor *= 10;
            }
            if(factor != 1) {
                multAddMe(factor, v);
            }
            trimLeadingZeros();
        }

        FDBigInteger::~FDBigInteger()
        {
        }

        JFDBigInteger FDBigInteger::valueOfPow52(jint p5, jint p2)
        {
            if(p5 != 0) {
                if(p2 == 0) {
                    return big5pow(p5);
                }
                else if(p5 < SMALL_5_POW.length()) {
                    jint pow5 = SMALL_5_POW[p5];
                    jint wordcount = p2 >> 5;
                    jint bitcount = p2 & 0x1f;
                    if(bitcount == 0) {
                        jarray<jint> pdata = { pow5 };
                        return jnew<FDBigInteger>(pdata, wordcount);
                    }
                    else {
                        jarray<jint> pdata = {
                            pow5 << bitcount,
                            static_cast<jint>(static_cast<juint>(pow5) >> (32 - bitcount))
                        };
                        return jnew<FDBigInteger>(pdata, wordcount);
                    }
                }
                else {
                    return big5pow(p5)->leftShift(p2);
                }
            }
            else {
                return valueOfPow2(p2);
            }
        }

        JFDBigInteger FDBigInteger::valueOfMulPow52(jlong value, jint p5, jint p2)
        {
            assert(p5 >= 0);
            assert(p2 >= 0);
            jint v0 = static_cast<jint>(value);
            jint v1 = static_cast<jint>(static_cast<julong>(value) >> 32);
            jint wordcount = p2 >> 5;
            jint bitcount = p2 & 0x1f;
            if(p5 != 0) {
                if(p5 < SMALL_5_POW.length()) {
                    jlong pow5 = SMALL_5_POW[p5] & LONG_MASK;
                    jlong carry = (v0 & LONG_MASK) * pow5;
                    v0 = static_cast<jint>(carry);
                    carry = static_cast<julong>(carry) >> 32;
                    carry = (v1 & LONG_MASK) * pow5 + carry;
                    v1 = static_cast<jint>(carry);
                    jint v2 = static_cast<jint>(static_cast<julong>(carry) >> 32);
                    if(bitcount == 0) {
                        jarray<jint> pdata = {v0, v1, v2};
                        return jnew<FDBigInteger>(pdata, wordcount);
                    }
                    else {
                        jarray<jint> pdata = {
                            v0 << bitcount,
                            static_cast<jint>((v1 << bitcount) | (static_cast<juint>(v0) >> (32 - bitcount))),
                            static_cast<jint>((v2 << bitcount) | (static_cast<juint>(v1) >> (32 - bitcount))),
                            static_cast<jint>(static_cast<juint>(v2) >> (32 - bitcount))
                        };
                        return jnew<FDBigInteger>(pdata, wordcount);
                    }
                }
                else {
                    JFDBigInteger pow5 = big5pow(p5);
                    jarray<jint> r;
                    if(v1 == 0) {
                        r = jnew_array<jint>(pow5->nWords + 1 + ((p2 != 0) ? 1 : 0));
                        mult(pow5->data, pow5->nWords, v0, r);
                    }
                    else {
                        r = jnew_array<jint>(pow5->nWords + 2 + ((p2 != 0) ? 1 : 0));
                        mult(pow5->data, pow5->nWords, v0, v1, r);
                    }
                    return jnew<FDBigInteger>(r, pow5->offset)->leftShift(p2);
                }
            }
            else if(p2 != 0) {
                if(bitcount == 0) {
                    jarray<jint> pdata = {v0, v1};
                    return jnew<FDBigInteger>(pdata, wordcount);
                }
                else {
                    jarray<jint> pdata = {
                        v0 << bitcount,
                        static_cast<jint>((v1 << bitcount) | (static_cast<juint>(v0) >> (32 - bitcount))),
                        static_cast<jint>(static_cast<juint>(v1) >> (32 - bitcount))
                    };
                    return jnew<FDBigInteger>(pdata, wordcount);
                }
            }
            jarray<jint> pdata = {v0, v1};
            return jnew<FDBigInteger>(pdata, 0);
        }

        JFDBigInteger FDBigInteger::valueOfPow2(jint p2)
        {
            jint wordcount = p2 >> 5;
            jint bitcount = p2 & 0x1f;
            jarray<jint> pdata = {1 << bitcount};
            return jnew<FDBigInteger>(pdata, wordcount);
        }

        jint FDBigInteger::getNormalizationBias()
        {
            if(nWords == 0) {
                throw IllegalArgumentException(jstr(u"Zero value cannot be normalized"));
            }
            jint zeros = Integer::numberOfLeadingZeros(data[nWords - 1]);
            return (zeros < 4) ? 28 + zeros : zeros - 4;
        }

        JFDBigInteger FDBigInteger::leftShift(jint shift)
        {
            if(shift == 0 || nWords == 0) {
                return shared_from_this();
            }
            jint wordcount = shift >> 5;
            jint bitcount = shift & 0x1f;
            if(this->isImmutable) {
                if(bitcount == 0) {
                    jarray<jint> pdata = Arrays::copyOf(data, nWords);
                    return jnew<FDBigInteger>(pdata, offset + wordcount);
                }
                else {
                    jint anticount = 32 - bitcount;
                    jint idx = nWords - 1;
                    jint prev = data[idx];
                    jint hi = static_cast<juint>(prev) >> anticount;
                    jarray<jint> result;
                    if(hi != 0) {
                        result = jnew_array<jint>(nWords + 1);
                        result[nWords] = hi;
                    }
                    else {
                        result = jnew_array<jint>(nWords);
                    }
                    leftShift(data, idx, result, bitcount, anticount, prev);
                    return jnew<FDBigInteger>(result, offset + wordcount);
                }
            }
            else {
                if(bitcount != 0) {
                    jint anticount = 32 - bitcount;
                    if((data[0] << bitcount) == 0) {
                        jint idx = 0;
                        jint prev = data[idx];
                        for(; idx < nWords - 1; idx++) {
                            jint v = (static_cast<juint>(prev) >> anticount);
                            prev = data[idx + 1];
                            v |= (prev << bitcount);
                            data[idx] = v;
                        }
                        jint v = static_cast<juint>(prev) >> anticount;
                        data[idx] = v;
                        if(v == 0) {
                            nWords--;
                        }
                        offset++;
                    }
                    else {
                        jint idx = nWords - 1;
                        jint prev = data[idx];
                        jint hi = static_cast<juint>(prev) >> anticount;
                        jarray<jint> result = data;
                        jarray<jint> src = data;
                        if(hi != 0) {
                            if(nWords == data.length()) {
                                result = jnew_array<jint>(nWords + 1);
                                data = result;
                            }
                            result[nWords++] = hi;
                        }
                        leftShift(src, idx, result, bitcount, anticount, prev);
                    }
                }
                offset += wordcount;
                return shared_from_this();
            }
        }

        jint FDBigInteger::quoRemIteration(JFDBigInteger S)
        {
            assert(!this->isImmutable);
            // ensure that this and S have the same number of
            // digits. If S is properly normalized and q < 10 then
            // this must be so.
            jint thSize = this->size();
            jint sSize = S->size();
            if(thSize < sSize) {
                // this value is significantly less than S, result of division is zero.
                // just mult this by 10.
                jint p = multAndCarryBy10(this->data, this->nWords, this->data);
                if(p != 0) {
                    this->data[nWords++] = p;
                }
                else {
                    trimLeadingZeros();
                }
                return 0;
            }
            else if(thSize > sSize) {
                throw IllegalArgumentException(jstr(u"disparate values"));
            }
            // estimate q the obvious way. We will usually be
            // right. If not, then we're only off by a little and
            // will re-add.
            jlong q = (this->data[this->nWords - 1] & LONG_MASK) / (S->data[S->nWords - 1] & LONG_MASK);
            jlong diff = multDiffMe(q, S);
            if(diff != 0LL) {
                //@ assert q != 0;
                //@ assert this.offset == \old(Math.min(this.offset, S.offset));
                //@ assert this.offset <= S.offset;

                // q is too big.
                // add S back in until this turns +. This should
                // not be very many times!
                jlong sum = 0LL;
                jint tStart = S->offset - this->offset;
                //@ assert tStart >= 0;
                jarray<jint> sd = S->data;
                jarray<jint> td = this->data;
                while(sum == 0LL) {
                    for(jint sIndex = 0, tIndex = tStart; tIndex < this->nWords; sIndex++, tIndex++) {
                        sum += (td[tIndex] & LONG_MASK) + (sd[sIndex] & LONG_MASK);
                        td[tIndex] = static_cast<jint>(sum);
                        sum = static_cast<julong>(sum) >> 32; // Signed or unsigned, answer is 0 or 1
                    }
                    //
                    // Originally the following line read
                    // "if ( sum !=0 && sum != -1 )"
                    // but that would be wrong, because of the
                    // treatment of the two values as entirely unsigned,
                    // it would be impossible for a carry-out to be interpreted
                    // as -1 -- it would have to be a single-bit carry-out, or +1.
                    //
                    assert(sum == 0 || sum == 1); // carry out of division correction
                    q -= 1;
                }
            }
            // finally, we can multiply this by 10.
            // it cannot overflow, right, as the high-order word has
            // at least 4 high-order zeros!
            jint p = multAndCarryBy10(this->data, this->nWords, this->data);
            assert(p == 0); // Carry out of *10
            trimLeadingZeros();
            return static_cast<jint>(q);
        }

        JFDBigInteger FDBigInteger::multBy10()
        {
            if(nWords == 0) {
                return shared_from_this();
            }
            if(isImmutable) {
                jarray<jint> res(nWords + 1);
                res[nWords] = multAndCarryBy10(data, nWords, res);
                return jnew<FDBigInteger>(res, offset);
            }
            else {
                jint p = multAndCarryBy10(this->data, this->nWords, this->data);
                if(p != 0) {
                    if(nWords == data.length()) {
                        if(data[0] == 0) {
                            System::arraycopy(data, 1, data, 0, --nWords);
                            offset++;
                        }
                        else {
                            data = Arrays::copyOf(data, data.length() + 1);
                        }
                    }
                    data[nWords++] = p;
                }
                else {
                    trimLeadingZeros();
                }
                return shared_from_this();
            }
        }

        JFDBigInteger FDBigInteger::multByPow52(jint p5, jint p2)
        {
            if(this->nWords == 0) {
                return shared_from_this();
            }
            JFDBigInteger res = shared_from_this();
            if(p5 != 0) {
                jarray<jint> r;
                jint extraSize = (p2 != 0) ? 1 : 0;
                if(p5 < SMALL_5_POW.length()) {
                    r = jnew_array<jint>(this->nWords + 1 + extraSize);
                    mult(this->data, this->nWords, SMALL_5_POW[p5], r);
                    res = jnew<FDBigInteger>(r, this->offset);
                }
                else {
                    JFDBigInteger pow5 = big5pow(p5);
                    r = jnew_array<jint>(this->nWords + pow5->size() + extraSize);
                    mult(this->data, this->nWords, pow5->data, pow5->nWords, r);
                    res = jnew<FDBigInteger>(r, this->offset + pow5->offset);
                }
            }
            return res->leftShift(p2);
        }

        JFDBigInteger FDBigInteger::leftInplaceSub(JFDBigInteger subtrahend)
        {
            assert(this->size() >= subtrahend->size());
            JFDBigInteger minuend;
            if(this->isImmutable) {
                jarray<jint> pdata = unboxing(std::static_pointer_cast<jarray<jint>>(this->data.clone()));
                minuend = jnew<FDBigInteger>(pdata, this->offset);
            }
            else {
                minuend = shared_from_this();
            }
            jint offsetDiff = subtrahend->offset - minuend->offset;
            jarray<jint> sData = subtrahend->data;
            jarray<jint> mData = minuend->data;
            jint subLen = subtrahend->nWords;
            jint minLen = minuend->nWords;
            if(offsetDiff < 0) {
                // need to expand minuend
                jint rLen = minLen - offsetDiff;
                if(rLen < mData.length()) {
                    System::arraycopy(mData, 0, mData, -offsetDiff, minLen);
                    Arrays::fill(mData, 0, -offsetDiff, 0);
                }
                else {
                    jarray<jint> r(rLen);
                    System::arraycopy(mData, 0, r, -offsetDiff, minLen);
                    minuend->data = mData = r;
                }
                minuend->offset = subtrahend->offset;
                minuend->nWords = minLen = rLen;
                offsetDiff = 0;
            }
            jlong borrow = 0LL;
            jint mIndex = offsetDiff;
            for(jint sIndex = 0; sIndex < subLen && mIndex < minLen; sIndex++, mIndex++) {
                jlong diff = (mData[mIndex] & LONG_MASK) - (sData[sIndex] & LONG_MASK) + borrow;
                mData[mIndex] = (jint) diff;
                borrow = diff >> 32; // signed shift
            }
            for(; borrow != 0 && mIndex < minLen; mIndex++) {
                jlong diff = (mData[mIndex] & LONG_MASK) + borrow;
                mData[mIndex] = (jint) diff;
                borrow = diff >> 32; // signed shift
            }
            assert(borrow == 0LL); // borrow out of subtract,
            // result should be positive
            minuend->trimLeadingZeros();
            return minuend;
        }

        JFDBigInteger FDBigInteger::rightInplaceSub(JFDBigInteger subtrahend)
        {
            assert(this->size() >= subtrahend->size());
            JFDBigInteger minuend = shared_from_this();
            if(subtrahend->isImmutable) {
                jarray<jint> pdata = jarray_unboxing<jint>(subtrahend->data.clone());
                subtrahend = jnew<FDBigInteger>(pdata, subtrahend->offset);
            }
            jint offsetDiff = minuend->offset - subtrahend->offset;
            jarray<jint> sData = subtrahend->data;
            jarray<jint> mData = minuend->data;
            jint subLen = subtrahend->nWords;
            jint minLen = minuend->nWords;
            if(offsetDiff < 0) {
                jint rLen = minLen;
                if(rLen < sData.length()) {
                    System::arraycopy(sData, 0, sData, -offsetDiff, subLen);
                    Arrays::fill(sData, 0, -offsetDiff, 0);
                }
                else {
                    jarray<jint> r(rLen);
                    System::arraycopy(sData, 0, r, -offsetDiff, subLen);
                    subtrahend->data = sData = r;
                }
                subtrahend->offset = minuend->offset;
                subLen -= offsetDiff;
                offsetDiff = 0;
            }
            else {
                jint rLen = minLen + offsetDiff;
                if(rLen >= sData.length()) {
                    subtrahend->data = sData = Arrays::copyOf(sData, rLen);
                }
            }
            //@ assert minuend == this && minuend.value() == \old(this.value());
            //@ assert mData == minuend.data && minLen == minuend.nWords;
            //@ assert subtrahend.offset + subtrahend.data.length >= minuend.size();
            //@ assert sData == subtrahend.data;
            //@ assert AP(subtrahend.data, subtrahend.data.length) << subtrahend.offset == \old(subtrahend.value());
            //@ assert subtrahend.offset == Math.min(\old(this.offset), minuend.offset);
            //@ assert offsetDiff == minuend.offset - subtrahend.offset;
            //@ assert 0 <= offsetDiff && offsetDiff + minLen <= sData.length;
            jint sIndex = 0;
            jlong borrow = 0LL;
            for(; sIndex < offsetDiff; sIndex++) {
                jlong diff = 0LL - (sData[sIndex] & LONG_MASK) + borrow;
                sData[sIndex] = static_cast<jint>(diff);
                borrow = diff >> 32; // signed shift
            }
            //@ assert sIndex == offsetDiff;
            for(jint mIndex = 0; mIndex < minLen; sIndex++, mIndex++) {
                //@ assert sIndex == offsetDiff + mIndex;
                jlong diff = (mData[mIndex] & LONG_MASK) - (sData[sIndex] & LONG_MASK) + borrow;
                sData[sIndex] = static_cast<jint>(diff);
                borrow = diff >> 32; // signed shift
            }
            assert(borrow == 0L); // borrow out of subtract,
            // result should be positive
            subtrahend->nWords = sIndex;
            subtrahend->trimLeadingZeros();
            return subtrahend;
        }

        jint FDBigInteger::cmp(JFDBigInteger other)
        {
            jint aSize = nWords + offset;
            jint bSize = other->nWords + other->offset;
            if(aSize > bSize) {
                return 1;
            }
            else if(aSize < bSize) {
                return -1;
            }
            jint aLen = nWords;
            jint bLen = other->nWords;
            while(aLen > 0 && bLen > 0) {
                jint a = data[--aLen];
                jint b = other->data[--bLen];
                if(a != b) {
                    return ((a & LONG_MASK) < (b & LONG_MASK)) ? -1 : 1;
                }
            }
            if(aLen > 0) {
                return checkZeroTail(data, aLen);
            }
            if(bLen > 0) {
                return -checkZeroTail(other->data, bLen);
            }
            return 0;
        }

        jint FDBigInteger::cmpPow52(jint p5, jint p2)
        {
            if(p5 == 0) {
                jint wordcount = p2 >> 5;
                jint bitcount = p2 & 0x1f;
                jint size = this->nWords + this->offset;
                if(size > wordcount + 1) {
                    return 1;
                }
                else if(size < wordcount + 1) {
                    return -1;
                }
                jint a = this->data[this->nWords - 1];
                jint b = 1 << bitcount;
                if(a != b) {
                    return ((a & LONG_MASK) < (b & LONG_MASK)) ? -1 : 1;
                }
                return checkZeroTail(this->data, this->nWords - 1);
            }
            return this->cmp(big5pow(p5)->leftShift(p2));
        }

        jint FDBigInteger::addAndCmp(JFDBigInteger x, JFDBigInteger y)
        {
            JFDBigInteger big;
            JFDBigInteger small;
            jint xSize = x->size();
            jint ySize = y->size();
            jint bSize;
            jint sSize;
            if(xSize >= ySize) {
                big = x;
                small = y;
                bSize = xSize;
                sSize = ySize;
            }
            else {
                big = y;
                small = x;
                bSize = ySize;
                sSize = xSize;
            }
            jint thSize = this->size();
            if(bSize == 0) {
                return thSize == 0 ? 0 : 1;
            }
            if(sSize == 0) {
                return this->cmp(big);
            }
            if(bSize > thSize) {
                return -1;
            }
            if(bSize + 1 < thSize) {
                return 1;
            }
            jlong top = (big->data[big->nWords - 1] & LONG_MASK);
            if(sSize == bSize) {
                top += (small->data[small->nWords - 1] & LONG_MASK);
            }
            if((static_cast<julong>(top) >> 32) == 0) {
                if((static_cast<julong>(top + 1) >> 32) == 0) {
                    // good case - no carry extension
                    if(bSize < thSize) {
                        return 1;
                    }
                    // here sum.nWords == this.nWords
                    jlong v = (this->data[this->nWords - 1] & LONG_MASK);
                    if(v < top) {
                        return -1;
                    }
                    if(v > top + 1) {
                        return 1;
                    }
                }
            }
            else {   // (top>>>32)!=0 guaranteed carry extension
                if(bSize + 1 > thSize) {
                    return -1;
                }
                // here sum.nWords == this.nWords
                top = static_cast<julong>(top) >> 32;
                jlong v = (this->data[this->nWords - 1] & LONG_MASK);
                if(v < top) {
                    return -1;
                }
                if(v > top + 1) {
                    return 1;
                }
            }
            return this->cmp(big->add(small));
        }

        void FDBigInteger::makeImmutable()
        {
            this->isImmutable = true;
        }

        void FDBigInteger::trimLeadingZeros()
        {
            jint i = nWords;
            if(i > 0 && (data[--i] == 0)) {
                //for (; i > 0 && data[i - 1] == 0; i--) ;
                while(i > 0 && data[i - 1] == 0) {
                    i--;
                }
                this->nWords = i;
                if(i == 0) {  // all words are zero
                    this->offset = 0;
                }
            }
        }

        void FDBigInteger::leftShift(jarray<jint> src, jint idx, jarray<jint> result, jint bitcount, jint anticount, jint prev)
        {
            for(; idx > 0; idx--) {
                jint v = (prev << bitcount);
                prev = src[idx - 1];
                v |= (static_cast<juint>(prev) >> anticount);
                result[idx] = v;
            }
            jint v = prev << bitcount;
            result[0] = v;
        }

        jint FDBigInteger::size()
        {
            return nWords + offset;
        }

        void FDBigInteger::mult(jarray<jint> s1, jint s1Len, jarray<jint> s2, jint s2Len, jarray<jint> dst)
        {
            for(jint i = 0; i < s1Len; i++) {
                jlong v = s1[i] & LONG_MASK;
                jlong p = 0LL;
                for(jint j = 0; j < s2Len; j++) {
                    p += (dst[i + j] & LONG_MASK) + v * (s2[j] & LONG_MASK);
                    dst[i + j] = static_cast<jint>(p);
                    p = static_cast<julong>(p) >> 32;
                }
                dst[i + s2Len] = static_cast<jint>(p);
            }
        }

        jint FDBigInteger::checkZeroTail(jarray<jint> a, jint from)
        {
            while(from > 0) {
                if(a[--from] != 0) {
                    return 1;
                }
            }
            return 0;
        }

        JFDBigInteger FDBigInteger::mult(jint i)
        {
            if(this->nWords == 0) {
                return shared_from_this();
            }
            jarray<jint> r(nWords + 1);
            mult(data, nWords, i, r);
            return jnew<FDBigInteger>(r, offset);
        }

        JFDBigInteger FDBigInteger::mult(JFDBigInteger other)
        {
            if(this->nWords == 0) {
                return shared_from_this();
            }
            if(this->size() == 1) {
                return other->mult(data[0]);
            }
            if(other->nWords == 0) {
                return other;
            }
            if(other->size() == 1) {
                return this->mult(other->data[0]);
            }
            jarray<jint> r(nWords + other->nWords);
            mult(this->data, this->nWords, other->data, other->nWords, r);
            return jnew<FDBigInteger>(r, this->offset + other->offset);
        }

        JFDBigInteger FDBigInteger::add(JFDBigInteger other)
        {
            JFDBigInteger big, small;
            jint bigLen, smallLen;
            jint tSize = this->size();
            jint oSize = other->size();
            if(tSize >= oSize) {
                big = shared_from_this();
                bigLen = tSize;
                small = other;
                smallLen = oSize;
            }
            else {
                big = other;
                bigLen = oSize;
                small = shared_from_this();
                smallLen = tSize;
            }
            jarray<jint> r(bigLen + 1);
            jint i = 0;
            jlong carry = 0LL;
            for(; i < smallLen; i++) {
                carry += (i < big->offset   ? 0LL : (big->data[i - big->offset] & LONG_MASK))
                         + ((i < small->offset ? 0LL : (small->data[i - small->offset] & LONG_MASK)));
                r[i] = static_cast<jint>(carry);
                carry >>= 32; // signed shift.
            }
            for(; i < bigLen; i++) {
                carry += (i < big->offset ? 0LL : (big->data[i - big->offset] & LONG_MASK));
                r[i] = static_cast<jint>(carry);
                carry >>= 32; // signed shift.
            }
            r[bigLen] = static_cast<jint>(carry);
            return jnew<FDBigInteger>(r, 0);
        }

        void FDBigInteger::multAddMe(jint iv, jint addend)
        {
            jlong v = iv & LONG_MASK;
            // unroll 0th iteration, doing addition.
            jlong p = v * (data[0] & LONG_MASK) + (addend & LONG_MASK);
            data[0] = static_cast<jint>(p);
            p = static_cast<julong>(p) >> 32;
            for(jint i = 1; i < nWords; i++) {
                p += v * (data[i] & LONG_MASK);
                data[i] = static_cast<jint>(p);
                p = static_cast<julong>(p) >> 32;
            }
            if(p != 0LL) {
                data[nWords++] = static_cast<jint>(p); // will fail noisily if illegal!
            }
        }

        jlong FDBigInteger::multDiffMe(jlong q, JFDBigInteger S)
        {
            jlong diff = 0LL;
            if(q != 0) {
                jint deltaSize = S->offset - this->offset;
                if(deltaSize >= 0) {
                    jarray<jint> sd = S->data;
                    jarray<jint> td = this->data;
                    for(jint sIndex = 0, tIndex = deltaSize; sIndex < S->nWords; sIndex++, tIndex++) {
                        diff += (td[tIndex] & LONG_MASK) - q * (sd[sIndex] & LONG_MASK);
                        td[tIndex] = static_cast<jint>(diff);
                        diff >>= 32; // N.B. SIGNED shift.
                    }
                }
                else {
                    deltaSize = -deltaSize;
                    jarray<jint> rd(nWords + deltaSize);
                    jint sIndex = 0;
                    jint rIndex = 0;
                    jarray<jint> sd = S->data;
                    for(; rIndex < deltaSize && sIndex < S->nWords; sIndex++, rIndex++) {
                        diff -= q * (sd[sIndex] & LONG_MASK);
                        rd[rIndex] = static_cast<jint>(diff);
                        diff >>= 32; // N.B. SIGNED shift.
                    }
                    jint tIndex = 0;
                    jarray<jint> td = this->data;
                    for(; sIndex < S->nWords; sIndex++, tIndex++, rIndex++) {
                        diff += (td[tIndex] & LONG_MASK) - q * (sd[sIndex] & LONG_MASK);
                        rd[rIndex] = static_cast<jint>(diff);
                        diff >>= 32; // N.B. SIGNED shift.
                    }
                    this->nWords += deltaSize;
                    this->offset -= deltaSize;
                    this->data = rd;
                }
            }
            return diff;
        }

        jint FDBigInteger::multAndCarryBy10(jarray<jint> src, jint srcLen, jarray<jint> dst)
        {
            jlong carry = 0;
            for(jint i = 0; i < srcLen; i++) {
                jlong product = (src[i] & LONG_MASK) * 10LL + carry;
                dst[i] = static_cast<jint>(product);
                carry = static_cast<julong>(product) >> 32;
            }
            return static_cast<jint>(carry);
        }

        void FDBigInteger::mult(jarray<jint> src, jint srcLen, jint value, jarray<jint> dst)
        {
            jlong val = value & LONG_MASK;
            jlong carry = 0;
            for(jint i = 0; i < srcLen; i++) {
                jlong product = (src[i] & LONG_MASK) * val + carry;
                dst[i] = static_cast<jint>(product);
                carry = static_cast<julong>(product) >> 32;
            }
            dst[srcLen] = static_cast<jint>(carry);
        }

        void FDBigInteger::mult(jarray<jint> src, jint srcLen, jint v0, jint v1, jarray<jint> dst)
        {
            jlong v = v0 & LONG_MASK;
            jlong carry = 0;
            for(jint j = 0; j < srcLen; j++) {
                jlong product = v * (src[j] & LONG_MASK) + carry;
                dst[j] = static_cast<jint>(product);
                carry = static_cast<julong>(product) >> 32;
            }
            dst[srcLen] = (jint) carry;
            v = v1 & LONG_MASK;
            carry = 0;
            for(jint j = 0; j < srcLen; j++) {
                jlong product = (dst[j + 1] & LONG_MASK) + v * (src[j] & LONG_MASK) + carry;
                dst[j + 1] = static_cast<jint>(product);
                carry = static_cast<julong>(product) >> 32;
            }
            dst[srcLen + 1] = static_cast<jint>(carry);
        }

        JFDBigInteger FDBigInteger::big5pow(jint p)
        {
            assert(p >= 0); // negative power of 5
            if(p < MAX_FIVE_POW) {
                return POW_5_CACHE[p];
            }
            return big5powRec(p);
        }

        JFDBigInteger FDBigInteger::big5powRec(jint p)
        {
            if(p < MAX_FIVE_POW) {
                return POW_5_CACHE[p];
            }
            // construct the value.
            // recursively.
            jint q, r;
            // in order to compute 5^p,
            // compute its square root, 5^(p/2) and square.
            // or, let q = p / 2, r = p -q, then
            // 5^p = 5^(q+r) = 5^q * 5^r
            q = p >> 1;
            r = p - q;
            JFDBigInteger bigq = big5powRec(q);
            if(r < SMALL_5_POW.length()) {
                return bigq->mult(SMALL_5_POW[r]);
            }
            else {
                return bigq->mult(big5powRec(r));
            }
        }

        const jarray<jint> FDBigInteger::SMALL_5_POW = {
            1,
            5,
            5 * 5,
            5 * 5 * 5,
            5 * 5 * 5 * 5,
            5 * 5 * 5 * 5 * 5,
            5 * 5 * 5 * 5 * 5 * 5,
            5 * 5 * 5 * 5 * 5 * 5 * 5,
            5 * 5 * 5 * 5 * 5 * 5 * 5 * 5,
            5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5,
            5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5,
            5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5,
            5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5,
            5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5
        };

        const jarray<jlong> FDBigInteger::LONG_5_POW = {
            1LL,
            5LL,
            5LL * 5,
            5LL * 5 * 5,
            5LL * 5 * 5 * 5,
            5LL * 5 * 5 * 5 * 5,
            5LL * 5 * 5 * 5 * 5 * 5,
            5LL * 5 * 5 * 5 * 5 * 5 * 5,
            5LL * 5 * 5 * 5 * 5 * 5 * 5 * 5,
            5LL * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5,
            5LL * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5,
            5LL * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5,
            5LL * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5,
            5LL * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5,
            5LL * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5,
            5LL * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5,
            5LL * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5,
            5LL * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5,
            5LL * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5,
            5LL * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5,
            5LL * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5,
            5LL * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5,
            5LL * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5,
            5LL * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5,
            5LL * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5,
            5LL * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5,
            5LL * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5 * 5,
        };

        const jint FDBigInteger::MAX_FIVE_POW = 340;

        jarray<JFDBigInteger> FDBigInteger::POW_5_CACHE(MAX_FIVE_POW);

        JFDBigInteger FDBigInteger::ZERO = jnew<FDBigInteger>();

        const jlong FDBigInteger::LONG_MASK = 0xffffffffL;

        jboolean FDBigInteger::isStaticInitializationBlock = StaticInitializationBlock();

        jboolean FDBigInteger::StaticInitializationBlock()
        {
            jint i = 0;
            while(i < SMALL_5_POW.length()) {
                jarray<jint> pdata = { SMALL_5_POW[i] };
                JFDBigInteger pow5 = jnew<FDBigInteger>(pdata, 0);
                pow5->makeImmutable();
                POW_5_CACHE[i] = pow5;
                i++;
            }
            JFDBigInteger prev = POW_5_CACHE[i - 1];
            while(i < MAX_FIVE_POW) {
                POW_5_CACHE[i] = prev = prev->mult(5);
                prev->makeImmutable();
                i++;
            }

            ZERO->makeImmutable();

            return true;
        }
    }
}
