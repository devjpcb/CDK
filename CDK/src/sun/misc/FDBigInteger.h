#ifndef SUN_MISC_FDBIGINTEGER_H
#define SUN_MISC_FDBIGINTEGER_H

#include "../../cpp/lang/Object.h"
#include "../../cpp/lang/Array.h"

namespace sun
{
    namespace misc
    {
        class FDBigInteger : public cpp::lang::Object, public std::enable_shared_from_this<FDBigInteger>
        {
        public:
            FDBigInteger();
            FDBigInteger(jarray<jint> data, jint offset);
            FDBigInteger(jlong lValue, jarray<jchar> digits, jint kDigits, jint nDigits);
            ~FDBigInteger();

            static JFDBigInteger valueOfPow52(jint p5, jint p2);
            static JFDBigInteger valueOfMulPow52(jlong value, jint p5, jint p2);
            static JFDBigInteger valueOfPow2(jint p2);
            jint getNormalizationBias();
            JFDBigInteger leftShift(jint shift);
            jint quoRemIteration(JFDBigInteger S);
            JFDBigInteger multBy10();
            JFDBigInteger multByPow52(jint p5, jint p2);
            JFDBigInteger leftInplaceSub(JFDBigInteger subtrahend);
            JFDBigInteger rightInplaceSub(JFDBigInteger subtrahend);
            jint cmp(JFDBigInteger other);
            jint cmpPow52(jint p5, jint p2);
            jint addAndCmp(JFDBigInteger x, JFDBigInteger y);
            void makeImmutable();
            cpp::lang::String toHexString();
            // TODO: BigInteger toBigInteger();
            // TODO: String toString()
            
        private:
            void trimLeadingZeros();
            static void leftShift(jarray<jint> src, jint idx, jarray<jint> result, jint bitcount, jint anticount, jint prev);
            jint size();
            static void mult(jarray<jint> s1, jint s1Len, jarray<jint> s2, jint s2Len, jarray<jint> dst);
            static jint checkZeroTail(jarray<jint> a, jint from);
            JFDBigInteger mult(jint i);
            JFDBigInteger mult(JFDBigInteger other);
            JFDBigInteger add(JFDBigInteger other);
            void multAddMe(jint iv, jint addend);
            jlong multDiffMe(jlong q, JFDBigInteger S);
            static jint multAndCarryBy10(jarray<jint> src, jint srcLen, jarray<jint> dst);
            static void mult(jarray<jint> src, jint srcLen, jint value, jarray<jint> dst);
            static void mult(jarray<jint> src, jint srcLen, jint v0, jint v1, jarray<jint> dst);
            static JFDBigInteger big5pow(jint p);
            static JFDBigInteger big5powRec(jint p);

            static const jarray<jint> SMALL_5_POW;
            static const jarray<jlong> LONG_5_POW;
            static const jint MAX_FIVE_POW;
            static jarray<JFDBigInteger> POW_5_CACHE;
            static JFDBigInteger ZERO;
            static const jlong LONG_MASK;
            
            jarray<jint> data; 
            jint offset;  
            jint nWords;  
            jboolean isImmutable;
            
            static jboolean isStaticInitializationBlock;
            static jboolean StaticInitializationBlock();
            
            friend class FloatingDecimal;
        };
    }
}

#endif // SUN_MISC_FDBIGINTEGER_H
