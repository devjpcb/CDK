#include "FloatConsts.h"

#include "../../cpp/lang/Float.h"

namespace sun
{
    namespace misc
    {
        const jfloat FloatConsts::POSITIVE_INFINITY = cpp::lang::Float::POSITIVE_INFINITY;
        
        const jfloat FloatConsts::MIN_VALUE = cpp::lang::Float::MIN_VALUE;
        
        const jint FloatConsts::SIGNIFICAND_WIDTH = 24;

        const jint FloatConsts::MAX_EXPONENT = 127;
        
        const jint FloatConsts::MIN_EXPONENT = -126;
        
        const jint FloatConsts::MIN_SUB_EXPONENT = MIN_EXPONENT - (SIGNIFICAND_WIDTH - 1);

        const jint FloatConsts::EXP_BIAS = 127;

        const jint FloatConsts::SIGN_BIT_MASK = 0x80000000;

        const jint FloatConsts::EXP_BIT_MASK = 0x7F800000;

        const jint FloatConsts::SIGNIF_BIT_MASK = 0x007FFFFF;
    }
}
