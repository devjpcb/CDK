#ifndef SUN_MISC_FLOATCONSTS_H
#define SUN_MISC_FLOATCONSTS_H

#include "../../cpp/lang/Object.h"

namespace sun
{
    namespace misc
    {
        class FloatConsts
        {
        public:
            FloatConsts() = delete;
            
            static const jfloat POSITIVE_INFINITY;
            static const jfloat MIN_VALUE;
            static const jint SIGNIFICAND_WIDTH;
            static const jint MAX_EXPONENT;
            static const jint MIN_EXPONENT;
            static const jint MIN_SUB_EXPONENT;
            static const jint EXP_BIAS;
            static const jint SIGN_BIT_MASK;
            static const jint EXP_BIT_MASK;
            static const jint SIGNIF_BIT_MASK;
        };
    }
}

#endif // SUN_MISC_FLOATCONSTS_H
