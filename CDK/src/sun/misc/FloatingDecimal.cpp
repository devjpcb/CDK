#include "FloatingDecimal.h"
#include "FDBigInteger.h"
#include "DoubleConsts.h"
#include "FloatConsts.h"
#include "../../cpp/lang/Integer.h"
#include "../../cpp/lang/Long.h"
#include "../../cpp/lang/Math.h"
#include "../../cpp/lang/Double.h"
#include "../../cpp/lang/Float.h"
#include "../../cpp/lang/Character.h"
#include "../../cpp/lang/StringBuilder.h"
#include "../../cpp/lang/StringBuffer.h"
#include "../../cpp/lang/System.h"
#include "../../cpp/util/Arrays.h"
#include "../../cpp/util/regex/Matcher.h"
#include "../../cpp/lang/IllegalArgumentException.h"
#include "../../cpp/lang/NumberFormatException.h"
#include "../../cpp/lang/StringIndexOutOfBoundsException.h"
#include "../../cpp/lang/AssertionError.h"
#include <cassert>

using namespace cpp::lang;
using namespace cpp::util;
using namespace cpp::util::regex;

namespace sun
{
    namespace misc
    {
        FloatingDecimal::FloatingDecimal()
        {
        }

        FloatingDecimal::~FloatingDecimal()
        {
        }

        JString FloatingDecimal::toJavaFormatString(jdouble d)
        {
            return getBinaryToASCIIConverter(d)->toJavaFormatString();
        }

        JString FloatingDecimal::toJavaFormatString(jfloat f)
        {
            return getBinaryToASCIIConverter(f)->toJavaFormatString();
        }
        
        void FloatingDecimal::appendTo(jdouble d, JAppendable buf)
        {
            getBinaryToASCIIConverter(d)->appendTo(buf);
        }
            
        void FloatingDecimal::appendTo(jfloat f, JAppendable buf)
        {
            getBinaryToASCIIConverter(f)->appendTo(buf);
        }

        jdouble FloatingDecimal::parseDouble(cpp::lang::JString s)
        {
            return readJavaFormatString(s)->doubleValue();
        }

        jfloat FloatingDecimal::parseFloat(cpp::lang::JString s)
        {
            return readJavaFormatString(s)->floatValue();
        }

        /*
         * ExceptionalBinaryToASCIIBuffer
         */
        FloatingDecimal::ExceptionalBinaryToASCIIBuffer::ExceptionalBinaryToASCIIBuffer(cpp::lang::JString image, jboolean isNegative) : image(image), _isNegative(isNegative)
        {
        }

        JString FloatingDecimal::ExceptionalBinaryToASCIIBuffer::toJavaFormatString()
        {
            return image;
        }

        void FloatingDecimal::ExceptionalBinaryToASCIIBuffer::appendTo(JAppendable buf)
        {
            JStringBuilder sbd = std::dynamic_pointer_cast<StringBuilder>(buf);
            JStringBuffer sbf = std::dynamic_pointer_cast<StringBuffer>(buf);

            if(sbd != nullptr) {
                sbd->append(image);
            }
            else if(sbf != nullptr) {
                sbf->append(image);
            }
            else {
                assert(false);
            }
        }

        jint FloatingDecimal::ExceptionalBinaryToASCIIBuffer::getDecimalExponent()
        {
            throw IllegalArgumentException(jstr(u"Exceptional value does not have an exponent"));
        }

        jint FloatingDecimal::ExceptionalBinaryToASCIIBuffer::getDigits(jarray<jchar> digits)
        {
            throw IllegalArgumentException(jstr(u"Exceptional value does not have digits"));
        }

        jboolean FloatingDecimal::ExceptionalBinaryToASCIIBuffer::isNegative()
        {
            return _isNegative;
        }

        jboolean FloatingDecimal::ExceptionalBinaryToASCIIBuffer::isExceptional()
        {
            return true;
        }

        jboolean FloatingDecimal::ExceptionalBinaryToASCIIBuffer::digitsRoundedUp()
        {
            throw IllegalArgumentException(jstr(u"Exceptional value is not rounded"));
        }

        jboolean FloatingDecimal::ExceptionalBinaryToASCIIBuffer::decimalDigitsExact()
        {
            throw IllegalArgumentException(jstr(u"Exceptional value is not exact"));
        }

        /*
         * BinaryToASCIIBuffer
         */

        FloatingDecimal::BinaryToASCIIBuffer::BinaryToASCIIBuffer() : digits(20), buffer(26), exactDecimalConversion(false), decimalDigitsRoundedUp(false)
        {
        }

        FloatingDecimal::BinaryToASCIIBuffer::BinaryToASCIIBuffer(jboolean isNegative, jarray<jchar> digits)
            : _isNegative(isNegative), decExponent(0), firstDigitIndex(0), nDigits(digits.length()), digits(digits), buffer(26), exactDecimalConversion(false), decimalDigitsRoundedUp(false)
        {
        }

        JString FloatingDecimal::BinaryToASCIIBuffer::toJavaFormatString()
        {
            jint len = getChars(buffer);
            return jnew<String>(buffer, 0, len);
        }

        void FloatingDecimal::BinaryToASCIIBuffer::appendTo(JAppendable buf)
        {
            JStringBuilder sbd = std::dynamic_pointer_cast<StringBuilder>(buf);
            JStringBuffer sbf = std::dynamic_pointer_cast<StringBuffer>(buf);

            jint len = getChars(buffer);
            if(sbd != nullptr) {
                sbd->append(buffer, 0, len);
            }
            else if(sbf != nullptr) {
                sbf->append(buffer, 0, len);
            }
            else {
                assert(false);
            }
        }

        jint FloatingDecimal::FloatingDecimal::BinaryToASCIIBuffer::getDecimalExponent()
        {
            return decExponent;
        }

        jint FloatingDecimal::BinaryToASCIIBuffer::getDigits(jarray<jchar> digits)
        {
            System::arraycopy(this->digits, firstDigitIndex, digits, 0, this->nDigits);
            return this->nDigits;
        }

        jboolean FloatingDecimal::BinaryToASCIIBuffer::isNegative()
        {
            return _isNegative;
        }

        jboolean FloatingDecimal::BinaryToASCIIBuffer::isExceptional()
        {
            return false;
        }

        jboolean FloatingDecimal::BinaryToASCIIBuffer::digitsRoundedUp()
        {
            return decimalDigitsRoundedUp;
        }

        jboolean FloatingDecimal::BinaryToASCIIBuffer::decimalDigitsExact()
        {
            return exactDecimalConversion;
        }

        void FloatingDecimal::BinaryToASCIIBuffer::setSign(jboolean isNegative)
        {
            this->_isNegative = isNegative;
        }

        void FloatingDecimal::BinaryToASCIIBuffer::developLongDigits(jint decExponent, jlong lvalue, jint insignificantDigits)
        {
            if(insignificantDigits != 0) {
                // Discard non-significant low-order bits, while rounding,
                // up to insignificant value.
                jlong pow10 = FDBigInteger::LONG_5_POW[insignificantDigits] << insignificantDigits; // 10^i == 5^i * 2^i;
                jlong residue = lvalue % pow10;
                lvalue /= pow10;
                decExponent += insignificantDigits;
                if(residue >= (pow10 >> 1)) {
                    // round up based on the low-order bits we're discarding
                    lvalue++;
                }
            }
            jint digitno = digits.length() - 1;
            jint c;
            if(lvalue <= Integer::MAX_VALUE) {
                assert(lvalue > 0LL); // lvalue <= 0
                // even easier subcase!
                // can do int arithmetic rather than long!
                jint ivalue = static_cast<jint>(lvalue);
                c = ivalue % 10;
                ivalue /= 10;
                while(c == 0) {
                    decExponent++;
                    c = ivalue % 10;
                    ivalue /= 10;
                }
                while(ivalue != 0) {
                    digits[digitno--] = static_cast<jchar>(c + u'0');
                    decExponent++;
                    c = ivalue % 10;
                    ivalue /= 10;
                }
                digits[digitno] = static_cast<jchar>(c + u'0');
            }
            else {
                // same algorithm as above (same bugs, too )
                // but using long arithmetic.
                c =  static_cast<jint>(lvalue % 10LL);
                lvalue /= 10LL;
                while(c == 0) {
                    decExponent++;
                    c =  static_cast<jint>(lvalue % 10LL);
                    lvalue /= 10LL;
                }
                while(lvalue != 0LL) {
                    digits[digitno--] = static_cast<jchar>(c + u'0');
                    decExponent++;
                    c = static_cast<jint>(lvalue % 10LL);
                    lvalue /= 10;
                }
                digits[digitno] = static_cast<jchar>(c + u'0');
            }
            this->decExponent = decExponent + 1;
            this->firstDigitIndex = digitno;
            this->nDigits = this->digits.length() - digitno;
        }

        void FloatingDecimal::BinaryToASCIIBuffer::dtoa(jint binExp, jlong fractBits, jint nSignificantBits, jboolean isCompatibleFormat)
        {
            assert(fractBits > 0); // fractBits here can't be zero or negative
            assert((fractBits & FRACT_HOB) != 0); // Hi-order bit should be set
            // Examine number. Determine if it is an easy case,
            // which we can do pretty trivially using float/long conversion,
            // or whether we must do real work.
            const jint tailZeros = Long::numberOfTrailingZeros(fractBits);

            // number of significant bits of fractBits;
            const jint nFractBits = EXP_SHIFT + 1 - tailZeros;

            // reset flags to default values as dtoa() does not always set these
            // flags and a prior call to dtoa() might have set them to incorrect
            // values with respect to the current state.
            decimalDigitsRoundedUp = false;
            exactDecimalConversion = false;

            // number of significant bits to the right of the point.
            jint nTinyBits = Math::max(0, nFractBits - binExp - 1);
            if(binExp <= MAX_SMALL_BIN_EXP && binExp >= MIN_SMALL_BIN_EXP) {
                // Look more closely at the number to decide if,
                // with scaling by 10^nTinyBits, the result will fit in
                // a long.
                if((nTinyBits < FDBigInteger::LONG_5_POW.length()) && ((nFractBits + N_5_BITS[nTinyBits]) < 64)) {
                    //
                    // We can do this:
                    // take the fraction bits, which are normalized.
                    // (a) nTinyBits == 0: Shift left or right appropriately
                    //     to align the binary point at the extreme right, i.e.
                    //     where a long int point is expected to be. The integer
                    //     result is easily converted to a string.
                    // (b) nTinyBits > 0: Shift right by EXP_SHIFT-nFractBits,
                    //     which effectively converts to long and scales by
                    //     2^nTinyBits. Then multiply by 5^nTinyBits to
                    //     complete the scaling. We know this won't overflow
                    //     because we just counted the number of bits necessary
                    //     in the result. The integer you get from this can
                    //     then be converted to a string pretty easily.
                    //
                    if(nTinyBits == 0) {
                        jint insignificant;
                        if(binExp > nSignificantBits) {
                            insignificant = insignificantDigitsForPow2(binExp - nSignificantBits - 1);
                        }
                        else {
                            insignificant = 0;
                        }
                        if(binExp >= EXP_SHIFT) {
                            fractBits <<= (binExp - EXP_SHIFT);
                        }
                        else {
                            fractBits = static_cast<julong>(fractBits) >> (EXP_SHIFT - binExp) ;
                        }
                        developLongDigits(0, fractBits, insignificant);
                        return;
                    }
                    //
                    // The following causes excess digits to be printed
                    // out in the single-float case. Our manipulation of
                    // halfULP here is apparently not correct. If we
                    // better understand how this works, perhaps we can
                    // use this special case again. But for the time being,
                    // we do not.
                    // else {
                    //     fractBits >>>= EXP_SHIFT+1-nFractBits;
                    //     fractBits//= long5pow[ nTinyBits ];
                    //     halfULP = long5pow[ nTinyBits ] >> (1+nSignificantBits-nFractBits);
                    //     developLongDigits( -nTinyBits, fractBits, insignificantDigits(halfULP) );
                    //     return;
                    // }
                    //
                }
            }
            //
            // This is the hard case. We are going to compute large positive
            // integers B and S and integer decExp, s.t.
            //      d = ( B / S )// 10^decExp
            //      1 <= B / S < 10
            // Obvious choices are:
            //      decExp = floor( log10(d) )
            //      B      = d// 2^nTinyBits// 10^max( 0, -decExp )
            //      S      = 10^max( 0, decExp)// 2^nTinyBits
            // (noting that nTinyBits has already been forced to non-negative)
            // I am also going to compute a large positive integer
            //      M      = (1/2^nSignificantBits)// 2^nTinyBits// 10^max( 0, -decExp )
            // i.e. M is (1/2) of the ULP of d, scaled like B.
            // When we iterate through dividing B/S and picking off the
            // quotient bits, we will know when to stop when the remainder
            // is <= M.
            //
            // We keep track of powers of 2 and powers of 5.
            //
            jint decExp = estimateDecExp(fractBits, binExp);
            jint B2, B5; // powers of 2 and powers of 5, respectively, in B
            jint S2, S5; // powers of 2 and powers of 5, respectively, in S
            jint M2, M5; // powers of 2 and powers of 5, respectively, in M

            B5 = Math::max(0, -decExp);
            B2 = B5 + nTinyBits + binExp;

            S5 = Math::max(0, decExp);
            S2 = S5 + nTinyBits;

            M5 = B5;
            M2 = B2 - nSignificantBits;

            //
            // the long integer fractBits contains the (nFractBits) interesting
            // bits from the mantissa of d ( hidden 1 added if necessary) followed
            // by (EXP_SHIFT+1-nFractBits) zeros. In the interest of compactness,
            // I will shift out those zeros before turning fractBits into a
            // FDBigInteger. The resulting whole number will be
            //      d * 2^(nFractBits-1-binExp).
            //
            fractBits = static_cast<julong>(fractBits) >> tailZeros;
            B2 -= nFractBits - 1;
            jint common2factor = Math::min(B2, S2);
            B2 -= common2factor;
            S2 -= common2factor;
            M2 -= common2factor;

            //
            // HACK!! For exact powers of two, the next smallest number
            // is only half as far away as we think (because the meaning of
            // ULP changes at power-of-two bounds) for this reason, we
            // hack M2. Hope this works.
            //
            if(nFractBits == 1) {
                M2 -= 1;
            }

            if(M2 < 0) {
                // oops.
                // since we cannot scale M down far enough,
                // we must scale the other values up.
                B2 -= M2;
                S2 -= M2;
                M2 =  0;
            }
            //
            // Construct, Scale, iterate.
            // Some day, we'll write a stopping test that takes
            // account of the asymmetry of the spacing of floating-point
            // numbers below perfect powers of 2
            // 26 Sept 96 is not that day.
            // So we use a symmetric test.
            //
            jint ndigit = 0;
            jboolean low, high;
            jlong lowDigitDifference;
            jint  q;

            //
            // Detect the special cases where all the numbers we are about
            // to compute will fit in int or long integers.
            // In these cases, we will avoid doing FDBigInteger arithmetic.
            // We use the same algorithms, except that we "normalize"
            // our FDBigIntegers before iterating. This is to make division easier,
            // as it makes our fist guess (quotient of high-order words)
            // more accurate!
            //
            // Some day, we'll write a stopping test that takes
            // account of the asymmetry of the spacing of floating-point
            // numbers below perfect powers of 2
            // 26 Sept 96 is not that day.
            // So we use a symmetric test.
            //
            // binary digits needed to represent B, approx.
            jint Bbits = nFractBits + B2 + ((B5 < N_5_BITS.length()) ? N_5_BITS[B5] : (B5 * 3));

            // binary digits needed to represent 10*S, approx.
            jint tenSbits = S2 + 1 + (((S5 + 1) < N_5_BITS.length()) ? N_5_BITS[(S5 + 1)] : ((S5 + 1) * 3));
            if(Bbits < 64 && tenSbits < 64) {
                if(Bbits < 32 && tenSbits < 32) {
                    // wa-hoo! They're all ints!
                    jint b = ((jint)fractBits * FDBigInteger::SMALL_5_POW[B5]) << B2;
                    jint s = FDBigInteger::SMALL_5_POW[S5] << S2;
                    jint m = FDBigInteger::SMALL_5_POW[M5] << M2;
                    jint tens = s * 10;
                    //
                    // Unroll the first iteration. If our decExp estimate
                    // was too high, our first quotient will be zero. In this
                    // case, we discard it and decrement decExp.
                    //
                    ndigit = 0;
                    q = b / s;
                    b = 10 * (b % s);
                    m *= 10;
                    low  = (b <  m);
                    high = (b + m > tens);
                    assert(q < 10); // excessively large digit
                    if((q == 0) && ! high) {
                        // oops. Usually ignore leading zero.
                        decExp--;
                    }
                    else {
                        digits[ndigit++] = static_cast<jchar>(u'0' + q);
                    }
                    //
                    // HACK! Java spec sez that we always have at least
                    // one digit after the . in either F- or E-form output.
                    // Thus we will need more than one digit if we're using
                    // E-form
                    //
                    if(!isCompatibleFormat || decExp < -3 || decExp >= 8) {
                        high = low = false;
                    }
                    while(! low && ! high) {
                        q = b / s;
                        b = 10 * (b % s);
                        m *= 10;
                        assert(q < 10);// excessively large digit
                        if(m > 0LL) {
                            low  = (b <  m);
                            high = (b + m > tens);
                        }
                        else {
                            // hack -- m might overflow!
                            // in this case, it is certainly > b,
                            // which won't
                            // and b+m > tens, too, since that has overflowed
                            // either!
                            low = true;
                            high = true;
                        }
                        digits[ndigit++] = static_cast<jchar>(u'0' + q);
                    }
                    lowDigitDifference = (b << 1) - tens;
                    exactDecimalConversion  = (b == 0);
                }
                else {
                    // still good! they're all longs!
                    jlong b = (fractBits * FDBigInteger::LONG_5_POW[B5]) << B2;
                    jlong s = FDBigInteger::LONG_5_POW[S5] << S2;
                    jlong m = FDBigInteger::LONG_5_POW[M5] << M2;
                    jlong tens = s * 10L;
                    //
                    // Unroll the first iteration. If our decExp estimate
                    // was too high, our first quotient will be zero. In this
                    // case, we discard it and decrement decExp.
                    //
                    ndigit = 0;
                    q = (jint)(b / s);
                    b = 10LL * (b % s);
                    m *= 10LL;
                    low  = (b <  m);
                    high = (b + m > tens);
                    assert(q < 10);// excessively large digit
                    if((q == 0) && ! high) {
                        // oops. Usually ignore leading zero.
                        decExp--;
                    }
                    else {
                        digits[ndigit++] = static_cast<jchar>(u'0' + q);
                    }
                    //
                    // HACK! Java spec sez that we always have at least
                    // one digit after the . in either F- or E-form output.
                    // Thus we will need more than one digit if we're using
                    // E-form
                    //
                    if(!isCompatibleFormat || decExp < -3 || decExp >= 8) {
                        high = low = false;
                    }
                    while(! low && ! high) {
                        q = (jint)(b / s);
                        b = 10 * (b % s);
                        m *= 10;
                        assert(q < 10);// excessively large digit
                        if(m > 0LL) {
                            low  = (b <  m);
                            high = (b + m > tens);
                        }
                        else {
                            // hack -- m might overflow!
                            // in this case, it is certainly > b,
                            // which won't
                            // and b+m > tens, too, since that has overflowed
                            // either!
                            low = true;
                            high = true;
                        }
                        digits[ndigit++] = static_cast<jchar>(u'0' + q);
                    }
                    lowDigitDifference = (b << 1) - tens;
                    exactDecimalConversion  = (b == 0);
                }
            }
            else {
                //
                // We really must do FDBigInteger arithmetic.
                // Fist, construct our FDBigInteger initial values.
                //
                JFDBigInteger Sval = FDBigInteger::valueOfPow52(S5, S2);
                jint shiftBias = Sval->getNormalizationBias();
                Sval = Sval->leftShift(shiftBias); // normalize so that division works better

                JFDBigInteger Bval = FDBigInteger::valueOfMulPow52(fractBits, B5, B2 + shiftBias);
                JFDBigInteger Mval = FDBigInteger::valueOfPow52(M5 + 1, M2 + shiftBias + 1);

                JFDBigInteger tenSval = FDBigInteger::valueOfPow52(S5 + 1, S2 + shiftBias + 1); //Sval.mult( 10 );
                //
                // Unroll the first iteration. If our decExp estimate
                // was too high, our first quotient will be zero. In this
                // case, we discard it and decrement decExp.
                //
                ndigit = 0;
                q = Bval->quoRemIteration(Sval);
                low  = (Bval->cmp(Mval) < 0);
                high = tenSval->addAndCmp(Bval, Mval) <= 0;

                assert(q < 10); // excessively large digit
                if((q == 0) && ! high) {
                    // oops. Usually ignore leading zero.
                    decExp--;
                }
                else {
                    digits[ndigit++] = static_cast<jchar>(u'0' + q);
                }
                //
                // HACK! Java spec sez that we always have at least
                // one digit after the . in either F- or E-form output.
                // Thus we will need more than one digit if we're using
                // E-form
                //
                if(!isCompatibleFormat || decExp < -3 || decExp >= 8) {
                    high = low = false;
                }
                while(! low && ! high) {
                    q = Bval->quoRemIteration(Sval);
                    assert(q < 10); // excessively large digit
                    Mval = Mval->multBy10(); //Mval = Mval.mult( 10 );
                    low  = (Bval->cmp(Mval) < 0);
                    high = tenSval->addAndCmp(Bval, Mval) <= 0;
                    digits[ndigit++] = static_cast<jchar>(u'0' + q);
                }
                if(high && low) {
                    Bval = Bval->leftShift(1);
                    lowDigitDifference = Bval->cmp(tenSval);
                }
                else {
                    lowDigitDifference = 0LL; // this here only for flow analysis!
                }
                exactDecimalConversion  = (Bval->cmp(FDBigInteger::ZERO) == 0);
            }
            this->decExponent = decExp + 1;
            this->firstDigitIndex = 0;
            this->nDigits = ndigit;
            //
            // Last digit gets rounded based on stopping condition.
            //
            if(high) {
                if(low) {
                    if(lowDigitDifference == 0LL) {
                        // it's a tie!
                        // choose based on which digits we like.
                        if((digits[firstDigitIndex + nDigits - 1] & 1) != 0) {
                            roundup();
                        }
                    }
                    else if(lowDigitDifference > 0) {
                        roundup();
                    }
                }
                else {
                    roundup();
                }
            }
        }

        void FloatingDecimal::BinaryToASCIIBuffer::roundup()
        {
            jint i = (firstDigitIndex + nDigits - 1);
            jint q = digits[i];
            if(q == u'9') {
                while(q == u'9' && i > firstDigitIndex) {
                    digits[i] = u'0';
                    q = digits[--i];
                }
                if(q == u'9') {
                    // carryout! High-order 1, rest 0s, larger exp.
                    decExponent += 1;
                    digits[firstDigitIndex] = u'1';
                    return;
                }
                // else fall through.
            }
            digits[i] = static_cast<jchar>(q + 1);
            decimalDigitsRoundedUp = true;
        }

        jint FloatingDecimal::BinaryToASCIIBuffer::estimateDecExp(jlong fractBits, jint binExp)
        {
            jdouble d2 = Double::longBitsToDouble(EXP_ONE | (fractBits & DoubleConsts::SIGNIF_BIT_MASK));
            jdouble d = (d2 - 1.5) * 0.289529654 + 0.176091259 + static_cast<jdouble>(binExp) * 0.301029995663981;
            jlong dBits = Double::doubleToRawLongBits(d);  //can't be NaN here so use raw
            jint exponent = (jint)((dBits & DoubleConsts::EXP_BIT_MASK) >> EXP_SHIFT) - DoubleConsts::EXP_BIAS;
            jboolean isNegative = (dBits & DoubleConsts::SIGN_BIT_MASK) != 0; // discover sign
            if(exponent >= 0 && exponent < 52) { // hot path
                jlong mask   = DoubleConsts::SIGNIF_BIT_MASK >> exponent;
                jint r = (jint)(((dBits & DoubleConsts::SIGNIF_BIT_MASK) | FRACT_HOB) >> (EXP_SHIFT - exponent));
                return isNegative ? (((mask & dBits) == 0LL) ? -r : -r - 1) : r;
            }
            else if(exponent < 0) {
                return (((dBits &~ DoubleConsts::SIGN_BIT_MASK) == 0) ? 0 : ((isNegative) ? -1 : 0));
            }
            else {   //if (exponent >= 52)
                return static_cast<jint>(d);
            }
        }

        jint FloatingDecimal::BinaryToASCIIBuffer::insignificantDigits(jint insignificant)
        {
            jint i;
            for(i = 0; insignificant >= 10LL; i++) {
                insignificant /= 10LL;
            }
            return i;
        }

        jint FloatingDecimal::BinaryToASCIIBuffer::insignificantDigitsForPow2(jint p2)
        {
            if(p2 > 1 && p2 < insignificantDigitsNumber.length()) {
                return insignificantDigitsNumber[p2];
            }
            return 0;
        }

        jint FloatingDecimal::BinaryToASCIIBuffer::getChars(jarray<jchar> result)
        {
            assert(nDigits <= 19); // generous bound on size of nDigits
            jint i = 0;
            if(_isNegative) {
                result[0] = u'-';
                i = 1;
            }
            if(decExponent > 0 && decExponent < 8) {
                // print digits.digits.
                jint charLength = Math::min(nDigits, decExponent);
                System::arraycopy(digits, firstDigitIndex, result, i, charLength);
                i += charLength;
                if(charLength < decExponent) {
                    charLength = decExponent - charLength;
                    Arrays::fill(result, i, i + charLength, u'0');
                    i += charLength;
                    result[i++] = u'.';
                    result[i++] = u'0';
                }
                else {
                    result[i++] = u'.';
                    if(charLength < nDigits) {
                        jint t = nDigits - charLength;
                        System::arraycopy(digits, firstDigitIndex + charLength, result, i, t);
                        i += t;
                    }
                    else {
                        result[i++] = u'0';
                    }
                }
            }
            else if(decExponent <= 0 && decExponent > -3) {
                result[i++] = u'0';
                result[i++] = u'.';
                if(decExponent != 0) {
                    Arrays::fill(result, i, i - decExponent, u'0');
                    i -= decExponent;
                }
                System::arraycopy(digits, firstDigitIndex, result, i, nDigits);
                i += nDigits;
            }
            else {
                result[i++] = digits[firstDigitIndex];
                result[i++] = u'.';
                if(nDigits > 1) {
                    System::arraycopy(digits, firstDigitIndex + 1, result, i, nDigits - 1);
                    i += nDigits - 1;
                }
                else {
                    result[i++] = u'0';
                }
                result[i++] = u'E';
                int e;
                if(decExponent <= 0) {
                    result[i++] = u'-';
                    e = -decExponent + 1;
                }
                else {
                    e = decExponent - 1;
                }
                // decExponent has 1, 2, or 3, digits
                if(e <= 9) {
                    result[i++] = static_cast<jchar>(e + u'0');
                }
                else if(e <= 99) {
                    result[i++] = static_cast<jchar>(e / 10 + u'0');
                    result[i++] = static_cast<jchar>(e % 10 + u'0');
                }
                else {
                    result[i++] = static_cast<jchar>(e / 100 + u'0');
                    e %= 100;
                    result[i++] = static_cast<jchar>(e / 10 + u'0');
                    result[i++] = static_cast<jchar>(e % 10 + u'0');
                }
            }
            return i;
        }

        const jarray<jint> FloatingDecimal::BinaryToASCIIBuffer::insignificantDigitsNumber = {
            0, 0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3,
            4, 4, 4, 5, 5, 5, 6, 6, 6, 6, 7, 7, 7,
            8, 8, 8, 9, 9, 9, 9, 10, 10, 10, 11, 11, 11,
            12, 12, 12, 12, 13, 13, 13, 14, 14, 14,
            15, 15, 15, 15, 16, 16, 16, 17, 17, 17,
            18, 18, 18, 19
        };

        const jarray<jint> FloatingDecimal::BinaryToASCIIBuffer::N_5_BITS = {
            0,
            3,
            5,
            7,
            10,
            12,
            14,
            17,
            19,
            21,
            24,
            26,
            28,
            31,
            33,
            35,
            38,
            40,
            42,
            45,
            47,
            49,
            52,
            54,
            56,
            59,
            61,
        };

        /*
         * PreparedASCIIToBinaryBuffer
         */

        FloatingDecimal::PreparedASCIIToBinaryBuffer::PreparedASCIIToBinaryBuffer(jdouble doubleVal, jfloat floatVal) : doubleVal(doubleVal), floatVal(floatVal)
        {
        }

        jdouble FloatingDecimal::PreparedASCIIToBinaryBuffer::doubleValue()
        {
            return doubleVal;
        }

        jfloat FloatingDecimal::PreparedASCIIToBinaryBuffer::floatValue()
        {
            return floatVal;
        }

        /*
         * ASCIIToBinaryBuffer
         */

        FloatingDecimal::ASCIIToBinaryBuffer::ASCIIToBinaryBuffer(jboolean negSign, jint decExponent, jarray<jchar> digits, jint n) : isNegative(negSign), decExponent(decExponent), digits(digits), nDigits(n)
        {
        }

        jdouble FloatingDecimal::ASCIIToBinaryBuffer::doubleValue()
        {
            jint kDigits = Math::min(nDigits, MAX_DECIMAL_DIGITS + 1);
            //
            // convert the lead kDigits to a long integer.
            //
            // (special performance hack: start to do it using int)
            jint iValue = static_cast<jint>(digits[0]) - static_cast<jint>(u'0');
            jint iDigits = Math::min(kDigits, INT_DECIMAL_DIGITS);
            for(jint i = 1; i < iDigits; i++) {
                iValue = iValue * 10 + static_cast<jint>(digits[i]) - static_cast<jint>(u'0');
            }
            jlong lValue = static_cast<jlong>(iValue);
            for(jint i = iDigits; i < kDigits; i++) {
                lValue = lValue * 10LL + static_cast<jlong>((static_cast<jint>(digits[i]) - static_cast<jint>(u'0')));
            }
            jdouble dValue = static_cast<jdouble>(lValue);
            jint exp = decExponent - kDigits;
            //
            // lValue now contains a long integer with the value of
            // the first kDigits digits of the number.
            // dValue contains the (double) of the same.
            //

            if(nDigits <= MAX_DECIMAL_DIGITS) {
                //
                // possibly an easy case.
                // We know that the digits can be represented
                // exactly. And if the exponent isn't too outrageous,
                // the whole thing can be done with one operation,
                // thus one rounding error.
                // Note that all our constructors trim all leading and
                // trailing zeros, so simple values (including zero)
                // will always end up here
                //
                if(exp == 0 || dValue == 0.0) {
                    return (isNegative) ? -dValue : dValue; // small floating integer
                }
                else if(exp >= 0) {
                    if(exp <= MAX_SMALL_TEN) {
                        //
                        // Can get the answer with one operation,
                        // thus one roundoff.
                        //
                        double rValue = dValue * SMALL_10_POW[exp];
                        return (isNegative) ? -rValue : rValue;
                    }
                    jint slop = MAX_DECIMAL_DIGITS - kDigits;
                    if(exp <= MAX_SMALL_TEN + slop) {
                        //
                        // We can multiply dValue by 10^(slop)
                        // and it is still "small" and exact.
                        // Then we can multiply by 10^(exp-slop)
                        // with one rounding.
                        //
                        dValue *= SMALL_10_POW[slop];
                        jdouble rValue = dValue * SMALL_10_POW[exp - slop];
                        return (isNegative) ? -rValue : rValue;
                    }
                    //
                    // Else we have a hard case with a positive exp.
                    //
                }
                else {
                    if(exp >= -MAX_SMALL_TEN) {
                        //
                        // Can get the answer in one division.
                        //
                        jdouble rValue = dValue / SMALL_10_POW[-exp];
                        return (isNegative) ? -rValue : rValue;
                    }
                    //
                    // Else we have a hard case with a negative exp.
                    //
                }
            }

            //
            // Harder cases:
            // The sum of digits plus exponent is greater than
            // what we think we can do with one error.
            //
            // Start by approximating the right answer by,
            // naively, scaling by powers of 10.
            //
            if(exp > 0) {
                if(decExponent > MAX_DECIMAL_EXPONENT + 1) {
                    //
                    // Lets face it. This is going to be
                    // Infinity. Cut to the chase.
                    //
                    return (isNegative) ? Double::NEGATIVE_INFINITY : Double::POSITIVE_INFINITY;
                }
                if((exp & 15) != 0) {
                    dValue *= SMALL_10_POW[exp & 15];
                }
                if((exp >>= 4) != 0) {
                    jint j;
                    for(j = 0; exp > 1; j++, exp >>= 1) {
                        if((exp & 1) != 0) {
                            dValue *= BIG_10_POW[j];
                        }
                    }
                    //
                    // The reason for the weird exp > 1 condition
                    // in the above loop was so that the last multiply
                    // would get unrolled. We handle it here.
                    // It could overflow.
                    //
                    jdouble t = dValue * BIG_10_POW[j];
                    if(Double::isInfinite(t)) {
                        //
                        // It did overflow.
                        // Look more closely at the result.
                        // If the exponent is just one too large,
                        // then use the maximum finite as our estimate
                        // value. Else call the result infinity
                        // and punt it.
                        // ( I presume this could happen because
                        // rounding forces the result here to be
                        // an ULP or two larger than
                        // Double.MAX_VALUE ).
                        //
                        t = dValue / 2.0;
                        t *= BIG_10_POW[j];
                        if(Double::isInfinite(t)) {
                            return (isNegative) ? Double::NEGATIVE_INFINITY : Double::POSITIVE_INFINITY;
                        }
                        t = Double::MAX_VALUE;
                    }
                    dValue = t;
                }
            }
            else if(exp < 0) {
                exp = -exp;
                if(decExponent < MIN_DECIMAL_EXPONENT - 1) {
                    //
                    // Lets face it. This is going to be
                    // zero. Cut to the chase.
                    //
                    return (isNegative) ? -0.0 : 0.0;
                }
                if((exp & 15) != 0) {
                    dValue /= SMALL_10_POW[exp & 15];
                }
                if((exp >>= 4) != 0) {
                    jint j;
                    for(j = 0; exp > 1; j++, exp >>= 1) {
                        if((exp & 1) != 0) {
                            dValue *= TINY_10_POW[j];
                        }
                    }
                    //
                    // The reason for the weird exp > 1 condition
                    // in the above loop was so that the last multiply
                    // would get unrolled. We handle it here.
                    // It could underflow.
                    //
                    jdouble t = dValue * TINY_10_POW[j];
                    if(t == 0.0) {
                        //
                        // It did underflow.
                        // Look more closely at the result.
                        // If the exponent is just one too small,
                        // then use the minimum finite as our estimate
                        // value. Else call the result 0.0
                        // and punt it.
                        // ( I presume this could happen because
                        // rounding forces the result here to be
                        // an ULP or two less than
                        // Double.MIN_VALUE ).
                        //
                        t = dValue * 2.0;
                        t *= TINY_10_POW[j];
                        if(t == 0.0) {
                            return (isNegative) ? -0.0 : 0.0;
                        }
                        t = Double::MIN_VALUE;
                    }
                    dValue = t;
                }
            }

            //
            // dValue is now approximately the result.
            // The hard part is adjusting it, by comparison
            // with FDBigInteger arithmetic.
            // Formulate the EXACT big-number result as
            // bigD0 * 10^exp
            //
            if(nDigits > MAX_NDIGITS) {
                nDigits = MAX_NDIGITS + 1;
                digits[MAX_NDIGITS] = u'1';
            }
            JFDBigInteger bigD0 = jnew<FDBigInteger>(lValue, digits, kDigits, nDigits);
            exp = decExponent - nDigits;

            jlong ieeeBits = Double::doubleToRawLongBits(dValue); // IEEE-754 bits of double candidate
            const jint B5 = Math::max(0, -exp); // powers of 5 in bigB, value is not modified inside correctionLoop
            const jint D5 = Math::max(0, exp); // powers of 5 in bigD, value is not modified inside correctionLoop
            bigD0 = bigD0->multByPow52(D5, 0);
            bigD0->makeImmutable();   // prevent bigD0 modification inside correctionLoop
            JFDBigInteger bigD = nullptr;
            int prevD2 = 0;


            while(true) {
                // here ieeeBits can't be NaN, Infinity or zero
                jint binexp = static_cast<jint>((static_cast<julong>(ieeeBits) >> EXP_SHIFT));
                jlong bigBbits = ieeeBits & DoubleConsts::SIGNIF_BIT_MASK;
                if(binexp > 0) {
                    bigBbits |= FRACT_HOB;
                }
                else {   // Normalize denormalized numbers.
                    assert(bigBbits != 0L); // doubleToBigInt(0.0)
                    jint leadingZeros = Long::numberOfLeadingZeros(bigBbits);
                    jint shift = leadingZeros - (63 - EXP_SHIFT);
                    bigBbits <<= shift;
                    binexp = 1 - shift;
                }
                binexp -= DoubleConsts::EXP_BIAS;
                jint lowOrderZeros = Long::numberOfTrailingZeros(bigBbits);
                bigBbits = static_cast<julong>(bigBbits) >> lowOrderZeros;
                const jint bigIntExp = binexp - EXP_SHIFT + lowOrderZeros;
                const jint bigIntNBits = EXP_SHIFT + 1 - lowOrderZeros;

                //
                // Scale bigD, bigB appropriately for
                // big-integer operations.
                // Naively, we multiply by powers of ten
                // and powers of two. What we actually do
                // is keep track of the powers of 5 and
                // powers of 2 we would use, then factor out
                // common divisors before doing the work.
                //
                jint B2 = B5; // powers of 2 in bigB
                jint D2 = D5; // powers of 2 in bigD
                jint Ulp2;   // powers of 2 in halfUlp.
                if(bigIntExp >= 0) {
                    B2 += bigIntExp;
                }
                else {
                    D2 -= bigIntExp;
                }
                Ulp2 = B2;
                // shift bigB and bigD left by a number s. t.
                // halfUlp is still an integer.
                jint hulpbias;
                if(binexp <= -DoubleConsts::EXP_BIAS) {
                    // This is going to be a denormalized number
                    // (if not actually zero).
                    // half an ULP is at 2^-(DoubleConsts.EXP_BIAS+EXP_SHIFT+1)
                    hulpbias = binexp + lowOrderZeros + DoubleConsts::EXP_BIAS;
                }
                else {
                    hulpbias = 1 + lowOrderZeros;
                }
                B2 += hulpbias;
                D2 += hulpbias;
                // if there are common factors of 2, we might just as well
                // factor them out, as they add nothing useful.
                jint common2 = Math::min(B2, Math::min(D2, Ulp2));
                B2 -= common2;
                D2 -= common2;
                Ulp2 -= common2;
                // do multiplications by powers of 5 and 2
                JFDBigInteger bigB = FDBigInteger::valueOfMulPow52(bigBbits, B5, B2);
                if(bigD == nullptr || prevD2 != D2) {
                    bigD = bigD0->leftShift(D2);
                    prevD2 = D2;
                }
                //
                // to recap:
                // bigB is the scaled-big-int version of our floating-point
                // candidate.
                // bigD is the scaled-big-int version of the exact value
                // as we understand it.
                // halfUlp is 1/2 an ulp of bigB, except for special cases
                // of exact powers of 2
                //
                // the plan is to compare bigB with bigD, and if the difference
                // is less than halfUlp, then we're satisfied. Otherwise,
                // use the ratio of difference to halfUlp to calculate a fudge
                // factor to add to the floating value, then go 'round again.
                //
                JFDBigInteger diff;
                jint cmpResult;
                jboolean overvalue;
                if((cmpResult = bigB->cmp(bigD)) > 0) {
                    overvalue = true; // our candidate is too big.
                    diff = bigB->leftInplaceSub(bigD); // bigB is not user further - reuse
                    if((bigIntNBits == 1) && (bigIntExp > -DoubleConsts::EXP_BIAS + 1)) {
                        // candidate is a normalized exact power of 2 and
                        // is too big (larger than Double.MIN_NORMAL). We will be subtracting.
                        // For our purposes, ulp is the ulp of the
                        // next smaller range.
                        Ulp2 -= 1;
                        if(Ulp2 < 0) {
                            // rats. Cannot de-scale ulp this far.
                            // must scale diff in other direction.
                            Ulp2 = 0;
                            diff = diff->leftShift(1);
                        }
                    }
                }
                else if(cmpResult < 0) {
                    overvalue = false; // our candidate is too small.
                    diff = bigD->rightInplaceSub(bigB); // bigB is not user further - reuse
                }
                else {
                    // the candidate is exactly right!
                    // this happens with surprising frequency
                    goto correctionLoop;
                }
                cmpResult = diff->cmpPow52(B5, Ulp2);
                if((cmpResult) < 0) {
                    // difference is small.
                    // this is close enough
                    goto correctionLoop;
                }
                else if(cmpResult == 0) {
                    // difference is exactly half an ULP
                    // round to some other value maybe, then finish
                    if((ieeeBits & 1) != 0) {  // half ties to even
                        ieeeBits += overvalue ? -1 : 1; // nextDown or nextUp
                    }
                    goto correctionLoop;
                }
                else {
                    // difference is non-trivial.
                    // could scale addend by ratio of difference to
                    // halfUlp here, if we bothered to compute that difference.
                    // Most of the time ( I hope ) it is about 1 anyway.
                    ieeeBits += overvalue ? -1 : 1; // nextDown or nextUp
                    if(ieeeBits == 0 || ieeeBits == DoubleConsts::EXP_BIT_MASK) {  // 0.0 or Double.POSITIVE_INFINITY
                        goto correctionLoop; // oops. Fell off end of range.
                    }
                    continue; // try again.
                }

            }
        correctionLoop:
            if(isNegative) {
                ieeeBits |= DoubleConsts::SIGN_BIT_MASK;
            }
            return Double::longBitsToDouble(ieeeBits);
        }

        jfloat FloatingDecimal::ASCIIToBinaryBuffer::floatValue()
        {
            jint kDigits = Math::min(nDigits, SINGLE_MAX_DECIMAL_DIGITS + 1);
            //
            // convert the lead kDigits to an integer.
            //
            jint iValue = static_cast<jint>(digits[0]) - static_cast<jint>(u'0');
            for(jint i = 1; i < kDigits; i++) {
                iValue = iValue * 10 + static_cast<jint>(digits[i]) - static_cast<jint>(u'0');
            }
            jfloat fValue = static_cast<jfloat>(iValue);
            jint exp = decExponent - kDigits;
            //
            // iValue now contains an integer with the value of
            // the first kDigits digits of the number.
            // fValue contains the (float) of the same.
            //

            if(nDigits <= SINGLE_MAX_DECIMAL_DIGITS) {
                //
                // possibly an easy case.
                // We know that the digits can be represented
                // exactly. And if the exponent isn't too outrageous,
                // the whole thing can be done with one operation,
                // thus one rounding error.
                // Note that all our constructors trim all leading and
                // trailing zeros, so simple values (including zero)
                // will always end up here.
                //
                if(exp == 0 || fValue == 0.0f) {
                    return (isNegative) ? -fValue : fValue; // small floating integer
                }
                else if(exp >= 0) {
                    if(exp <= SINGLE_MAX_SMALL_TEN) {
                        //
                        // Can get the answer with one operation,
                        // thus one roundoff.
                        //
                        fValue *= SINGLE_SMALL_10_POW[exp];
                        return (isNegative) ? -fValue : fValue;
                    }
                    int slop = SINGLE_MAX_DECIMAL_DIGITS - kDigits;
                    if(exp <= SINGLE_MAX_SMALL_TEN + slop) {
                        //
                        // We can multiply fValue by 10^(slop)
                        // and it is still "small" and exact.
                        // Then we can multiply by 10^(exp-slop)
                        // with one rounding.
                        //
                        fValue *= SINGLE_SMALL_10_POW[slop];
                        fValue *= SINGLE_SMALL_10_POW[exp - slop];
                        return (isNegative) ? -fValue : fValue;
                    }
                    //
                    // Else we have a hard case with a positive exp.
                    //
                }
                else {
                    if(exp >= -SINGLE_MAX_SMALL_TEN) {
                        //
                        // Can get the answer in one division.
                        //
                        fValue /= SINGLE_SMALL_10_POW[-exp];
                        return (isNegative) ? -fValue : fValue;
                    }
                    //
                    // Else we have a hard case with a negative exp.
                    //
                }
            }
            else if((decExponent >= nDigits) && (nDigits + decExponent <= MAX_DECIMAL_DIGITS)) {
                //
                // In double-precision, this is an exact floating integer.
                // So we can compute to double, then shorten to float
                // with one round, and get the right answer.
                //
                // First, finish accumulating digits.
                // Then convert that integer to a double, multiply
                // by the appropriate power of ten, and convert to float.
                //
                jlong lValue = static_cast<jlong>(iValue);
                for(jint i = kDigits; i < nDigits; i++) {
                    lValue = lValue * 10L + static_cast<jlong>(static_cast<jint>(digits[i]) - static_cast<jint>(u'0'));
                }
                jdouble dValue = static_cast<jdouble>(lValue);
                exp = decExponent - nDigits;
                dValue *= SMALL_10_POW[exp];
                fValue = static_cast<jfloat>(dValue);
                return (isNegative) ? -fValue : fValue;

            }
            //
            // Harder cases:
            // The sum of digits plus exponent is greater than
            // what we think we can do with one error.
            //
            // Start by approximating the right answer by,
            // naively, scaling by powers of 10.
            // Scaling uses doubles to avoid overflow/underflow.
            //
            jdouble dValue = fValue;
            if(exp > 0) {
                if(decExponent > SINGLE_MAX_DECIMAL_EXPONENT + 1) {
                    //
                    // Lets face it. This is going to be
                    // Infinity. Cut to the chase.
                    //
                    return (isNegative) ? Float::NEGATIVE_INFINITY : Float::POSITIVE_INFINITY;
                }
                if((exp & 15) != 0) {
                    dValue *= SMALL_10_POW[exp & 15];
                }
                if((exp >>= 4) != 0) {
                    int j;
                    for(j = 0; exp > 0; j++, exp >>= 1) {
                        if((exp & 1) != 0) {
                            dValue *= BIG_10_POW[j];
                        }
                    }
                }
            }
            else if(exp < 0) {
                exp = -exp;
                if(decExponent < SINGLE_MIN_DECIMAL_EXPONENT - 1) {
                    //
                    // Lets face it. This is going to be
                    // zero. Cut to the chase.
                    //
                    return (isNegative) ? -0.0f : 0.0f;
                }
                if((exp & 15) != 0) {
                    dValue /= SMALL_10_POW[exp & 15];
                }
                if((exp >>= 4) != 0) {
                    int j;
                    for(j = 0; exp > 0; j++, exp >>= 1) {
                        if((exp & 1) != 0) {
                            dValue *= TINY_10_POW[j];
                        }
                    }
                }
            }
            fValue = Math::max(Float::MIN_VALUE, Math::min(Float::MAX_VALUE, static_cast<jfloat>(dValue)));

            //
            // fValue is now approximately the result.
            // The hard part is adjusting it, by comparison
            // with FDBigInteger arithmetic.
            // Formulate the EXACT big-number result as
            // bigD0 * 10^exp
            //
            if(nDigits > SINGLE_MAX_NDIGITS) {
                nDigits = SINGLE_MAX_NDIGITS + 1;
                digits[SINGLE_MAX_NDIGITS] = '1';
            }
            JFDBigInteger bigD0 = jnew<FDBigInteger>(iValue, digits, kDigits, nDigits);
            exp = decExponent - nDigits;

            jint ieeeBits = Float::floatToRawIntBits(fValue); // IEEE-754 bits of float candidate
            const jint B5 = Math::max(0, -exp); // powers of 5 in bigB, value is not modified inside correctionLoop
            const jint D5 = Math::max(0, exp); // powers of 5 in bigD, value is not modified inside correctionLoop
            bigD0 = bigD0->multByPow52(D5, 0);
            bigD0->makeImmutable();   // prevent bigD0 modification inside correctionLoop
            JFDBigInteger bigD = nullptr;
            jint prevD2 = 0;

            while(true) {
                // here ieeeBits can't be NaN, Infinity or zero
                jint binexp = static_cast<juint>(ieeeBits) >> SINGLE_EXP_SHIFT;
                jint bigBbits = ieeeBits & FloatConsts::SIGNIF_BIT_MASK;
                if(binexp > 0) {
                    bigBbits |= SINGLE_FRACT_HOB;
                }
                else {   // Normalize denormalized numbers.
                    assert(bigBbits != 0); // floatToBigInt(0.0)
                    jint leadingZeros = Integer::numberOfLeadingZeros(bigBbits);
                    jint shift = leadingZeros - (31 - SINGLE_EXP_SHIFT);
                    bigBbits <<= shift;
                    binexp = 1 - shift;
                }
                binexp -= FloatConsts::EXP_BIAS;
                jint lowOrderZeros = Integer::numberOfTrailingZeros(bigBbits);
                bigBbits = static_cast<juint>(bigBbits) >> lowOrderZeros;
                const jint bigIntExp = binexp - SINGLE_EXP_SHIFT + lowOrderZeros;
                const jint bigIntNBits = SINGLE_EXP_SHIFT + 1 - lowOrderZeros;

                //
                // Scale bigD, bigB appropriately for
                // big-integer operations.
                // Naively, we multiply by powers of ten
                // and powers of two. What we actually do
                // is keep track of the powers of 5 and
                // powers of 2 we would use, then factor out
                // common divisors before doing the work.
                //
                jint B2 = B5; // powers of 2 in bigB
                jint D2 = D5; // powers of 2 in bigD
                jint Ulp2;   // powers of 2 in halfUlp.
                if(bigIntExp >= 0) {
                    B2 += bigIntExp;
                }
                else {
                    D2 -= bigIntExp;
                }
                Ulp2 = B2;
                // shift bigB and bigD left by a number s. t.
                // halfUlp is still an integer.
                jint hulpbias;
                if(binexp <= -FloatConsts::EXP_BIAS) {
                    // This is going to be a denormalized number
                    // (if not actually zero).
                    // half an ULP is at 2^-(FloatConsts.EXP_BIAS+SINGLE_EXP_SHIFT+1)
                    hulpbias = binexp + lowOrderZeros + FloatConsts::EXP_BIAS;
                }
                else {
                    hulpbias = 1 + lowOrderZeros;
                }
                B2 += hulpbias;
                D2 += hulpbias;
                // if there are common factors of 2, we might just as well
                // factor them out, as they add nothing useful.
                jint common2 = Math::min(B2, Math::min(D2, Ulp2));
                B2 -= common2;
                D2 -= common2;
                Ulp2 -= common2;
                // do multiplications by powers of 5 and 2
                JFDBigInteger bigB = FDBigInteger::valueOfMulPow52(bigBbits, B5, B2);
                if(bigD == nullptr || prevD2 != D2) {
                    bigD = bigD0->leftShift(D2);
                    prevD2 = D2;
                }
                //
                // to recap:
                // bigB is the scaled-big-int version of our floating-point
                // candidate.
                // bigD is the scaled-big-int version of the exact value
                // as we understand it.
                // halfUlp is 1/2 an ulp of bigB, except for special cases
                // of exact powers of 2
                //
                // the plan is to compare bigB with bigD, and if the difference
                // is less than halfUlp, then we're satisfied. Otherwise,
                // use the ratio of difference to halfUlp to calculate a fudge
                // factor to add to the floating value, then go 'round again.
                //
                JFDBigInteger diff;
                jint cmpResult;
                jboolean overvalue;
                if((cmpResult = bigB->cmp(bigD)) > 0) {
                    overvalue = true; // our candidate is too big.
                    diff = bigB->leftInplaceSub(bigD); // bigB is not user further - reuse
                    if((bigIntNBits == 1) && (bigIntExp > -FloatConsts::EXP_BIAS + 1)) {
                        // candidate is a normalized exact power of 2 and
                        // is too big (larger than Float.MIN_NORMAL). We will be subtracting.
                        // For our purposes, ulp is the ulp of the
                        // next smaller range.
                        Ulp2 -= 1;
                        if(Ulp2 < 0) {
                            // rats. Cannot de-scale ulp this far.
                            // must scale diff in other direction.
                            Ulp2 = 0;
                            diff = diff->leftShift(1);
                        }
                    }
                }
                else if(cmpResult < 0) {
                    overvalue = false; // our candidate is too small.
                    diff = bigD->rightInplaceSub(bigB); // bigB is not user further - reuse
                }
                else {
                    // the candidate is exactly right!
                    // this happens with surprising frequency
                    goto correctionLoop;
                }
                cmpResult = diff->cmpPow52(B5, Ulp2);
                if((cmpResult) < 0) {
                    // difference is small.
                    // this is close enough
                    goto correctionLoop;
                }
                else if(cmpResult == 0) {
                    // difference is exactly half an ULP
                    // round to some other value maybe, then finish
                    if((ieeeBits & 1) != 0) {  // half ties to even
                        ieeeBits += overvalue ? -1 : 1; // nextDown or nextUp
                    }
                    goto correctionLoop;
                }
                else {
                    // difference is non-trivial.
                    // could scale addend by ratio of difference to
                    // halfUlp here, if we bothered to compute that difference.
                    // Most of the time ( I hope ) it is about 1 anyway.
                    ieeeBits += overvalue ? -1 : 1; // nextDown or nextUp
                    if(ieeeBits == 0 || ieeeBits == FloatConsts::EXP_BIT_MASK) {  // 0.0 or Float.POSITIVE_INFINITY
                        goto correctionLoop; // oops. Fell off end of range.
                    }
                    continue; // try again.
                }

            }
        correctionLoop:
        
            if(isNegative) {
                ieeeBits |= FloatConsts::SIGN_BIT_MASK;
            }
            return Float::intBitsToFloat(ieeeBits);
        }

        const jarray<jdouble> FloatingDecimal::ASCIIToBinaryBuffer::SMALL_10_POW = {
            1.0e0,
            1.0e1, 1.0e2, 1.0e3, 1.0e4, 1.0e5,
            1.0e6, 1.0e7, 1.0e8, 1.0e9, 1.0e10,
            1.0e11, 1.0e12, 1.0e13, 1.0e14, 1.0e15,
            1.0e16, 1.0e17, 1.0e18, 1.0e19, 1.0e20,
            1.0e21, 1.0e22
        };

        const jarray<jfloat> FloatingDecimal::ASCIIToBinaryBuffer::SINGLE_SMALL_10_POW = {
            1.0e0f,
            1.0e1f, 1.0e2f, 1.0e3f, 1.0e4f, 1.0e5f,
            1.0e6f, 1.0e7f, 1.0e8f, 1.0e9f, 1.0e10f
        };

        const jarray<jdouble> FloatingDecimal::ASCIIToBinaryBuffer::BIG_10_POW = {
            1e16, 1e32, 1e64, 1e128, 1e256
        };

        const jarray<jdouble> FloatingDecimal::ASCIIToBinaryBuffer::TINY_10_POW = {
            1e-16, 1e-32, 1e-64, 1e-128, 1e-256
        };

        const jint FloatingDecimal::ASCIIToBinaryBuffer::MAX_SMALL_TEN = SMALL_10_POW.length() - 1;
        const jint FloatingDecimal::ASCIIToBinaryBuffer::SINGLE_MAX_SMALL_TEN = SINGLE_SMALL_10_POW.length() - 1;

        
        const JPattern FloatingDecimal::HexFloatPattern::VALUE = Pattern::compile(
                    //1           234                   56                7                   8      9
                    jstr(u"([-+])?0[xX](((\\p{XDigit}+)\\.?)|((\\p{XDigit}*)\\.(\\p{XDigit}+)))[pP]([-+])?(\\p{Digit}+)[fFdD]?")
                );

        FloatingDecimal::JBinaryToASCIIConverter FloatingDecimal::getBinaryToASCIIConverter(jdouble d)
        {
            return getBinaryToASCIIConverter(d, true);
        }

        FloatingDecimal::JBinaryToASCIIConverter FloatingDecimal::getBinaryToASCIIConverter(jdouble d, jboolean isCompatibleFormat)
        {
            jlong dBits = Double::doubleToRawLongBits(d);
            jboolean isNegative = (dBits & DoubleConsts::SIGN_BIT_MASK) != 0; // discover sign
            jlong fractBits = dBits & DoubleConsts::SIGNIF_BIT_MASK;
            jint  binExp = (jint)((dBits & DoubleConsts::EXP_BIT_MASK) >> EXP_SHIFT);
            // Discover obvious special cases of NaN and Infinity.
            if(binExp == (jint)(DoubleConsts::EXP_BIT_MASK >> EXP_SHIFT)) {
                if(fractBits == 0LL) {
                    return isNegative ? B2AC_NEGATIVE_INFINITY : B2AC_POSITIVE_INFINITY;
                }
                else {
                    return B2AC_NOT_A_NUMBER;
                }
            }
            // Finish unpacking
            // Normalize denormalized numbers.
            // Insert assumed high-order bit for normalized numbers.
            // Subtract exponent bias.
            int  nSignificantBits;
            if(binExp == 0) {
                if(fractBits == 0LL) {
                    // not a denorm, just a 0!
                    return isNegative ? B2AC_NEGATIVE_ZERO : B2AC_POSITIVE_ZERO;
                }
                int leadingZeros = Long::numberOfLeadingZeros(fractBits);
                int shift = leadingZeros - (63 - EXP_SHIFT);
                fractBits <<= shift;
                binExp = 1 - shift;
                nSignificantBits =  64 - leadingZeros; // recall binExp is  - shift count.
            }
            else {
                fractBits |= FRACT_HOB;
                nSignificantBits = EXP_SHIFT + 1;
            }
            binExp -= DoubleConsts::EXP_BIAS;
            JBinaryToASCIIBuffer buf = getBinaryToASCIIBuffer();
            buf->setSign(isNegative);
            // call the routine that actually does all the hard work.
            buf->dtoa(binExp, fractBits, nSignificantBits, isCompatibleFormat);
            return buf;
        }

        FloatingDecimal::JBinaryToASCIIConverter FloatingDecimal::getBinaryToASCIIConverter(jfloat f)
        {
            jint fBits = Float::floatToRawIntBits(f);
            jboolean isNegative = (fBits & FloatConsts::SIGN_BIT_MASK) != 0;
            jint fractBits = fBits & FloatConsts::SIGNIF_BIT_MASK;
            jint binExp = (fBits & FloatConsts::EXP_BIT_MASK) >> SINGLE_EXP_SHIFT;
            // Discover obvious special cases of NaN and Infinity.
            if(binExp == (FloatConsts::EXP_BIT_MASK >> SINGLE_EXP_SHIFT)) {
                if(fractBits == 0LL) {
                    return isNegative ? B2AC_NEGATIVE_INFINITY : B2AC_POSITIVE_INFINITY;
                }
                else {
                    return B2AC_NOT_A_NUMBER;
                }
            }
            // Finish unpacking
            // Normalize denormalized numbers.
            // Insert assumed high-order bit for normalized numbers.
            // Subtract exponent bias.
            jint  nSignificantBits;
            if(binExp == 0) {
                if(fractBits == 0) {
                    // not a denorm, just a 0!
                    return isNegative ? B2AC_NEGATIVE_ZERO : B2AC_POSITIVE_ZERO;
                }
                jint leadingZeros = Integer::numberOfLeadingZeros(fractBits);
                jint shift = leadingZeros - (31 - SINGLE_EXP_SHIFT);
                fractBits <<= shift;
                binExp = 1 - shift;
                nSignificantBits =  32 - leadingZeros; // recall binExp is  - shift count.
            }
            else {
                fractBits |= SINGLE_FRACT_HOB;
                nSignificantBits = SINGLE_EXP_SHIFT + 1;
            }
            binExp -= FloatConsts::EXP_BIAS;
            JBinaryToASCIIBuffer buf = getBinaryToASCIIBuffer();
            buf->setSign(isNegative);
            // call the routine that actually does all the hard work.
            buf->dtoa(binExp, (static_cast<jlong>(fractBits)) << (EXP_SHIFT - SINGLE_EXP_SHIFT), nSignificantBits, true);
            return buf;
        }

        FloatingDecimal::JBinaryToASCIIBuffer FloatingDecimal::getBinaryToASCIIBuffer()
        {
            // TODO: return threadLocalBinaryToASCIIBuffer.get();
            return jnew<FloatingDecimal::BinaryToASCIIBuffer>();
        }

        FloatingDecimal::JASCIIToBinaryConverter FloatingDecimal::readJavaFormatString(JString in)
        {
            jboolean isNegative = false;
            jboolean signSeen   = false;
            jint     decExp;
            jchar    c;

            try {
                in = in->trim(); // don't fool around with white space.
                // throws NullPointerException if null
                jint len = in->length();
                if(len == 0) {
                    throw NumberFormatException(jstr(u"empty String"));
                }
                jint i = 0;
                switch(in->charAt(i)) {
                    case u'-':
                        isNegative = true;
                        //FALLTHROUGH
                    case u'+':
                        i++;
                        signSeen = true;
                }
                c = in->charAt(i);
                if(c == u'N') { // Check for NaN
                    if((len - i) == NAN_LENGTH && in->indexOf(NAN_REP, i) == i) {
                        return A2BC_NOT_A_NUMBER;
                    }
                    // something went wrong, throw exception
                    goto parseNumber;
                }
                else if(c == u'I') {   // Check for Infinity strings
                    if((len - i) == INFINITY_LENGTH && in->indexOf(INFINITY_REP, i) == i) {
                        return isNegative ? A2BC_NEGATIVE_INFINITY : A2BC_POSITIVE_INFINITY;
                    }
                    // something went wrong, throw exception
                    goto parseNumber;
                }
                else if(c == u'0')  {    // check for hexadecimal floating-point number
                    if(len > i + 1) {
                        jchar ch = in->charAt(i + 1);
                        if(ch == u'x' || ch == u'X') {   // possible hex string
                            return parseHexString(in);
                        }
                    }
                }  // look for and process decimal floating-point string

                jarray<jchar> digits(len);
                jint    nDigits = 0;
                jboolean decSeen = false;
                jint decPt = 0;
                jint nLeadZero = 0;
                jint nTrailZero = 0;


                while(i < len) {
                    c = in->charAt(i);
                    if(c == u'0') {
                        nLeadZero++;
                    }
                    else if(c == u'.') {
                        if(decSeen) {
                            // already saw one ., this is the 2nd.
                            throw NumberFormatException(jstr(u"multiple points"));
                        }
                        decPt = i;
                        if(signSeen) {
                            decPt -= 1;
                        }
                        decSeen = true;
                    }
                    else {
                        goto skipLeadingZerosLoop;
                    }
                    i++;
                }
            skipLeadingZerosLoop:

                while(i < len) {
                    c = in->charAt(i);
                    if(c >= u'1' && c <= u'9') {
                        digits[nDigits++] = c;
                        nTrailZero = 0;
                    }
                    else if(c == u'0') {
                        digits[nDigits++] = c;
                        nTrailZero++;
                    }
                    else if(c == u'.') {
                        if(decSeen) {
                            // already saw one ., this is the 2nd.
                            throw NumberFormatException(jstr(u"multiple points"));
                        }
                        decPt = i;
                        if(signSeen) {
                            decPt -= 1;
                        }
                        decSeen = true;
                    }
                    else {
                        goto digitLoop;
                    }
                    i++;
                }

            digitLoop:
                nDigits -= nTrailZero;
                //
                // At this point, we've scanned all the digits and decimal
                // point we're going to see. Trim off leading and trailing
                // zeros, which will just confuse us later, and adjust
                // our initial decimal exponent accordingly.
                // To review:
                // we have seen i total characters.
                // nLeadZero of them were zeros before any other digits.
                // nTrailZero of them were zeros after any other digits.
                // if ( decSeen ), then a . was seen after decPt characters
                // ( including leading zeros which have been discarded )
                // nDigits characters were neither lead nor trailing
                // zeros, nor point
                //
                //
                // special hack: if we saw no non-zero digits, then the
                // answer is zero!
                // Unfortunately, we feel honor-bound to keep parsing!
                //
                jboolean isZero = (nDigits == 0);
                if(isZero &&  nLeadZero == 0) {
                    // we saw NO DIGITS AT ALL,
                    // not even a crummy 0!
                    // this is not allowed.
                    goto parseNumber; // go throw exception
                }
                //
                // Our initial exponent is decPt, adjusted by the number of
                // discarded zeros. Or, if there was no decPt,
                // then its just nDigits adjusted by discarded trailing zeros.
                //
                if(decSeen) {
                    decExp = decPt - nLeadZero;
                }
                else {
                    decExp = nDigits + nTrailZero;
                }

                //
                // Look for 'e' or 'E' and an optionally signed integer.
                //
                if((i < len) && (((c = in->charAt(i)) == u'e') || (c == u'E'))) {
                    jint expSign = 1;
                    jint expVal  = 0;
                    jint reallyBig = Integer::MAX_VALUE / 10;
                    jboolean expOverflow = false;
                    switch(in->charAt(++i)) {
                        case u'-':
                            expSign = -1;
                            //FALLTHROUGH
                        case u'+':
                            i++;
                    }
                    jint expAt = i;
                expLoop:
                    while(i < len) {
                        if(expVal >= reallyBig) {
                            // the next character will cause integer
                            // overflow.
                            expOverflow = true;
                        }
                        c = in->charAt(i++);
                        if(c >= u'0' && c <= u'9') {
                            expVal = expVal * 10 + (static_cast<jint>(c) - static_cast<jint>(u'0'));
                        }
                        else {
                            i--;           // back up.
                            goto expLoop; // stop parsing exponent.
                        }
                    }
                    jint expLimit = BIG_DECIMAL_EXPONENT + nDigits + nTrailZero;
                    if(expOverflow || (expVal > expLimit)) {
                        //
                        // The intent here is to end up with
                        // infinity or zero, as appropriate.
                        // The reason for yielding such a small decExponent,
                        // rather than something intuitive such as
                        // expSign*Integer.MAX_VALUE, is that this value
                        // is subject to further manipulation in
                        // doubleValue() and floatValue(), and I don't want
                        // it to be able to cause overflow there!
                        // (The only way we can get into trouble here is for
                        // really outrageous nDigits+nTrailZero, such as 2 billion. )
                        //
                        decExp = expSign * expLimit;
                    }
                    else {
                        // this should not overflow, since we tested
                        // for expVal > (MAX+N), where N >= abs(decExp)
                        decExp = decExp + expSign * expVal;
                    }

                    // if we saw something not a digit ( or end of string )
                    // after the [Ee][+-], without seeing any digits at all
                    // this is certainly an error. If we saw some digits,
                    // but then some trailing garbage, that might be ok.
                    // so we just fall through in that case.
                    // HUMBUG
                    if(i == expAt) {
                        goto parseNumber; // certainly bad
                    }
                }
                //
                // We parsed everything we could.
                // If there are leftovers, then this is not good input!
                //
                if(i < len &&
                   ((i != len - 1) ||
                    (in->charAt(i) != u'f' &&
                     in->charAt(i) != u'F' &&
                     in->charAt(i) != u'd' &&
                     in->charAt(i) != u'D'))) {
                    goto parseNumber; // go throw exception
                }
                if(isZero) {
                    return isNegative ? A2BC_NEGATIVE_ZERO : A2BC_POSITIVE_ZERO;
                }
                return jnew<ASCIIToBinaryBuffer>(isNegative, decExp, digits, nDigits);
            }
            catch(StringIndexOutOfBoundsException& e) { }
        parseNumber:
            throw NumberFormatException(jstr(u"For input string: \"") + in + jstr(u"\""));
        }

        FloatingDecimal::JASCIIToBinaryConverter FloatingDecimal::parseHexString(JString s)
        {
            // Verify string is a member of the hexadecimal floating-point
            // string language.
            JPattern p = HexFloatPattern::VALUE;
            JMatcher m = p->matcher(s);
            jboolean validInput = m->matches();
            if(!validInput) {
                // Input does not match pattern
                throw NumberFormatException(jstr(u"For input string: \"") + s + jstr(u"\""));
            }
            else {   // validInput
                //
                // We must isolate the sign, significand, and exponent
                // fields.  The sign value is straightforward.  Since
                // floating-point numbers are stored with a normalized
                // representation, the significand and exponent are
                // interrelated.
                //
                // After extracting the sign, we normalized the
                // significand as a hexadecimal value, calculating an
                // exponent adjust for any shifts made during
                // normalization.  If the significand is zero, the
                // exponent doesn't need to be examined since the output
                // will be zero.
                //
                // Next the exponent in the input string is extracted.
                // Afterwards, the significand is normalized as a *binary*
                // value and the input value's normalized exponent can be
                // computed.  The significand bits are copied into a
                // double significand; if the string has more logical bits
                // than can fit in a double, the extra bits affect the
                // round and sticky bits which are used to round the final
                // value.
                //
                //  Extract significand sign
                JString group1 = m->group(1);
                jboolean isNegative = ((group1 != nullptr) && group1->equals(jstr(u"-")));

                //  Extract Significand magnitude
                //
                // Based on the form of the significand, calculate how the
                // binary exponent needs to be adjusted to create a
                // normalized//hexadecimal* floating-point number; that
                // is, a number where there is one nonzero hex digit to
                // the left of the (hexa)decimal point.  Since we are
                // adjusting a binary, not hexadecimal exponent, the
                // exponent is adjusted by a multiple of 4.
                //
                // There are a number of significand scenarios to consider;
                // letters are used in indicate nonzero digits:
                //
                // 1. 000xxxx       =>      x.xxx   normalized
                //    increase exponent by (number of x's - 1)*4
                //
                // 2. 000xxx.yyyy =>        x.xxyyyy        normalized
                //    increase exponent by (number of x's - 1)*4
                //
                // 3. .000yyy  =>   y.yy    normalized
                //    decrease exponent by (number of zeros + 1)*4
                //
                // 4. 000.00000yyy => y.yy normalized
                //    decrease exponent by (number of zeros to right of point + 1)*4
                //
                // If the significand is exactly zero, return a properly
                // signed zero.
                //

                JString significandString = nullptr;
                jint signifLength = 0;
                jint exponentAdjust = 0;
                {
                    jint leftDigits = 0; // number of meaningful digits to
                    // left of "decimal" point
                    // (leading zeros stripped)
                    jint rightDigits = 0; // number of digits to right of
                    // "decimal" point; leading zeros
                    // must always be accounted for
                    //
                    // The significand is made up of either
                    //
                    // 1. group 4 entirely (integer portion only)
                    //
                    // OR
                    //
                    // 2. the fractional portion from group 7 plus any
                    // (optional) integer portions from group 6.
                    //
                    JString group4;
                    if((group4 = m->group(4)) != nullptr) {   // Integer-only significand
                        // Leading zeros never matter on the integer portion
                        significandString = stripLeadingZeros(group4);
                        leftDigits = significandString->length();
                    }
                    else {
                        // Group 6 is the optional integer; leading zeros
                        // never matter on the integer portion
                        JString group6 = stripLeadingZeros(m->group(6));
                        leftDigits = group6->length();

                        // fraction
                        JString group7 = m->group(7);
                        rightDigits = group7->length();

                        // Turn "integer.fraction" into "integer"+"fraction"
                        significandString =
                            ((group6 == nullptr) ? jstr(u"") : group6) + // is the null
                            // check necessary?
                            group7;
                    }

                    significandString = stripLeadingZeros(significandString);
                    signifLength = significandString->length();

                    //
                    // Adjust exponent as described above
                    //
                    if(leftDigits >= 1) {   // Cases 1 and 2
                        exponentAdjust = 4 * (leftDigits - 1);
                    }
                    else {                  // Cases 3 and 4
                        exponentAdjust = -4 * (rightDigits - signifLength + 1);
                    }

                    // If the significand is zero, the exponent doesn't
                    // matter; return a properly signed zero.

                    if(signifLength == 0) {  // Only zeros in input
                        return isNegative ? A2BC_NEGATIVE_ZERO : A2BC_POSITIVE_ZERO;
                    }
                }

                //  Extract Exponent
                //
                // Use an int to read in the exponent value; this should
                // provide more than sufficient range for non-contrived
                // inputs.  If reading the exponent in as an int does
                // overflow, examine the sign of the exponent and
                // significand to determine what to do.
                //
                JString group8 = m->group(8);
                jboolean positiveExponent = (group8 == nullptr) || group8->equals(jstr(u"+"));
                jlong unsignedRawExponent;
                try {
                    unsignedRawExponent = Integer::parseInt(m->group(9));
                }
                catch(NumberFormatException& e) {
                    // At this point, we know the exponent is
                    // syntactically well-formed as a sequence of
                    // digits.  Therefore, if an NumberFormatException
                    // is thrown, it must be due to overflowing int's
                    // range.  Also, at this point, we have already
                    // checked for a zero significand.  Thus the signs
                    // of the exponent and significand determine the
                    // final result:
                    //
                    //                      significand
                    //                      +               -
                    // exponent     +       +infinity       -infinity
                    //              -       +0.0            -0.0
                    return isNegative ?
                           (positiveExponent ? A2BC_NEGATIVE_INFINITY : A2BC_NEGATIVE_ZERO)
                               : (positiveExponent ? A2BC_POSITIVE_INFINITY : A2BC_POSITIVE_ZERO);

                }

                jlong rawExponent =
                    (positiveExponent ? 1L : -1L) * // exponent sign
                    unsignedRawExponent;            // exponent magnitude

                // Calculate partially adjusted exponent
                jlong exponent = rawExponent + exponentAdjust;

                // Starting copying non-zero bits into proper position in
                // a long; copy explicit bit too; this will be masked
                // later for normal values.

                jboolean round = false;
                jboolean sticky = false;
                jint nextShift = 0;
                jlong significand = 0L;
                // First iteration is different, since we only copy
                // from the leading significand bit; one more exponent
                // adjust will be needed...

                // IMPORTANT: make leadingDigit a long to avoid
                // surprising shift semantics!
                jlong leadingDigit = getHexDigit(significandString, 0);

                //
                // Left shift the leading digit (53 - (bit position of
                // leading 1 in digit)); this sets the top bit of the
                // significand to 1.  The nextShift value is adjusted
                // to take into account the number of bit positions of
                // the leadingDigit actually used.  Finally, the
                // exponent is adjusted to normalize the significand
                // as a binary value, not just a hex value.
                //
                if(leadingDigit == 1) {
                    significand |= leadingDigit << 52;
                    nextShift = 52 - 4;
                    // exponent += 0
                }
                else if(leadingDigit <= 3) {    // [2, 3]
                    significand |= leadingDigit << 51;
                    nextShift = 52 - 5;
                    exponent += 1;
                }
                else if(leadingDigit <= 7) {    // [4, 7]
                    significand |= leadingDigit << 50;
                    nextShift = 52 - 6;
                    exponent += 2;
                }
                else if(leadingDigit <= 15) {    // [8, f]
                    significand |= leadingDigit << 49;
                    nextShift = 52 - 7;
                    exponent += 3;
                }
                else {
                    throw AssertionError(u"Result from digit conversion too large!");
                }
                // The preceding if-else could be replaced by a single
                // code block based on the high-order bit set in
                // leadingDigit.  Given leadingOnePosition,

                // significand |= leadingDigit << (SIGNIFICAND_WIDTH - leadingOnePosition);
                // nextShift = 52 - (3 + leadingOnePosition);
                // exponent += (leadingOnePosition-1);

                //
                // Now the exponent variable is equal to the normalized
                // binary exponent.  Code below will make representation
                // adjustments if the exponent is incremented after
                // rounding (includes overflows to infinity) or if the
                // result is subnormal.
                //

                // Copy digit into significand until the significand can't
                // hold another full hex digit or there are no more input
                // hex digits.
                jint i = 0;
                for(i = 1;
                    i < signifLength && nextShift >= 0;
                    i++) {
                    jlong currentDigit = getHexDigit(significandString, i);
                    significand |= (currentDigit << nextShift);
                    nextShift -= 4;
                }

                // After the above loop, the bulk of the string is copied.
                // Now, we must copy any partial hex digits into the
                // significand AND compute the round bit and start computing
                // sticky bit.

                if(i < signifLength) {  // at least one hex input digit exists
                    jlong currentDigit = getHexDigit(significandString, i);

                    // from nextShift, figure out how many bits need
                    // to be copied, if any
                    switch(nextShift) {  // must be negative
                        case -1:
                            // three bits need to be copied in; can
                            // set round bit
                            significand |= ((currentDigit & 0xEL) >> 1);
                            round = (currentDigit & 0x1L) != 0L;
                            break;

                        case -2:
                            // two bits need to be copied in; can
                            // set round and start sticky
                            significand |= ((currentDigit & 0xCL) >> 2);
                            round = (currentDigit & 0x2L) != 0L;
                            sticky = (currentDigit & 0x1L) != 0;
                            break;

                        case -3:
                            // one bit needs to be copied in
                            significand |= ((currentDigit & 0x8L) >> 3);
                            // Now set round and start sticky, if possible
                            round = (currentDigit & 0x4L) != 0L;
                            sticky = (currentDigit & 0x3L) != 0;
                            break;

                        case -4:
                            // all bits copied into significand; set
                            // round and start sticky
                            round = ((currentDigit & 0x8L) != 0);  // is top bit set?
                            // nonzeros in three low order bits?
                            sticky = (currentDigit & 0x7L) != 0;
                            break;

                        default:
                            throw AssertionError(u"Unexpected shift distance remainder.");
                            // break;
                    }

                    // Round is set; sticky might be set.

                    // For the sticky bit, it suffices to check the
                    // current digit and test for any nonzero digits in
                    // the remaining unprocessed input.
                    i++;
                    while(i < signifLength && !sticky) {
                        currentDigit = getHexDigit(significandString, i);
                        sticky = sticky || (currentDigit != 0);
                        i++;
                    }

                }
                // else all of string was seen, round and sticky are
                // correct as false.

                // Float calculations
                jint floatBits = isNegative ? FloatConsts::SIGN_BIT_MASK : 0;
                if(exponent >= FloatConsts::MIN_EXPONENT) {
                    if(exponent > FloatConsts::MAX_EXPONENT) {
                        // Float.POSITIVE_INFINITY
                        floatBits |= FloatConsts::EXP_BIT_MASK;
                    }
                    else {
                        jint threshShift = DoubleConsts::SIGNIFICAND_WIDTH - FloatConsts::SIGNIFICAND_WIDTH - 1;
                        jboolean floatSticky = (significand & ((1L << threshShift) - 1)) != 0 || round || sticky;
                        jint iValue = static_cast<jint>(static_cast<julong>(significand) >> threshShift);
                        if((iValue & 3) != 1 || floatSticky) {
                            iValue++;
                        }
                        floatBits |= ((((static_cast<jint>(exponent)) + (FloatConsts::EXP_BIAS - 1))) << SINGLE_EXP_SHIFT) + (iValue >> 1);
                    }
                }
                else {
                    if(exponent < FloatConsts::MIN_SUB_EXPONENT - 1) {
                        // 0
                    }
                    else {
                        // exponent == -127 ==> threshShift = 53 - 2 + (-149) - (-127) = 53 - 24
                        jint threshShift = static_cast<jint>((DoubleConsts::SIGNIFICAND_WIDTH - 2 + FloatConsts::MIN_SUB_EXPONENT) - exponent);
                        assert(threshShift >= DoubleConsts::SIGNIFICAND_WIDTH - FloatConsts::SIGNIFICAND_WIDTH);
                        assert(threshShift < DoubleConsts::SIGNIFICAND_WIDTH);
                        jboolean floatSticky = (significand & ((1L << threshShift) - 1)) != 0 || round || sticky;
                        jint iValue = static_cast<jint>(static_cast<julong>(significand) >> threshShift);
                        if((iValue & 3) != 1 || floatSticky) {
                            iValue++;
                        }
                        floatBits |= iValue >> 1;
                    }
                }
                jfloat fValue = Float::intBitsToFloat(floatBits);

                // Check for overflow and update exponent accordingly.
                if(exponent > DoubleConsts::MAX_EXPONENT) {          // Infinite result
                    // overflow to properly signed infinity
                    return isNegative ? A2BC_NEGATIVE_INFINITY : A2BC_POSITIVE_INFINITY;
                }
                else {    // Finite return value
                    if(exponent <= DoubleConsts::MAX_EXPONENT &&  // (Usually) normal result
                       exponent >= DoubleConsts::MIN_EXPONENT) {

                        // The result returned in this block cannot be a
                        // zero or subnormal; however after the
                        // significand is adjusted from rounding, we could
                        // still overflow in infinity.

                        // AND exponent bits into significand; if the
                        // significand is incremented and overflows from
                        // rounding, this combination will update the
                        // exponent correctly, even in the case of
                        // Double.MAX_VALUE overflowing to infinity.

                        significand = (((exponent +
                                         static_cast<jlong>(DoubleConsts::EXP_BIAS)) <<
                                        (DoubleConsts::SIGNIFICAND_WIDTH - 1))
                                       & DoubleConsts::EXP_BIT_MASK) |
                                      (DoubleConsts::SIGNIF_BIT_MASK & significand);

                    }
                    else {    // Subnormal or zero
                        // (exponent < DoubleConsts.MIN_EXPONENT)

                        if(exponent < (DoubleConsts::MIN_SUB_EXPONENT - 1)) {
                            // No way to round back to nonzero value
                            // regardless of significand if the exponent is
                            // less than -1075.
                            return isNegative ? A2BC_NEGATIVE_ZERO : A2BC_POSITIVE_ZERO;
                        }
                        else {   //  -1075 <= exponent <= MIN_EXPONENT -1 = -1023
                            //
                            // Find bit position to round to; recompute
                            // round and sticky bits, and shift
                            // significand right appropriately.
                            //

                            sticky = sticky || round;
                            round = false;

                            // Number of bits of significand to preserve is
                            // exponent - abs_min_exp +1
                            // check:
                            // -1075 +1074 + 1 = 0
                            // -1023 +1074 + 1 = 52

                            jint bitsDiscarded = 53 -
                                                 ((int) exponent - DoubleConsts::MIN_SUB_EXPONENT + 1);
                            assert(bitsDiscarded >= 1 && bitsDiscarded <= 53);

                            // What to do here:
                            // First, isolate the new round bit
                            round = (significand & (1L << (bitsDiscarded - 1))) != 0L;
                            if(bitsDiscarded > 1) {
                                // create mask to update sticky bits; low
                                // order bitsDiscarded bits should be 1
                                jlong mask = ~((~0L) << (bitsDiscarded - 1));
                                sticky = sticky || ((significand & mask) != 0L);
                            }

                            // Now, discard the bits
                            significand = significand >> bitsDiscarded;

                            significand = (((static_cast<jlong>(DoubleConsts::MIN_EXPONENT - 1) +  // subnorm exp.
                                             static_cast<jlong>(DoubleConsts::EXP_BIAS)) <<
                                            (DoubleConsts::SIGNIFICAND_WIDTH - 1))
                                           & DoubleConsts::EXP_BIT_MASK) |
                                          (DoubleConsts::SIGNIF_BIT_MASK & significand);
                        }
                    }

                    // The significand variable now contains the currently
                    // appropriate exponent bits too.

                    //
                    // Determine if significand should be incremented;
                    // making this determination depends on the least
                    // significant bit and the round and sticky bits.
                    //
                    // Round to nearest even rounding table, adapted from
                    // table 4.7 in "Computer Arithmetic" by IsraelKoren.
                    // The digit to the left of the "decimal" point is the
                    // least significant bit, the digits to the right of
                    // the point are the round and sticky bits
                    //
                    // Number       Round(x)
                    // x0.00        x0.
                    // x0.01        x0.
                    // x0.10        x0.
                    // x0.11        x1. = x0. +1
                    // x1.00        x1.
                    // x1.01        x1.
                    // x1.10        x1. + 1
                    // x1.11        x1. + 1
                    //
                    jboolean leastZero = ((significand & 1L) == 0L);
                    if((leastZero && round && sticky) ||
                       ((!leastZero) && round)) {
                        significand++;
                    }

                    jdouble value = isNegative ?
                                    Double::longBitsToDouble(significand | DoubleConsts::SIGN_BIT_MASK) :
                                    Double::longBitsToDouble(significand);

                    return jnew<PreparedASCIIToBinaryBuffer>(value, fValue);
                }
            }

            return nullptr;
        }

        JString FloatingDecimal::stripLeadingZeros(JString s)
        {
            // return  s.replaceFirst("^0+", "");
            if(!s->isEmpty() && s->charAt(0) == u'0') {
                for(int i = 1; i < s->length(); i++) {
                    if(s->charAt(i) != u'0') {
                        return s->substring(i);
                    }
                }
                return jstr(u"");
            }
            return s;
        }

        jint FloatingDecimal::getHexDigit(JString s, jint position)
        {
            jint value = Character::digit(s->charAt(position), 16);
            if(value <= -1 || value >= 16) {
                throw AssertionError(jstr(u"Unexpected failure of digit conversion of ") + boxing(s->charAt(position)));
            }
            return value;
        }

        const JString FloatingDecimal::INFINITY_REP = jstr(u"Infinity");
        const jint FloatingDecimal::INFINITY_LENGTH = INFINITY_REP->length();
        const JString FloatingDecimal::NAN_REP = jstr(u"NaN");
        const jint FloatingDecimal::NAN_LENGTH = NAN_REP->length();

        const jarray<jchar> FloatingDecimal::DEFAULT_DIGITS = {u'0'};

        const FloatingDecimal::JBinaryToASCIIConverter FloatingDecimal::B2AC_POSITIVE_INFINITY = jnew<FloatingDecimal::ExceptionalBinaryToASCIIBuffer>(FloatingDecimal::INFINITY_REP, false);;
        const FloatingDecimal::JBinaryToASCIIConverter FloatingDecimal::B2AC_NEGATIVE_INFINITY = jnew<FloatingDecimal::ExceptionalBinaryToASCIIBuffer>(jstr(u"-") + FloatingDecimal::INFINITY_REP, true);
        const FloatingDecimal::JBinaryToASCIIConverter FloatingDecimal::B2AC_NOT_A_NUMBER = jnew<FloatingDecimal::ExceptionalBinaryToASCIIBuffer>(FloatingDecimal::NAN_REP, false);
        const FloatingDecimal::JBinaryToASCIIConverter FloatingDecimal::B2AC_POSITIVE_ZERO = jnew<FloatingDecimal::BinaryToASCIIBuffer>(false, FloatingDecimal::DEFAULT_DIGITS);
        const FloatingDecimal::JBinaryToASCIIConverter FloatingDecimal::B2AC_NEGATIVE_ZERO = jnew<FloatingDecimal::BinaryToASCIIBuffer>(true, FloatingDecimal::DEFAULT_DIGITS);

        const FloatingDecimal::JASCIIToBinaryConverter FloatingDecimal::A2BC_POSITIVE_INFINITY = jnew<FloatingDecimal::PreparedASCIIToBinaryBuffer>(Double::POSITIVE_INFINITY, Float::POSITIVE_INFINITY);
        const FloatingDecimal::JASCIIToBinaryConverter FloatingDecimal::A2BC_NEGATIVE_INFINITY = jnew<FloatingDecimal::PreparedASCIIToBinaryBuffer>(Double::NEGATIVE_INFINITY, Float::NEGATIVE_INFINITY);
        const FloatingDecimal::JASCIIToBinaryConverter FloatingDecimal::A2BC_NOT_A_NUMBER = jnew<FloatingDecimal::PreparedASCIIToBinaryBuffer>(Double::NaN, Float::NaN);
        const FloatingDecimal::JASCIIToBinaryConverter FloatingDecimal::A2BC_POSITIVE_ZERO = jnew<FloatingDecimal::PreparedASCIIToBinaryBuffer>(0.0, 0.0f);
        const FloatingDecimal::JASCIIToBinaryConverter FloatingDecimal::A2BC_NEGATIVE_ZERO = jnew<FloatingDecimal::PreparedASCIIToBinaryBuffer>(-0.0, -0.0f);

        const jint FloatingDecimal::EXP_SHIFT = DoubleConsts::SIGNIFICAND_WIDTH - 1;
        const jlong FloatingDecimal::EXP_ONE = static_cast<jlong>(DoubleConsts::EXP_BIAS) << EXP_SHIFT;
        const jlong FloatingDecimal::FRACT_HOB = (1LL << EXP_SHIFT);
        const jint FloatingDecimal::MAX_SMALL_BIN_EXP = 62;
        const jint FloatingDecimal::MIN_SMALL_BIN_EXP = -(63 / 3);
        const jint FloatingDecimal::MAX_DECIMAL_DIGITS = 15;
        const jint FloatingDecimal::MAX_DECIMAL_EXPONENT = 308;
        const jint FloatingDecimal::MIN_DECIMAL_EXPONENT = -324;
        const jint FloatingDecimal::BIG_DECIMAL_EXPONENT = 324;
        const jint FloatingDecimal::MAX_NDIGITS = 1100;

        const jint FloatingDecimal::SINGLE_EXP_SHIFT = FloatConsts::SIGNIFICAND_WIDTH - 1;
        const jint FloatingDecimal::SINGLE_FRACT_HOB = 1 << SINGLE_EXP_SHIFT;
        const jint FloatingDecimal::SINGLE_MAX_DECIMAL_DIGITS = 7;
        const jint FloatingDecimal::SINGLE_MAX_DECIMAL_EXPONENT = 38;
        const jint FloatingDecimal::SINGLE_MIN_DECIMAL_EXPONENT = -45;
        const jint FloatingDecimal::SINGLE_MAX_NDIGITS = 200;

        const jint FloatingDecimal::INT_DECIMAL_DIGITS = 9;
    }
}
