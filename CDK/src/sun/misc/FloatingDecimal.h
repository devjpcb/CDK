#ifndef SUN_MISC_FLOATINGDECIMAL_H
#define SUN_MISC_FLOATINGDECIMAL_H

#include "../../cpp/lang/Object.h"
#include "../../cpp/lang/Array.h"
#include "../../cpp/lang/CharSequence.h"
#include "../../cpp/lang/Appendable.h"
#include "../../cpp/lang/String.h"
//#include "../../cpp/util/regex/Pattern.h"

namespace sun
{
    namespace misc
    {
        class FloatingDecimal : cpp::lang::Object
        {
        public:
            FloatingDecimal();
            ~FloatingDecimal();

            static cpp::lang::JString toJavaFormatString(jdouble d);
            static cpp::lang::JString toJavaFormatString(jfloat f);
            static void appendTo(jdouble d, cpp::lang::JAppendable buf);
            static void appendTo(jfloat f, cpp::lang::JAppendable buf);
            static jdouble parseDouble(cpp::lang::JString s);
            static jfloat parseFloat(cpp::lang::JString s);

            class BinaryToASCIIConverter : public cpp::lang::Object
            {
            public:
                virtual cpp::lang::JString toJavaFormatString() = 0;
                virtual void appendTo(cpp::lang::JAppendable buf) = 0;
                virtual jint getDecimalExponent() = 0;
                virtual jint getDigits(jarray<jchar> digits) = 0;
                virtual jboolean isNegative() = 0;
                virtual jboolean isExceptional() = 0;
                virtual jboolean digitsRoundedUp() = 0;
                virtual jboolean decimalDigitsExact() = 0;
            };
            
            using JBinaryToASCIIConverter = std::shared_ptr<BinaryToASCIIConverter>;

            class ExceptionalBinaryToASCIIBuffer : public BinaryToASCIIConverter
            {
            public:
                ExceptionalBinaryToASCIIBuffer(cpp::lang::JString image, jboolean isNegative);
                virtual cpp::lang::JString toJavaFormatString();
                virtual void appendTo(cpp::lang::JAppendable buf);
                virtual jint getDecimalExponent();
                virtual jint getDigits(jarray<jchar> digits);
                virtual jboolean isNegative();
                virtual jboolean isExceptional();
                virtual jboolean digitsRoundedUp();
                virtual jboolean decimalDigitsExact();

            private:
                const cpp::lang::JString image;
                jboolean _isNegative;
            };
            
            using JExceptionalBinaryToASCIIBuffer = std::shared_ptr<ExceptionalBinaryToASCIIBuffer>;

            class BinaryToASCIIBuffer : public BinaryToASCIIConverter
            {
            public:
                BinaryToASCIIBuffer();
                BinaryToASCIIBuffer(jboolean isNegative, jarray<jchar> digits);
                virtual cpp::lang::JString toJavaFormatString();
                virtual void appendTo(cpp::lang::JAppendable buf);
                virtual jint getDecimalExponent();
                virtual jint getDigits(jarray<jchar> digits);
                virtual jboolean isNegative();
                virtual jboolean isExceptional();
                virtual jboolean digitsRoundedUp();
                virtual jboolean decimalDigitsExact();

            private:
                void setSign(jboolean isNegative);
                void developLongDigits(jint decExponent, jlong lvalue, jint insignificantDigits);
                void dtoa(jint binExp, jlong fractBits, jint nSignificantBits, jboolean isCompatibleFormat);
                void roundup();
                static jint estimateDecExp(jlong fractBits, jint binExp);
                static jint insignificantDigits(jint insignificant);
                static jint insignificantDigitsForPow2(jint p2);
                jint getChars(jarray<jchar> result);

                static const jarray<jint> insignificantDigitsNumber;
                static const jarray<jint> N_5_BITS;

                jboolean _isNegative;
                jint decExponent;
                jint firstDigitIndex;
                jint nDigits;
                jarray<jchar> digits;
                jarray<jchar> buffer;
                jboolean exactDecimalConversion;
                jboolean decimalDigitsRoundedUp;

                friend class FloatingDecimal;
            };
            
            using JBinaryToASCIIBuffer = std::shared_ptr<BinaryToASCIIBuffer>;

            class ASCIIToBinaryConverter : public cpp::lang::Object
            {
            public:
                virtual jdouble doubleValue() = 0;
                virtual jfloat floatValue() = 0;
            };
            
            using JASCIIToBinaryConverter = std::shared_ptr<ASCIIToBinaryConverter>;

            class PreparedASCIIToBinaryBuffer : public ASCIIToBinaryConverter
            {
            public:
                PreparedASCIIToBinaryBuffer(jdouble doubleVal, jfloat floatVal);
                jdouble doubleValue();
                jfloat floatValue();

            private:
                jdouble doubleVal;
                jfloat floatVal;
            };
            
            using JPreparedASCIIToBinaryBuffer = std::shared_ptr<PreparedASCIIToBinaryBuffer>;

            class ASCIIToBinaryBuffer : public ASCIIToBinaryConverter
            {
            public:
                ASCIIToBinaryBuffer(jboolean negSign, jint decExponent, jarray<jchar> digits, jint n);
                jdouble doubleValue();
                jfloat floatValue();

            private:
                jboolean isNegative;
                jint decExponent;
                jarray<jchar> digits;
                jint nDigits;

                static const jarray<jdouble> SMALL_10_POW;
                static const jarray<jfloat> SINGLE_SMALL_10_POW;

                static const jint MAX_SMALL_TEN;
                static const jint SINGLE_MAX_SMALL_TEN;

                static const jarray<jdouble> BIG_10_POW;
                static const jarray<jdouble> TINY_10_POW;
            };
            
            using JASCIIToBinaryBuffer = std::shared_ptr<ASCIIToBinaryBuffer>;

            class HexFloatPattern
            {
                /**
                 * Grammar is compatible with hexadecimal floating-point constants
                 * described in section 6.4.4.2 of the C99 specification.
                 */
            private:
                static const cpp::util::regex::JPattern VALUE;
                
                friend FloatingDecimal;
            };

            static JBinaryToASCIIConverter getBinaryToASCIIConverter(jdouble d);
            static JBinaryToASCIIConverter getBinaryToASCIIConverter(jdouble d, jboolean isCompatibleFormat);
            static JBinaryToASCIIConverter getBinaryToASCIIConverter(jfloat f);
            static JBinaryToASCIIBuffer getBinaryToASCIIBuffer();

            static JASCIIToBinaryConverter readJavaFormatString(cpp::lang::JString in);
            static JASCIIToBinaryConverter parseHexString(cpp::lang::JString s);
            static cpp::lang::JString stripLeadingZeros(cpp::lang::JString s);
            static jint getHexDigit(cpp::lang::JString s, jint position);

            static const cpp::lang::JString INFINITY_REP;
            static const jint INFINITY_LENGTH;
            static const cpp::lang::JString NAN_REP;
            static const jint NAN_LENGTH;
            static const jarray<jchar> DEFAULT_DIGITS;

            static const JBinaryToASCIIConverter B2AC_POSITIVE_INFINITY;
            static const JBinaryToASCIIConverter B2AC_NEGATIVE_INFINITY;
            static const JBinaryToASCIIConverter B2AC_NOT_A_NUMBER;
            static const JBinaryToASCIIConverter B2AC_POSITIVE_ZERO;
            static const JBinaryToASCIIConverter B2AC_NEGATIVE_ZERO;

            static const JASCIIToBinaryConverter A2BC_POSITIVE_INFINITY;
            static const JASCIIToBinaryConverter A2BC_NEGATIVE_INFINITY;
            static const JASCIIToBinaryConverter A2BC_NOT_A_NUMBER;
            static const JASCIIToBinaryConverter A2BC_POSITIVE_ZERO;
            static const JASCIIToBinaryConverter A2BC_NEGATIVE_ZERO;

            static const jint EXP_SHIFT;
            static const jlong EXP_ONE;
            static const jlong FRACT_HOB;
            static const jint MAX_SMALL_BIN_EXP;
            static const jint MIN_SMALL_BIN_EXP;
            static const jint MAX_DECIMAL_DIGITS;
            static const jint MAX_DECIMAL_EXPONENT;
            static const jint MIN_DECIMAL_EXPONENT;
            static const jint BIG_DECIMAL_EXPONENT;
            static const jint MAX_NDIGITS;

            static const jint SINGLE_EXP_SHIFT;
            static const jint SINGLE_FRACT_HOB;
            static const jint SINGLE_MAX_DECIMAL_DIGITS;
            static const jint SINGLE_MAX_DECIMAL_EXPONENT;
            static const jint SINGLE_MIN_DECIMAL_EXPONENT;
            static const jint SINGLE_MAX_NDIGITS;

            static const jint INT_DECIMAL_DIGITS;
        };
    }
}

#endif // SUN_MISC_FLOATINGDECIMAL_H
