#ifndef SUN_MISC_JAVAIOACCESS_H
#define SUN_MISC_JAVAIOACCESS_H

#include "../../cpp/lang/Object.h"

namespace sun
{
    namespace misc
    {
        class JavaIOAccess : public cpp::lang::Object
        {
        public:
            virtual cpp::io::JConsole console() = 0;
            virtual cpp::nio::charset::JCharset charset() = 0;
        };
    }
}

#endif // SUN_MISC_JAVAIOACCESS_H
