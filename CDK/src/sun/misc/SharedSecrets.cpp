#include "SharedSecrets.h"
#include "Unsafe.h"
#include "../../cpp/io/Console.h"

using namespace cpp::io;

namespace sun
{
    namespace misc
    {
        void SharedSecrets::setJavaIOAccess(JJavaIOAccess jia) 
        {
            javaIOAccess = jia;
        }
        
        JJavaIOAccess SharedSecrets::getJavaIOAccess() 
        {
            if (javaIOAccess == nullptr) {
                unsafe->ensureClassInitialized(Console::clazz);
            }
            return javaIOAccess;
        }
        
        JUnsafe SharedSecrets::unsafe = Unsafe::getUnsafe();
        JJavaIOAccess SharedSecrets::javaIOAccess;
    }
}
