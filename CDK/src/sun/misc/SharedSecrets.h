#ifndef SUN_MISC_SHAREDSECRETS_H
#define SUN_MISC_SHAREDSECRETS_H

#include "../../cpp/lang/Object.h"
#include "JavaIOAccess.h"

namespace sun
{
    namespace misc
    {
        class SharedSecrets : public cpp::lang::Object
        {
        public:
            static void setJavaIOAccess(JJavaIOAccess jia);
            static JJavaIOAccess getJavaIOAccess();
            
        private:
            static JUnsafe unsafe;
            static JJavaIOAccess javaIOAccess;
        };
    }
}

#endif // SUN_MISC_SHAREDSECRETS_H
