#ifndef SUN_MISC_UNSAFE_H
#define SUN_MISC_UNSAFE_H

#include "../../cpp/lang/Object.h"

namespace sun
{
    namespace misc
    {
        class Unsafe : public cpp::lang::Object
        {
        public:
            static JUnsafe getUnsafe();
            
            void ensureClassInitialized(cpp::lang::JClass c);
        };
    }
}

#endif // SUN_MISC_UNSAFE_H
