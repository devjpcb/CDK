#include "VM.h"

using namespace cpp::lang;

namespace sun
{

    namespace misc
    {
        VM::VM()
        {
        }

        VM::~VM()
        {
        }
        
        void VM::unsuspendSomeThreads()
        {
            
        }
        
        Thread::State VM::toThreadState(jint threadStatus)
        {
            if ((threadStatus & JVMTI_THREAD_STATE_RUNNABLE) != 0) {
                return Thread::State::RUNNABLE;
            } else if ((threadStatus & JVMTI_THREAD_STATE_BLOCKED_ON_MONITOR_ENTER) != 0) {
                return Thread::State::BLOCKED;
            } else if ((threadStatus & JVMTI_THREAD_STATE_WAITING_INDEFINITELY) != 0) {
                return Thread::State::WAITING;
            } else if ((threadStatus & JVMTI_THREAD_STATE_WAITING_WITH_TIMEOUT) != 0) {
                return Thread::State::TIMED_WAITING;
            } else if ((threadStatus & JVMTI_THREAD_STATE_TERMINATED) != 0) {
                return Thread::State::TERMINATED;
            } else if ((threadStatus & JVMTI_THREAD_STATE_ALIVE) == 0) {
                return Thread::State::NEW;
            } else {
                return Thread::State::RUNNABLE;
            }
        }
        
        jboolean VM::isSystemDomainLoader(cpp::lang::JClassLoader loader)
        {
            return loader == nullptr;
        }
        
        const jint VM::JVMTI_THREAD_STATE_ALIVE = 0x0001;
        const jint VM::JVMTI_THREAD_STATE_TERMINATED = 0x0002;
        const jint VM::JVMTI_THREAD_STATE_RUNNABLE = 0x0004;
        const jint VM::JVMTI_THREAD_STATE_BLOCKED_ON_MONITOR_ENTER = 0x0400;
        const jint VM::JVMTI_THREAD_STATE_WAITING_INDEFINITELY = 0x0010;
        const jint VM::JVMTI_THREAD_STATE_WAITING_WITH_TIMEOUT = 0x0020;
    }
}
