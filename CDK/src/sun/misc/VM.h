#ifndef SUN_MISC_VM_H
#define SUN_MISC_VM_H

#include "../../cpp/lang/Thread.h"

namespace sun
{
    namespace misc
    {
        class VM
        {
        public:
            VM();
            ~VM();
            
            static void unsuspendSomeThreads();
            static cpp::lang::Thread::State toThreadState(jint threadStatus);
            static jboolean isSystemDomainLoader(cpp::lang::JClassLoader loader);
            
        private:
            static const jint JVMTI_THREAD_STATE_ALIVE;
            static const jint JVMTI_THREAD_STATE_TERMINATED;
            static const jint JVMTI_THREAD_STATE_RUNNABLE;
            static const jint JVMTI_THREAD_STATE_BLOCKED_ON_MONITOR_ENTER;
            static const jint JVMTI_THREAD_STATE_WAITING_INDEFINITELY;
            static const jint JVMTI_THREAD_STATE_WAITING_WITH_TIMEOUT;
            
            friend class cpp::lang::Thread;
        };
    }
}

#endif // SUN_MISC_VM_H
