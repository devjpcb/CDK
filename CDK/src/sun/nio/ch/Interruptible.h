#ifndef INTERRUPTIBLE_H
#define INTERRUPTIBLE_H

#include "../../../cpp/lang/Object.h"

namespace sun
{
    namespace nio
    {
        namespace ch
        {
            class Interruptible : public cpp::lang::Object
            {
            public:
                virtual void interrupt(cpp::lang::JThread t) = 0;
            };
        }
    }
}

#endif // INTERRUPTIBLE_H
