#ifndef SUN_MISC_CS_STREAMENCODER_H
#define SUN_MISC_CS_STREAMENCODER_H

#include "../../../cpp/io/Writer.h"

namespace sun
{
    namespace nio
    {
        namespace cs
        {
            class StreamEncoder : public cpp::io::Writer
            {
            public:
                StreamEncoder();
                ~StreamEncoder();
                
                static JStreamEncoder forOutputStreamWriter(cpp::io::JOutputStream out, cpp::lang::JObject lock, cpp::lang::JString charsetName);
                void close();
            };
        }
    }
}

#endif // SUN_MISC_CS_STREAMENCODER_H
