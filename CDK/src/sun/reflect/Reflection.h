#ifndef SUN_REFLECTION_REFLECTION_H
#define SUN_REFLECTION_REFLECTION_H

#include "../../cpp/lang/Object.h"

namespace sun
{
    namespace reflect
    {
        class Reflection : public cpp::lang::Object
        {
        public:
            static cpp::lang::JClass getCallerClass();
        };
    }
}

#endif // SUN_REFLECTION_REFLECTION_H
