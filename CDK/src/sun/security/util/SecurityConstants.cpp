#include "SecurityConstants.h"
#include "../../../cpp/lang/Operators.h"
#include "../../../cpp/lang/String.h"
#include "../../../cpp/lang/RuntimePermission.h"
#include "../../../cpp/security/AllPermission.h"

namespace sun
{
    namespace security
    {
        namespace util
        {
            const cpp::lang::JRuntimePermission SecurityConstants::GET_CLASSLOADER_PERMISSION = jnew<cpp::lang::RuntimePermission>(jstr(u"getClassLoader"));
            const cpp::security::JAllPermission SecurityConstants::ALL_PERMISSION = jnew<cpp::security::AllPermission>();
        }
    }
}
