#ifndef SECURITYCONSTANTS_H
#define SECURITYCONSTANTS_H

#include "../../../cpp/lang/Object.h"

namespace sun
{
    namespace security
    {
        namespace util
        {
            class SecurityConstants
            {
            public:
                static const cpp::lang::JRuntimePermission GET_CLASSLOADER_PERMISSION;
                static const cpp::security::JAllPermission ALL_PERMISSION;
            };
        }
    }
}

#endif // SECURITYCONSTANTS_H
