#include "BaseLocale.h"
#include "../../../cpp/lang/String.h"

using cpp::lang::JString;

namespace sun
{
    namespace util
    {
        namespace locale
        {
            BaseLocale::BaseLocale()
            {
            }

            BaseLocale::~BaseLocale()
            {
            }
            
            const JString BaseLocale::SEP = jstr(u"_");
        }
    }
}
