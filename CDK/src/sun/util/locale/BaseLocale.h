#ifndef SUN_UTIL_LOCALE_BASELOCALE_H
#define SUN_UTIL_LOCALE_BASELOCALE_H

#include "../../../cpp/lang/Object.h"

namespace sun
{
    namespace util
    {
        namespace locale
        {
            class BaseLocale : public cpp::lang::Object
            {
            public:
                BaseLocale();
                ~BaseLocale();
                
            private:
                static const  cpp::lang::JString SEP;
            };
        }
    }
}

#endif // SUN_UTIL_LOCALE_BASELOCALE_H
