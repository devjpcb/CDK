#include <UnitTest++/UnitTest++.h>

#include <cpp/lang/Boolean.h>
#include <cpp/lang/String.h>
#include <cpp/lang/Class.h>

using namespace cpp::lang;

TEST(Test_CppLangBoolean_FALSE)
{
    JBoolean a = Boolean::FALSE;
    CHECK(!a->booleanValue());
}

TEST(Test_CppLangBoolean_TRUE)
{
    JBoolean a = Boolean::TRUE;
    CHECK(a->booleanValue());
}

TEST(Test_CppLangBoolean_booleanValue)
{
    JBoolean a = boxing(true);
    CHECK(a->booleanValue());

    JBoolean b = boxing(false);
    CHECK(!b->booleanValue());
}

TEST(Test_CppLangBoolean_compare)
{
    jint a = Boolean::compare(true, false);
    CHECK(a == 1);

    jint b = Boolean::compare(false, true);
    CHECK(b == -1);

    jint c = Boolean::compare(true, true);
    CHECK(c == 0);

    jint d = Boolean::compare(false, false);
    CHECK(d == 0);
}

TEST(Test_CppLangBoolean_compareTo)
{
    JBoolean a = boxing(true);
    JBoolean b = boxing(false);

    CHECK(a->compareTo(b) == 1);
    CHECK(b->compareTo(a) == -1);
    CHECK(a->compareTo(a) == 0);
    CHECK(b->compareTo(b) == 0);
}

TEST(Test_CppLangBoolean_equals)
{
    JBoolean a = boxing(true);
    JBoolean b = boxing(false);

    CHECK(a->equals(a));
    CHECK(!a->equals(b));
    CHECK(b->equals(b));
}

TEST(Test_CppLangBoolean_hashCode)
{
    JBoolean a = boxing(true);
    JBoolean b = boxing(false);

    CHECK(a->hashCode() != b->hashCode());
}

TEST(Test_CppLangBoolean_logicalAnd)
{
    CHECK(Boolean::logicalAnd(true, true));
    CHECK(!Boolean::logicalAnd(false, true));
    CHECK(!Boolean::logicalAnd(true, false));
    CHECK(!Boolean::logicalAnd(false, false));
}

TEST(Test_CppLangBoolean_logicalOr)
{
    CHECK(Boolean::logicalOr(true, true));
    CHECK(Boolean::logicalOr(true, false));
    CHECK(Boolean::logicalOr(false, true));
    CHECK(!Boolean::logicalOr(false, false));
}

TEST(Test_CppLangBoolean_logicalXor)
{
    CHECK(Boolean::logicalXor(true, false));
    CHECK(Boolean::logicalXor(false, true));
    CHECK(!Boolean::logicalXor(true, true));
    CHECK(!Boolean::logicalXor(false, false));
}

TEST(Test_CppLangBoolean_valueOf)
{
    CHECK(Boolean::valueOf(true)->booleanValue());
    CHECK(!Boolean::valueOf(false)->booleanValue());
}

TEST(Test_CppLangBoolean_getClass)
{
    //TODO implementar un jcast<Boolean> o jcast<JBoolean>
    JClass classBoolean = Class::forName(jstr(u"cpp::lang::Boolean"));
    JBoolean obj1 = std::dynamic_pointer_cast<Boolean>(classBoolean->newInstance());
    
    CHECK(obj1->getClass()->getSimpleName()->equals(jstr(u"Boolean")));
    CHECK(obj1->getClass()->getPackage()->getName()->equals(jstr(u"cpp::lang")));
}