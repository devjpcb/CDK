#include <UnitTest++/UnitTest++.h>

#include <cpp/lang/Byte.h>
#include <cpp/lang/String.h>
#include <cpp/lang/NumberFormatException.h>

using namespace cpp::lang;

TEST(Test_CppLangByte_byteValue)
{
    JByte a = boxing<jbyte>(45);
    CHECK(a->byteValue() == 45);
}

TEST(Test_CppLangByte_compare)
{
    CHECK(Byte::compare(45, 90) == -45);
    CHECK(Byte::compare(90, 45) == 45);
}

TEST(Test_CppLangByte_compareTo)
{
    JByte a = boxing<jbyte>(45);
    JByte b = boxing<jbyte>(90);

    CHECK(a->compareTo(b) == -45);
    CHECK(b->compareTo(a) == 45);
}

TEST(Test_CppLangByte_doubleValue)
{
    JByte a = boxing<jbyte>(45);
    JByte b = boxing<jbyte>(90);

    CHECK(a->doubleValue() == 45.0);
    CHECK(b->doubleValue() == 90.0);
}

TEST(Test_CppLangByte_equals)
{
    JByte a = boxing<jbyte>(45);
    JByte b = boxing<jbyte>(90);

    CHECK(a->equals(a));
    CHECK(!a->equals(b));
    CHECK(!b->equals(a));
}

TEST(Test_CppLangByte_floatValue)
{
    JByte a = boxing<jbyte>(45);
    JByte b = boxing<jbyte>(90);

    CHECK(a->floatValue() == 45.0f);
    CHECK(b->floatValue() == 90.0f);
}

TEST(Test_CppLangByte_hashCode)
{
    JByte a = boxing<jbyte>(45);
    JByte b = boxing<jbyte>(90);

    CHECK(a->hashCode() == 45);
    CHECK(b->hashCode() == 90);
    CHECK(a->hashCode() != b->hashCode());
}

TEST(Test_CppLangByte_intValue)
{
    JByte a = boxing<jbyte>(45);
    JByte b = boxing<jbyte>(90);

    CHECK(a->intValue() == 45);
    CHECK(b->intValue() == 90);
}

TEST(Test_CppLangByte_longValue)
{
    JByte a = boxing<jbyte>(45);
    JByte b = boxing<jbyte>(90);

    CHECK(a->longValue() == 45LL);
    CHECK(b->longValue() == 90LL);
}

TEST(Test_CppLangByte_shortValue)
{
    JByte a = boxing<jbyte>(45);
    JByte b = boxing<jbyte>(90);

    CHECK(a->shortValue() == 45);
    CHECK(b->shortValue() == 90);
}

TEST(Test_CppLangByte_toUnsignedInt)
{
    CHECK(Byte::toUnsignedInt(2) == 2);
    CHECK(Byte::toUnsignedInt(-2) == 254);
}

TEST(Test_CppLangByte_toUnsignedLong)
{
    CHECK(Byte::toUnsignedLong(2) == 2);
    CHECK(Byte::toUnsignedLong(-2) == 254);
}

TEST(Test_CppLangByte_valueOf)
{
    JByte value = Byte::valueOf(127);
    CHECK(value->byteValue() == 127);
}

TEST(Test_CppLangByte_parseByte)
{
    JByte value = boxing(Byte::parseByte(jstr(u"55")));
    CHECK(value->byteValue() == 55);
    
    CHECK_THROW( {
        JByte value = boxing(Byte::parseByte(jstr(u"255")));
    }, NumberFormatException);
}