#include <UnitTest++/UnitTest++.h>

#include <cpp/lang/Character.h>
#include <cpp/lang/String.h>

using namespace cpp::lang;

TEST(Test_CppLangCharacter_charCount)
{
    CHECK(Character::charCount(10000) == 1);
    CHECK(Character::charCount(65536) == 2);
}

TEST(Test_CppLangCharacter_charValue)
{
    JCharacter chr = jnew<Character>(u'a');
    CHECK(chr->charValue() == u'a');
}

TEST(Test_CppLangCharacter_codePointAt)
{
    CHECK(Character::codePointAt({ u'a', u'b', u'c', u'd', u'e' }, 1, 3) == 98);
    CHECK(Character::codePointAt(jstr(u"Hello"), 4) == 111);
}

TEST(Test_CppLangCharacter_codePointBefore)
{
    CHECK(Character::codePointBefore({ u'a', u'b', u'c', u'd', u'e' }, 2) == 98);
    CHECK(Character::codePointBefore({ u'A', u'b', u'C', u'd'}, 3, 1) == 67);
    CHECK(Character::codePointBefore(jstr(u"Hello"), 4) == 108);
   
}

TEST(Test_CppLangCharacter_codePointCount)
{
     CHECK(Character::codePointCount({ u'a', u'b', u'c', u'd', u'e' }, 1, 3) == 3);
     CHECK(Character::codePointCount(jstr(u"Hello World!"), 4, 8) == 4);
}

TEST(Test_CppLangCharacter_compare)
{
    CHECK(Character::compare(u'a', u'a') == 0);
    CHECK(Character::compare(u'a', u'b') == -1);
    CHECK(Character::compare(u'b', u'a') == 1);
}

TEST(Test_CppLangCharacter_compareTo)
{
    JCharacter chr1 = jnew<Character>(u'a');
    JCharacter chr2 = jnew<Character>(u'a');
    CHECK(chr1->compareTo(chr2) == 0);
}

TEST(Test_CppLangCharacter_digit)
{
    CHECK(Character::digit(u'9', 2) == -1);
    CHECK(Character::digit(u'5', 10) == 5);
    CHECK(Character::digit(0x0034, 8) == 4);
    CHECK(Character::digit(0x004a, 8) == -1);
}

TEST(Test_CppLangCharacter_equals)
{
    JCharacter chr1 = jnew<Character>(u'a');
    JCharacter chr2 = jnew<Character>(u'a');
    CHECK(chr1->equals(chr2));
}

TEST(Test_CppLangCharacter_forDigit)
{
    CHECK(Character::forDigit(3, 10) == u'3');
    CHECK(Character::forDigit(14, 16) == u'e');
}

TEST(Test_CppLangCharacter_getDirectionality)
{
    CHECK(Character::getDirectionality(u'M') == 0);
    CHECK(Character::getDirectionality(u'\u06ff') == 2);
    CHECK(Character::getDirectionality(0x2323) == 13);
    CHECK(Character::getDirectionality(0x2c60) == 0);
}

TEST(Test_CppLangCharacter_getNumericValue)
{
    CHECK(Character::getNumericValue(u'j') == 19);
    CHECK(Character::getNumericValue(u'4') == 4);
    CHECK(Character::getNumericValue(0x0068) == 17);
    CHECK(Character::getNumericValue(0xff30) == 25);
}

TEST(Test_CppLangCharacter_hashCode)
{
    JCharacter chr1 = jnew<Character>(u'z');
    JCharacter chr2 = jnew<Character>(u'Z');
    CHECK(chr1->hashCode() == 122);
    CHECK(chr2->hashCode() == 90);
}

TEST(Test_CppLangCharacter_highSurrogate)
{
    CHECK(Character::highSurrogate(0x008f) == 55232);
    CHECK(Character::highSurrogate(0x0123) == 55232);
}

TEST(Test_CppLangCharacter_isBmpCodePoint)
{
    CHECK(Character::isBmpCodePoint(0x0000));
    CHECK(Character::isBmpCodePoint(0x0666));
    CHECK(Character::isBmpCodePoint(0xffff));
    CHECK_EQUAL(Character::isBmpCodePoint(0x10000), false);
}

TEST(Test_CppLangCharacter_isHighSurrogate)
{
    CHECK(Character::isHighSurrogate(0xd8b5));
    CHECK_EQUAL(Character::isHighSurrogate(0x0c7f), false);
}

TEST(Test_CppLangCharacter_isLowSurrogate)
{
    CHECK(Character::isLowSurrogate(0xdc28));
    CHECK_EQUAL(Character::isLowSurrogate(u'a'), false);
}

TEST(Test_CppLangCharacter_isSupplementaryCodePoint)
{
    CHECK(Character::isSupplementaryCodePoint(0x10ffd));
    CHECK_EQUAL(Character::isSupplementaryCodePoint(0x004ca), false);
}