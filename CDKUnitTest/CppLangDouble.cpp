#include <UnitTest++/UnitTest++.h>

#include <cpp/lang/Double.h>
#include <cpp/lang/String.h>

using namespace cpp::lang;

TEST(Test_CppLangDouble_byteValue)
{
    JDouble a = boxing(456.12345);
    CHECK(a->byteValue() == -56);
}

TEST(Test_CppLangDouble_compare)
{
    CHECK(Double::compare(56.78, 78.90) == -1);
    CHECK(Double::compare(78.90, 56.78) == 1);
    CHECK(Double::compare(56.78, 56.78) == 0);
}

TEST(Test_CppLangDouble_compareTo)
{
    JDouble a = boxing(56.78);
    JDouble b = boxing(78.90);

    CHECK(a->compareTo(b) == -1);
    CHECK(b->compareTo(a) == 1);
    CHECK(a->compareTo(a) == 0);
}

TEST(Test_CppLangDouble_doubleToLongBits)
{
    CHECK(Double::doubleToLongBits(456.12345) == 4646731824783231759);
}

TEST(Test_CppLangDouble_doubleToRawLongBits)
{
    CHECK(Double::doubleToRawLongBits(456.12345) == 4646731824783231759);
}

TEST(Test_CppLangDouble_doubleValue)
{
    JDouble a = boxing(456.12345);
    CHECK(a->doubleValue() == 456.12345);
}

TEST(Test_CppLangDouble_equals)
{
    JDouble a = boxing(56.78);
    JDouble b = boxing(78.90);

    CHECK(a->equals(a));
    CHECK(!a->equals(b));
}

TEST(Test_CppLangDouble_floatValue)
{
    JDouble a = boxing(34.12345678901235);
    CHECK(a->floatValue() == 34.123455f);
}

TEST(Test_CppLangDouble_hashCode)
{
    JDouble a = boxing(34.12345678901235);
    CHECK(a->hashCode() == 786082993);
}

TEST(Test_CppLangDouble_intValue)
{
    JDouble a = boxing(34.12345678901235);
    CHECK(a->intValue() == 34);
}

TEST(Test_CppLangDouble_isFinite)
{
    jdouble zero = 0.0;
    jdouble a = 4.0 / zero;
    CHECK(!Double::isFinite(a));
}

TEST(Test_CppLangDouble_isInfinite)
{
    jdouble zero = 0.0;
    jdouble a = 4.0 / zero;
    CHECK(Double::isInfinite(a));
}

TEST(Test_CppLangDouble_isNaN)
{
    jdouble zero = 0.0;
    jdouble a = 0.0 / zero;
    CHECK(Double::isNaN(a));
}

TEST(Test_CppLangDouble_longBitsToDouble)
{
    CHECK(Double::longBitsToDouble(54564561) == 2.6958475E-316);
}

TEST(Test_CppLangDouble_longValue)
{
    JDouble a = boxing(34.12345678901235);
    CHECK(a->longValue() == 34);
}

TEST(Test_CppLangDouble_max)
{
    CHECK(Double::max(34.12345678901235, 34.12345678901234) == 34.12345678901235);
}

TEST(Test_CppLangDouble_min)
{
    CHECK(Double::min(34.12345678901235, 34.12345678901234) == 34.12345678901234);
}

TEST(Test_CppLangDouble_parseDouble)
{
    JDouble a = jnew<Double>(jstr(u"34.12345678901235"));
    CHECK(a->doubleValue() == 34.12345678901235);
}

TEST(Test_CppLangDouble_shortValue)
{
    JDouble a = boxing(34.12345678901235);
    CHECK(a->shortValue() == 34);
}

TEST(Test_CppLangDouble_sum)
{
    CHECK(Double::sum(34.12345678901235, 34.12345678901235) == 68.2469135780247);
}

TEST(Test_CppLangDouble_toString)
{
    JString v = Double::toString(145.5467854678);
    CHECK(v->equals(jstr(u"145.5467854678")));
}

TEST(Test_CppLangDouble_valueOf)
{
    JDouble a = Double::valueOf(34.12345678901235);
    CHECK(a->doubleValue() == 34.12345678901235);
}