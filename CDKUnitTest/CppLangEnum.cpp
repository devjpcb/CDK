#include <UnitTest++/UnitTest++.h>

#include <cpp/lang/Enum.h>
#include <iostream>

jenum_begin(Day)
    jenum_declare(SUNDAY)
    jenum_declare(MONDAY)
    jenum_declare(TUESDAY)
    jenum_declare(WEDNESDAY)
    jenum_declare(THURSDAY)
    jenum_declare(FRIDAY)
    jenum_declare(SATURDAY)
jenum_end

jenum_define(Day, 0, SUNDAY)
jenum_define(Day, 1, MONDAY)
jenum_define(Day, 2, TUESDAY)
jenum_define(Day, 3, WEDNESDAY)
jenum_define(Day, 4, THURSDAY)
jenum_define(Day, 5, FRIDAY)
jenum_define(Day, 6, SATURDAY)

TEST(Test_CppLangEnum_declare)
{
    JDay day = Day::JSATURDAY;

    jenum_switch(day) 
    {
        case Day::SATURDAY:
            CHECK(true);
            break;
        default:
            CHECK(false);
    }
}

TEST(Test_CppLangEnum_enums) 
{
    CHECK(Day::JSUNDAY->ordinal() == 0);
    CHECK(Day::JMONDAY->ordinal() == 1);
    CHECK(Day::JTUESDAY->ordinal() == 2);
    CHECK(Day::JWEDNESDAY->ordinal() == 3);
    CHECK(Day::JTHURSDAY->ordinal() == 4);
    CHECK(Day::JFRIDAY->ordinal() == 5);
    CHECK(Day::JSATURDAY->ordinal() == 6);
}