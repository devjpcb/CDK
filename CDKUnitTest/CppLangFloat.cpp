#include <UnitTest++/UnitTest++.h>

#include <cpp/lang/Float.h>
#include <cpp/lang/String.h>

using namespace cpp::lang;

TEST(Test_CppLangFloat_byteValue)
{
    JFloat a = boxing(456.12345f);
    CHECK(a->byteValue() == -56);
}

TEST(Test_CppLangFloat_compare)
{
    CHECK(Float::compare(56.78f, 78.90f) == -1);
    CHECK(Float::compare(78.90f, 56.78f) == 1);
    CHECK(Float::compare(56.78f, 56.78f) == 0);
}

TEST(Test_CppLangFloat_compareTo)
{
    JFloat a = boxing(56.78f);
    JFloat b = boxing(78.90f);

    CHECK(a->compareTo(b) == -1);
    CHECK(b->compareTo(a) == 1);
    CHECK(a->compareTo(a) == 0);
}

TEST(Test_CppLangFloat_doubleValue)
{
    JFloat a = boxing(456.12345f);
    CHECK(a->doubleValue() == 456.1234436035156f);
}

TEST(Test_CppLangFloat_equals)
{
    JFloat a = boxing(56.78f);
    JFloat b = boxing(78.90f);

    CHECK(a->equals(a));
    CHECK(!a->equals(b));
}

TEST(Test_CppLangFloat_floatToIntBits)
{
    CHECK(Float::floatToIntBits(456.12345f) == 1139019725);
}

TEST(Test_CppLangFloat_floatToRawIntBits)
{
    CHECK(Float::floatToRawIntBits(456.12345f) == 1139019725);
}

TEST(Test_CppLangFloat_floatValue)
{
    JFloat a = boxing(34.123455f);
    CHECK(a->floatValue() == 34.123455f);
}

TEST(Test_CppLangFloat_hashCode)
{
    JFloat a = boxing(34.123455f);
    CHECK(a->hashCode() == 1107852907);
}

TEST(Test_CppLangFloat_intBitsToFloat)
{
    CHECK(Float::intBitsToFloat(54564561) == 5.6596857583178974E-37);
}

TEST(Test_CppLangFloat_intValue)
{
    JFloat a = boxing(34.123455f);
    CHECK(a->intValue() == 34);
}

TEST(Test_CppLangFloat_isFinite)
{
    jfloat zero = 0.0f;
    jfloat a = 4.0f / zero;
    CHECK(!Float::isFinite(a));
}

TEST(Test_CppLangFloat_isInfinite)
{
    jfloat zero = 0.0f;
    jfloat a = 0.0f / zero;
    CHECK(Float::isNaN(a));
}

TEST(Test_CppLangFloat_isNaN)
{
    jfloat zero = 0.0;
    jfloat a = 0.0 / zero;
    CHECK(Float::isNaN(a));
}

TEST(Test_CppLangFloat_longValue)
{
    JFloat a = boxing(34.123455f);
    CHECK(a->longValue() == 34);
}

TEST(Test_CppLangFloat_max)
{
    CHECK(Float::max(34.123455f, 34.123454f) == 34.123455f);
}

TEST(Test_CppLangFloat_min)
{
    CHECK(Float::min(34.123455f, 34.123454f) == 34.123454f);
}

TEST(Test_CppLangFloat_parseFloat)
{
    JFloat a = jnew<Float>(jstr(u"34.123455f"));
    CHECK(a->floatValue() == 34.123455f);
}

TEST(Test_CppLangFloat_shortValue)
{
    JFloat a = boxing(34.123455f);
    CHECK(a->shortValue() == 34);
}

TEST(Test_CppLangFloat_sum)
{
    CHECK(Float::sum(34.123455f, 34.123455f) == 68.24691f);
}

TEST(Test_CppLangFloat_toString)
{
    JString v = Float::toString(10.54678f);
    CHECK(v->equals(jstr(u"10.54678")));
}

TEST(Test_CppLangFloat_valueOf)
{
    JFloat a = Float::valueOf(34.123455f);
    CHECK(a->doubleValue() == 34.123455f);
}