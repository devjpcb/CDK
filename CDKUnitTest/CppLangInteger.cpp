#include <UnitTest++/UnitTest++.h>

#include <cpp/lang/Integer.h>
#include <cpp/lang/String.h>

using namespace cpp::lang;

TEST(Test_CppLangInteger_bitCount)
{
    CHECK(Integer::bitCount(255) == 8);
    CHECK(Integer::bitCount(-1) == 32);
    CHECK(Integer::bitCount(3) == 2);
}

TEST(Test_CppLangInteger_byteValue)
{
    JInteger a = boxing(123456);
    CHECK(a->byteValue() == 64);
}

TEST(Test_CppLangInteger_compare)
{
    CHECK(Integer::compare(123456, 123455) == 1);
    CHECK(Integer::compare(123455, 123456) == -1);
    CHECK(Integer::compare(123456, 123456) == 0);
}

TEST(Test_CppLangInteger_compareTo)
{
    JInteger a = boxing(123456);
    JInteger b = boxing(123455);

    CHECK(a->compareTo(b) == 1);
    CHECK(b->compareTo(a) == -1);
    CHECK(a->compareTo(a) == 0);
}

TEST(Test_CppLangInteger_compareUnsigned)
{
    CHECK(Integer::compareUnsigned(8, -10) == -1);
    CHECK(Integer::compareUnsigned(-10, 8) == 1);
    CHECK(Integer::compareUnsigned(60, 60) == 0);
}

TEST(Test_CppLangInteger_divideUnsigned)
{
    CHECK(Integer::divideUnsigned(-1, 3) == 1431655765);
}

TEST(Test_CppLangInteger_doubleValue)
{
    JInteger a = boxing(123456);
    CHECK(a->doubleValue() == 123456.0);
}

TEST(Test_CppLangInteger_equals)
{
    JInteger a = boxing(123456);
    JInteger b = boxing(123455);

    CHECK(a->equals(a));
    CHECK(!a->equals(b));
}

TEST(Test_CppLangInteger_floatValue)
{
    JInteger a = boxing(123456);
    CHECK(a->floatValue() == 123456.0f);
}

TEST(Test_CppLangInteger_hashCode)
{
    JInteger a = boxing(123456);
    CHECK(a->hashCode() == 123456);
    CHECK(Integer::hashCode(123456) == 123456);
}

TEST(TestInteger_highestOneBit)
{
    CHECK(Integer::highestOneBit(170) == 128);
    CHECK(Integer::highestOneBit(170005) == 131072);
}

TEST(Test_CppLangInteger_intValue)
{
    JInteger a = boxing(123456);
    CHECK(a->intValue() == 123456);
}

TEST(Test_CppLangInteger_longValue)
{
    JInteger a = boxing(123456);
    CHECK(a->longValue() == 123456);
}

TEST(Test_CppLangInteger_lowestOneBit)
{
    CHECK(Integer::lowestOneBit(170) == 2);
    CHECK(Integer::lowestOneBit(170005) == 1);
}

TEST(Test_CppLangInteger_max)
{
    CHECK(Integer::max(10, 8) == 10);
}

TEST(Test_CppLangInteger_min)
{
    CHECK(Integer::min(10, 8) == 8);
}

TEST(Test_CppLangInteger_numberOfLeadingZeros)
{
    CHECK(Integer::numberOfLeadingZeros(170) == 24);
    CHECK(Integer::numberOfLeadingZeros(170005) == 14);
}

TEST(Test_CppLangInteger_numberOfTrailingZeros)
{
    CHECK(Integer::numberOfTrailingZeros(170) == 1);
    CHECK(Integer::numberOfTrailingZeros(170005) == 0);
}

TEST(Test_CppLangInteger_remainderUnsigned)
{
    CHECK(Integer::remainderUnsigned(-8, 5) == 3);
}

TEST(Test_CppLangInteger_reverse)
{
    CHECK(Integer::reverse(170) == 1426063360);
    CHECK(Integer::reverse(170005) == -1474740224);
    CHECK(Integer::reverse(-170) == 1795162111);
    CHECK(Integer::reverse(-170005) == -672743425);
}

TEST(Test_CppLangInteger_reverseBytes)
{
    CHECK(Integer::reverseBytes(170) == -1442840576);
    CHECK(Integer::reverseBytes(170005) == 362283520);
    CHECK(Integer::reverseBytes(-170) == 1459617791);
    CHECK(Integer::reverseBytes(-170005) == -345506305);
}

TEST(Test_CppLangInteger_rotateLeft)
{
    CHECK(Integer::rotateLeft(2, 4) == 32);
    CHECK(Integer::rotateLeft(-2, -4) == -268435457);
}

TEST(Test_CppLangInteger_rotateRight)
{
    CHECK(Integer::rotateRight(2, 4) == 536870912);
    CHECK(Integer::rotateRight(-2, -4) == -17);
}

TEST(Test_CppLangInteger_shortValue)
{
    JInteger a = boxing(123456);
    CHECK(a->shortValue() == -7616);
}

TEST(Test_CppLangInteger_signum)
{
    CHECK(Integer::signum(50) == 1);
    CHECK(Integer::signum(-50) == -1);
    CHECK(Integer::signum(0) == 0);
}

TEST(Test_CppLangInteger_sum)
{
    CHECK(Integer::sum(10, 20) == 30);
}

TEST(Test_CppLangInteger_toUnsignedLong)
{
    CHECK(Integer::toUnsignedLong(-1) == 4294967295);
}

TEST(Test_CppLangInteger_valueOf)
{
    JInteger a = Integer::valueOf(123456);
    CHECK(a->intValue() == 123456);
}

TEST(Test_CppLangInteger_parseInt)
{
    CHECK(Integer::parseInt(jstr(u"0"), 10) == 0);
    CHECK(Integer::parseInt(jstr(u"50"), 10) == 50);
    CHECK(Integer::parseInt(jstr(u"-50"), 10) == -50);
}