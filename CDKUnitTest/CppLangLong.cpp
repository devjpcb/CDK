#include <UnitTest++/UnitTest++.h>

#include <cpp/lang/Long.h>

using namespace cpp::lang;

TEST(Test_CppLangLong_bitCount)
{
    CHECK(Long::bitCount(255) == 8);
    CHECK(Long::bitCount(-1) == 64);
    CHECK(Long::bitCount(3) == 2);
}

TEST(Test_CppLangLong_byteValue)
{
    JLong a = boxing(123456LL);
    CHECK(a->byteValue() == 64);
}

TEST(Test_CppLangLong_compare)
{
    CHECK(Long::compare(123456, 123455) == 1);
    CHECK(Long::compare(123455, 123456) == -1);
    CHECK(Long::compare(123456, 123456) == 0);
}

TEST(Test_CppLangLong_compareTo)
{
    JLong a = boxing(123456LL);
    JLong b = boxing(123455LL);

    CHECK(a->compareTo(b) == 1);
    CHECK(b->compareTo(a) == -1);
    CHECK(a->compareTo(a) == 0);
}

TEST(Test_CppLangLong_compareUnsigned)
{
    CHECK(Long::compareUnsigned(8, -10) == -1);
    CHECK(Long::compareUnsigned(-10, 8) == 1);
    CHECK(Long::compareUnsigned(60, 60) == 0);
}

TEST(Test_CppLangLong_doubleValue)
{
    JLong a = boxing(123456LL);
    CHECK(a->doubleValue() == 123456.0);
}

TEST(Test_CppLangLong_equals)
{
    JLong a = boxing(123456LL);
    JLong b = boxing(123455LL);

    CHECK(a->equals(a));
    CHECK(!a->equals(b));
}

TEST(Test_CppLangLong_floatValue)
{
    JLong a = boxing(123456LL);
    CHECK(a->floatValue() == 123456.0f);
}

TEST(Test_CppLangLong_hashCode)
{
    JLong a = boxing(123456LL);
    CHECK(a->hashCode() == 123456);
    CHECK(Long::hashCode(123456) == 123456);
}

TEST(Test_CppLangLong_highestOneBit)
{
    CHECK(Long::highestOneBit(170) == 128);
    CHECK(Long::highestOneBit(170005) == 131072);
}

TEST(Test_CppLangLong_intValue)
{
    JLong a = boxing(123456LL);
    CHECK(a->intValue() == 123456);
}

TEST(Test_CppLangLong_longValue)
{
    JLong a = boxing(123456LL);
    CHECK(a->longValue() == 123456);
}

TEST(Test_CppLangLong_lowestOneBit)
{
    CHECK(Long::lowestOneBit(170) == 2);
    CHECK(Long::lowestOneBit(170005) == 1);
}

TEST(Test_CppLangLong_max)
{
    CHECK(Long::max(10, 8) == 10);
}

TEST(Test_CppLangLong_min)
{
    CHECK(Long::min(10, 8) == 8);
}

TEST(Test_CppLangLong_numberOfLeadingZeros)
{
    CHECK(Long::numberOfLeadingZeros(170) == 56);
    CHECK(Long::numberOfLeadingZeros(170005) == 46);
}

TEST(Test_CppLangLong_numberOfTrailingZeros)
{
    CHECK(Long::numberOfTrailingZeros(170) == 1);
    CHECK(Long::numberOfTrailingZeros(170005) == 0);
}

TEST(Test_CppLangLong_reverse)
{
    CHECK(Long::reverse(170) == 6124895493223874560);
    CHECK(Long::reverse(170005) == -6333961032175714304);
    CHECK(Long::reverse(-170) == 7710162562058289151);
    CHECK(Long::reverse(-170005) == -2889411004679061505);
}

TEST(Test_CppLangLong_reverseBytes)
{
    CHECK(Long::reverseBytes(170) == -6196953087261802496);
    CHECK(Long::reverseBytes(170005) == 1555995870279761920);
    CHECK(Long::reverseBytes(-170) == 6269010681299730431);
    CHECK(Long::reverseBytes(-170005) == -1483938276241833985);
}

TEST(Test_CppLangLong_rotateLeft)
{
    CHECK(Long::rotateLeft(2, 4) == 32);
    CHECK(Long::rotateLeft(-2, -4) == -1152921504606846977);
}

TEST(Test_CppLangLong_rotateRight)
{
    CHECK(Long::rotateRight(2, 4) == 2305843009213693952);
    CHECK(Long::rotateRight(-2, -4) == -17);
}

TEST(Test_CppLangLong_shortValue)
{
    JLong a = boxing(123456LL);
    CHECK(a->shortValue() == -7616);
}

TEST(Test_CppLangLong_signum)
{
    CHECK(Long::signum(50) == 1);
    CHECK(Long::signum(-50) == -1);
    CHECK(Long::signum(0) == 0);
}

TEST(Test_CppLangLong_sum)
{
    CHECK(Long::sum(10, 20) == 30);
}

TEST(Test_CppLangLong_valueOf)
{
    JLong a = Long::valueOf(123456LL);
    CHECK(a->intValue() == 123456);
}