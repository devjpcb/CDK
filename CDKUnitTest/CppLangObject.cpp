#include <UnitTest++/UnitTest++.h>

#include <cpp/lang/Object.h>
#include <cpp/lang/String.h>
#include <cpp/lang/Class.h>

using namespace cpp::lang;

TEST(Test_CppLangObject_equals)
{
    JObject obj1 = jnew<Object>();
    JObject obj2 = jnew<Object>();

    jboolean result = obj1->equals(obj1);
    CHECK_EQUAL(result, true);

    result = obj1->equals(obj2);
    CHECK_EQUAL(result, false);
}

TEST(Test_CppLangObject_getClass)
{
    JClass objectClass = Class::forName(jstr(u"cpp::lang::Object"));
    CHECK(objectClass->getCanonicalName()->equals(jstr(u"cpp::lang::Object")));
    CHECK(objectClass->getComponentType() == nullptr);
    CHECK(objectClass->getModifiers() == 1);
    CHECK(objectClass->getName()->equals(jstr(u"cpp::lang::Object")));
    CHECK(objectClass->getPackage()->getName()->equals(jstr(u"cpp::lang")));
    CHECK(objectClass->getSimpleName()->equals(jstr(u"Object")));
    CHECK(objectClass->isAnnotation() == false);
    CHECK(objectClass->isAnonymousClass() == false);
    CHECK(objectClass->isArray() == false);
    CHECK(objectClass->isEnum() == false);
    CHECK(objectClass->isInterface() == false);
    CHECK(objectClass->isLocalClass() == false);
    CHECK(objectClass->isMemberClass() == false);
    CHECK(objectClass->isPrimitive() == false);
    CHECK(objectClass->isSynthetic() == false);
    CHECK(objectClass->isSynthetic() == false);
    JObject objectInstance = objectClass->newInstance();
    CHECK(objectInstance != nullptr);
    CHECK(objectClass->toGenericString()->equals(jstr(u"public class cpp::lang::Object")));
    CHECK(objectClass->toString()->equals(jstr(u"class cpp::lang::Object")));
}

TEST(Test_CppLangObject_hashCode)
{
    JObject obj1 = jnew<Object>();
    JObject obj2 = jnew<Object>();

    CHECK(obj1->hashCode() > 0);
    CHECK(obj2->hashCode() > 0);
    CHECK(obj1->hashCode() != obj2->hashCode());
}

TEST(Test_CppLangObject_toString)
{
    JObject obj1 = jnew<Object>();
    CHECK(obj1->toString()->indexOf(jstr(u"cpp::lang::Object@")) != -1);
}