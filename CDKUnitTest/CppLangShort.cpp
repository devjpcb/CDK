#include <UnitTest++/UnitTest++.h>

#include <cpp/lang/Short.h>

using namespace cpp::lang;

TEST(Test_CppLangShort_byteValue)
{
    JShort a = boxing<jshort>(45);
    CHECK(a->byteValue() == 45);
}

TEST(Test_CppLangShort_compare)
{
    CHECK(Short::compare(45, 90) == -45);
    CHECK(Short::compare(90, 45) == 45);
}

TEST(Test_CppLangShort_compareTo)
{
    JShort a = boxing<jshort>(45);
    JShort b = boxing<jshort>(90);

    CHECK(a->compareTo(b) == -45);
    CHECK(b->compareTo(a) == 45);
}

TEST(Test_CppLangShort_doubleValue)
{
    JShort a = boxing<jshort>(45);
    JShort b = boxing<jshort>(90);

    CHECK(a->doubleValue() == 45.0);
    CHECK(b->doubleValue() == 90.0);
}

TEST(Test_CppLangShort_equals)
{
    JShort a = boxing<jshort>(45);
    JShort b = boxing<jshort>(90);

    CHECK(a->equals(a));
    CHECK(!a->equals(b));
    CHECK(!b->equals(a));
}

TEST(Test_CppLangShort_floatValue)
{
    JShort a = boxing<jshort>(45);
    JShort b = boxing<jshort>(90);

    CHECK(a->floatValue() == 45.0f);
    CHECK(b->floatValue() == 90.0f);
}

TEST(Test_CppLangShort_hashCode)
{
    JShort a = boxing<jshort>(45);
    JShort b = boxing<jshort>(90);

    CHECK(a->hashCode() == 45);
    CHECK(b->hashCode() == 90);
    CHECK(a->hashCode() != b->hashCode());
}

TEST(Test_CppLangShort_intValue)
{
    JShort a = boxing<jshort>(45);
    JShort b = boxing<jshort>(90);

    CHECK(a->intValue() == 45);
    CHECK(b->intValue() == 90);
}

TEST(Test_CppLangShort_longValue)
{
    JShort a = boxing<jshort>(45);
    JShort b = boxing<jshort>(90);

    CHECK(a->longValue() == 45LL);
    CHECK(b->longValue() == 90LL);
}

TEST(Test_CppLangShort_reverseBytes)
{
    CHECK(Short::reverseBytes(170) == -22016);
    CHECK(Short::reverseBytes(17005) == 27970);
}

TEST(Test_CppLangShort_shortValue)
{
    JShort a = boxing<jshort>(45);
    JShort b = boxing<jshort>(90);

    CHECK(a->shortValue() == 45);
    CHECK(b->shortValue() == 90);
}

TEST(Test_CppLangShort_toUnsignedInt)
{
    CHECK(Short::toUnsignedInt(2) == 2);
    CHECK(Short::toUnsignedInt(-2) == 65534);
}

TEST(Test_CppLangShort_toUnsignedLong)
{
    CHECK(Short::toUnsignedLong(2) == 2);
    CHECK(Short::toUnsignedLong(-2) == 65534);
}

TEST(Test_CppLangShort_valueOf)
{
    JShort value = Short::valueOf(127);
    CHECK(value->byteValue() == 127);
}