#include <UnitTest++/UnitTest++.h>

#include <cpp/lang/String.h>
#include <string>

using namespace cpp::lang;

TEST(Test_CppLangString_String)
{
    JString str1 = jnew<String>(jstr(u"hola mundo"));
    JString str2 = jnew<String>(jstr(u"hola mundo"));
    
    CHECK(str1->equals(str2));
}

TEST(Test_CppLangString_NativeString)
{
    JString str1 = to_jstr("hola mundo");
    JString str2 = to_jstr(L"hola mundo");
    JString str3 = jstr(u"hola mundo");
    JString str4 = jstr(u"\000\000");

    std::string nstr1 = to_cppstr(str1);
    std::wstring nstr2 = to_cppwstr(str2);
    std::u16string nstr3 = to_cppstr16(str3);

    CHECK(str1->length() == static_cast<jint>(nstr1.length()));
    CHECK(str2->length() == static_cast<jint>(nstr2.length()));
    CHECK(str3->length() == static_cast<jint>(nstr3.length()));
    CHECK(str4->length() == 2);
}

TEST(Test_CppLangString_charAt)
{
    JString str1 = jstr(u"hola mundo");

    CHECK(str1->charAt(0) == u'h');
    CHECK(str1->charAt(4) == u' ');
    CHECK(str1->charAt(9) == u'o');
}

TEST(Test_CppLangString_codePointAt)
{
    JString str1 = jstr(u"hola mundo");

    CHECK(str1->codePointAt(0) == 104);
    CHECK(str1->codePointAt(4) == 32);
    CHECK(str1->codePointAt(9) == 111);
}

TEST(Test_CppLangString_codePointBefore)
{
    JString str1 = jstr(u"hola mundo");

    CHECK(str1->codePointBefore(1) == 104);
    CHECK(str1->codePointBefore(4) == 97);
    CHECK(str1->codePointBefore(9) == 100);
}

TEST(Test_CppLangString_codePointCount)
{
    JString str1 = jstr(u"hola mundo");

    CHECK(str1->codePointCount(0, 3) == 3);
    CHECK(str1->codePointCount(3, 6) == 3);
    CHECK(str1->codePointCount(6, 9) == 3);
}

TEST(Test_CppLangString_compareTo)
{
    JString str1 = jstr(u"hola mundo");
    JString str2 = jstr(u"adios mundo");

    CHECK(str1->compareTo(str1) == 0);
    CHECK(str1->compareTo(str2) == 7);
    CHECK(str2->compareTo(str1) == -7);
}

TEST(Test_CppLangString_concat)
{
    JString str1 = jstr(u"hola mundo");
    JString str2 = jstr(u"adios mundo");

    JString str3 = str1->concat(jstr(u" "))->concat(str2);

    CHECK(str3->length() == 22);
}

TEST(Test_CppLangString_copyValueOf)
{
    jarray<jchar> str1 = {u'h', u'o', u'l', u'a'};

    CHECK(String::copyValueOf(str1)->length() == 4);
}

TEST(Test_CppLangString_endsWith)
{
    JString str1 = jstr(u"hola mundo");
    
    CHECK(str1->endsWith(jstr(u"do")));
}

TEST(Test_CppLangString_equals)
{
    JString str1 = jstr(u"hola mundo");
    JString str2 = jstr(u"adios mundo");
    
    CHECK(str1->equals(str1));
    CHECK(!str1->equals(str2));
}

TEST(Test_CppLangString_equalsIgnoreCase)
{
    JString str1 = jstr(u"TRUE");
    JString str2 = jstr(u"Hola");

    CHECK(str1->equalsIgnoreCase(jstr(u"true")));
    CHECK(str2->equalsIgnoreCase(jstr(u"holA")));
}

TEST(Test_CppLangString_getChars)
{
}

TEST(Test_CppLangString_hashCode)
{
    JString str1 = jstr(u"hola mundo");
    JString str2 = jstr(u"adios mundo");
    
    CHECK(str1->hashCode() == -1567660435);
    CHECK(str2->hashCode() == -1303743429);
}

TEST(Test_CppLangString_indexOf)
{
    JString str1 = jstr(u"hola mundo");
    
    CHECK(str1->indexOf(jstr(u"mundo")) == 5);
    CHECK(str1->indexOf(jstr(u"ios")) == -1);
}

TEST(Test_CppLangString_isEmpty)
{
    JString str1 = jstr(u"");
    
    CHECK(str1->isEmpty());
}

TEST(Test_CppLangString_lastIndexOf)
{
    JString str1 = jstr(u"hola mundo");
    
    CHECK(str1->lastIndexOf(jstr(u"o")) == 9);
    CHECK(str1->lastIndexOf(jstr(u"a")) == 3);
}

TEST(Test_CppLangString_length)
{
    JString str1 = jstr(u"hola mundo");
    
    CHECK(str1->length() == 10);
}

TEST(Test_CppLangString_offsetByCodePoints)
{
    JString str1 = jstr(u"aacdefaa");
    
    CHECK(str1->offsetByCodePoints(2, 4) == 6);
}

TEST(Test_CppLangString_replace)
{
    JString str1 = jstr(u"hola mundo");
    str1 = str1->replace(u'o', u'O');
    CHECK(str1->charAt(9) == u'O');
}

TEST(Test_CppLangString_startsWith)
{
    JString str1 = jstr(u"hola mundo");
    
    CHECK(str1->startsWith(jstr(u"hol")));
}

TEST(Test_CppLangString_substring)
{
    JString str1 = jstr(u"hola mundo");
    JString str2 = str1->substring(5);
    
    CHECK(str2->equals(jstr(u"mundo")));
}

TEST(Test_CppLangString_toCharArray)
{
    JString str1 = jstr(u"hola mundo");

    CHECK(str1->toCharArray().length() == 10);
}

TEST(Test_CppLangString_toString)
{
    JString str1 = jstr(u"hola mundo");
    JString str2 = str1->toString();
    
    CHECK(str1->equals(str2));
}

TEST(Test_CppLangString_trim)
{
    JString str1 = jstr(u" hola mundo ");
    
    CHECK(str1->trim()->equals(jstr(u"hola mundo")));
}

TEST(Test_CppLangString_internal)
{
    JString str1 = jstr(u"C++");
    JString str2 = jstr(u"C++");
    
    CHECK(str1 == str2);
}