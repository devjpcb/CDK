#include <UnitTest++/UnitTest++.h>

#include <cpp/lang/String.h>
#include <cpp/lang/StringBuffer.h>

using namespace cpp::lang;

TEST(Test_CppLangStringBuffer_StringBuffer)
{
    JStringBuffer sb = jnew<StringBuffer>();
    sb->append(jstr(u"hola"))->append(10.5);
    sb->append(10);
    sb->append(jstr(u"mundo"));
    
    sb->reverse();
    
    JString str = sb->toString();
    
    CHECK(str->equals(jstr(u"odnum015.01aloh")));
}