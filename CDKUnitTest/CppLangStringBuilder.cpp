#include <UnitTest++/UnitTest++.h>

#include <cpp/lang/String.h>
#include <cpp/lang/StringBuilder.h>

using namespace cpp::lang;

TEST(Test_CppLangStringBuilder_StringBuilder)
{
    JStringBuilder sb = jnew<StringBuilder>();
    sb->append(jstr(u"hola"))->append(10.5f);
    sb->append(10);
    sb->append(jstr(u"mundo"));
    
    sb->reverse();
    
    JString str = sb->toString();
    
    CHECK(str->equals(jstr(u"odnum015.01aloh")));
}