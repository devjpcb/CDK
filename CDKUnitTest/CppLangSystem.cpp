#include <UnitTest++/UnitTest++.h>

#include <cpp/lang/System.h>

using namespace cpp::lang;

TEST(Test_CppLangSystem_arraycopy)
{
    jarray<jint> a = {1, 2, 3, 4, 5, 6};
    jarray<jint> b(6);

    System::arraycopy(a, 1, b, 1, 4);

    CHECK(b[0] == 0);
    CHECK(b[1] == 2);
    CHECK(b[2] == 3);
    CHECK(b[3] == 4);
    CHECK(b[4] == 5);
    CHECK(b[5] == 0);
}