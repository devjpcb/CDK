#include <UnitTest++/UnitTest++.h>

#include <cpp/lang/Thread.h>
#include <cpp/lang/ThreadGroup.h>
#include <cpp/lang/Runnable.h>
#include <iostream>

using namespace cpp::lang;

TEST(Test_CppLangThread_currentThread)
{
    JThread mainThread = Thread::currentThread();
    CHECK(mainThread != nullptr);
}

TEST(Test_CppLangThread_start)
{
    class HelloRunnable : public Runnable {
    public:
        void run() {
            
        }
    };
    
    JThread thread = jnew<Thread>(jnew<HelloRunnable>());
    thread->start();
    thread->join();
    
    Thread::sleep(500);
}