#include <UnitTest++/UnitTest++.h>

#include <cpp/lang/String.h>
#include <cpp/util/ArrayList.h>

using namespace cpp::lang;
using namespace cpp::util;

#include <iostream>

TEST(Test_CppUtilArrayList_ArrayList)
{
    JArrayList<JString> al = jnew<ArrayList<JString>>();
    
    al->add(jstr(u"hola"));
    al->add(jstr(u"mundo"));
    
    CHECK(al->size() == 2);
    
    al->remove(1);
    
    CHECK(al->get(0)->equals(jstr(u"hola")));
    
    al->addAll(al);
    
    CHECK(al->size() == 2);
}