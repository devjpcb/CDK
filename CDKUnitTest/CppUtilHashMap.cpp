#include <UnitTest++/UnitTest++.h>

#include <cpp/lang/String.h>
#include <cpp/util/HashMap.h>

using namespace cpp::lang;
using namespace cpp::util;


TEST(Test_CppUtilHashMap_HashMap_size)
{
    JHashMap<JString, JString> hm = jnew<HashMap<JString, JString>>();
    
    hm->put(jstr(u"A"), jstr(u"Hola"));
    hm->put(jstr(u"B"), jstr(u"Adios"));
    hm->put(jstr(u"C"), jstr(u"Mundo"));

    CHECK(hm->size() == 3);
}

TEST(Test_CppUtilHashMap_HashMap_get)
{
    JHashMap<JString, JString> hm = jnew<HashMap<JString, JString>>();
    
    hm->put(jstr(u"A"), jstr(u"Hola"));
    hm->put(jstr(u"B"), jstr(u"Adios"));
    hm->put(jstr(u"C"), jstr(u"Mundo"));
    
    JString str = hm->get(jstr(u"B"));
    
    CHECK(str != nullptr);
    CHECK(str->equals(jstr(u"Adios")));
}

TEST(Test_CppUtilHashMap_HashMap_remove)
{
    JHashMap<JString, JString> hm = jnew<HashMap<JString, JString>>();
    
    hm->put(jstr(u"A"), jstr(u"Hola"));
    hm->put(jstr(u"B"), jstr(u"Adios"));
    hm->put(jstr(u"C"), jstr(u"Mundo"));
    
    hm->remove(jstr(u"A"));
    
    CHECK(hm->size() == 2);
}

TEST(Test_CppUtilHashMap_HashMap_replace)
{
    JHashMap<JString, JString> hm = jnew<HashMap<JString, JString>>();
    
    hm->put(jstr(u"A"), jstr(u"Hola"));
    hm->put(jstr(u"B"), jstr(u"Adios"));
    hm->put(jstr(u"C"), jstr(u"Mundo"));
    
    hm->replace(jstr(u"C"), jstr(u"oooh"));
    
    JString str = hm->get(jstr(u"C"));
    
    CHECK(str != nullptr);
    CHECK(str->equals(jstr(u"oooh")));
}