#include <UnitTest++/UnitTest++.h>

#include <cpp/lang/String.h>
#include <cpp/lang/Integer.h>
#include <cpp/util/Hashtable.h>

using namespace cpp::lang;
using namespace cpp::util;


TEST(Test_CppUtilHashtable_size)
{
    JHashtable<JString, JInteger> ht = jnew<Hashtable<JString, JInteger>>();
    
    ht->put(jstr(u"one"), boxing(1));
    ht->put(jstr(u"two"), boxing(2));
    ht->put(jstr(u"three"), boxing(3));
    
    CHECK(ht->size() == 3);
}

TEST(Test_CppUtilHashtable_get)
{
    JHashtable<JString, JInteger> ht = jnew<Hashtable<JString, JInteger>>();
    
    ht->put(jstr(u"one"), boxing(1));
    ht->put(jstr(u"two"), boxing(2));
    ht->put(jstr(u"three"), boxing(3));
    
    JInteger value = ht->get(jstr(u"two"));
    
    CHECK(value->equals(boxing(2)));
}


TEST(Test_CppUtilHashtable_remove)
{
    JHashtable<JString, JInteger> ht = jnew<Hashtable<JString, JInteger>>();
    
    ht->put(jstr(u"one"), boxing(1));
    ht->put(jstr(u"two"), boxing(2));
    ht->put(jstr(u"three"), boxing(3));
    
    ht->remove(jstr(u"one"));
    
    CHECK(ht->size() == 2);
}

TEST(Test_CppUtilHashtable_replace)
{
    JHashtable<JString, JInteger> ht = jnew<Hashtable<JString, JInteger>>();
    
    ht->put(jstr(u"one"), boxing(1));
    ht->put(jstr(u"two"), boxing(2));
    ht->put(jstr(u"three"), boxing(3));
    
    ht->replace(jstr(u"one"), boxing(5));
    
    JInteger value = ht->get(jstr(u"one"));
    
    CHECK(value == boxing(5));
}