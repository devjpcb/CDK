#include <UnitTest++/UnitTest++.h>

#include <cpp/lang/String.h>
#include <cpp/lang/Integer.h>
#include <cpp/util/Properties.h>

using namespace cpp::lang;
using namespace cpp::util;


TEST(Test_CppUtilHashtable_getProperty)
{
    JProperties prop = jnew<Properties>();
    
    prop->setProperty(jstr(u"propertyKey"), jstr(u"propertyValue"));
    
    JString value = prop->getProperty(jstr(u"propertyKey"));
    
    CHECK(value->equals(jstr(u"propertyValue")));
}