#include <UnitTest++/UnitTest++.h>
#include <PrimitiveDataTypes.h>
#include <cpp/lang/Array.h>


TEST(Test_PrimitiveDataTypes_jarray_length)
{
    jarray<jint> a(5);
    a[0] = 1;
    a[1] = 2;
    a[2] = 3;
    a[3] = 4;
    a[4] = 5;

    CHECK(a.length() == 5);
}

TEST(Test_PrimitiveDataTypes_foreach)
{
    jarray<jint> b;
    b = jnew_array<jint>(10);
    b[0] = 11;
    b[1] = 22;
    b[2] = 33;
    b[3] = 44;
    b[4] = 55;
    b[5] = 66;
    b[6] = 77;
    b[7] = 88;
    b[8] = 99;
    b[9] = 11;

    CHECK(b.length() == 10);

    for(jint& v : b) {
        CHECK(v > 0);
    }
}

TEST(Test_PrimitiveDataTypes_jarray_asign)
{
    jarray<jint> a(5);
    a[0] = 1;
    a[1] = 2;
    a[2] = 3;
    a[3] = 4;
    a[4] = 5;
    
    jarray<jint> d;

    d = a;

    CHECK(d[2] == 3);
}

TEST(Test_PrimitiveDataTypes_jarray_initialization)
{
    jarray<jint> e = { 1, 2, 3, 4, 5, 6 };

    jint len1 = e.length();

    CHECK(len1 == 6);
}

TEST(Test_PrimitiveDataTypes_jarray_const_initialization)
{
    const jarray<jint> f = { 1, 2, 3, 4, 5, 6 };

    const jint len2 = f.length();

    CHECK(len2 == 6);
    
    for(const jint& v : f) {
        CHECK(v > 0);
        CHECK(v <= 6);
    }
}

TEST(Test_PrimitiveDataTypes_jarray_native_initialization)
{
    const std::array<jint, 10> h = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

    jarray<jint> i = h;

    CHECK(i.length() == 10);

    const std::vector<jint> j = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

    jarray<jint> k = j;

    CHECK(k.length() == 10);

    const jint m[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

    jarray<jint> n = m;

    CHECK(n.length() == 10);
}

TEST(Test_PrimitiveDataTypes_jarray_char_initialization)
{
    char16_t v1[] = u"\000\000";
    jarray<jchar> z1 = v1;
    CHECK(z1.length() == 2);

    jarray<jbyte> z2 = "\000\000";
    CHECK(z2.length() == 2);
}